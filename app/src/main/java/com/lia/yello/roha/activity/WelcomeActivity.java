package com.lia.yello.roha.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button  btnNext;
    TextView btnSkip;
    private SessionManager sessionManager;
    ProgressDialog progressDialog;
    private static final String INTRO_CARD1 = "intro_card_1";
    public static final String MyPREFERENCES = "MyPrefs";
    private static final String SHOWCASE_ID = "sequence example";
    public boolean processClick = true;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
       if(StaticInfo.welcome == false){
           progressDialog = new ProgressDialog(WelcomeActivity.this);
           progressDialog.setTitle("");
           progressDialog.setMessage(getString(R.string.pleasewait));
           progressDialog.setCancelable(false);
           progressDialog.show();
           //launchHomeScreen();
           //finish();
       }

        // Making notification bar transparent
       if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip = (TextView) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnSkip.setEnabled(true);
        btnSkip.setClickable(true);


        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3,
               // R.layout.welcome_slide4
        };

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager, true);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(processClick) {
                    btnSkip.setEnabled(false);
                    btnSkip.setClickable(false);
                    Intent intent=new Intent(WelcomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                }
                else {
                    if(processClick) {
                        Intent intent=new Intent(WelcomeActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                    processClick = false;
                }
            }
        });

    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#x2B2C;"));
            //dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            //dots[i].setHeight(20);
            //dots[i].setWidth(40);
           // dots[i].setBackground(getDrawable(R.drawable.rect_corner));
            //dots[i].setPadding(10,0,10,0);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        progressDialog.dismiss();
        StaticInfo.welcome=false;
        try {
            if (sessionManager.getusername() != null && sessionManager.getusername().length() > 0) {
                Log.d("fgvbnds","login user");
                Log.d("lodin user",""+ sessionManager.getguest());
                sessionManager.setguest(1);
                Intent intent=new Intent(WelcomeActivity.this, RohaWaterDashboardActivity.class);
                startActivity(intent);
                //finish();
            }
            else {
                if (!sessionManager.gettoken().equals("")){
                    Log.d("fgvbnds","login user");
                    Log.d("lodin user",""+ sessionManager.getguest());
                    sessionManager.setguest(1);
                    Intent intent=new Intent(WelcomeActivity.this, RohaWaterDashboardActivity.class);
                    startActivity(intent);
                    //finish();
                }
                else {
                    sessionManager.setguest(1);
                    Log.d("fgvbnds","login user");
                    Intent intent1=new Intent(WelcomeActivity.this,LoginActivity.class);
                    startActivity(intent1);
                    //finish();
                }
            }
        } catch (Exception e) {
            progressDialog.dismiss();
            Log.d("Error", e.getMessage());
        }

    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(R.string.start);
                btnSkip.setVisibility(View.VISIBLE);
            } else {
                // still pages are left
                btnNext.setText(R.string.next);
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
       /* Intent intent = new Intent(WelcomeActivity.this, LanguageActivity.class);
        startActivity(intent);*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        btnSkip.setEnabled(true);
        btnSkip.setClickable(true);
    }
}