package com.lia.yello.roha.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.lia.yello.roha.Fragment.CartFragment;
import com.lia.yello.roha.Fragment.RohaHomeOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.CartDataModel;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Rohahomesummaryadapter1  extends RecyclerView.Adapter<Rohahomesummaryadapter1.MyViewHolder> {
    private Context mcontext;
    private List<CartDataModel> dataSet;
    private RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment;
    private RohahomeDashboradActivity rohahomeDashboradActivity;
    public boolean isClickable = true;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, txtquantity,cost;
        CardView cardview;
        ImageView menuimage,imgminus,imgplus,delete;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.txtquantity = (TextView) itemView.findViewById(R.id.txtquantity);
            this.menuimage = (ImageView) itemView.findViewById(R.id.menuimage);
            this.imgminus = (ImageView) itemView.findViewById(R.id.imgminus);
            this.imgplus = (ImageView) itemView.findViewById(R.id.imgplus);
            this.cost = (TextView) itemView.findViewById(R.id.cost);
            this.delete = (ImageView) itemView.findViewById(R.id.delete);
            this.cardview = (CardView) itemView.findViewById(R.id.cardview1_orders);
        }
    }

    public Rohahomesummaryadapter1(Context context, List<CartDataModel> data, RohaHomeOrderSummaryFragment helpFragment,RohahomeDashboradActivity rohahomeDashboradActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohaHomeOrderSummaryFragment = helpFragment;
        this.rohahomeDashboradActivity = rohahomeDashboradActivity;
    }

    @Override
    public Rohahomesummaryadapter1.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_cart, parent, false);
        Rohahomesummaryadapter1.MyViewHolder myViewHolder = new Rohahomesummaryadapter1.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        Glide.with(mcontext).load(dataSet.get(listPosition).getPimage()).placeholder(R.drawable.defaultimg).into(holder.menuimage);
        holder.title.setText(dataSet.get(listPosition).getPname());
        holder.txtquantity.setText(""+dataSet.get(listPosition).getQty());
        holder.cost.setText(""+dataSet.get(listPosition).getProductprice()+" SAR");

        if(dataSet.get(listPosition).getIsstock() == 0){
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isClickable == true) {
                        StaticInfo.cartdatachanged = true;
                        rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(), 0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), 0, false, false, true);
                        dataSet.remove(listPosition);
                        notifyDataSetChanged();
                        rohaHomeOrderSummaryFragment.recycleradapter2();
                        if (dataSet.size() == 0) {
                            rohahomeDashboradActivity.sessionManager.setCartcount(0);
                            rohahomeDashboradActivity.cartBadge.setVisibility(View.GONE);
                            Fragment fragment = new CartFragment();
                            rohahomeDashboradActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

                        }
                   /* JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id",dataSet.get(listPosition).getCartid());
                        deletecartrequestJSON(requestbody);
                        sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    }
                }
            });
            holder.cardview.setAlpha(0.4f);
            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isClickable == true) {
                        rohahomeDashboradActivity.sessionManager.snackbarToast(mcontext.getString(R.string.stocknotavailable), holder.cardview);
                    }
                }
            });

        }
        else {
            holder.imgplus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isClickable == true) {
                        if (dataSet.get(listPosition).getStockquantity() > Integer.parseInt(holder.txtquantity.getText().toString())) {
                            StaticInfo.cartdatachanged = true;
                            rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(), 0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), Integer.parseInt(holder.txtquantity.getText().toString()) + 1, false, true, false);
                            holder.txtquantity.setText("" + (Integer.parseInt(holder.txtquantity.getText().toString()) + 1));
                            rohaHomeOrderSummaryFragment.nsubtotal = rohaHomeOrderSummaryFragment.nsubtotal + dataSet.get(listPosition).getProductprice();
                            rohaHomeOrderSummaryFragment.nfqty = rohaHomeOrderSummaryFragment.nfqty + 1;
                            rohaHomeOrderSummaryFragment.nvat = ((rohaHomeOrderSummaryFragment.nsubtotal / 100.0f) * StaticInfo.vat);
                            double totalamt = (rohaHomeOrderSummaryFragment.nsubtotal + rohaHomeOrderSummaryFragment.nvat);
                            rohaHomeOrderSummaryFragment.subtotal1.setText("" + String.format(Locale.ENGLISH, "%.2f", rohaHomeOrderSummaryFragment.nsubtotal) + " SAR");
                            rohaHomeOrderSummaryFragment.txt_total.setText(""+mcontext.getString(R.string.total)+" : " + String.format(Locale.ENGLISH, "%.2f", totalamt) + " SAR");
                            rohaHomeOrderSummaryFragment.txt_vat.setText("" + String.format(Locale.ENGLISH, "%.2f", rohaHomeOrderSummaryFragment.nvat) + " SAR");
                            rohaHomeOrderSummaryFragment.recycleradapter2();

                /*  //  holder.txtquantity.setText(""+(dataSet.get(listPosition).getQty()+1));
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id",dataSet.get(listPosition).getCartid());
                        requestbody.put("quantity",dataSet.get(listPosition).getQty()+1);
                        requestbody.put("cost",(dataSet.get(listPosition).getQty()+1)*dataSet.get(listPosition).getProductprice());
                        updatecartrequestJSON(requestbody);
                       // sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                        } else {
                            rohahomeDashboradActivity.sessionManager.snackbarToast(mcontext.getString(R.string.stocknotavailable), holder.imgplus);
                        }
                    }
                }
            });
            holder.imgminus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isClickable == true) {
                        if (dataSet.get(listPosition).getQty() <= 1) {
                            // session.snackbarToast(rohaHomeOrderSummaryFragment.getString(R.string.minimumquantityis10),holder.imgminus);
                        } else {
                            StaticInfo.cartdatachanged = true;
                            rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(), 0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), Integer.parseInt(holder.txtquantity.getText().toString()) - 1, false, true, false);
                            holder.txtquantity.setText("" + (Integer.parseInt(holder.txtquantity.getText().toString()) - 1));
                            rohaHomeOrderSummaryFragment.nsubtotal = rohaHomeOrderSummaryFragment.nsubtotal - dataSet.get(listPosition).getProductprice();
                            rohaHomeOrderSummaryFragment.nfqty = rohaHomeOrderSummaryFragment.nfqty - 1;
                            rohaHomeOrderSummaryFragment.nvat = ((rohaHomeOrderSummaryFragment.nsubtotal / 100.0f) * StaticInfo.vat);
                            double totalamt = (rohaHomeOrderSummaryFragment.nsubtotal + rohaHomeOrderSummaryFragment.nvat);
                            rohaHomeOrderSummaryFragment.subtotal1.setText("" + String.format(Locale.ENGLISH, "%.2f", rohaHomeOrderSummaryFragment.nsubtotal) + " SAR");
                            rohaHomeOrderSummaryFragment.txt_total.setText(""+mcontext.getString(R.string.total)+" : " + String.format(Locale.ENGLISH, "%.2f", totalamt) + " SAR");
                            rohaHomeOrderSummaryFragment.txt_vat.setText("" + String.format(Locale.ENGLISH, "%.2f", rohaHomeOrderSummaryFragment.nvat) + " SAR");
                            rohaHomeOrderSummaryFragment.recycleradapter2();

                 /*  // holder.txtquantity.setText(""+(dataSet.get(listPosition).getQty()-1));
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id",dataSet.get(listPosition).getCartid());
                        requestbody.put("quantity",dataSet.get(listPosition).getQty()-1);
                        requestbody.put("cost",(dataSet.get(listPosition).getQty()-1)*dataSet.get(listPosition).getProductprice());
                        updatecartrequestJSON(requestbody);
                       // sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                        }
                    }
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isClickable == true){
                        StaticInfo.cartdatachanged = true;
                        rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(), 0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), 0, false, false, true);
                        rohaHomeOrderSummaryFragment.nsubtotal = rohaHomeOrderSummaryFragment.nsubtotal - (dataSet.get(listPosition).getProductprice() * Integer.parseInt(holder.txtquantity.getText().toString()));
                        rohaHomeOrderSummaryFragment.nfqty = rohaHomeOrderSummaryFragment.nfqty - Integer.parseInt(holder.txtquantity.getText().toString());
                        rohaHomeOrderSummaryFragment.nvat = ((rohaHomeOrderSummaryFragment.nsubtotal / 100.0f) * StaticInfo.vat);
                        double totalamt = (rohaHomeOrderSummaryFragment.nsubtotal + rohaHomeOrderSummaryFragment.nvat);
                        rohaHomeOrderSummaryFragment.subtotal1.setText("" + String.format(Locale.ENGLISH, "%.2f", rohaHomeOrderSummaryFragment.nsubtotal) + " SAR");
                        rohaHomeOrderSummaryFragment.txt_total.setText(""+mcontext.getString(R.string.total)+" : " + String.format(Locale.ENGLISH, "%.2f", totalamt) + " SAR");
                        rohaHomeOrderSummaryFragment.txt_vat.setText("" + String.format(Locale.ENGLISH, "%.2f", rohaHomeOrderSummaryFragment.nvat) + " SAR");
                        dataSet.remove(listPosition);
                        notifyDataSetChanged();
                        if (dataSet.size() == 0) {
                            rohahomeDashboradActivity.sessionManager.setCartcount(0);
                            rohahomeDashboradActivity.cartBadge.setVisibility(View.GONE);
                            rohahomeDashboradActivity.subchangefragment( new CartFragment(),null);
                        }
                        rohaHomeOrderSummaryFragment.recycleradapter2();
                       /* JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("id",dataSet.get(listPosition).getCartid());
                            deletecartrequestJSON(requestbody);
                           // sessionManager.progressdialogshow();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    private void updatecartrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updatecart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                //sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        rohaHomeOrderSummaryFragment.ntotal = 0;
                        rohaHomeOrderSummaryFragment.nsubtotal = 0;
                        rohaHomeOrderSummaryFragment.nvat =0;
                        rohaHomeOrderSummaryFragment.listcart();
                    }
                    else {
                        rohahomeDashboradActivity.sessionManager.snackbarToast(obj.getString("message"),rohaHomeOrderSummaryFragment.recycler1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
              //  sessionManager.progressdialogdismiss();
                rohahomeDashboradActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void deletecartrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deletecart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
               // sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        rohaHomeOrderSummaryFragment.ntotal = 0;
                        rohaHomeOrderSummaryFragment.nsubtotal = 0;
                        rohaHomeOrderSummaryFragment.nvat =0;
                        rohaHomeOrderSummaryFragment.listcart();
                    }
                    else {
                        rohahomeDashboradActivity.sessionManager.snackbarToast(obj.getString("message"),rohaHomeOrderSummaryFragment.recycler1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
               // sessionManager.progressdialogdismiss();
                rohahomeDashboradActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}


