package com.lia.yello.roha.model;

import android.content.Context;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.lia.yello.roha.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListorderDatamodel1 {
    LatLng latlng;
    Context context;
    int id, no_of_carton, iswish;
    Double total_price,delivery_charge,vat,rating,subtotal;
    String paymenttype, order_date, units, status, delivered_date,delivered_time,order_time, place, ismecca, refToken, comment, suggestion, day_id, time_from, time_to, discount_id, session, discounttype,discountvalue;
    JSONArray details;
    JSONObject full_obj;

    public ListorderDatamodel1(Context context,
                               String id,
                               String no_of_carton,
                               String iswish,
                               String rating,
                               String vat,
                               String subtotal,
                               String total_price,
                               String delivery_charge,
                               String paymenttype,
                               String order_date,
                               String units,
                               String status,
                               String delivered_date,
                               String delivered_time,
                               String order_time,
                               String place,
                               String ismecca,
                               String comment,
                               String suggestion,
                               String day_id,
                               String time_from,
                               String time_to,
                               String discount_id,
                               String session,
                               String type, String discountvalue, JSONArray details, JSONObject full_obj) {
        this.context = context;
        this.id = Integer.parseInt(id);
        this.subtotal = Double.parseDouble(subtotal);
        this.no_of_carton = Integer.parseInt(no_of_carton);
        this.iswish = Integer.parseInt(iswish);
        this.rating = Double.parseDouble(rating);
        this.vat = Double.parseDouble(vat);
        if(total_price.equalsIgnoreCase("")){
            this.total_price = 0.0;
        }
        else {
            this.total_price = Double.parseDouble(total_price);
        }
        this.delivery_charge = Double.parseDouble(delivery_charge);
        this.paymenttype = paymenttype;
        this.order_date = order_date;
        this.units = units;
        this.status = status;
        this.delivered_date = delivered_date;
        this.delivered_time = delivered_time;
        this.order_time = order_time;
        this.place = place;
        this.ismecca = ismecca;
        this.refToken = refToken;
        this.comment = comment;
        this.suggestion = suggestion;
        this.day_id = day_id;
        this.time_from = time_from;
        this.time_to = time_to;
        this.discount_id = discount_id;
        this.session = session;
        this.discounttype = type;
        this.discountvalue = discountvalue;
        this.details = details;
        this.full_obj = full_obj;
    }

    public JSONArray getDetails(){
        return details;
    }

    public JSONObject getFull_obj() {
        return full_obj;
    }

    public String getDiscountvalue(){
        return discountvalue;
    }

    public Double getSubtotal(){
        return subtotal;
    }

    public LatLng getLatlng() {
        return latlng;
    }

    public String getOrder_time() {
        return order_time;
    }

    public Context getContext() {
        return context;
    }

    public int getId() {
        return id;
    }

    public int getNo_of_carton() {
        return no_of_carton;
    }

    public String getDelivered_time() {
        return delivered_time;
    }

    public int getIswish() {
        return iswish;
    }

    public Double getRating() {
        return rating;
    }

    public Double getVat() {
        return vat;
    }

    public Double getTotal_price() {
        return total_price;
    }

    public Double getDelivery_charge() {
        return delivery_charge;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public String getOrder_date() {
        return order_date;
    }

    public String getUnits() {
        return units;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getDelivered_date() {
        return delivered_date;
    }

    public String getPlace() {
        return place;
    }

    public String getIsmecca() {
        return ismecca;
    }

    public String getRefToken() {
        return refToken;
    }

    public String getComment() {
        return comment;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public String getDay_id() {
        return day_id;
    }

    public String getTime_from() {
        return time_from;
    }

    public String getTime_to() {
        return time_to;
    }

    public String getDiscount_id() {
        return discount_id;
    }

    public String getSession() {
        return session;
    }

    public String getDiscounttype() {
        return discounttype;
    }
}
