package com.lia.yello.roha.model;

public class PlacesItem {
    private String Title;
//    private String Snippet;
    private Double Latitude;
    private Double  Longitude;

    public PlacesItem(String Title,Double Latitude,Double Longitude) {
        this.Title = Title;
        this.Latitude = Latitude;
        this.Longitude = Longitude;

    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}
