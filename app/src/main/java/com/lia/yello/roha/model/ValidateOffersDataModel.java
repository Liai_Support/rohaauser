package com.lia.yello.roha.model;

public class ValidateOffersDataModel {
    String Offername,type,discount;
    int id;
    Double minimumorderamount;
    public ValidateOffersDataModel(int id, String Offername, String type, String discount,Double minimumorderamount){
        this.id=id;
        this.Offername=Offername;
        this.type=type;
        this.discount=discount;
        this.minimumorderamount = minimumorderamount;

    }

    public Double getMinimumorderamount(){
        return minimumorderamount;
    }
    public int getId() {
        return id;
    }

    public String getOffername() {
        return Offername;
    }

    public String getType() {
        return type;
    }

    public String getDiscount() {
        return discount;
    }
}
