package com.lia.yello.roha.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.adapter.ReceiverAdapter;
import com.lia.yello.roha.model.ReceiverDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ReceiverDetailsFragment extends Fragment implements View.OnClickListener{
    View view;
    private static RecyclerView.Adapter adapter1;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView1;
    private static ArrayList<ReceiverDataModel> data = new ArrayList<ReceiverDataModel>();
    public Button proceed;
    public SessionManager sessionManager;
    ImageView back;
    TextView skip;
    public ArrayList<String> markertitlearray = new ArrayList<>();
    public ArrayList<String> markerplacearray = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_receiver_details, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(getActivity());
        }
        ((RohaWaterDashboardActivity)getActivity()).txt_delv_add.setVisibility(View.GONE);
        ((RohaWaterDashboardActivity)getActivity()).txt_loc.setVisibility(View.GONE);
        ((RohaWaterDashboardActivity)getActivity()).profile_txt.setVisibility(View.VISIBLE);
        ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(""+getString(R.string.receiverdetails));

        markertitlearray = LocalData.getInstance().getOverallMarkersTitle();
        markerplacearray = LocalData.getInstance().getOverallMarkersbranchname();
        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler1);
        recyclerView1.setHasFixedSize(false);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView1.setLayoutManager(layoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        Log.d("xhvchxbvc",""+LocalData.getInstance().getBodyarray());
        data = new ArrayList<ReceiverDataModel>();
        for (int i = 0; i < markertitlearray.size(); i++) {
            try {
                data.add(new ReceiverDataModel(markertitlearray.get(i),markerplacearray.get(i),LocalData.getInstance().getBodyarray().getJSONObject(i).getString("receivername"),LocalData.getInstance().getBodyarray().getJSONObject(i).getString("receivermno"),LocalData.getInstance().getBodyarray().getJSONObject(i).getString("receiverdesc")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter1 = new ReceiverAdapter(getContext(),data,ReceiverDetailsFragment.this);
        recyclerView1.setAdapter(adapter1);
        proceed = (Button) view.findViewById(R.id.proceed);
        skip = (TextView) view.findViewById(R.id.skip);
        proceed.setOnClickListener(this);
        skip.setOnClickListener(this);
        back = (ImageView) view.findViewById(R.id.back);
        back.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(getActivity());
        }
        else {
            if (view == skip) {
                ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new PaymentFragment(),null);
            }
            else if (view == proceed) {
                ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new PaymentFragment(),null);
            }
            else if (view == back) {
                getActivity().onBackPressed();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void load(int position){
        for(int i=0;i<LocalData.getInstance().getBodyarray().length();i++){
            if(i == position) {
                try {
                    JSONObject sdjc = LocalData.getInstance().getBodyarray().getJSONObject(i);
                    sdjc.put("receivername", data.get(i).getRname());
                    sdjc.put("receivermno", data.get(i).getRmno());
                    sdjc.put("receiverdesc", data.get(i).getRdesc());
                    LocalData.getInstance().getBodyarray().put(i, sdjc);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
