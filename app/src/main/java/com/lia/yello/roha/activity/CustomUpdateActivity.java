package com.lia.yello.roha.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.RandomAccessFile;

public class CustomUpdateActivity extends AppCompatActivity {
    String currentVersion = "";
    SessionManager sessionManager;
    RelativeLayout view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_update);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
       /* if(getIntent() != null){
            String one = getIntent().getStringExtra("one");
            String two = getIntent().getStringExtra("two");
            String three = getIntent().getStringExtra("three");
            String four = getIntent().getStringExtra("four");
            Toast.makeText(this,""+one+".."+two+".."+three+".."+four,Toast.LENGTH_LONG).show();
        }*/
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        new GetVersionCode().execute();
    }

    class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = "";

            try {
               /* Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + CustomUpdateActivity.this.getPackageName()  + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }*/
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +  CustomUpdateActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    sessionManager.setIsVersion(true);
                    sessionManager.versiondialog();
                }
                else {
                    sessionManager.setIsVersion(false);
                    Intent intent = new Intent(CustomUpdateActivity.this, LanguageActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }
}

