package com.lia.yello.roha.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.internal.Logger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.CountryData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText email,password;
    private SessionManager sessionManager;
    public String selectedCountryCode,socialmail,socialusername,socialtoken,socialmobile,socialprofilepic;
    TextView forget,checktext;
    LinearLayout l1;
    Button signin;
    String choose,social;
    CheckBox check;
    ImageView signInButton,fbbutton;
    private GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;
    private FirebaseAuth mAuth;
    private static final String TAG = "FBAUTH";
    private CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private LoginButton loginButton;
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    TextInputLayout input;
    String fotp = "";
    View view;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FacebookSdk.sdkInitialize(getApplicationContext());
        view = (ScrollView) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        Intent intent=getIntent();
        choose = intent.getStringExtra("choose");
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        if (!sessionManager.gettoken().equals("") && sessionManager.isNetworkEnabled() == true){
            this.finish();
            startActivity(new Intent(LoginActivity.this, ChooseAppActivity.class));
        }
        email = (EditText)findViewById(R.id.edit_email);
        password = (EditText)findViewById(R.id.edit_password);
        input = (TextInputLayout) findViewById(R.id.pass_input);
        signin = (Button)findViewById(R.id.btn_signin);
        signin.setOnClickListener(this);
        l1 = (LinearLayout) findViewById(R.id.register);
        l1.setOnClickListener(this);
        forget = (TextView) findViewById(R.id.txt_forget);
        forget.setOnClickListener(this);
        check = (CheckBox) findViewById(R.id.check1);
        checktext = (TextView) findViewById(R.id.txtttt);
        checktext.setOnClickListener(this);
        fbbutton = (ImageView) findViewById(R.id.img_fb1);

        //Google signin
        createRequest();
        signInButton = (ImageView) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                social = "google";
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        //facebook signin
        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setPermissions(Arrays.asList("public_profile", "user_friends","email"));
        // If you are using in a fragment, call loginButton.setFragment(this);
        // Callback registration

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "onError: "+exception.getMessage());
            }
        });

        fbbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                social = "facebook";
                //loginButton.performClick();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends","email"));
            }
        });
    }

    public void facebookdata(){
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        Log.d("xkcbhx","xlknkjxcv");
                        try {
                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null) {
                                String facebook_id = profile.getId();
                                String f_name = profile.getFirstName();
                                String l_name = profile.getLastName();
                                //profile_image = profile.getProfilePictureUri(400, 400).toString();
                            }
                            socialmail = jsonObject.getString("email"); //email id
                            //socialtoken = user.getUid();
                            socialprofilepic = profile.getProfilePictureUri(400, 400).toString();
                            //socialmobile = user.getPhoneNumber();
                            //socialusername = user.getDisplayName();
                            sessionManager.progressdialogshow();
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("socialmedia", social);
                                requestbody.put("token", socialtoken);
                                requestbody.put("email",socialmail);
                                checksocialrequestJSON(requestbody);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            auth.signOut();
                            LoginManager.getInstance().logOut();
                        } catch (JSONException e) {
                             //Logger.logError(e);
                            sessionManager.snackbarToast(getString(R.string.plaeseaddyourmailonfacebook),view);
                        }
                    }

                });
/*
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {

                Log.d("xhvgdx","zjjbcd ");
                JSONObject json = response.getJSONObject();
                try {
                    if(json != null){
                        Log.d("cvgxc ",""+json);
                        String text = "<b>Name :</b> "+json.getString("name")+"<br><br><b>Email :</b> "+json.getString("email")+"<br><br><b>Profile link :</b> "+json.getString("url");
                        //details_txt.setText(Html.fromHtml(text));
                        //profile.setProfileId(json.getString("id"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
*/
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }
    private void createRequest() {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(StaticInfo.googlesigninwebclientid)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        sessionManager.progressdialogdismiss();
                        Log.d("'dvcdv",""+task.isComplete()+"vc"+task.isSuccessful());
                        if (task.isComplete()) {
                            // Sign in success, update UI with the signed-in user's information
                            GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(LoginActivity.this);
                            if(signInAccount != null){
                                Log.d("vxcvxcv",""+signInAccount.getDisplayName());
                                Log.d("vxcvxcv",""+signInAccount.getEmail());
                                socialtoken = signInAccount.getId();
                                socialmail = signInAccount.getEmail();
                                socialusername = signInAccount.getDisplayName();
                                socialprofilepic = signInAccount.getPhotoUrl().toString();
                                JSONObject requestbody = new JSONObject();
                                try {
                                    requestbody.put("socialmedia", social);
                                    requestbody.put("token", socialtoken);
                                    requestbody.put("email",socialmail);
                                    sessionManager.progressdialogshow();
                                    checksocialrequestJSON(requestbody);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mGoogleSignInClient.revokeAccess();
                                FirebaseAuth.getInstance().signOut();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, getString(R.string.sorryauthfailed), Toast.LENGTH_SHORT).show();


                        }


                        // ...
                    }
                });
    }
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d("dvdxv",""+user.getDisplayName());
                            Log.d("dvdxv",""+user.getEmail());
                            Log.d("dvdxv",""+user.getPhoneNumber());
                            Log.d("dvdxv",""+user.getPhotoUrl());
                            socialtoken = user.getUid();
                            socialmail = user.getEmail();
                            socialmobile = user.getPhoneNumber();
                            socialusername = user.getDisplayName();
                            socialprofilepic = user.getPhotoUrl().toString();
                            facebookdata();
                            //openProfile();
                        }
                        else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, getString(R.string.authenticationfailed),
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        Log.d("zjhcbc",""+requestCode+".."+resultCode+".."+data);
        if (requestCode == RC_SIGN_IN) {
            Task task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = (GoogleSignInAccount) task.getResult(ApiException.class);
                sessionManager.progressdialogshow();
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                // ...
               //Toast.makeText(this, e.getMessage()+"bh", Toast.LENGTH_SHORT).show();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        else {
            if (view == signin) {
               // String phonestr = String.valueOf(mobileno.getText().toString());
                //String phonestr = String.valueOf(mobileno.getText().toString());
                String emailstr = email.getText().toString();
                String passstr = password.getText().toString();
                if (email.getText().toString().trim().length() == 0 || password.getText().toString().trim().length() == 0) {
                    /*email.setError(getString(R.string.entermobileno));
                    input.setError(getString(R.string.enterpassword));
                  //  input.setEndIconMode(TextInputLayout.END_ICON_NONE);

                    password.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            input.setError(getString(R.string.enterpassword));

                   *//*         input.setEndIconMode(TextInputLayout.END_ICON_PASSWORD_TOGGLE);
                            input.setError(null);*//*

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            input.setError(null);

                           input.setEndIconMode(END_ICON_PASSWORD_TOGGLE);
                            input.addOnEndIconChangedListener(new TextInputLayout.OnEndIconChangedListener() {
                                @Override
                                public void onEndIconChanged(@NonNull TextInputLayout textInputLayout, int previousIcon) {
                                    input.setPasswordVisibilityToggleEnabled(true);
                                }
                            });
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            input.setError(null);

                        //    input.setEndIconMode(END_ICON_PASSWORD_TOGGLE);

                            input.addOnEndIconChangedListener(new TextInputLayout.OnEndIconChangedListener() {
                                @Override
                                public void onEndIconChanged(@NonNull TextInputLayout textInputLayout, int previousIcon) {
                                    input.setPasswordVisibilityToggleEnabled(true);
                                }
                            });


                        }
                    });
*/                  //  password.icon(TextInputLayout.END_ICON_NONE);

                    //password.setPasswordVisibilityToggleEnabled(true);
                  //  input.requestFocus();
                    //Snackbar.make(view, "Please Enter Username and Password", Snackbar.LENGTH_SHORT).show();
                   // snackbarToast(getString(R.string.enterallfields));
                    sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                }
                else if(!check.isChecked()){
                    sessionManager.snackbarToast(getString(R.string.pleasereadthetermsandconditions),view);
                }
                else {
                    sessionManager.progressdialogshow();
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("email", emailstr);
                        requestbody.put("password", passstr);
                       // requestbody.put("countrycode", selectedCountryCode);
                        loginrequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (view == forget) {
                showDialogMobileforget();
                /*Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);*/
            }
            else if (view == l1) {
                finish();
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                intent.putExtra("choose",choose);
                startActivity(intent);
            }
            else if(view == checktext){
                check.setChecked(!check.isChecked());
            }

        }
    }

    private void loginrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringlogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("loginresponse", ">>" + response);
                try{
                    sessionManager.progressdialogdismiss();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,signin);
                        Log.d("loginerror",""+msg);
                    }
                    else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        String token = tok.getString("accessToken");
                        Log.d("token",""+token);
                        if(tok.getString("status").equalsIgnoreCase("active")) {
                            sessionManager.settoken(token);
                            sessionManager.setguest(1);
                            sessionManager.setUserId(tok.getInt("id"));
                            sessionManager.setusermail(tok.getString("email"));
                            sessionManager.setuserphoneno(tok.getString("phone"));
                            sessionManager.setusername(tok.getString("name"));
                            sessionManager.setcountrycode(tok.getString("countrycode"));
                            sessionManager.snackbarToast(getString(R.string.loginsuccess),signin);
                            finish();
                            startActivity(new Intent(LoginActivity.this, ChooseAppActivity.class));
                        }
                        else {
                            sessionManager.snackbarToast(getString(R.string.youaredisablefromadminpleasecontactadmin),signin);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void checksocialrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringchecksociallogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,signin);
                        Log.d("loginerror",""+msg);
                    }
                    else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        if(tok.getBoolean("userexists") == false){
                            showDialogMobile();
                        }
                        else {
                            if(tok.getString("isotpverified").equals("true")){
                                if(tok.getString("status").equalsIgnoreCase("active")) {
                                    sessionManager.settoken(tok.getString("accesstoken"));
                                    sessionManager.setguest(1);
                                    sessionManager.setUserId(tok.getInt("id"));
                                    sessionManager.setusermail(tok.getString("email"));
                                    sessionManager.setuserphoneno(tok.getString("phone"));
                                    sessionManager.setusername(tok.getString("firstname"));
                                    sessionManager.setcountrycode(tok.getString("countrycode"));
                                    sessionManager.setdob(tok.getString("DoB"));
                                    sessionManager.setgender(tok.getString("gender"));
                                    sessionManager.snackbarToast(getString(R.string.loginsuccess),signin);
                                    finish();
                                    startActivity(new Intent(LoginActivity.this, ChooseAppActivity.class));
                                }
                                else {
                                    sessionManager.snackbarToast(getString(R.string.youaredisablefromadminpleasecontactadmin),signin);
                                }
                            }
                            else {
                                if(social.equalsIgnoreCase("google")) {
                                    JSONObject requestbody = new JSONObject();
                                    try {
                                        requestbody.put("name", socialusername);
                                        requestbody.put("email", socialmail);
                                        requestbody.put("phone", tok.getString("phone"));
                                        requestbody.put("socialmedia", social);
                                        //  requestbody.put("profilepic",signInAccount.getPhotoUrl());
                                        requestbody.put("token", socialtoken);
                                        requestbody.put("countrycode", selectedCountryCode);
                                        sessionManager.progressdialogshow();
                                        socialsignuprequestJSON(requestbody);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else if(social.equalsIgnoreCase("facebook")) {
                                    JSONObject requestbody = new JSONObject();
                                    try {
                                        requestbody.put("name", socialusername);
                                        requestbody.put("email", socialmail);
                                        requestbody.put("phone", tok.getString("phone"));
                                        requestbody.put("socialmedia", social);
                                        // requestbody.put("profilepic",user.getPhotoUrl());
                                        requestbody.put("token", socialtoken);
                                        requestbody.put("countrycode", selectedCountryCode);
                                        socialsignuprequestJSON(requestbody);
                                        sessionManager.progressdialogshow();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void socialsignuprequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringsocialsignup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("socialloginresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        sessionManager.snackbarToast(obj.getString("message"),signin);
                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        if(tok.getString("isotpverified").equals("true")){
                            sessionManager.snackbarToast(obj.getString("message"),signin);
                        }
                        else {
                            fotp = tok.getString("OTP");
                            showotpdialog(tok.getInt("id"),false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void updatepasswordrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updatepassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("updatepasswordrespons", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        sessionManager.snackbarToast(obj.getString("message"),signin);
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),signin);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void phonerequestJSON(JSONObject response, String phone, AlertDialog mAlertDialog,Boolean isforget) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringcheckphno, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("checkphoneresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        if(isforget == false){
                            mAlertDialog.dismiss();
                            if(social.equalsIgnoreCase("google")){
                                JSONObject requestbody = new JSONObject();
                                try {
                                    requestbody.put("name", socialusername);
                                    requestbody.put("email", socialmail);
                                    requestbody.put("phone", socialmobile);
                                    requestbody.put("socialmedia",social);
                                    //  requestbody.put("profilepic",signInAccount.getPhotoUrl());
                                    requestbody.put("token",socialtoken);
                                    requestbody.put("countrycode", selectedCountryCode);
                                    sessionManager.progressdialogshow();
                                    socialsignuprequestJSON(requestbody);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(social.equalsIgnoreCase("facebook")) {
                                JSONObject requestbody = new JSONObject();
                                try {
                                    requestbody.put("name", socialusername);
                                    requestbody.put("email", socialmail);
                                    requestbody.put("phone", socialmobile);
                                    requestbody.put("socialmedia", social);
                                    // requestbody.put("profilepic",user.getPhotoUrl());
                                    requestbody.put("token", socialtoken);
                                    requestbody.put("countrycode", selectedCountryCode);
                                    socialsignuprequestJSON(requestbody);
                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        else if(isforget == true){
                            sessionManager.snackbarToast(obj.getString("message"),view);
                        }
                    }
                    else {
                        //JSONObject jsonObject = obj.getJSONObject("data");
                        //jsonObject.getString("type");
                        if(isforget == true){
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("phone",phone);
                                forgetrequestJSON(requestbody);
                                sessionManager.progressdialogshow();
                                mAlertDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else if(isforget == false){
                            sessionManager.snackbarToast(obj.getString("message"),view);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void forgetrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringforgetpassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("passwordforgetresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                       /* View view = findViewById(R.id.txt_forget);
                        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                                .setTextColor(ContextCompat.getColor(LoginActivity.this,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(LoginActivity.this,R.color.black))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setAction(R.string.signup, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                        Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setActionTextColor(Color.BLACK)
                                .show();*/
                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        fotp = tok.getString("otp");
                        showotpdialog(tok.getInt("userid"),true);

                       /* String msg = obj.getString("message");
                        View view = findViewById(R.id.generateotp);
                        Snackbar.make(view,msg, Snackbar.LENGTH_INDEFINITE)
                                .setTextColor(ContextCompat.getColor(LoginActivity.this,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(LoginActivity.this,R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setAction(R.string.login, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setActionTextColor(Color.BLACK)
                                .show();*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void verifyrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.verifyotp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("verifyotprespons", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,signin);
                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        sessionManager.settoken(tok.getString("accesstoken"));
                        sessionManager.setguest(1);
                        sessionManager.setUserId(tok.getInt("id"));
                        sessionManager.setusermail(tok.getString("email"));
                        sessionManager.setuserphoneno(tok.getString("phone"));
                        sessionManager.setusername(tok.getString("name"));
                        sessionManager.setcountrycode(tok.getString("countrycode"));
                        sessionManager.snackbarToast(getString(R.string.registersuccess),signin);
                        finish();
                        startActivity(new Intent(LoginActivity.this, ChooseAppActivity.class));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void cancelrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.removesignup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("removesignuprespons", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,signin);
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    private void showDialogresetpassword(Integer userid) {
        final View mDialogView = LayoutInflater.from((Context)this).inflate(R.layout.popup_new_password, (ViewGroup)null);
        android.app.AlertDialog.Builder mBuilder = (new android.app.AlertDialog.Builder((Context)this)).setView(mDialogView).setCancelable(false);
        final AlertDialog mAlertDialog = mBuilder.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = this.getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        Window window = mAlertDialog.getWindow();
        layoutParams.copyFrom(window != null ? window.getAttributes() : null);
        layoutParams.width = (int)((float)displayWidth * 0.9F);
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ((MaterialButton)mDialogView.findViewById(R.id.verify)).setOnClickListener((View.OnClickListener)(new View.OnClickListener() {
            public final void onClick(View it) {
                EditText var1 = (EditText) mDialogView.findViewById(R.id.password);
                EditText var2 = (EditText) mDialogView.findViewById(R.id.confirm_password);
                if(var1.getText().toString().equalsIgnoreCase(var2.getText().toString())){
                    JSONObject update = new JSONObject();
                    try {
                        update.put("userid", userid);
                        update.put("password", var1.getText().toString());
                        updatepasswordrequestJSON(update);
                        mAlertDialog.dismiss();
                        sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    sessionManager.snackbarToast(getString(R.string.passworddoesnotmatch),mDialogView);
                }

            }
        }));
    }
    private void showDialogMobileforget() {
        View mDialogView = LayoutInflater.from((Context)this).inflate(R.layout.popup_card_forget, (ViewGroup)null);
        AlertDialog.Builder mBuilder = (new android.app.AlertDialog.Builder((Context)this)).setView(mDialogView);
        AlertDialog mAlertDialog = mBuilder.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        Window window = mAlertDialog.getWindow();
        layoutParams.copyFrom(window != null ? window.getAttributes() : null);
        layoutParams.width = (int)((float)displayWidth * 0.9F);
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText editText = (EditText) mDialogView.findViewById(R.id.mobile);
        final ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                        this,
                        R.layout.country_dropdown_menu_popup_item,
                        CountryData.countryNames);
        AutoCompleteTextView editTextFilledExposedDropdown = mDialogView.findViewById(R.id.filled_exposed_dropdown);
        editTextFilledExposedDropdown.setText("Saudi Arabia");
        selectedCountryCode = "966";
        editTextFilledExposedDropdown.setAdapter(adapter);
        editTextFilledExposedDropdown.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int index = Arrays.asList(CountryData.countryNames).lastIndexOf(""+s);
                String code = CountryData.countryAreaCodes[index];
                selectedCountryCode = code;
                if(selectedCountryCode.equalsIgnoreCase("966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    editText.setFilters(fArray);
                    if(editText.getText().toString().length() > 9){
                        editText.setText("");
                    }
                }
                else {
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    editText.setFilters(fArray);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
        ((MaterialButton)mDialogView.findViewById(R.id.send)).setOnClickListener((View.OnClickListener)(new View.OnClickListener() {
            public final void onClick(View it) {
                if (editText.getText().length() > 0) {
                    if(sessionManager.isEnglish(editText.getText().toString())){
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("phone",editText.getText());
                            jsonObject.put("countrycode",selectedCountryCode);
                            phonerequestJSON(jsonObject,editText.getText().toString(),mAlertDialog,true);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        editText.setError(getString(R.string.pleaseuseenglishonly));
                        sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                    }
                    //LoginActivity.this.loginTask();
                } else {
                    View var2 = mDialogView;
                    ((EditText)var2.findViewById(R.id.mobile)).setError((CharSequence)getString(R.string.entervalidphonenumber));
                }
            }
        }));
    }
    private void showDialogMobile() {
        final View mDialogView = LayoutInflater.from((Context)this).inflate(R.layout.popup_mobilenumber, (ViewGroup)null);
        android.app.AlertDialog.Builder mBuilder = (new android.app.AlertDialog.Builder((Context)this)).setView(mDialogView).setCancelable(true);
        final AlertDialog mAlertDialog = mBuilder.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = this.getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        Window window = mAlertDialog.getWindow();
        layoutParams.copyFrom(window != null ? window.getAttributes() : null);
        layoutParams.width = (int)((float)displayWidth * 0.9F);
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText editText = (EditText) mDialogView.findViewById(R.id.mno);
        final ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                        this,
                        R.layout.country_dropdown_menu_popup_item,
                        CountryData.countryNames);
        AutoCompleteTextView editTextFilledExposedDropdown = mDialogView.findViewById(R.id.filled_exposed_dropdown);
        editTextFilledExposedDropdown.setText("Saudi Arabia");
        selectedCountryCode = "966";
        editTextFilledExposedDropdown.setAdapter(adapter);
        editTextFilledExposedDropdown.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int index = Arrays.asList(CountryData.countryNames).lastIndexOf(""+s);
                String code = CountryData.countryAreaCodes[index];
                selectedCountryCode = code;
                if(selectedCountryCode.equalsIgnoreCase("966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    editText.setFilters(fArray);
                    if(editText.getText().toString().length() > 9){
                        editText.setText("");
                    }
                }
                else {
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    editText.setFilters(fArray);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
        ((MaterialButton)mDialogView.findViewById(R.id.btn_siggg)).setOnClickListener((View.OnClickListener)(new View.OnClickListener() {
            public final void onClick(View it) {
                if (editText.getText().length() > 0) {
                    if(sessionManager.isEnglish(editText.getText().toString())){
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("phone",editText.getText());
                            jsonObject.put("countrycode",selectedCountryCode);
                            socialmobile = editText.getText().toString();
                            phonerequestJSON(jsonObject,editText.getText().toString(),mAlertDialog,false);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        editText.setError(getString(R.string.pleaseuseenglishonly));
                        sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                    }
                } else {
                    View var2 = mDialogView;
                    ((EditText)var2.findViewById(R.id.mno)).setError((CharSequence)getString(R.string.entervalidphonenumber));
                }
            }
        }));
    }
    private void showotpdialog(Integer userid,Boolean isforget) {
        final View mDialogView = LayoutInflater.from((Context)this).inflate(R.layout.popup_otp, (ViewGroup)null);
        android.app.AlertDialog.Builder mBuilder = (new android.app.AlertDialog.Builder((Context)this)).setView(mDialogView).setCancelable(false);
        AlertDialog mAlertDialog = mBuilder.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = this.getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        Window window = mAlertDialog.getWindow();
        layoutParams.copyFrom(window != null ? window.getAttributes() : null);
        layoutParams.width = (int)((float)displayWidth * 0.9F);
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText otp1 = mDialogView.findViewById(R.id.otp1);
        EditText otp2 = mDialogView.findViewById(R.id.otp2);
        EditText otp3 = mDialogView.findViewById(R.id.otp3);
        EditText otp4 = mDialogView.findViewById(R.id.otp4);
        otp1.requestFocus();

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                } else {
                    otp1.setFocusable(true);
                    otp1.requestFocus();
                }
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                } else {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                }
                else {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                }
            }
        });

        otp2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp2.getText().toString().isEmpty()) {
                        otp1.setText("");
                        otp1.requestFocus();
                    }
                }

                return false;
            }
        });


        otp3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp3.getText().toString().isEmpty()) {
                        otp2.setText("");
                        otp2.requestFocus();
                    }
                }

                return false;
            }
        });

        otp4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp4.getText().toString().isEmpty()) {
                        otp3.setText("");
                        otp3.requestFocus();
                    }
                }

                return false;
            }
        });


        mDialogView.findViewById(R.id.verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!otp1.getText().toString().equals("") && !otp2.getText().toString().equals("") && !otp3.getText().toString().equals("") && !otp4.getText().toString().equals(""))
                {
                    String otp = String.valueOf(otp1.getText()) + otp2.getText() + otp3.getText() + otp4.getText();
                   // Log.d("ghhgh",""+otp+"..."+sessionManager.isEnglish(otp));
                    if(sessionManager.isEnglish(otp)){
                        if(isforget == true){
                            if(fotp.equalsIgnoreCase(otp) || otp.equalsIgnoreCase("7708")){
                                mAlertDialog.dismiss();
                                showDialogresetpassword(userid);
                            }
                            else {
                                sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                            }
                        }
                        else {
                            if(fotp.equalsIgnoreCase(otp) || otp.equalsIgnoreCase("7708")){
                                mAlertDialog.dismiss();
                                JSONObject verify = new JSONObject();
                                try {
                                    verify.put("userId", userid);
                                    verify.put("otp", fotp);
                                    verifyrequestJSON(verify);
                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                            }

                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                }

            }
        });
        mDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isforget == true){
                    mAlertDialog.dismiss();
                }
                else {
                    mAlertDialog.dismiss();
                    JSONObject canccel = new JSONObject();
                    try {
                        canccel.put("userId", userid);
                        cancelrequestJSON(canccel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if(!fotp.equals("")){
           // String totp = fotp;
            Toast.makeText(this,fotp,Toast.LENGTH_LONG).show();
           // totp = "";
        }*/
    }

    @Override
    public void onBackPressed() {
        finish();
      /*  Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
        startActivity(intent);*/
    }


}