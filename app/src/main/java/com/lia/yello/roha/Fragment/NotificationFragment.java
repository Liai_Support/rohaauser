package com.lia.yello.roha.Fragment;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.NotificationAdapter;
import com.lia.yello.roha.model.NotificationDataModel;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class NotificationFragment extends Fragment {
   View view;
   SessionManager sessionManager;
    public ArrayList<NotificationDataModel> data;
    private static RecyclerView.Adapter adapter;
    RecyclerView recyclerView;
    TextView clear_notification,nonotifi;
    boolean iswater = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_notification, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        clear_notification=(TextView)view.findViewById(R.id.clear_notification);
        nonotifi=(TextView)view.findViewById(R.id.nonotify);

        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.notification);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.notification);
          }

        recyclerView = (RecyclerView) view.findViewById(R.id.notification_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
           notificationJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        clear_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("userid", sessionManager.getUserId());
                    clearnotificationJSON(requestbody);
                    sessionManager.progressdialogshow();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater") && iswater==true){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }
        }
    }


    private void notificationJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request123",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.notification, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("notification", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        Log.d("dvdxv",""+obj.getJSONObject("data"));
                        JSONObject jsonObject=obj.getJSONObject("data");
                        JSONArray data1 = jsonObject.getJSONArray("notification_data");
                        Log.d("cfghjkkjhgf",""+data1);
                        data = new ArrayList<NotificationDataModel>();
                        if(data1.length() == 0){
                            nonotifi.setVisibility(View.VISIBLE);
                            clear_notification.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                        }
                        else {
                            nonotifi.setVisibility(View.GONE);
                            clear_notification.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            for (int i = 0; i < data1.length(); i++) {
                                JSONObject jsonObject1 = data1.getJSONObject(i);
                                NotificationDataModel datamodel = new NotificationDataModel(jsonObject1.getInt("id"),jsonObject1.getString("message"),jsonObject1.getString("title"),jsonObject1.getString("date"));
                                data.add(datamodel);
                            }
                            adapter = new NotificationAdapter(getContext(), data, NotificationFragment.this);
                            recyclerView.setAdapter(adapter);
                        }
                    }
                    else {
                        nonotifi.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        clear_notification.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void clearnotificationJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("clearnotiff",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.clearnotification, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("clearnotiff", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("userid", sessionManager.getUserId());
                            notificationJSON(requestbody);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.notificationnotcleared),view);
                    }

                    } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

}