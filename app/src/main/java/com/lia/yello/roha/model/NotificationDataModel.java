package com.lia.yello.roha.model;

public class NotificationDataModel  {
    String message,title,date;
    int id;
    public NotificationDataModel(int id,String message,String title,String date){
        this.id=id;
        this.message=message;
        this.title=title;
        this.date=date;

    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }


    public String getTitle() {
        return title;
    }
}

