package com.lia.yello.roha.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.lia.yello.roha.R;
import com.lia.yello.roha.model.BannerDataModel;
import com.lia.yello.roha.model.IbanDataModel;
import com.lia.yello.roha.model.OfferDataModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomebannerAdapter extends PagerAdapter {

    private Activity activity;
    private List<BannerDataModel> dataSet;
    private List<OfferDataModel> dataSet1;

    public HomebannerAdapter(Activity activity, List<BannerDataModel> data, List<OfferDataModel> data1){
        this.activity = activity;
        this.dataSet = data;
        this.dataSet1 = data1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (activity).getLayoutInflater();
        //creating  xml file for custom viewpager
        View viewItem = inflater.inflate(R.layout.card_banner, container, false);
        //finding id
        ImageView imageView =  viewItem.findViewById(R.id.imageView);
        if(position < dataSet1.size()){
            int listPosition = position;
            Picasso.get().load(dataSet1.get(listPosition).getOimage()).placeholder(R.drawable.defaultimg).error(R.drawable.defaultimg).into(imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(dataSet1.get(listPosition).getType().equalsIgnoreCase("SAR")){
                        final Dialog dialog = new Dialog(activity);
                        dialog.setContentView(R.layout.popup_offers_promocode);
                        dialog.setTitle(activity.getString(R.string.offerdetails));
                        dialog.setCancelable(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        lp.gravity = Gravity.CENTER;

                        dialog.getWindow().setAttributes(lp);

                        // set the custom dialog components - text, image and button
                        TextView promocode = (TextView) dialog.findViewById(R.id.promocode);
                        ImageView cpy = (ImageView) dialog.findViewById(R.id.promo_copy);
                        TextView from = (TextView) dialog.findViewById(R.id.validfrom);
                        TextView to = (TextView) dialog.findViewById(R.id.validto);
                        TextView value = (TextView) dialog.findViewById(R.id.value);
                        TextView moa = (TextView) dialog.findViewById(R.id.moa);

                        promocode.setText(""+dataSet1.get(listPosition).getPromocode());
                        from.setText(""+dataSet1.get(listPosition).getFrom());
                        to.setText(""+dataSet1.get(listPosition).getTo());
                        value.setText(""+dataSet1.get(listPosition).getValue()+" SAR");
                        moa.setText(""+dataSet1.get(listPosition).getMinimumorderamount()+" SAR");
                        cpy.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("label", dataSet1.get(listPosition).getPromocode());
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(activity,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
                            }
                        });
               /* Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/
                        dialog.show();
                    }
                    else if(dataSet1.get(listPosition).getType().equalsIgnoreCase("Carton")){
                        final Dialog dialog = new Dialog(activity);
                        dialog.setContentView(R.layout.popup_offers_promocode);
                        dialog.setTitle(activity.getString(R.string.offerdetails));
                        dialog.setCancelable(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        lp.gravity = Gravity.CENTER;

                        dialog.getWindow().setAttributes(lp);

                        // set the custom dialog components - text, image and button
                        TextView promocode = (TextView) dialog.findViewById(R.id.promocode);
                        ImageView cpy = (ImageView) dialog.findViewById(R.id.promo_copy);
                        TextView from = (TextView) dialog.findViewById(R.id.validfrom);
                        TextView to = (TextView) dialog.findViewById(R.id.validto);
                        TextView value = (TextView) dialog.findViewById(R.id.value);
                        TextView moa = (TextView) dialog.findViewById(R.id.moa);
                        TextView disc = (TextView) dialog.findViewById(R.id.txt_disc);

                        disc.setText(""+activity.getString(R.string.offer));
                        promocode.setText(""+dataSet1.get(listPosition).getPromocode());
                        from.setText(""+dataSet1.get(listPosition).getFrom());
                        to.setText(""+dataSet1.get(listPosition).getTo());

                        value.setText(""+(int) dataSet1.get(listPosition).getValue()+" - "+dataSet1.get(listPosition).getCartonname());
                        moa.setText(""+dataSet1.get(listPosition).getMinimumorderamount()+" SAR");
                        cpy.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("label", dataSet1.get(listPosition).getPromocode());
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(activity,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
                            }
                        });
               /* Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/
                        dialog.show();
                    }
                    else {
                        final Dialog dialog = new Dialog(activity);
                        dialog.setContentView(R.layout.popup_offers_promocode);
                        dialog.setTitle(activity.getString(R.string.offerdetails));
                        dialog.setCancelable(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        lp.gravity = Gravity.CENTER;

                        dialog.getWindow().setAttributes(lp);

                        // set the custom dialog components - text, image and button
                        TextView promocode = (TextView) dialog.findViewById(R.id.promocode);
                        ImageView cpy = (ImageView) dialog.findViewById(R.id.promo_copy);
                        TextView from = (TextView) dialog.findViewById(R.id.validfrom);
                        TextView to = (TextView) dialog.findViewById(R.id.validto);
                        TextView value = (TextView) dialog.findViewById(R.id.value);
                        TextView moa = (TextView) dialog.findViewById(R.id.moa);

                        promocode.setText(""+dataSet1.get(listPosition).getPromocode());
                        from.setText(""+dataSet1.get(listPosition).getFrom());
                        to.setText(""+dataSet1.get(listPosition).getTo());
                        value.setText(""+dataSet1.get(listPosition).getValue()+" %");
                        moa.setText(""+dataSet1.get(listPosition).getMinimumorderamount()+" SAR");

                        cpy.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("label", dataSet1.get(listPosition).getPromocode());
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(activity,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
                            }
                        });
                        dialog.show();
                    }


                }
            });
        }
        else {
            //int listPosition = position-dataSet.size();
            Picasso.get().load(dataSet.get(position-dataSet1.size()).getOimage()).placeholder(R.drawable.defaultimg).error(R.drawable.defaultimg).into(imageView);
        }

        container.addView(viewItem);
        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return dataSet.size()+dataSet1.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        container.removeView((View) object);
    }

}


