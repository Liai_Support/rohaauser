package com.lia.yello.roha.model;

public class AddressListDataModel {
    String Location,city;
    int id,bid;
    double lat,lng;
    String addresstype;


    public AddressListDataModel(String atype,int bid, String location, String city, int id, double lat, double lng){
        this.Location = location;
        this.id=id;
        this.lat=lat;
        this.bid = bid;
        this.addresstype = atype;
        this.lng=lng;
        this.city = city;
    }

    public String getAddresstype(){
        return addresstype;
    }

    public String getLocation() {
        return Location;
    }

    public int getBid() {
        return bid;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public int getId() {
        return id;
    }

    public String getCity(){
        return city;
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }
}
