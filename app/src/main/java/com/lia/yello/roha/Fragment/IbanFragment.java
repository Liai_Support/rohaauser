package com.lia.yello.roha.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.utility.VolleyMultipartRequest;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IbanFragment extends Fragment implements View.OnClickListener {

    View view;
    ImageView ibanimg,back;
    Button upload,submit;
    SessionManager sessionManager;
    String currentPhotoPath = null;
    Uri currentPhotoPathFileUri = null;
    File currentPhotoFile = null;
    private Bitmap bitmap = null;
    public JSONArray newarray = new JSONArray() ;
    private final static int IMAGE_RESULT = 200;
    Bundle args;
    RelativeLayout relative;
    TextView test;
    VolleyMultipartRequest.DataPart imagedata = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_iban, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        args = getArguments();
        ibanimg = (ImageView) view.findViewById(R.id.iban_img);
        back = (ImageView) view.findViewById(R.id.back);
        back.setOnClickListener(this);
        upload = (Button) view.findViewById(R.id.upload);
        submit = (Button) view.findViewById(R.id.submit);
        relative = (RelativeLayout) view.findViewById(R.id.relative);
        test = (TextView) view.findViewById(R.id.test);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("title");
        ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(getString(R.string.transferdetails));

        upload.setText(getResources().getString(R.string.uploadthretransferRecipt)  + LocalData.getInstance().getTotalprice() + "SAR" );
        upload.setOnClickListener(this);
        submit.setOnClickListener(this);
        submit.setVisibility(View.VISIBLE);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        //Note: I have placed this code in onResume for demostration purpose. Be careful when you use it in
        // production code
    }
    public static class MyHurlStack extends HurlStack {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {

            HttpURLConnection connection = super.createConnection(url);
            connection.setChunkedStreamingMode(0);
            return connection;
        }
    }


    @Override
    public void onClick(View view) {
        if(view == upload){
            if(sessionManager.isPermissionsEnabled() == false){
                sessionManager.permissionsEnableRequest();
            }
            else {
                showFileChooser1();
                //showPictureDialog();
            }
        }
        else if(view == submit){
            if(imagedata != null) {
                ordercomplete();
            }
            else {
                sessionManager.snackbarToast(getString(R.string.pleaseuploadimage),view);
            }
        }
        else if(view == back){
            getActivity().onBackPressed();
        }
    }

    private void showFileChooser1() {
        startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT);
    }

    public Intent getPickImageChooserIntent() {
        Uri outputFileUri = null;
        try {
            outputFileUri = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            Log.d("hjcb",""+intent);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
            else {
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, getString(R.string.selectsource));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("vjncv1",""+data+".."+requestCode+".."+resultCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_RESULT) {
                if(data!= null && data.getData() != null){
                    Uri contentUri = data.getData();
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFilePath = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
                    String RealimageFilePath = getImageFilePath(data);
                    ibanimg.setImageURI(contentUri);
                    try {
                       // bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentUri);
                        byte[] byteArray = getBytes(getActivity().getContentResolver().openInputStream(contentUri));
                        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                        long imagename = System.currentTimeMillis();
                        imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(bitmap));
                        submit.setVisibility(View.VISIBLE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    String filePath = getImageFilePath(data);
                    if (filePath != null) {
                        Log.d("filePath", String.valueOf(filePath));
                        bitmap = BitmapFactory.decodeFile(filePath);
                        ibanimg.setImageBitmap(bitmap);
                        long imagename = System.currentTimeMillis();
                        imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(bitmap));
                        submit.setVisibility(View.VISIBLE);
                    }
                }
            }

        }
    }

    private String getImageFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;
        if (isCamera) return currentPhotoPath;
        else  return getPathFromURI(data.getData());
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    //gallery
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
    private String getFileExt(Uri contentUri) {
        ContentResolver c = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }

    //camera
    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        if(image != null){
            currentPhotoFile = image;
            currentPhotoPath = image.getAbsolutePath();
            currentPhotoPathFileUri = FileProvider.getUriForFile(getContext(),
                    "com.lia.yello.roha.android.fileprovider",
                    image);
        }
        return currentPhotoPathFileUri;
    }
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.getActivity().sendBroadcast(mediaScanIntent);
    }

    //file manager and files
    @SuppressLint("NewApi")
    public String getPathFromURI(Context context, Uri uri) {
        Log.d("hyfhtf",""+uri);
        String filePath = "";
        String fileId = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = fileId.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String selector = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, selector, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private void ordercomplete() {
        sessionManager.progressdialogshow();
        if(sessionManager.getchoose().equals("rohahome")){
            placeorderhomeJSON();
        }
        else {
            if(sessionManager.getIsWish() == 1){
                try {
                    newarray = new JSONArray() ;
                    JSONArray conv = LocalData.getInstance().getBodyarray();
                    Log.d("old body",""+conv);
                    for(int i=0;i<conv.length();i++){
                        JSONObject js =  conv.getJSONObject(i);
                        String mosquename = js.getString("mosquename");
                        byte[] data1 = mosquename.getBytes("UTF-8");
                        String bmosquename = Base64.encodeToString(data1, Base64.NO_WRAP);
                        String placename = js.getString("place");
                        byte[] data11 = placename.getBytes("UTF-8");
                        String bplace = Base64.encodeToString(data11, Base64.NO_WRAP);
                        double lat = js.getDouble("lat");
                        double lng = js.getDouble("lng");
                        int ismeccca = js.getInt("ismecca");
                        int cartcount = js.getInt("cartcount");
                        double productprice = js.getDouble("productprice");
                        //Log.d("dsvsdv"+i,""+i+""+js.getString("receivername"));
                        String rname = js.getString("receivername");
                        byte[] data2 = rname.getBytes("UTF-8");
                        String brname = Base64.encodeToString(data2, Base64.DEFAULT);
                        String rmno = js.getString("receivermno");
                        byte[] data4 = rmno.getBytes("UTF-8");
                        String brmno = Base64.encodeToString(data4, Base64.DEFAULT);
                        String rdesc = js.getString("receiverdesc");
                        byte[] data3 = rdesc.getBytes("UTF-8");
                        String brdesc = Base64.encodeToString(data3, Base64.DEFAULT);
                        String unit = js.getString("units_qty");
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("lat",lat);
                        jsonObject.put("lng",lng);
                        jsonObject.put("ismecca",ismeccca);
                        jsonObject.put("place",bplace);
                        jsonObject.put("mosquename",bmosquename);
                        jsonObject.put("cartcount",cartcount);
                        jsonObject.put("productprice",productprice);
                        jsonObject.put("receivername",brname);
                        jsonObject.put("receivermno",brmno);
                        jsonObject.put("receiverdesc",brdesc);
                        jsonObject.put("units_qty",unit);
                        newarray.put(jsonObject);
                    }
                    //Log.d("final body",""+newarray);
                    placeordermeccamedinarequestJSON(newarray);
                } catch (JSONException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            else if(sessionManager.getIsWish() == 0){
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("wish_place", sessionManager.base64convert(LocalData.getInstance().getPlace()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                placeorderiswishrequestJSON(LocalData.getInstance().getWishlistArray());
            }
        }

    }

    private void placeorderhomeJSON() {
      /*  final String requestBody = response.toString();
        Log.d("reques34",String.valueOf(response));*/
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringplaceorderhome,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("dsvddscv",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                int orderid = jsonObject.getInt("orderId");
                                Bundle args = new Bundle();
                                args.putBoolean("ibandata",true);
                                args.putString("orderid",""+StaticInfo.order_prefix+"H"+orderid);
                                ((RohahomeDashboradActivity)getActivity()).subchangefragment(new SuccessFragment(),args);
                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue",view);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        sessionManager.volleyerror(error);
                        //sessionManager.snackbarToast(getString(R.string.orderfailed),view);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ContentType","multipart/form-data; boundary=tYhL_Uv6shSE" );
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("qty",""+ LocalData.getInstance().getTotalqty());
                params.put("price",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("branch",""+LocalData.getInstance().getBranchid());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("orderaddress",""+sessionManager.base64convert(LocalData.getInstance().getOrderaddress()));
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("orderlat",""+LocalData.getInstance().getOrderlat());
                params.put("orderlng",""+LocalData.getInstance().getOrderlng());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                params.put("discountamount",""+LocalData.getInstance().getDiscountvalue());
              //  params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());


                Log.d("param",""+params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
               /* params.put("data", ""+response);
                Log.d("param order",""+params);*/
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                //  long imagename = System.currentTimeMillis();
                params.put("ibanimg",imagedata);
                Log.d("param img",""+params);
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }

    private void placeordermeccamedinarequestJSON(JSONArray response) {
        final String requestBody = response.toString();
        Log.d("request body mecca",requestBody);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringplaceorderwater,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("Response1",""+response.toString());
                        Log.d("Response7",""+new String(String.valueOf(response)));
                        Log.d("Response2",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.d("Json",""+obj);
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                int orderid = jsonObject.getInt("orderId");
                                Bundle args = new Bundle();
                                args.putBoolean("ibandata",true);
                                args.putString("orderid",""+StaticInfo.order_prefix+"M"+orderid);
                                ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("TAG", "onResponse: "+e);
                            sessionManager.snackbarToast("Backend Side Issue"+new String(response.data),view);
                            relative.setVisibility(View.GONE);
                            test.setVisibility(View.VISIBLE);
                            test.setText(""+new String(response.data));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        sessionManager.volleyerror(error);
                        //sessionManager.snackbarToast(getString(R.string.orderfailed),view);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ContentType","multipart/form-data; boundary=tYhL_Uv6shSE" );
//                params.put("Accept", "charset=utf-8");
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("totalcarton",""+LocalData.getInstance().getTotalqty());
                params.put("totalprice",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("iswish",""+ sessionManager.getIsWish());
                params.put("unitsqty",""+LocalData.getInstance().getUnitqty());
                params.put("ismecca",""+ LocalData.getInstance().getIsmeccaIntHeaderArray());
                params.put("place",""+ LocalData.getInstance().getIsmeccaStrHeaderArray());
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());
                params.put("appversion","2.7");

                Log.d("Header",""+params);
                return params;
            }



            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("data", requestBody);
                Log.d("param body mecca",""+params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                //  long imagename = System.currentTimeMillis();
                if(imagedata != null) {
                    params.put("ibanimg", imagedata);
                    Log.d("param img1", "" + params);
                }
                else {
                    params.put("ibanimg", new DataPart());
                    Log.d("param img2", "" + params);
                }
                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        HttpStack stack = new MyHurlStack();

        Volley.newRequestQueue(getContext(),stack).add(volleyMultipartRequest);
    }

    private void placeorderiswishrequestJSON(ArrayList<JSONObject> response) {
        final String requestBody = response.toString();
        Log.d("reques34",requestBody);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringplaceorderwater,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("sdsfg","uploading");
                        Log.d("sdsfg",""+response);
                        Log.d("dsvddscv",""+response.toString());
                        Log.d("dsvddscv",""+new String(response.data));
                        try {
                          /*  try {
                                String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                                Log.d("tftf",""+json);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }*/
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.d("sdsfg","uploading"+obj);
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                int orderid = jsonObject.getInt("orderId");
                                Bundle args = new Bundle();
                                args.putBoolean("ibandata",true);
                                args.putString("orderid",""+StaticInfo.order_prefix+orderid);
                                ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),args);

                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue"+new String(response.data),view);
                            relative.setVisibility(View.GONE);
                            test.setVisibility(View.VISIBLE);
                            test.setText(""+new String(response.data));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        sessionManager.volleyerror(error);
                       // sessionManager.snackbarToast(getString(R.string.orderfailed),view);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ContentType","multipart/form-data; boundary=tYhL_Uv6shSE");
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("totalcarton",""+LocalData.getInstance().getTotalqty());
                params.put("totalprice",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("iswish",""+ sessionManager.getIsWish());
                params.put("unitsqty",""+LocalData.getInstance().getUnitqty());
                params.put("place",""+LocalData.getInstance().getWishlistId());
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());
                params.put("appversion","2.7");

                Log.d("param",""+params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("data", ""+requestBody);
                Log.d("param order",""+params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                    // long imagename = System.currentTimeMillis();
                    params.put("ibanimg",imagedata);
                    Log.d("param img",""+params);
                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        HttpStack stack = new MyHurlStack();
        Volley.newRequestQueue(getContext(),stack).add(volleyMultipartRequest);
    }
}