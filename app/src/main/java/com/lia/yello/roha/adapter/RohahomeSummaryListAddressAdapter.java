package com.lia.yello.roha.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.RohaHomeOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.AddressListDataModel;

import java.util.List;

public class RohahomeSummaryListAddressAdapter extends RecyclerView.Adapter<RohahomeSummaryListAddressAdapter.MyViewHolder> {
    private Context mcontext;
    private List<AddressListDataModel> dataSet;
    private RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment;
    public int mSelectedItem = -1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RadioButton location;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.location = (RadioButton) itemView.findViewById(R.id.txt_location);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);
            location.setOnClickListener(clickListener);
        }
    }

    public RohahomeSummaryListAddressAdapter(Context context, List<AddressListDataModel>  data, RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohaHomeOrderSummaryFragment = rohaHomeOrderSummaryFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_listaddress, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        holder.location.setText(dataSet.get(listPosition).getLocation());
        holder.location.setChecked(listPosition == mSelectedItem);

         if(mSelectedItem == -1) {
            if(rohaHomeOrderSummaryFragment.sessionManager.getdaddr().equals(dataSet.get(listPosition).getLocation()) && rohaHomeOrderSummaryFragment.sessionManager.iscurrentlocationselected()==false){
                holder.location.setChecked(true);
                rohaHomeOrderSummaryFragment.clocation.setChecked(false);
                rohaHomeOrderSummaryFragment.sessionManager.setIscurrentlocationselected(false);
            }
        }

      /*

        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rohaHomeOrderSummaryFragment.faddr = dataSet.get(listPosition).getLocation();
                rohaHomeOrderSummaryFragment.flat = dataSet.get(listPosition).getLat();
                rohaHomeOrderSummaryFragment.flng = dataSet.get(listPosition).getLng();
                rohaHomeOrderSummaryFragment.bid = dataSet.get(listPosition).getBid();

                String s = holder.location.getText().toString();
                rohaHomeOrderSummaryFragment.txt_delivery_loc1.setText(""+s);
                rohaHomeOrderSummaryFragment.txt_delivery_loc2.setText(""+s);
                Log.d("location",""+s);
                rohaHomeOrderSummaryFragment.location=dataSet.get(listPosition).getCity();

            }
        });*/

        if(holder.location.isChecked()){
            rohaHomeOrderSummaryFragment.faddr = dataSet.get(listPosition).getLocation();
            rohaHomeOrderSummaryFragment.flat = dataSet.get(listPosition).getLat();
            rohaHomeOrderSummaryFragment.flng = dataSet.get(listPosition).getLng();
            rohaHomeOrderSummaryFragment.bid = dataSet.get(listPosition).getBid();

            String s = holder.location.getText().toString();
            rohaHomeOrderSummaryFragment.txt_delivery_loc1.setText(""+s);
            rohaHomeOrderSummaryFragment.txt_delivery_loc2.setText(""+s);
            Log.d("location",""+s);
            rohaHomeOrderSummaryFragment.fcity =dataSet.get(listPosition).getCity();
            rohaHomeOrderSummaryFragment.clocation.setChecked(false);
            rohaHomeOrderSummaryFragment.sessionManager.setIscurrentlocationselected(false);
        }



    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}