package com.lia.yello.roha.adapter;
import static android.content.ContentValues.TAG;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.Fragment.YourwishFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.model.YourwishDatamodel;
import com.lia.yello.roha.utility.LocalData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
public class YourwishAdapter extends RecyclerView.Adapter<YourwishAdapter.MyViewHolder> {
    private Context mcontext;
    private List<YourwishDatamodel> dataSet;
    private YourwishFragment yourwishActivity;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView img, icon;
        LinearLayout linr;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.name_yourwish);
            this.img = (ImageView) itemView.findViewById(R.id.img_yourwish);
            this.icon = (ImageView) itemView.findViewById(R.id.iconyourwish);
            this.linr = (LinearLayout) itemView.findViewById(R.id.linr_ur_wish_card);
            this.cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }

    public YourwishAdapter(Context context, List<YourwishDatamodel> data, YourwishFragment yourwishActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.yourwishActivity = yourwishActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardyourwishlistnew, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final YourwishDatamodel listData = dataSet.get(position);
        holder.name.setText(listData.getTitle());
        Log.d(TAG, "onBindViewHolder: " + listData.getIcon() + "==" + listData.getImage());
        if (!listData.getIcon().isEmpty() && listData.getIcon() != "" && !listData.getIcon().equals("")) {
            Picasso.get().load(listData.getIcon()).into(holder.icon);
        }
        if (!listData.getImage().isEmpty() && listData.getImage() != "" && !listData.getImage().equals("")) {
            Picasso.get().load(listData.getImage()).into(holder.img);
        }
        holder.linr.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                JSONObject wishlistObject = new JSONObject();
                try {
                    wishlistObject.put("title", listData.getTitle());
                    wishlistObject.put("place", listData.getTitle());
                    wishlistObject.put("id", listData.getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int i = checkWishlistObject(wishlistObject);
                if (i != 0) {
                    Log.d(TAG, "onClick:object " + wishlistObject+i);

                    yourwishActivity.wishlistArray.remove(i-1);
                    holder.cardView.setRadius(10);
                    holder.cardView.setBackground(mcontext.getResources().getDrawable(R.drawable.bg_border_10dp_white));
                    holder.cardView.setBackgroundTintList(ColorStateList.valueOf(mcontext.getResources().getColor(R.color.white)));
                    holder.linr.setBackgroundResource(R.color.white);
                    holder.name.setTextColor(R.color.black);
                } else {
                    holder.cardView.setBackground(mcontext.getResources().getDrawable(R.drawable.bg_border_10dp_white));
                    holder.cardView.setBackgroundTintList(ColorStateList.valueOf(mcontext.getResources().getColor(R.color.yellow)));
                    holder.linr.setBackgroundResource(R.color.yellow);
                    holder.cardView.setRadius(10);
                    holder.name.setTextColor(R.color.black);
                    yourwishActivity.wishlistArray.put(wishlistObject);

                }
                Log.d(TAG, "onClick:wisharray " + yourwishActivity.wishlistArray);

             /* Bundle args = new Bundle();
                args.putString("placename",""+listData.getTitle());
                ((RohaWaterDashboardActivity)yourwishActivity.getActivity()).subchangefragment(new RohaWaterOrderSummaryFragment(), args);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private int checkWishlistObject(JSONObject jsonObject) {
        if (yourwishActivity.wishlistArray != null) {
            for (int i = 0; i < yourwishActivity.wishlistArray.length(); i++) {

                try {
                    if (yourwishActivity.wishlistArray.getJSONObject(i).toString().equals(jsonObject.toString())) {
                        return i + 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

}