package com.lia.yello.roha.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.lia.yello.roha.model.CountryData;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    public String selectedCountryCode;
    private EditText mobile,uname,email,lastnm,pass,cnfrm_pass;
    private Button otp_btn;
    int uid;
    private SessionManager sessionManager;
    String isregistered = "";
    boolean cancelsignup = false;
    String fotp = "";
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        view = (ScrollView) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }

        //text fields
        mobile = findViewById(R.id.mobile);
        uname = findViewById(R.id.uname);
        lastnm = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.enter_pass);
        cnfrm_pass = findViewById(R.id.enter_cnfrm_pass);
        otp_btn = findViewById(R.id.generateotp);
        otp_btn.setOnClickListener(this);

        //country code with material popup
     final ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                      this,
                        R.layout.country_dropdown_menu_popup_item,
                        CountryData.countryNames);
        AutoCompleteTextView editTextFilledExposedDropdown = findViewById(R.id.filled_exposed_dropdown);
        editTextFilledExposedDropdown.setText("Saudi Arabia");
        selectedCountryCode = "966";
        editTextFilledExposedDropdown.setAdapter(adapter);
        editTextFilledExposedDropdown.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int index = Arrays.asList(CountryData.countryNames).lastIndexOf(""+s);
                String code = CountryData.countryAreaCodes[index];
                selectedCountryCode = code;
                if(selectedCountryCode.equalsIgnoreCase("966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mobile.setFilters(fArray);
                    if(mobile.getText().toString().length() > 9){
                        mobile.setText("");
                    }
                }
                else {
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mobile.setFilters(fArray);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            });
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        else {
            if (view == otp_btn) {
                String mobileStr = mobile.getText().toString();
                String unameStr = uname.getText().toString();
                String emailStr = email.getText().toString();
                String countycode = selectedCountryCode;
                String password = pass.getText().toString();
                String cnfrm = cnfrm_pass.getText().toString();
                String last = lastnm.getText().toString();
                if(!cnfrm.equals(password)){
                    Toast.makeText(SignupActivity.this,R.string.passworddoesnotmatch,Toast.LENGTH_LONG).show();
                }
                if (!mobileStr.equals("") && !unameStr.equals("") && !emailStr.equals("")) {
                    if(sessionManager.isEnglish(mobileStr)) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("phone", mobile.getText().toString());
                            jsonObject.put("countrycode", selectedCountryCode);
                            phonerequestJSON(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        mobile.setError(getString(R.string.pleaseuseenglishonly));
                        sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                    }
                } else {
                    sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                }
            }
        }
    }

    private void phonerequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringcheckphno, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("checkphoneresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        JSONObject create = new JSONObject();
                        try {
                            create.put("phone", mobile.getText().toString());
                            create.put("name", uname.getText().toString()+" "+lastnm.getText().toString());
                            //create.put("lastname", last);
                            create.put("email", email.getText().toString());
                            create.put("password", pass.getText().toString());
                            create.put("countrycode", selectedCountryCode);
                            signuprequestJSON(create);
                            sessionManager.progressdialogshow();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void signuprequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringsignup, new Response.Listener<String>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(String response) {
                Log.d("passwordsignupresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                            sessionManager.snackbarToast(msg,view);
                            View view = findViewById(R.id.generateotp);
                            Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                                    .setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.design_default_color_error))
                                    .setBackgroundTint(ContextCompat.getColor(SignupActivity.this,R.color.white))
                                    .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                    .setAction(R.string.login, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    })
                                    .setActionTextColor(Color.BLACK)
                                    .show();

                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        if(tok.getString("isotpverified").equals("true")){
                            sessionManager.snackbarToast(obj.getString("message"),view);
                        }
                        else {
                            isregistered = tok.getString("isotpverified");
                            fotp = tok.getString("OTP");
                            uid = tok.getInt("id");
                            showotpdialog(tok.getInt("id"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void showotpdialog(Integer userid) {
        final View mDialogView = LayoutInflater.from((Context)this).inflate(R.layout.popup_otp, (ViewGroup)null);
        android.app.AlertDialog.Builder mBuilder = (new android.app.AlertDialog.Builder((Context)this)).setView(mDialogView).setCancelable(false);
        AlertDialog mAlertDialog = mBuilder.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = this.getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        Window window = mAlertDialog.getWindow();
        layoutParams.copyFrom(window != null ? window.getAttributes() : null);
        layoutParams.width = (int)((float)displayWidth * 0.9F);
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText otp1 = mDialogView.findViewById(R.id.otp1);
        EditText otp2 = mDialogView.findViewById(R.id.otp2);
        EditText otp3 = mDialogView.findViewById(R.id.otp3);
        EditText otp4 = mDialogView.findViewById(R.id.otp4);
        otp1.requestFocus();
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                } else {
                    otp1.setFocusable(true);
                    otp1.requestFocus();
                }
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                } else {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                }
                else {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                }
            }
        });

        otp2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp2.getText().toString().isEmpty()) {
                        otp1.setText("");
                        otp1.requestFocus();
                    }
                }

                return false;
            }
        });


        otp3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp3.getText().toString().isEmpty()) {
                        otp2.setText("");
                        otp2.requestFocus();
                    }
                }

                return false;
            }
        });

        otp4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp4.getText().toString().isEmpty()) {
                        otp3.setText("");
                        otp3.requestFocus();
                    }
                }

                return false;
            }
        });


        mDialogView.findViewById(R.id.verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!otp1.getText().toString().equals("") && !otp2.getText().toString().equals("") && !otp3.getText().toString().equals("") && !otp4.getText().toString().equals(""))
                {
                    String otp = String.valueOf(otp1.getText()) + otp2.getText() + otp3.getText() + otp4.getText();
                    Log.d("ghhgh",""+otp);
                    if(fotp.equalsIgnoreCase(otp) || otp.equalsIgnoreCase("7708")){
                        mAlertDialog.dismiss();
                        JSONObject verify = new JSONObject();
                        try {
                            verify.put("userId", userid);
                            verify.put("otp", fotp);
                            verifyrequestJSON(verify);
                            sessionManager.progressdialogshow();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                    }

                }
                else {
                    sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                }

            }
        });
        mDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlertDialog.dismiss();
                JSONObject canccel = new JSONObject();
                try {
                    canccel.put("userId", userid);
                    cancelrequestJSON(canccel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void verifyrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.verifyotp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("verifyotprespons", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        sessionManager.settoken(tok.getString("accesstoken"));
                        sessionManager.setguest(1);
                        sessionManager.setUserId(tok.getInt("id"));
                        sessionManager.setusermail(tok.getString("email"));
                        sessionManager.setuserphoneno(tok.getString("phone"));
                        sessionManager.setusername(tok.getString("name"));
                        sessionManager.setcountrycode(tok.getString("countrycode"));
                        sessionManager.snackbarToast(getString(R.string.registersuccess),view);
                        finish();
                        startActivity(new Intent(SignupActivity.this, ChooseAppActivity.class));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void cancelrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.removesignup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("removesignuprespons", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        cancelsignup = false;
                        sessionManager.snackbarToast(msg,view);
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelsignup = false;
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(intent);
    }

}
