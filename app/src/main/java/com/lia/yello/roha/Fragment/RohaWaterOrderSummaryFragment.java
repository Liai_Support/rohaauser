package com.lia.yello.roha.Fragment;
import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.adapter.RohawaterSummaryAdapter1;
import com.lia.yello.roha.adapter.RohawaterSummaryAdapter2;
import com.lia.yello.roha.adapter.YourwishSummaryAdapter;
import com.lia.yello.roha.model.SummaryDataModel1;
import com.lia.yello.roha.model.SummaryDataModel2;
import com.lia.yello.roha.model.WaterproductDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RohaWaterOrderSummaryFragment extends Fragment implements View.OnClickListener{
    View view;
    public  RecyclerView.Adapter adapter2,adapter3;
    public RohawaterSummaryAdapter1 adapter1;
    private RecyclerView.LayoutManager layoutManager;
    public RecyclerView recyclerView1,recyclerView2,recyclerView3;
    public ArrayList<SummaryDataModel1> data1 = new ArrayList<SummaryDataModel1>();
    public ArrayList<SummaryDataModel2> data2 = new ArrayList<SummaryDataModel2>();
    public ArrayList<WaterproductDataModel> data;
    public JSONArray mosquedetarray = new JSONArray();
    public ArrayList<JSONObject> wishlistarray = new ArrayList<JSONObject>();
    public int FinaltotCartqty,meccatotCartqty,medinatotCartqty,jeddahtotCartqty;
    public int discount_id = 0;
    public String discount_type = "";
    public String discount_value = "";
    public Double FinaltotCost;
    public int totcartonqty;
    public Double FinalUnitCost;
    public String unitsqty;
    public Double Finalvatvatvalue;
    public Double totalamount;
    public int mpid;
    public  String promocodee;
    public SessionManager sessionManager;
    Double deliverycharge;
    public ArrayList<String> deletedbranch = new ArrayList<>();
    ImageView back;
    Button next,cancel;
    public ArrayList<LatLng> overallmarkersLatLng = new ArrayList<>();
    public ArrayList<String> overallmarkerstitle = new ArrayList<>();
    public ArrayList<Integer> overallmarkersbranchid = new ArrayList<>();
    public ArrayList<String> overallmarkersbranchname = new ArrayList<>();
    public ArrayList<Integer> overallmarkersbranchstock = new ArrayList<>();

    public ArrayList<LatLng> noverallmarkersLatLng = new ArrayList<>();
    public ArrayList<String> noverallmarkerstitle = new ArrayList<>();
    public ArrayList<Integer> noverallmarkersbranchid = new ArrayList<>();
    public ArrayList<String> noverallmarkersbranchname = new ArrayList<>();
    public ArrayList<Integer> noverallmarkersbranchstock = new ArrayList<>();

    public JSONObject productdata = new JSONObject();
    public TextView subtottext,subtotval,totval,vatvalue,txt_discount,txt_promo_code2,txt_promo_code1,txt_promocode,deliveryvalue,deliverytext;
    public LinearLayout linear_delivery,linear_promocode;
    TextView radio1,radio2;
    public Double totcost,totcostincludedelivery;
    String place;
    ArrayList<Integer> wishlistplace ;
    int placesize;
    Boolean isdeliverycharge = true;
    EditText promocode;
    RelativeLayout relv_promo_code;
    View promo_view;
    TextView apply;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_water_order_summary, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(getActivity());
        }
        linear_promocode=(LinearLayout)view.findViewById(R.id.linr4_cart7);
        promo_view=(View)view.findViewById(R.id.view_cart7);
        txt_promocode = (TextView) view.findViewById(R.id.txt_promocode);
        txt_promo_code2 = (TextView)view.findViewById(R.id.txt_promo_code2);
        apply=(TextView)view.findViewById(R.id.btn_apply);
        promocode=(EditText)view.findViewById(R.id.promocode);
        txt_discount = (TextView) view.findViewById(R.id.txt_discount);
        txt_promo_code1 = (TextView)view.findViewById(R.id.txt_promo_code1);
        relv_promo_code=(RelativeLayout)view.findViewById(R.id.relv_promo_code);
        txt_promo_code1.setOnClickListener(this);
        txt_promo_code2.setOnClickListener(this);
        apply.setOnClickListener(this);
        Log.d(TAG, "onCreateView:IsWish "+sessionManager.getIsWish());

        if(sessionManager.getIsWish() == 1){
            ScrollView scrollView = (ScrollView) view.findViewById(R.id.scr);
            scrollView.setAlpha(1);
            next = (Button) view.findViewById(R.id.next);
            cancel = (Button) view.findViewById(R.id.cancel);
            back = (ImageView) view.findViewById(R.id.back);
            subtottext = (TextView) view.findViewById(R.id.subtottext);
            subtotval = (TextView) view.findViewById(R.id.subtottxtval);
            totval = (TextView) view.findViewById(R.id.tottxtval);
            vatvalue  = (TextView) view.findViewById(R.id.vattxtval);
            deliveryvalue = (TextView) view.findViewById(R.id.deliveryval);
            radio1 = (TextView) view.findViewById(R.id.radio1);
            radio2 = (TextView) view.findViewById(R.id.radio2);
            linear_delivery = (LinearLayout) view.findViewById(R.id.linear_delivery);
            View v1 = (View) view.findViewById(R.id.linear_delivery_view);
            recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler1);
            recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView2 = (RecyclerView) view.findViewById(R.id.recycler2);
            recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));

            cancel.setOnClickListener(this);
            next.setOnClickListener(this);
            back.setOnClickListener(this);
            radio1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    linear_delivery.setVisibility(View.VISIBLE);
                    radio1.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                    radio1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                    radio2.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                    radio2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                    v1.setVisibility(View.VISIBLE);
                    isdeliverycharge = true;
                    calc();
                }
            });
            radio2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    linear_delivery.setVisibility(View.GONE);
                    radio2.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                    radio2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                    radio1.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                    radio1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                    v1.setVisibility(View.GONE);
                    isdeliverycharge = false;
                    calc();
                }
            });



            Bundle args = getArguments();
            overallmarkersLatLng = args.getParcelableArrayList("overallmarkersLatLng");
            overallmarkerstitle = args.getStringArrayList("overallmarkerstitle");
            overallmarkersbranchid = args.getIntegerArrayList("overallmarkersbranchid");
            overallmarkersbranchname = args.getStringArrayList("overallmarkersbranchname");
            overallmarkersbranchstock = args.getIntegerArrayList("overallmarkersbranchstock");
            try {
                productdata = new JSONObject(args.getString("productdata"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

           /* if (sessionManager.getlang()=="ar"|| sessionManager.getlang().equalsIgnoreCase("ar")) {
                deliveryvalue.setGravity(View.FOCUS_LEFT);
                subtotval.setGravity(View.FOCUS_LEFT);
                vatvalue.setGravity(View.FOCUS_LEFT);
                deliveryvalue.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                subtotval.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                vatvalue.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            }*/
            setvalue();
        }
        else if((sessionManager.getIsWish() == 0)){
            txt_promo_code1.setVisibility(View.GONE);
            next = (Button) view.findViewById(R.id.next);
            cancel = (Button) view.findViewById(R.id.cancel);
            back = (ImageView) view.findViewById(R.id.back);
            subtottext = (TextView) view.findViewById(R.id.subtottext);
            subtotval = (TextView) view.findViewById(R.id.subtottxtval);
            totval = (TextView) view.findViewById(R.id.tottxtval);
            vatvalue = (TextView) view.findViewById(R.id.vattxtval);
            linear_delivery = (LinearLayout) view.findViewById(R.id.linear_delivery);
            deliveryvalue = (TextView) view.findViewById(R.id.deliveryval);
            recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler1);
            recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView2 = (RecyclerView) view.findViewById(R.id.recycler2);
            recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView3 = (RecyclerView)view.findViewById(R.id.recycler0);
            recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
            LinearLayoutManager HorizontalLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView3.setLayoutManager(HorizontalLayout);

            back.setOnClickListener(this);
            cancel.setOnClickListener(this);
            next.setOnClickListener(this);
            next.setEnabled(false);
            cancel.setEnabled(false);
            FinaltotCartqty = 0;
            totcartonqty = 0;
            FinaltotCost = 0.0;
            FinalUnitCost = 0.0;
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("branchid", 0);
                productdatarequestJSON(requestbody);
                sessionManager.progressdialogshow();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Bundle args = getArguments();
            place = args.getString("wisharray");
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(getString(R.string.doverselect));


            data1 = new ArrayList<SummaryDataModel1>();
            data2 = new ArrayList<SummaryDataModel2>();
            LatLng d = new LatLng(0.0,0.0);
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(place);
                placesize = jsonArray.length();
                Log.d(TAG, "onCreateView: placesize"+placesize);
            for (int i=0;i < placesize ;i++){
                data1.add(new SummaryDataModel1(jsonArray.getJSONObject(i).getString("place"),d,jsonArray.getJSONObject(i).getInt("id"),"",0,0));

            }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter1 = new RohawaterSummaryAdapter1(getContext(),data1,this);
            recyclerView1.setAdapter(adapter1);

            /*  if (sessionManager.getlang()=="ar"|| sessionManager.getlang().equalsIgnoreCase("ar")) {
                deliveryvalue.setGravity(View.FOCUS_LEFT);
                deliveryvalue.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                subtotval.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                subtotval.setGravity(View.FOCUS_LEFT);
                vatvalue.setGravity(View.FOCUS_LEFT);
                vatvalue.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                //calc();
            }
            else{
               //calc();
            }*/

        }
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        Log.d("hftvhgvh",""+"Detach");
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater")){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }

            if(deletedbranch.size()!=0){
                ((RohaWaterDashboardActivity)getActivity()).rohawaterHomeFragment.overallmarkersLatLng = noverallmarkersLatLng;
                ((RohaWaterDashboardActivity)getActivity()).rohawaterHomeFragment.overallmarkerstitle = noverallmarkerstitle;
                ((RohaWaterDashboardActivity)getActivity()).rohawaterHomeFragment.overallmarkersbranchid = noverallmarkersbranchid;
                ((RohaWaterDashboardActivity)getActivity()).rohawaterHomeFragment.overallmarkersbranchname = noverallmarkersbranchname;
                ((RohaWaterDashboardActivity)getActivity()).rohawaterHomeFragment.overallmarkersbranchstock = noverallmarkersbranchstock;
                ((RohaWaterDashboardActivity)getActivity()).rohawaterHomeFragment.conResume(deletedbranch);
            }
        }
    }

    public void  setvalue(){
        noverallmarkersLatLng = new ArrayList<LatLng>();
        noverallmarkerstitle = new ArrayList<String>();
        noverallmarkersbranchid = new ArrayList<Integer>();
        noverallmarkersbranchname = new ArrayList<String>();
        noverallmarkersbranchstock = new ArrayList<Integer>();
        FinaltotCartqty = 0;
        totcartonqty = 0;
        FinalUnitCost = 0.0;
        FinaltotCost = 0.0;
        mpid = 0;
        unitsqty = "";
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.MECCA)){
                noverallmarkersLatLng.add(overallmarkersLatLng.get(i));
                noverallmarkerstitle.add(overallmarkerstitle.get(i));
                noverallmarkersbranchid.add(overallmarkersbranchid.get(i));
                noverallmarkersbranchname.add(overallmarkersbranchname.get(i));
                noverallmarkersbranchstock.add(overallmarkersbranchstock.get(i));
            }
        }
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.MEDINA)){
                noverallmarkersLatLng.add(overallmarkersLatLng.get(i));
                noverallmarkerstitle.add(overallmarkerstitle.get(i));
                noverallmarkersbranchid.add(overallmarkersbranchid.get(i));
                noverallmarkersbranchname.add(overallmarkersbranchname.get(i));
                noverallmarkersbranchstock.add(overallmarkersbranchstock.get(i));
            }
        }
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.JEDDAH)){
                noverallmarkersLatLng.add(overallmarkersLatLng.get(i));
                noverallmarkerstitle.add(overallmarkerstitle.get(i));
                noverallmarkersbranchid.add(overallmarkersbranchid.get(i));
                noverallmarkersbranchname.add(overallmarkersbranchname.get(i));
                noverallmarkersbranchstock.add(overallmarkersbranchstock.get(i));
            }
        }
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.RIYADH)){
                noverallmarkersLatLng.add(overallmarkersLatLng.get(i));
                noverallmarkerstitle.add(overallmarkerstitle.get(i));
                noverallmarkersbranchid.add(overallmarkersbranchid.get(i));
                noverallmarkersbranchname.add(overallmarkersbranchname.get(i));
                noverallmarkersbranchstock.add(overallmarkersbranchstock.get(i));
            }
        }
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(!overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.MECCA) && !overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.MEDINA) && !overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.JEDDAH) && !overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.RIYADH)){
                noverallmarkersLatLng.add(overallmarkersLatLng.get(i));
                noverallmarkerstitle.add(overallmarkerstitle.get(i));
                noverallmarkersbranchid.add(overallmarkersbranchid.get(i));
                noverallmarkersbranchname.add(overallmarkersbranchname.get(i));
                noverallmarkersbranchstock.add(overallmarkersbranchstock.get(i));
            }
        }
        String branchname = overallmarkersbranchname.get(0);
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(!branchname.equalsIgnoreCase(overallmarkersbranchname.get(i))){
                ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(""+getString(R.string.selectedmosques));
                break;
            }
            else {
                ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(""+sessionManager.getbranchname(overallmarkersbranchid.get(i))+" "+getString(R.string.mosques));
            }
        }

        try {
            totcartonqty = productdata.getInt("cartonTotQty");
           // FinaltotCartqty = meccaproductdata.getInt("cartonTotQty");
            FinalUnitCost = productdata.getDouble("cartonUnitCost");
            FinaltotCost = productdata.getDouble("cartonTotCost");
            mpid = productdata.getInt("mpid");
            unitsqty = productdata.getString("unitsqty");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("brancharray",""+noverallmarkersbranchname);
        Log.d("branchid",""+noverallmarkersbranchid);
        Log.d("branchstock",""+noverallmarkersbranchstock);
        Log.d("producrdata",""+ productdata);

        data1 = new ArrayList<SummaryDataModel1>();
        for (int i = 0; i < noverallmarkerstitle.size(); i++) {
            data1.add(new SummaryDataModel1(noverallmarkerstitle.get(i), noverallmarkersLatLng.get(i), noverallmarkersbranchid.get(i),noverallmarkersbranchname.get(i),noverallmarkersbranchstock.get(i),0));
        }
        adapter1 = new RohawaterSummaryAdapter1(getContext(),data1,this);
        recyclerView1.setAdapter(adapter1);

        // calc();
    }

    public void secondlay(){
        data2 = new ArrayList<SummaryDataModel2>();
        for (int i = 0; i < data1.size(); i++) {
            data2.add(new SummaryDataModel2(data1.get(i).getTitle(),data1.get(i).getQty(),data1.get(i).getQty()*FinalUnitCost,0));
        }
        adapter2 = new RohawaterSummaryAdapter2(getContext(),data2, this);
        recyclerView2.setAdapter(adapter2);

        if(sessionManager.getIsWish() == 1) {
            mosquedetarray = new JSONArray();
            for (int i = 0; i < data1.size(); i++) {
                JSONObject mosquedetobject = new JSONObject();
                try {
                    mosquedetobject.put("lat", data1.get(i).getLatlng().latitude);
                    mosquedetobject.put("lng", data1.get(i).getLatlng().longitude);
                    mosquedetobject.put("ismecca", data1.get(i).getBid());
                    mosquedetobject.put("place", data1.get(i).getBname());
                    mosquedetobject.put("mosquename", data1.get(i).getTitle());
                    mosquedetobject.put("cartcount", data1.get(i).getQty());
                    mosquedetobject.put("productprice", (data1.get(i).getQty() * FinalUnitCost));
                    mosquedetobject.put("units_qty", unitsqty);
                    mosquedetobject.put("receivername", "");
                    mosquedetobject.put("receivermno", "");
                    mosquedetobject.put("receiverdesc", "");
                    mosquedetarray.put(mosquedetobject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.d("xckjvgxfbn",""+mosquedetarray);
        }
        else if (sessionManager.getIsWish() == 0){
            wishlistarray =new ArrayList<JSONObject>();
            wishlistplace = new ArrayList<>(data1.size());
            for (int i = 0; i < data1.size(); i++) {
                JSONObject wishobject = new JSONObject();
                try {
                    wishobject.put("wish_place",sessionManager.base64convert(data1.get(i).getTitle()) );
                    wishobject.put("cartcount", data1.get(i).getQty());
                    wishobject.put("productprice", data1.get(i).getQty() * FinalUnitCost);
                    wishlistplace.add(data1.get(i).getBid());
                    wishlistarray.add(wishobject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.d("xckjvgxfbn",""+wishlistarray+"======"+ wishlistplace);
        }
    }

    public void calc(){
        FinaltotCartqty = 0;
        for(int i=0;i<data1.size();i++){
            FinaltotCartqty = FinaltotCartqty+data1.get(i).getQty();
        }

        if(isdeliverycharge == true){
            String st = getString(R.string.subtotal);
            subtottext.setText(""+st+"(" + FinaltotCartqty + " *" + FinalUnitCost + ")");
            if(StaticInfo.delivery == 0 || StaticInfo.delivery == 0.0){
                linear_delivery.setVisibility(View.GONE);
                deliverycharge = StaticInfo.delivery;
            }
            else {
                deliveryvalue.setText(""+String.format(Locale.ENGLISH,"%.2f", StaticInfo.delivery)+ " SAR");
                deliverycharge = StaticInfo.delivery;
            }
            totcost = FinaltotCartqty * FinalUnitCost;
            totcostincludedelivery = (FinaltotCartqty * FinalUnitCost)+deliverycharge;
            subtotval.setText(""+String.format(Locale.ENGLISH,"%.2f",totcost)+ " SAR");
            double amount = Double.parseDouble(String.valueOf(StaticInfo.vat));
            Finalvatvatvalue = (totcostincludedelivery / 100.0f) * amount;
            vatvalue.setText(""+String.format(Locale.ENGLISH,"%.2f", Finalvatvatvalue)+ " SAR");
            totalamount = totcost + Finalvatvatvalue + StaticInfo.delivery;
            totval.setText(""+getString(R.string.total)+": "+String.format(Locale.ENGLISH,"%.2f", totalamount)+ " SAR");
        }
        else {
            String st = getString(R.string.subtotal);
            subtottext.setText(""+st+"(" + FinaltotCartqty + " *" + FinalUnitCost + ")");
            if(StaticInfo.delivery == 0 || StaticInfo.delivery == 0.0){
                linear_delivery.setVisibility(View.GONE);
                deliverycharge = StaticInfo.delivery;
            }
            else {
                deliveryvalue.setText(""+String.format(Locale.ENGLISH,"%.2f", 0.0)+ " SAR");
                deliverycharge = 0.0;
            }
            totcost = FinaltotCartqty * FinalUnitCost;
            totcostincludedelivery = (FinaltotCartqty * FinalUnitCost)+deliverycharge;
            subtotval.setText(""+String.format(Locale.ENGLISH,"%.2f",totcost)+ " SAR");
            double amount = Double.parseDouble(String.valueOf(StaticInfo.vat));
            Finalvatvatvalue = (totcostincludedelivery / 100.0f) * amount;
            vatvalue.setText(""+String.format(Locale.ENGLISH,"%.2f", Finalvatvatvalue)+ " SAR");
            totalamount = totcostincludedelivery + Finalvatvatvalue + 0;
            totval.setText(""+getString(R.string.total)+": "+String.format(Locale.ENGLISH,"%.2f", totalamount)+ " SAR");
        }

    }

    private void productdatarequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringwaterproductdata, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("productdataresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        Log.d("ghj",""+obj.getString("error"));
                        JSONObject obj1 = new JSONObject(obj.getString("data"));
                        JSONArray data1 = new JSONArray(obj1.getString("product_details"));
                        data = new ArrayList<WaterproductDataModel>();
                        for (int i=0; i<data1.length(); i++){
                            JSONObject jsonObject = data1.getJSONObject(i);
                            WaterproductDataModel datamodel = new WaterproductDataModel(jsonObject.getString("pimage"),jsonObject.getString("product_name"),Integer.parseInt(jsonObject.getString("id")),Integer.parseInt(jsonObject.getString("product_master_id")), Integer.parseInt(jsonObject.getString("totQty")), Double.parseDouble(jsonObject.getString("totCost")), Double.parseDouble(jsonObject.getString("UnitCost")),jsonObject.getInt("stockquantity"));
                            if (Integer.parseInt(jsonObject.getString("totQty")) >= (placesize*10)){
                                data.add(datamodel);

                            }
                        }
                        adapter3 = new YourwishSummaryAdapter(getContext(),data, RohaWaterOrderSummaryFragment.this);
                        recyclerView3.setAdapter(adapter3);
                        adapter1.isClickable = false;
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                }catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    public void dynamiccount(String unitqty,final int mpidd,final Double gettotalcost,final Double getunitcost, final int gettotqty,int stock) {


        ScrollView scrollView = (ScrollView) view.findViewById(R.id.scr);
        scrollView.setAlpha(1);
        next = (Button) view.findViewById(R.id.next);
        cancel = (Button) view.findViewById(R.id.cancel);
        back = (ImageView) view.findViewById(R.id.back);
        subtottext = (TextView) view.findViewById(R.id.subtottext);
        subtotval = (TextView) view.findViewById(R.id.subtottxtval);
        totval = (TextView) view.findViewById(R.id.tottxtval);
        vatvalue = (TextView) view.findViewById(R.id.vattxtval);
        deliveryvalue = (TextView) view.findViewById(R.id.deliveryval);
        radio1 = (TextView) view.findViewById(R.id.radio1);
        radio2 = (TextView) view.findViewById(R.id.radio2);
        linear_delivery = (LinearLayout) view.findViewById(R.id.linear_delivery);
        View v1 = (View) view.findViewById(R.id.linear_delivery_view);
        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler1);
        recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView2 = (RecyclerView) view.findViewById(R.id.recycler2);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));

        back.setOnClickListener(this);
        cancel.setOnClickListener(this);
        next.setOnClickListener(this);
        next.setEnabled(true);
        cancel.setEnabled(true);

        FinaltotCartqty = 0;
        totcartonqty = gettotqty;
        FinaltotCost = gettotalcost;
        FinalUnitCost = getunitcost;
        mpid = mpidd;
        unitsqty = unitqty;
        Log.d("dhvhcd1",""+unitqty);
        Log.d("dhvhcd2",""+mpidd);
        Log.d("dhvhcd3",""+gettotalcost);
        Log.d("dhvhcd4",""+getunitcost);
        Log.d("dhvhcd5",""+gettotqty);
        Log.d("dhvhcd6",""+stock);
        data1 = new ArrayList<SummaryDataModel1>();
        data2 = new ArrayList<SummaryDataModel2>();
        LatLng d = new LatLng(0.0,0.0);
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(place);

            for (int i=0;i < jsonArray.length();i++){
                data1.add(new SummaryDataModel1(jsonArray.getJSONObject(i).getString("place"),d,jsonArray.getJSONObject(i).getInt("id"),"",stock,0));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        data1.add(new SummaryDataModel1(place,d,0,"",stock,0));
        adapter1 = new RohawaterSummaryAdapter1(getContext(),data1, RohaWaterOrderSummaryFragment.this);
        recyclerView1.setAdapter(adapter1);
        adapter1.isClickable = true;

        radio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linear_delivery.setVisibility(View.VISIBLE);
                radio1.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                radio1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                radio2.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                radio2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                v1.setVisibility(View.VISIBLE);
                isdeliverycharge = true;
                calc();
            }
        });
        radio2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linear_delivery.setVisibility(View.GONE);
                radio2.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                radio2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                radio1.setBackground(getResources().getDrawable(R.drawable.bg_border_10dp_white));
                radio1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                v1.setVisibility(View.GONE);
                isdeliverycharge = false;
                calc();
            }
        });

      /*  if (sessionManager.getlang()=="ar"|| sessionManager.getlang().equalsIgnoreCase("ar")) {
            deliveryvalue.setGravity(View.FOCUS_LEFT);
            deliveryvalue.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            subtotval.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            subtotval.setGravity(View.FOCUS_LEFT);
            vatvalue.setGravity(View.FOCUS_LEFT);
            vatvalue.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
           // calc();
        }
        else{
           // calc();
        }*/
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(getActivity());
        }
        else {
            if (view == back) {
               // getActivity().onBackPressed();
                /*Intent intent = new Intent(SummaryActivity.this, HomeActivity.class);
                startActivity(intent);*/
            }
            else if (view == next) {
                if (FinaltotCartqty >= totcartonqty) {
                    if(sessionManager.getIsWish() == 1) {
                        LocalData.getInstance().setBodyarray(mosquedetarray);
                        LocalData.getInstance().setOverallMarkersLatlng(noverallmarkersLatLng);
                        LocalData.getInstance().setOverallMarkersbranchid(noverallmarkersbranchid);
                        LocalData.getInstance().setOverallMarkersbranchname(noverallmarkersbranchname);
                        LocalData.getInstance().setOverallMarkersTitle(noverallmarkerstitle);
                        LocalData.getInstance().setTotalprice(String.format(Locale.ENGLISH,"%.2f", totalamount));
                        LocalData.getInstance().setSubtotal(String.format(Locale.ENGLISH,"%.2f", totcost));
                        LocalData.getInstance().setVat(String.format(Locale.ENGLISH,"%.2f", Finalvatvatvalue));
                        LocalData.getInstance().setDeliverycharge(String.format(Locale.ENGLISH,"%.2f", deliverycharge));
                       // LocalData.getInstance().setPlace(placename);
                        LocalData.getInstance().setIsmeccaIntHeaderArray(removeDuplicates(noverallmarkersbranchid));
                        LocalData.getInstance().setIsmeccaStrHeaderArray(removeDuplicates(noverallmarkersbranchname));
                        LocalData.getInstance().setUnitqty(unitsqty);
                        LocalData.getInstance().setTotalqty(String.valueOf(FinaltotCartqty));
                        LocalData.getInstance().setProductid(String.valueOf(mpid));
                        LocalData.getInstance().setNotes("");
                        LocalData.getInstance().setTimeid("");
                        LocalData.getInstance().setDayid("");
                        LocalData.getInstance().setDiscountid(String.valueOf(discount_id));
                        LocalData.getInstance().setDiscountvalue(String.valueOf(discount_value));
                        LocalData.getInstance().setDiscounttype(String.valueOf(discount_type));

                        Log.d("wertyuiop[","oiuyhtgrewsdfrgthyjuikl");
                        ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new ReceiverDetailsFragment(),null);
                    }
                    else if(sessionManager.getIsWish() == 0){
                        LocalData.getInstance().setTotalprice(String.format(Locale.ENGLISH,"%.2f", totalamount));
                        LocalData.getInstance().setSubtotal(String.format(Locale.ENGLISH,"%.2f", totcost));
                        LocalData.getInstance().setVat(String.format(Locale.ENGLISH,"%.2f", Finalvatvatvalue));
                        LocalData.getInstance().setDeliverycharge(String.format(Locale.ENGLISH,"%.2f", deliverycharge));
                        LocalData.getInstance().setPlace(place);
                        LocalData.getInstance().setWishlistId(wishlistplace);
                        LocalData.getInstance().setWishlistArray(wishlistarray);
//                        LocalData.getInstance().setWishlistplace(Arrays.toString(wishlistplace));
                        LocalData.getInstance().setUnitqty(unitsqty);
                        LocalData.getInstance().setTotalqty(String.valueOf(FinaltotCartqty));
                        LocalData.getInstance().setProductid(String.valueOf(mpid));
                        LocalData.getInstance().setNotes("");
                        LocalData.getInstance().setTimeid("");
                        LocalData.getInstance().setDayid("");
                        LocalData.getInstance().setDiscountid(String.valueOf(discount_id));
                        LocalData.getInstance().setDiscountvalue(String.valueOf(discount_value));
                        LocalData.getInstance().setDiscounttype(String.valueOf(discount_type));


                      /*  Intent intent = new Intent(getActivity(), PaymentActivity1.class);
                        startActivity(intent);*/


                        ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new PaymentFragment(),null);
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.totalCartoncountisbelow)+" "+totcartonqty,view);
                }
            }
            else if(view == cancel){
              //  getActivity().onBackPressed();
                //Fragment fragment= new RohawaterHomeFragment();
              // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                getActivity().getSupportFragmentManager().popBackStack();
               // getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            }
            else if(view == txt_promo_code1){
                txt_promo_code1.setVisibility(View.GONE);
                relv_promo_code.setVisibility(View.VISIBLE);
            }
            else if(view == txt_promo_code2){
                relv_promo_code.setVisibility(View.GONE);
                txt_promo_code1.setVisibility(View.VISIBLE);
            }
            else if(view == apply){
                sessionManager.hideKeyboard();
                if (promocode.getText().toString().equals("")){
                    sessionManager.snackbarToast(getString(R.string.enterpromocode),view);
                }
                else {
                    promocodee = promocode.getText().toString().replaceAll("\\s", "");
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("userid", sessionManager.getUserId());
                        requestbody.put("promocode", promocodee);
                        requestbody.put("offerfor", "water");
                        validoffersJSON(requestbody);
                        sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //setvalue();
    }

    private void validoffersJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.validate_offers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("validoffersdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("data");
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if(jsonObject.getDouble("minimum_offer_amount") <= totalamount) {
                            if(jsonObject.getString("type").equalsIgnoreCase("Carton")) {
                                int value = (int) jsonObject.getDouble("discount");
                                ArrayList<String> dbname = new ArrayList<>();
                                String dbnameStr = "";
                                ArrayList<String> barray = removeDuplicates(noverallmarkersbranchname);
                                ArrayList<Integer> bstock = removeDuplicates1(noverallmarkersbranchname,noverallmarkersbranchstock);
                                int bcount = barray.size();
                                for(int i=0;i<barray.size();i++){
                                    int tvalue = 0;
                                    for(int j=0;j<adapter1.nbranchname.size();j++){
                                        if(adapter1.nbranchname.get(j).equalsIgnoreCase(barray.get(i))){
                                            tvalue = tvalue+adapter1.nvaluecount.get(j);
                                        }
                                    }
                                    if(tvalue+value > bstock.get(i)){
                                        dbname.add(barray.get(i));
                                        dbnameStr = dbnameStr+barray.get(i)+",";
                                    }
                                }

                                if(dbname.size()==0){
                                    sessionManager.snackbarToast(getString(R.string.promocodeapplied), apply);
                                    txt_discount.setText(getString(R.string.offer)+"-"+getString(R.string.eachmosque));
                                    linear_promocode.setVisibility(View.VISIBLE);
                                    promo_view.setVisibility(View.VISIBLE);
                                    relv_promo_code.setVisibility(View.GONE);
                                    txt_promo_code1.setVisibility(View.VISIBLE);
                                    txt_promo_code1.setText(getString(R.string.promocodeapplied));
                                    txt_promo_code1.setEnabled(false);
                                    txt_promocode.setText("" + (int) jsonObject.getDouble("discount") + " - " +getString(R.string.Carton));
                                    // txt_promocode.setText("" + (int) jsonObject.getDouble("discount") + " - " + jsonObject.getString("carton_name"));
                                    apply.setEnabled(false);
                                    adapter1.isClickable = false;
                                    radio1.setEnabled(false);
                                    radio2.setEnabled(false);
                                    discount_id = jsonObject.getInt("id");
                                    discount_type = jsonObject.getString("type");
                                    discount_value = String.valueOf((int)jsonObject.getDouble("discount"));
                                    data2 = new ArrayList<SummaryDataModel2>();
                                    for (int i=0;i<mosquedetarray.length();i++){
                                        JSONObject jsonObject1 = mosquedetarray.getJSONObject(i);
                                        data2.add(new SummaryDataModel2(jsonObject1.getString("mosquename"),Integer.parseInt(jsonObject1.getString("cartcount")),Double.parseDouble(jsonObject1.getString("productprice")),value));
                                        jsonObject1.put("lat",jsonObject1.getString("lat"));
                                        jsonObject1.put("lng",jsonObject1.getString("lng"));
                                        jsonObject1.put("ismecca",jsonObject1.getString("ismecca"));
                                        jsonObject1.put("place",jsonObject1.getString("place"));
                                        jsonObject1.put("mosquename",jsonObject1.getString("mosquename"));
                                        jsonObject1.put("cartcount",Integer.parseInt(jsonObject1.getString("cartcount"))+value);
                                        jsonObject1.put("productprice",jsonObject1.getString("productprice"));
                                        jsonObject1.put("units_qty",jsonObject1.getString("units_qty"));
                                        mosquedetarray.put(i,jsonObject1);
                                        FinaltotCartqty = FinaltotCartqty+value;
                                    }
                                    adapter2 = new RohawaterSummaryAdapter2(getContext(),data2, RohaWaterOrderSummaryFragment.this);
                                    recyclerView2.setAdapter(adapter2);
                                }
                                else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    builder.setMessage("("+dbnameStr+") "+getString(R.string.offeroutofstockpopup))
                                            .setCancelable(true)
                                            .setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    linear_promocode.setVisibility(View.VISIBLE);
                                                    promo_view.setVisibility(View.VISIBLE);
                                                    relv_promo_code.setVisibility(View.GONE);
                                                    txt_promo_code1.setVisibility(View.VISIBLE);
                                                    dialog.dismiss();
                                                }
                                            })
                                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    sessionManager.snackbarToast(getString(R.string.promocodeapplied), apply);
                                                    txt_discount.setText(getString(R.string.offer)+"-"+getString(R.string.eachmosque));
                                                    linear_promocode.setVisibility(View.VISIBLE);
                                                    promo_view.setVisibility(View.VISIBLE);
                                                    relv_promo_code.setVisibility(View.GONE);
                                                    txt_promo_code1.setVisibility(View.VISIBLE);
                                                    txt_promo_code1.setText(getString(R.string.promocodeapplied));
                                                    txt_promo_code1.setEnabled(false);
                                                    try {
                                                        txt_promocode.setText("" + (int) jsonObject.getDouble("discount") + " - " +getString(R.string.Carton));
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    // txt_promocode.setText("" + (int) jsonObject.getDouble("discount") + " - " + jsonObject.getString("carton_name"));
                                                    apply.setEnabled(false);
                                                    adapter1.isClickable = false;
                                                    radio1.setEnabled(false);
                                                    radio2.setEnabled(false);
                                                    try {
                                                        discount_id = jsonObject.getInt("id");
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    data2 = new ArrayList<SummaryDataModel2>();
                                                    for (int j=0;j<dbname.size();j++){
                                                        for (int i=0;i<mosquedetarray.length();i++){
                                                            JSONObject jsonObject1 = null;
                                                            try {
                                                                jsonObject1 = mosquedetarray.getJSONObject(i);
                                                                if(!jsonObject1.getString("branch").equalsIgnoreCase(dbname.get(j))){
                                                                    data2.add(new SummaryDataModel2(jsonObject1.getString("mosquename"),Integer.parseInt(jsonObject1.getString("cartcount")),Double.parseDouble(jsonObject1.getString("productprice")),value));
                                                                    jsonObject1.put("lat",jsonObject1.getString("lat"));
                                                                    jsonObject1.put("lng",jsonObject1.getString("lng"));
                                                                    jsonObject1.put("ismecca",jsonObject1.getString("ismecca"));
                                                                    jsonObject1.put("place",jsonObject1.getString("place"));
                                                                    jsonObject1.put("mosquename",jsonObject1.getString("mosquename"));
                                                                    jsonObject1.put("cartcount",Integer.parseInt(jsonObject1.getString("cartcount"))+value);
                                                                    jsonObject1.put("productprice",jsonObject1.getString("productprice"));
                                                                    jsonObject1.put("units_qty",jsonObject1.getString("units_qty"));
                                                                    mosquedetarray.put(i,jsonObject1);
                                                                    FinaltotCartqty = FinaltotCartqty+value;
                                                                }
                                                                else {
                                                                    data2.add(new SummaryDataModel2(jsonObject1.getString("mosquename"),Integer.parseInt(jsonObject1.getString("cartcount")),Double.parseDouble(jsonObject1.getString("productprice")),0));
                                                                }
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                    adapter2 = new RohawaterSummaryAdapter2(getContext(),data2, RohaWaterOrderSummaryFragment.this);
                                                    recyclerView2.setAdapter(adapter2);
                                                    dialog.dismiss();
                                                }
                                            });
                                    final AlertDialog dialog = builder.create();
                                    dialog.show(); //show() should be called before dialog.getButton().
                                    final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                                    positiveButton.setBackgroundColor(Color.TRANSPARENT);
                                    LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                                    positiveButtonLL.gravity = Gravity.CENTER;
                                    positiveButton.setLayoutParams(positiveButtonLL);
                                }
                                Log.d("hzchcgc",""+mosquedetarray);
                            }
                            else if(jsonObject.getString("type").equalsIgnoreCase("Percentage")){
                                Double sss=Double.parseDouble(jsonObject.getString("discount"));
                                Double dd=(totalamount / sss);
                                txt_promocode.setText(""+String.format(Locale.ENGLISH,"%.2f", dd)+" SAR");
                                sessionManager.snackbarToast(getString(R.string.promocodeapplied), apply);
                                txt_discount.setText(""+getString(R.string.discount));
                                linear_promocode.setVisibility(View.VISIBLE);
                                promo_view.setVisibility(View.VISIBLE);
                                relv_promo_code.setVisibility(View.GONE);
                                txt_promo_code1.setVisibility(View.VISIBLE);
                                txt_promo_code1.setText(getString(R.string.promocodeapplied));
                                txt_promo_code1.setEnabled(false);
                                apply.setEnabled(false);
                                adapter1.isClickable = false;
                                radio1.setEnabled(false);
                                radio2.setEnabled(false);
                                try {
                                    discount_id = jsonObject.getInt("id");
                                    discount_type = jsonObject.getString("type");
                                    discount_value = String.valueOf(sss);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                totalamount = totalamount-dd;
                                totval.setText(""+getString(R.string.total)+": "+String.format(Locale.ENGLISH,"%.2f", totalamount)+ " SAR");
                            }
                            else if(jsonObject.getString("type").equalsIgnoreCase("SAR")){
                                Double s = Double.parseDouble(jsonObject.getString("discount"));
                                txt_promocode.setText(""+String.format(Locale.ENGLISH,"%.2f", s)+" SAR");
                                sessionManager.snackbarToast(getString(R.string.promocodeapplied), apply);
                                txt_discount.setText(""+getString(R.string.discount));
                                linear_promocode.setVisibility(View.VISIBLE);
                                promo_view.setVisibility(View.VISIBLE);
                                relv_promo_code.setVisibility(View.GONE);
                                txt_promo_code1.setVisibility(View.VISIBLE);
                                txt_promo_code1.setText(getString(R.string.promocodeapplied));
                                txt_promo_code1.setEnabled(false);
                                apply.setEnabled(false);
                                adapter1.isClickable = false;
                                radio1.setEnabled(false);
                                radio2.setEnabled(false);
                                try {
                                    discount_id = jsonObject.getInt("id");
                                    discount_type = jsonObject.getString("type");
                                    discount_value = String.valueOf(s);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                totalamount = totalamount-s;
                                totval.setText(""+getString(R.string.total)+": "+String.format(Locale.ENGLISH,"%.2f", totalamount)+ " SAR");
                            }
                        }
                        else {
                            sessionManager.snackbarToast(String.valueOf(getString(R.string.thisofferonlyapplicableorderamountabove)+" "+jsonObject.getDouble("minimum_offer_amount")+" SAR"),apply);
                        }
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    private static class MyItemDecoration extends RecyclerView.ItemDecoration {
        private final int offset;
        public MyItemDecoration(Context context) {
            this.offset = context.getResources().getDimensionPixelSize(R.dimen.card_offset);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(offset, offset, offset, offset);
        }
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        // Create a new ArrayList
        ArrayList<T> newList = new ArrayList<T>();

        // Traverse through the first list
        for (T element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }

    public static <T> ArrayList<Integer> removeDuplicates1(ArrayList<String> list, ArrayList<Integer> list1) {
        // Create a new ArrayList
        ArrayList<String> newList = new ArrayList<String>();
        ArrayList<Integer> newList1 = new ArrayList<Integer>();

        // Traverse through the first list
        for (int i=0;i<list.size();i++) {
            // If this element is not present in newList
            // then add it
            if (!newList.contains(list.get(i))) {
                newList.add(list.get(i));
                newList1.add(list1.get(i));
            }
        }

        // return the new list
        return newList1;
    }

}
















