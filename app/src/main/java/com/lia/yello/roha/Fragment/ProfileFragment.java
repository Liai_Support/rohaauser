package com.lia.yello.roha.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.FirebaseAuth;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.LoginNewActivity;
import com.lia.yello.roha.activity.ResetPasswordActivity;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.utility.VolleyMultipartRequest;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class ProfileFragment extends Fragment  implements View.OnClickListener {

    private SessionManager sessionManager;
    View view;
    public TextView name;
    private final static int IMAGE_RESULT = 200;

    Bitmap bitmap = null;
    private int mYear, mMonth, mDay, mHour, mMinute;

    ImageView profilepic;
    RadioButton male,female;
    Button edit,logout;
    public EditText full_nm, mobile, email, dob, address;
    String gender;
    String currentPhotoPath =  null;
    File currentPhotoFile = null;
    Uri currentPhotoPathFileUri = null;
    LinearLayout addaddress;
    SwitchCompat switchCompat,switchCompat2;
    VolleyMultipartRequest.DataPart imagedata;
    boolean iswater = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(getActivity());
        }
        addaddress = (LinearLayout) view.findViewById(R.id.linear_addadress);

        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.profile);
            ((RohahomeDashboradActivity) getActivity()).botm_nav.setVisibility(View.VISIBLE);
            ((RohahomeDashboradActivity) getActivity()).home_bottom_icon.setVisibility(View.VISIBLE);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        if(((RohahomeDashboradActivity)getActivity()).langfrom.equalsIgnoreCase("profile")){
                            getActivity().finish();
                            Intent intent = new Intent(getActivity(),RohahomeDashboradActivity.class);
                            startActivity(intent);
                        }
                        else {
                            ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                        }
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((RohahomeDashboradActivity)getActivity()).langfrom.equalsIgnoreCase("profile")){
                        getActivity().finish();
                        Intent intent = new Intent(getActivity(),RohahomeDashboradActivity.class);
                        startActivity(intent);
                    }
                    else {
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }
            });
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
            addaddress.setVisibility(View.GONE);
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.profile);
            ((RohaWaterDashboardActivity) getActivity()).botm_nav.setVisibility(View.VISIBLE);
            ((RohaWaterDashboardActivity) getActivity()).home_bottom_icon.setVisibility(View.VISIBLE);
        }

        switchCompat=(SwitchCompat)view.findViewById(R.id.customSwitch1);
        switchCompat2=(SwitchCompat)view.findViewById(R.id.customSwitch2);
        edit = (Button) view.findViewById(R.id.btn_edit);
        profilepic = (ImageView) view.findViewById(R.id.profilepic);
        full_nm = (EditText) view.findViewById(R.id.full_nm);
        email = (EditText) view.findViewById(R.id.email);
        mobile = (EditText) view.findViewById(R.id.mobile_edit);
        dob = (EditText) view.findViewById(R.id.dob);
        address = (EditText) view.findViewById(R.id.address);
        male = (RadioButton) view.findViewById(R.id.male);
        female = (RadioButton) view.findViewById(R.id.female);
        logout = (Button) view.findViewById(R.id.btn_logout);
        isEnabled(false);

        if(sessionManager.getlang().equals("en")){
            switchCompat.setChecked(false);
        }
        else {
            switchCompat.setChecked(true);
        }

        if(sessionManager.isoffernotify() == true){
            switchCompat2.setChecked(true);
        }
        else {
            switchCompat2.setChecked(false);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     sessionManager.setAppLocale("ar");
                     if(sessionManager.getchoose().equals("rohahome")){
                         Intent i = new Intent(getActivity(), RohahomeDashboradActivity.class);
                         i.putExtra("langfrom","profile");
                         startActivity(i);
                         ((Activity) getActivity()).overridePendingTransition(0, 0);
                         ((Activity) getActivity()).finish();
                     }
                     else {
                         Intent i = new Intent(getActivity(), RohaWaterDashboardActivity.class);
                         i.putExtra("langfrom","profile");
                         startActivity(i);
                         ((Activity) getActivity()).overridePendingTransition(0, 0);
                         ((Activity) getActivity()).finish();
                     }
                 }
                 else {
                     sessionManager.setAppLocale("en");
                     if(sessionManager.getchoose().equals("rohahome")){
                         Intent i = new Intent(getActivity(), RohahomeDashboradActivity.class);
                         i.putExtra("langfrom","profile");
                         startActivity(i);
                         ((Activity) getActivity()).overridePendingTransition(0, 0);
                         ((Activity) getActivity()).finish();
                     }
                     else {
                         Intent i = new Intent(getActivity(), RohaWaterDashboardActivity.class);
                         i.putExtra("langfrom","profile");
                         startActivity(i);
                         ((Activity) getActivity()).overridePendingTransition(0, 0);
                         ((Activity) getActivity()).finish();
                     }
                 }
             }
         });
        switchCompat2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sessionManager.setoffernotify(true);
                   /* JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id", session.getUserId());
                        offerupdatestatusrequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }
                else {
                    sessionManager.setoffernotify(false);
                }
            }
        });
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sessionManager.isPermissionsEnabled() == false){
                    sessionManager.permissionsEnableRequest();
                }
                else {
                    showFileChooser1();
                    //showPictureDialog();
                }
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit.getText().equals("Edit") ||  edit.getText().equals("خروج")){
                    edit.setText(R.string.update);
                    isEnabled(true);
                }
                else if(edit.getText().equals("Update") || edit.getText().equals("تحديث")){
                    if(validinput() == false){
                        Toast.makeText(getActivity(), R.string.somefieldareinvalid, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        String namestr = full_nm.getText().toString();
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("id", sessionManager.getUserId());
                            requestbody.put("name", namestr);
                            requestbody.put("email", email.getText().toString());
                            requestbody.put("phone", sessionManager.getuserphoneno());
                            requestbody.put("address", sessionManager.getaddress());
                            requestbody.put("dob", dob.getText().toString());
                            requestbody.put("gender", gender);
                            requestbody.put("countrycode", sessionManager.getcountrycode());
                            JSONArray jsonArray = new JSONArray();
                            jsonArray.put(requestbody);
                            profileupdaterequestJSON(jsonArray);
                            sessionManager.progressdialogshow();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
               // Toast.makeText(getActivity(), "First Fragment", Toast.LENGTH_LONG).show();
            }
        });
      /*  edit.setLongClickable(true);
        edit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent1= new Intent(getActivity(), ResetPasswordActivity.class);
                startActivity(intent1);
                return false;
            }
        });*/
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male.setChecked(true);
                female.setChecked(false);
                gender = "male";
            }
        });
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male.setChecked(false);
                female.setChecked(true);
                gender = "female";
            }
        });
        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((RohahomeDashboradActivity)getActivity()).mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                ((RohahomeDashboradActivity)getActivity()).txt_delv_add.setVisibility(View.VISIBLE);
                ((RohahomeDashboradActivity)getActivity()).txt_loc.setVisibility(View.VISIBLE);
                ((RohahomeDashboradActivity)getActivity()).profile_txt.setVisibility(View.GONE);
                Bundle args = new Bundle();
                args.putBoolean("update",false);
                ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddaddressMapFragment(),args);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  // Configure Google Sign In
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken("1003591081634-vk2lvdju36917u227ohjnvg6vfetdkmq.apps.googleusercontent.com")
                        .requestEmail()
                        .build();
                // Build a GoogleSignInClient with the options specified by gso.
                mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
                mGoogleSignInClient.revokeAccess();*/
                FirebaseAuth.getInstance().signOut();
                sessionManager.settoken("");
                sessionManager.setusername("");
                sessionManager.setusermail("");
                sessionManager.setUserId(0);
                sessionManager.setguest(0);
                sessionManager.setuserphoneno("");
                getActivity().finish();
                Intent intent = new Intent(getActivity(), LoginNewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        viewprofile();
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater") && iswater==true){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }
        }
    }

    public boolean validinput(){
        if(full_nm.getText().toString().equals("")){
            return false;
        }
        else if(email.getText().toString().equals("")){
            return false;
        }
        return true;
    }

    public void isEnabled(Boolean value){
        profilepic.setEnabled(value);
        full_nm.setEnabled(value);
        email.setEnabled(value);
        mobile.setEnabled(false);
        address.setEnabled(false);
        male.setEnabled(value);
        female.setEnabled(value);
        dob.setEnabled(value);
    }

    public void viewprofile(){
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getUserId());
            profileviewrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void profileviewrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringviewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        sessionManager.setusername(tok.getString("name"));
                        sessionManager.setusermail(tok.getString("email"));
                        sessionManager.setcountrycode(tok.getString("countrycode"));
                        sessionManager.setuserphoneno(tok.getString("phone"));
                        sessionManager.setdob(tok.getString("dob"));
                        sessionManager.setgender(tok.getString("gender"));
                        sessionManager.setprofilepic(tok.getString("image"));
                        sessionManager.setseletedaddressid(tok.getInt("user_address"));
                        if(sessionManager.getchoose().equalsIgnoreCase("rohawater")){
                            address.setText(sessionManager.getcaddr());
                            sessionManager.setaddress(address.getText().toString());
                        }
                        else if(sessionManager.getchoose().equalsIgnoreCase("rohahome")) {
                            if (sessionManager.getseletedaddressid() == 0) {
                                address.setText(sessionManager.getcaddr());
                                sessionManager.setaddress(address.getText().toString());
                            } else {
                                address.setText(sessionManager.getdaddr());
                                sessionManager.setaddress(address.getText().toString());
                            }
                        }

                        full_nm.setText(sessionManager.getusername());
                        email.setText(sessionManager.getusermail());
                        mobile.setText("+"+sessionManager.getcountrycode()+" "+sessionManager.getuserphoneno());
                        if(sessionManager.getgender().equalsIgnoreCase("male")){
                            male.setChecked(true);
                            female.setChecked(false);
                            gender = sessionManager.getgender();
                        }
                        else if(sessionManager.getgender().equalsIgnoreCase("female")){
                            male.setChecked(false);
                            female.setChecked(true);
                            gender = sessionManager.getgender();
                        }
                        dob.setText(sessionManager.getdob());
                        edit.setText(getString(R.string.edit));
                        isEnabled(false);
                        if(sessionManager.getchoose().equals("rohahome")) {
                            Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(profilepic);
                            Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(((RohahomeDashboradActivity)getActivity()).nav_profile_pic);
                        }
                        else {
                            Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(profilepic);
                            Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(((RohaWaterDashboardActivity) getActivity()).nav_profile_pic);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
                sessionManager.progressdialogdismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        /*if (v == profile_img) {
            Intent i = new Intent(getActivity().getApplication(), RohahomeDashboradActivity.class);
            startActivity(i);
        }*/
    }

    private void showFileChooser1() {
        startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT);
    }

    public Intent getPickImageChooserIntent() {
        Uri outputFileUri = null;
        try {
            outputFileUri = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            Log.d("hjcb",""+intent);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
            else {
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, getString(R.string.selectsource));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("vjncv1",""+data+".."+requestCode+".."+resultCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_RESULT) {
                if(data!= null && data.getData() != null) {
                    Uri contentUri = data.getData();
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFilePath = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
                    String RealimageFilePath = getImageFilePath(data);

                    Bitmap image = BitmapFactory.decodeFile(RealimageFilePath);//loading the large bitmap is fine.
                    int w = image.getWidth();//get width
                    int h = image.getHeight();//get height
                    int aspRat = w / h;//get aspect ratio
                    int W = 100;//do whatever you want with width. Fixed, screen size, anything
                    int H = W * aspRat;//set the height based on width and aspect ratio
                    Log.d("xhvcbx",""+w+"=="+h+"=="+W+"=="+H);
                    Bitmap b = Bitmap.createScaledBitmap(image, 1920,1080 , false);//scale the bitmap
                    profilepic.setImageBitmap(b);//set the image view
                    image = null;//save memory on the bitmap called 'image'
                    long imagename = System.currentTimeMillis();
                    imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(b));

                   /* profilepic.setImageURI(contentUri);
                    try {
                        // bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentUri);
                        byte[] byteArray = getBytes(getActivity().getContentResolver().openInputStream(contentUri));
                        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                        long imagename = System.currentTimeMillis();
                        imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(bitmap));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
                else {
                    String filePath = getImageFilePath(data);
                    if (filePath != null) {
                        Log.d("filePath", String.valueOf(filePath));
                        bitmap = BitmapFactory.decodeFile(filePath);
                        profilepic.setImageBitmap(bitmap);
                        long imagename = System.currentTimeMillis();
                        imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(bitmap));
                    }
                }
            }
        }
    }

    private String getImageFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;
        if (isCamera) return currentPhotoPath;
        else  return getPathFromURI(data.getData());
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    //gallery
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
    private String getFileExt(Uri contentUri) {
        ContentResolver c = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }

    //camera
    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        if(image != null){
            currentPhotoFile = image;
            currentPhotoPath = image.getAbsolutePath();
            currentPhotoPathFileUri = FileProvider.getUriForFile(getContext(),
                    "com.lia.yello.roha.android.fileprovider",
                    image);
        }
        return currentPhotoPathFileUri;
    }
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.getActivity().sendBroadcast(mediaScanIntent);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //file manager and files
    @SuppressLint("NewApi")
    public String getPathFromURI(Context context, Uri uri) {
        Log.d("hyfhtf",""+uri);
        String filePath = "";
        String fileId = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = fileId.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String selector = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, selector, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    private void profileupdaterequestJSON(JSONArray response) {
        final String requestBody = response.toString();
        Log.d("reques34",String.valueOf(response));
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringprofileupdate,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("updateprofileresponse1",""+response);
                        Log.d("updateprofileresponse2",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            if(obj.getBoolean("error") == false){
                               viewprofile();
                            }
                            else {
                                viewprofile();
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue",view);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.volleyerror(error);
                        sessionManager.progressdialogdismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer "+ sessionManager.gettoken());
                Log.d("param",""+params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("data", ""+response);
                Log.d("param order",""+params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if(imagedata != null){
                   // long imagename = System.currentTimeMillis();
                    params.put("image", imagedata);
                }
                Log.d("param img",""+params);
                return params;
            }

            /*   @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }*/

        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }

    public void setAppLocale(String localeCode) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            conf.locale = new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_Lang", localeCode);
        editor.apply();
        res.updateConfiguration(conf, dm);
        if(sessionManager.getchoose().equals("rohahome")){
            Intent i = new Intent(getActivity(), RohahomeDashboradActivity.class);
            i.putExtra("langfrom","profile");
            startActivity(i);
            ((Activity) getActivity()).overridePendingTransition(0, 0);
            ((Activity) getActivity()).finish();
        }
        else {
            Intent i = new Intent(getActivity(), RohaWaterDashboardActivity.class);
            i.putExtra("langfrom","profile");
            startActivity(i);
            ((Activity) getActivity()).overridePendingTransition(0, 0);
            ((Activity) getActivity()).finish();
        }

    }

}