package com.lia.yello.roha.adapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.MyOrderFragment;
import com.lia.yello.roha.Fragment.OrderTrackingFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.ListorderDatamodel1;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
public class MyOrdersWaterAdapter1 extends RecyclerView.Adapter<MyOrdersWaterAdapter1.MyViewHolder> {
    private MyOrderFragment myOrderFragment;
    private Context mcontext;
    private JSONArray jsonArray;
    private List<ListorderDatamodel1> dataSet;




    public MyOrdersWaterAdapter1(Context context, ArrayList<ListorderDatamodel1> data, MyOrderFragment myOrderFragment, JSONArray obj1) {
        this.dataSet = data;
        this.mcontext = context;
        this. myOrderFragment= myOrderFragment;
        this.jsonArray = obj1;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product,order_id,amount;
        CardView  card;
        ImageView arraw;
        TextView status;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.card = (CardView) itemView.findViewById(R.id.card_main);
            this.product = (TextView) itemView.findViewById(R.id.myorder_txt_product);
            this.amount = (TextView) itemView.findViewById(R.id.amount);
            this.order_id = (TextView) itemView.findViewById(R.id.myorder_txt_id);
            this.status = (TextView) itemView.findViewById(R.id.status);
            this.arraw = (ImageView) itemView.findViewById(R.id.arraw);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_myorder1, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        if(myOrderFragment.sessionManager.getlang().equalsIgnoreCase("ar")){
            holder.arraw.setRotationY(180);
        }

        holder.amount.setText(""+dataSet.get(listPosition).getTotal_price()+" SAR");
        if(dataSet.get(listPosition).getIswish()==0){
            holder.order_id.setText(""+ StaticInfo.order_prefix+""+dataSet.get(listPosition).getId());
        }
        else {
            holder.order_id.setText(""+ StaticInfo.order_prefix+"M"+dataSet.get(listPosition).getId());
        }

        int pending = 0,hold = 0,reject = 0,assigned = 0,completed = 0,cancelled = 0;
        for(int i=0;i<dataSet.get(listPosition).getDetails().length();i++){
            try {
                JSONObject jsonObject = dataSet.get(listPosition).getDetails().getJSONObject(i);
                if(jsonObject.getString("status").equalsIgnoreCase("pending")) {
                    pending++;
                }
                else if(jsonObject.getString("status").equalsIgnoreCase("hold")){
                    hold++;
                }
                else if(jsonObject.getString("status").equalsIgnoreCase("reject")){
                    reject++;
                }
                else if(jsonObject.getString("status").equalsIgnoreCase("assigned")){
                    assigned++;
                }
                else if(jsonObject.getString("status").equalsIgnoreCase("completed")){
                    completed++;
                }
                else if(jsonObject.getString("status").equalsIgnoreCase("cancelled")){
                    cancelled++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(pending > 0){
            holder.status.setText(""+mcontext.getString(R.string.pending));
            dataSet.get(listPosition).setStatus(mcontext.getString(R.string.pending));
        }
        else if(hold > 0){
            holder.status.setText(""+mcontext.getString(R.string.hold));
            dataSet.get(listPosition).setStatus(mcontext.getString(R.string.hold));
        }
        else if(reject > 0){
            holder.status.setText(""+mcontext.getString(R.string.rejected));
            dataSet.get(listPosition).setStatus(mcontext.getString(R.string.rejected));
        }
        else if(assigned > 0){
            holder.status.setText(""+mcontext.getString(R.string.outfordelivery));
            dataSet.get(listPosition).setStatus(mcontext.getString(R.string.outfordelivery));
        }
        else if(completed > 0){
            holder.status.setText(""+mcontext.getString(R.string.completed));
            dataSet.get(listPosition).setStatus(mcontext.getString(R.string.completed));
        }
        else if(cancelled > 0){
            holder.status.setText(""+mcontext.getString(R.string.cancelled));
            dataSet.get(listPosition).setStatus(mcontext.getString(R.string.cancelled));
        }


        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myOrderFragment.sessionManager.getchoose().equals("rohahome")) {
                    Bundle args = new Bundle();
                    args.putString("type","water");
                    args.putInt("donationid",dataSet.get(listPosition).getId());
                    args.putString("placename", dataSet.get(listPosition).getPlace());
                    args.putInt("iswish",dataSet.get(listPosition).getIswish());
                    args.putString("orderdate",dataSet.get(listPosition).getOrder_date());
                    args.putString("deliverydate", dataSet.get(listPosition).getDelivered_date());
                    args.putDouble("amount",dataSet.get(listPosition).getTotal_price());
                    args.putString("paytype", dataSet.get(listPosition).getPaymenttype());
                    args.putString("status",dataSet.get(listPosition).getStatus());
                    args.putString("arraay",String.valueOf(jsonArray));
                    args.putDouble("subtotal",dataSet.get(listPosition).getSubtotal());
                    args.putDouble("vat",dataSet.get(listPosition).getVat());
                    args.putDouble("delivery_charge",dataSet.get(listPosition).getDelivery_charge());
                    args.putString("order_time",dataSet.get(listPosition).getOrder_time());
                    args.putString("delivered_time",dataSet.get(listPosition).getDelivered_time());
                    args.putString("time_from",dataSet.get(listPosition).getTime_from());
                    args.putString("delivered_date",dataSet.get(listPosition).getDelivered_date());
                    args.putDouble("rating",dataSet.get(listPosition).getRating());
                    args.putString("comment",dataSet.get(listPosition).getComment());
                    args.putString("suggestion",dataSet.get(listPosition).getSuggestion());
                    ((RohahomeDashboradActivity)myOrderFragment.getActivity()).subchangefragment(new OrderTrackingFragment(),args);
                }
                else if(myOrderFragment.sessionManager.getchoose().equals("rohawater")) {
                    Bundle args = new Bundle();
                    args.putString("type","water");
                    args.putInt("donationid",dataSet.get(listPosition).getId());
                    args.putString("placename", dataSet.get(listPosition).getPlace());
                    args.putInt("iswish",dataSet.get(listPosition).getIswish());
                    args.putString("orderdate",dataSet.get(listPosition).getOrder_date());
                    args.putString("deliverydate", dataSet.get(listPosition).getDelivered_date());
                    args.putDouble("amount",dataSet.get(listPosition).getTotal_price());
                    args.putString("paytype", dataSet.get(listPosition).getPaymenttype());
                    args.putString("status",dataSet.get(listPosition).getStatus());
                    args.putString("arraay",String.valueOf(jsonArray));
                    args.putDouble("subtotal",dataSet.get(listPosition).getSubtotal());
                    args.putDouble("vat",dataSet.get(listPosition).getVat());
                    args.putDouble("delivery_charge",dataSet.get(listPosition).getDelivery_charge());
                    args.putString("order_time",dataSet.get(listPosition).getOrder_time());
                    args.putString("delivered_time",dataSet.get(listPosition).getDelivered_time());
                    args.putString("time_from",dataSet.get(listPosition).getTime_from());
                    args.putString("delivered_date",dataSet.get(listPosition).getDelivered_date());
                    args.putDouble("rating",dataSet.get(listPosition).getRating());
                    args.putString("comment",dataSet.get(listPosition).getComment());
                    args.putString("suggestion",dataSet.get(listPosition).getSuggestion());
                    args.putString("full_obj",""+dataSet.get(listPosition).getFull_obj());
                    ((RohaWaterDashboardActivity)myOrderFragment.getActivity()).subchangefragment(new OrderTrackingFragment(),args);
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
















