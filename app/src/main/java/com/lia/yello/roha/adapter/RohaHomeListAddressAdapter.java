package com.lia.yello.roha.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.lia.yello.roha.Fragment.AddAddressFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.AddressListDataModel;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RohaHomeListAddressAdapter extends RecyclerView.Adapter<RohaHomeListAddressAdapter.MyViewHolder> {
    private Context mcontext;
    private List<AddressListDataModel> dataSet;
    private RohahomeDashboradActivity rohahomeDashboradActivity;
    ImageView add,delete;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView addr,type;
        LinearLayout addr_select;
        public MyViewHolder(View itemView) {
            super(itemView);
            addr = (TextView) itemView.findViewById(R.id.address);
            type = (TextView) itemView.findViewById(R.id.type);
            add = (ImageView) itemView.findViewById(R.id.add);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            addr_select = (LinearLayout) itemView.findViewById(R.id.addr_select);

        }
    }

    public RohaHomeListAddressAdapter(Context context, List<AddressListDataModel>  data, RohahomeDashboradActivity rohahomeDashboradActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohahomeDashboradActivity = rohahomeDashboradActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_address, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        holder.addr.setText(dataSet.get(listPosition).getLocation());
        holder.type.setText(dataSet.get(listPosition).getAddresstype());

        holder.addr_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rohahomeDashboradActivity.sessionManager.isbranchAvailable(dataSet.get(listPosition).getCity()) != 0){
                    if(rohahomeDashboradActivity.sessionManager.isCartempty() == true){
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("userid", rohahomeDashboradActivity.sessionManager.getUserId());
                            requestbody.put("addressid", dataSet.get(listPosition).getId());
                            updateaddressidrequestJSON(requestbody,dataSet.get(listPosition).getLocation(),dataSet.get(listPosition).getLat(),dataSet.get(listPosition).getLng(),dataSet.get(listPosition).getBid(),dataSet.get(listPosition).getCity());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        if(rohahomeDashboradActivity.sessionManager.getdbranchid() == dataSet.get(listPosition).getBid()){
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("userid", rohahomeDashboradActivity.sessionManager.getUserId());
                                requestbody.put("addressid", dataSet.get(listPosition).getId());
                                updateaddressidrequestJSON(requestbody,dataSet.get(listPosition).getLocation(),dataSet.get(listPosition).getLat(),dataSet.get(listPosition).getLng(),dataSet.get(listPosition).getBid(),dataSet.get(listPosition).getCity());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(rohahomeDashboradActivity);
                            builder.setTitle(mcontext.getString(R.string.alert));
                            builder.setMessage("Your Current Delivery address is "+rohahomeDashboradActivity.sessionManager.getdcity()+" if you change your cart products will delete automatically are you sure to continue")
                                    .setCancelable(true)
                                    .setNegativeButton(mcontext.getString(R.string.cancel),new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                            dialog.dismiss();
                                        }
                                    })
                                    .setPositiveButton(mcontext.getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things;
                                            JSONObject requestbody = new JSONObject();
                                            try {
                                                requestbody.put("userid",rohahomeDashboradActivity.sessionManager.getUserId());
                                                emptycartrequestJSON(requestbody,dataSet.get(listPosition).getId(),dataSet.get(listPosition).getLocation(),dataSet.get(listPosition).getLat(),dataSet.get(listPosition).getLng(),dataSet.get(listPosition).getBid(),dataSet.get(listPosition).getCity());
                                                dialog.dismiss();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                            final AlertDialog dialog = builder.create();
                            dialog.show(); //show() should be called before dialog.getButton().

                            final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                            positiveButton.setBackgroundColor(Color.TRANSPARENT);
                            //positiveButton.setTextColor(Color.BLACK);
                            LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                            positiveButtonLL.gravity = Gravity.CENTER;
                            positiveButton.setLayoutParams(positiveButtonLL);
                        }
                    }
                }
                else {
                    rohahomeDashboradActivity.noserviceDialog(dataSet.get(listPosition).getCity());
                }
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rohahomeDashboradActivity.mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                Bundle args = new Bundle();
                args.putInt("faid", dataSet.get(listPosition).getId());
                args.putString("faddr", dataSet.get(listPosition).getLocation());
                args.putString("fcity",dataSet.get(listPosition).getCity());
                args.putDouble("flat",dataSet.get(listPosition).getLat());
                args.putDouble("flng", dataSet.get(listPosition).getLng());
                args.putString("fatype", dataSet.get(listPosition).getAddresstype());
                args.putInt("fbranchid", dataSet.get(listPosition).getBid());
                args.putBoolean("update",true);
                rohahomeDashboradActivity.subchangefragment(new AddAddressFragment(), args);
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("adresdcgsg",""+holder.addr.getText());
                Log.d("hfuirufuiffi",""+(rohahomeDashboradActivity.txt_loc.getText().toString()));
            if( dataSet.get(listPosition).getLocation().equals(rohahomeDashboradActivity.txt_loc.getText().toString())){

                Toast.makeText(rohahomeDashboradActivity,R.string.deliverylocationcannotbedeleted,Toast.LENGTH_SHORT).show();

            }
         else{
             JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("userid", rohahomeDashboradActivity.sessionManager.getUserId());
                    requestbody.put("addressid", dataSet.get(listPosition).getId());
                    deleterequestJSON(requestbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    private void emptycartrequestJSON(JSONObject response,int id,String location, double lat, double lng, int bid, String city) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deleteallcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                //rohahomeDashboradActivity.session.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        rohahomeDashboradActivity.listcart();
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("userid", rohahomeDashboradActivity.sessionManager.getUserId());
                            requestbody.put("addressid", id);
                            updateaddressidrequestJSON(requestbody,location,lat,lng,bid,city);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        rohahomeDashboradActivity.sessionManager.snackbarToast(obj.getString("message"),rohahomeDashboradActivity.botm_nav);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
              //  rohahomeDashboradActivity.session.progressdialogdismiss();
                rohahomeDashboradActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    private void deleterequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deleteaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("deletedataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        rohahomeDashboradActivity.listaddress(true);
                        rohahomeDashboradActivity.sessionManager.snackbarToast(obj.getString("message"),delete);
                    } else {
                        rohahomeDashboradActivity.listaddress(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                rohahomeDashboradActivity.sessionManager.volleyerror(error);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void updateaddressidrequestJSON(JSONObject response, String location, double lat, double lng, int bid, String city) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updateaddressid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        rohahomeDashboradActivity.sessionManager.setdaddr(""+location);
                        rohahomeDashboradActivity.sessionManager.setdlat(""+lat);
                        rohahomeDashboradActivity.sessionManager.setdlng(""+lng);
                        rohahomeDashboradActivity.txt_loc.setText(""+location);
                        rohahomeDashboradActivity.branchid = bid;
                        rohahomeDashboradActivity.sessionManager.setdbranchid(bid);
                        rohahomeDashboradActivity.sessionManager.setdcity(""+city);
                        rohahomeDashboradActivity.sessionManager.setIscurrentlocationselected(false);
                        rohahomeDashboradActivity.mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        rohahomeDashboradActivity.homefragmentload();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                rohahomeDashboradActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
               // int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
               // params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
               // Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }
}