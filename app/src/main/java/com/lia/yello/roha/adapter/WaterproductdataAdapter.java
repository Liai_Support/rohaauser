package com.lia.yello.roha.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.Fragment.RohawaterHomeFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.model.WaterproductDataModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class WaterproductdataAdapter extends RecyclerView.Adapter<WaterproductdataAdapter.MyViewHolder> {
    private Context mcontext;
    private List<WaterproductDataModel> dataSet;
    private RohawaterHomeFragment rohawaterHomeFragment;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView unit_cost,name;
        TextView tot_Cost;
        ImageView pimage;
        CardView linear;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.pname);
            this.unit_cost = (TextView) itemView.findViewById(R.id.unitcost);
            this.tot_Cost = (TextView) itemView.findViewById(R.id.tot_Cost);
            this.linear = (CardView) itemView.findViewById(R.id.card);
            this.pimage = (ImageView) itemView.findViewById(R.id.product_image);

        }
    }

    public WaterproductdataAdapter(Context context, List<WaterproductDataModel> data, RohawaterHomeFragment rohawaterDashboardActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohawaterHomeFragment = rohawaterDashboardActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_waterproductdata, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        Glide.with(mcontext).load(dataSet.get(listPosition).getPimage()).placeholder(R.drawable.defaultimg).into(holder.pimage);

        holder.name.setText(""+dataSet.get(listPosition).getPname());
        holder.unit_cost.setText(dataSet.get(listPosition).gettotqty()+" "+ rohawaterHomeFragment.getString(R.string.Carton));
        double y = dataSet.get(listPosition).gettotcost();
        int h = (int) y;
        holder.tot_Cost.setText(String.format(Locale.ENGLISH,"%.2f",y)+" SAR");

        if(dataSet.get(listPosition).getStockquantity() < dataSet.get(listPosition).gettotqty()){
            holder.linear.setAlpha(0.5f);
            holder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int cartTot = rohawaterHomeFragment.overallmarkersLatLng.size() * 10;
                    if (rohawaterHomeFragment.overallmarkersLatLng.size() == 0) {
                        rohawaterHomeFragment.sessionManager.snackbarToast(rohawaterHomeFragment.getString(R.string.chooseatleastonelocationfirst),holder.linear);
                    }
                    else {
                        if (cartTot <= dataSet.get(listPosition).gettotqty()) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("mpid",dataSet.get(listPosition).getMpid());
                                jsonObject.put("cartonTotCost",dataSet.get(listPosition).gettotcost());
                                jsonObject.put("cartonTotQty",dataSet.get(listPosition).gettotqty());
                                jsonObject.put("cartonUnitCost",dataSet.get(listPosition).getunitcost());
                                jsonObject.put("unitsqty",dataSet.get(listPosition).getPname());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            rohawaterHomeFragment.sessionManager.setIsWish(1);
                            Bundle args = new Bundle();
                            args.putParcelableArrayList("overallmarkersLatLng", rohawaterHomeFragment.overallmarkersLatLng);
                            args.putStringArrayList("overallmarkerstitle", rohawaterHomeFragment.overallmarkerstitle);
                            args.putIntegerArrayList("overallmarkersbranchid", rohawaterHomeFragment.overallmarkersbranchid);
                            args.putStringArrayList("overallmarkersbranchname", rohawaterHomeFragment.overallmarkersbranchname);
                            args.putIntegerArrayList("overallmarkersbranchstock", rohawaterHomeFragment.overallmarkersbranchstock);
                            args.putString("productdata",jsonObject.toString());
                            ((RohaWaterDashboardActivity) rohawaterHomeFragment.getActivity()).subchangefragment(new RohaWaterOrderSummaryFragment(),args);
                        }
                        else {
                            rohawaterHomeFragment.sessionManager.snackbarToast(rohawaterHomeFragment.getString(R.string.youcanselectmultiplemosquebutminimum10Cartonpermosque),holder.linear);
                        }
                    }
                }
            });
        }
        else {
            holder.linear.setAlpha(1f);
            holder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int cartTot = rohawaterHomeFragment.overallmarkersLatLng.size() * 10;
                    if (rohawaterHomeFragment.overallmarkersLatLng.size() == 0) {
                        rohawaterHomeFragment.sessionManager.snackbarToast(rohawaterHomeFragment.getString(R.string.chooseatleastonelocationfirst),holder.linear);
                    }
                    else {
                        if (cartTot <= dataSet.get(listPosition).gettotqty()) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("mpid",dataSet.get(listPosition).getMpid());
                                jsonObject.put("cartonTotCost",dataSet.get(listPosition).gettotcost());
                                jsonObject.put("cartonTotQty",dataSet.get(listPosition).gettotqty());
                                jsonObject.put("cartonUnitCost",dataSet.get(listPosition).getunitcost());
                                jsonObject.put("unitsqty",dataSet.get(listPosition).getPname());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            rohawaterHomeFragment.sessionManager.setIsWish(1);
                            Bundle args = new Bundle();
                            args.putParcelableArrayList("overallmarkersLatLng", rohawaterHomeFragment.overallmarkersLatLng);
                            args.putStringArrayList("overallmarkerstitle", rohawaterHomeFragment.overallmarkerstitle);
                            args.putIntegerArrayList("overallmarkersbranchid", rohawaterHomeFragment.overallmarkersbranchid);
                            args.putStringArrayList("overallmarkersbranchname", rohawaterHomeFragment.overallmarkersbranchname);
                            args.putIntegerArrayList("overallmarkersbranchstock", rohawaterHomeFragment.overallmarkersbranchstock);
                            args.putString("productdata",jsonObject.toString());
                            ((RohaWaterDashboardActivity) rohawaterHomeFragment.getActivity()).subchangefragment(new RohaWaterOrderSummaryFragment(),args);
                        }
                        else {
                            rohawaterHomeFragment.sessionManager.snackbarToast(rohawaterHomeFragment.getString(R.string.youcanselectmultiplemosquebutminimum10Cartonpermosque),holder.linear);
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}