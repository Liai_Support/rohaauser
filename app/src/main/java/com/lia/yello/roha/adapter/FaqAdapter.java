package com.lia.yello.roha.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.HelpFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.FaqDataModel;
import com.lia.yello.roha.utility.SessionManager;

import java.util.List;

public class FaqAdapter  extends RecyclerView.Adapter<FaqAdapter.MyViewHolder> {
    private Context mcontext;
    private List<FaqDataModel> dataSet;
    private HelpFragment helpFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView question, answer;
        CardView faqcardview;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.question = (TextView) itemView.findViewById(R.id.question);
            this.answer = (TextView) itemView.findViewById(R.id.answer);
            this.faqcardview = (CardView) itemView.findViewById(R.id.faq_cardview);
        }
    }

    public FaqAdapter(Context context, List<FaqDataModel> data, HelpFragment helpFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.helpFragment = helpFragment;
    }

    @Override
    public FaqAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_faq, parent, false);
        FaqAdapter.MyViewHolder myViewHolder = new FaqAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {

        if (helpFragment.sessionManager.getlang().equals("en")){
            holder.question.setText(dataSet.get(listPosition).getQuestion());
            holder.answer.setText(dataSet.get(listPosition).getAnswer());

            holder.question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.answer.getVisibility() == View.GONE){
                        holder.question.setCompoundDrawablesWithIntrinsicBounds( null, null,mcontext.getDrawable(R.drawable.dropdown_icon), null);
                        // holder.binding.question.setCompoundDrawables(null,null,context.getDrawable(R.drawable.ic_baseline_expand_less_24),null);
                        holder.answer.setVisibility(View.VISIBLE);
                    }
                    else {
                        holder.question.setCompoundDrawablesWithIntrinsicBounds( null, null,mcontext.getDrawable(R.drawable.dropup_icon1), null);
                        holder.answer.setVisibility(View.GONE);
                    }


                }
            });
        }else {
            holder.question.setText(dataSet.get(listPosition).getaQuestion());
            holder.answer.setText(dataSet.get(listPosition).getaAnswer());

            holder.question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.answer.getVisibility() == View.GONE){
                        holder.question.setCompoundDrawablesWithIntrinsicBounds( null, null,mcontext.getDrawable(R.drawable.dropdown_icon), null);
                        // holder.binding.question.setCompoundDrawables(null,null,context.getDrawable(R.drawable.ic_baseline_expand_less_24),null);
                        holder.answer.setVisibility(View.VISIBLE);
                    }
                    else {
                        holder.question.setCompoundDrawablesWithIntrinsicBounds( null, null,mcontext.getDrawable(R.drawable.dropup_icon1), null);
                        holder.answer.setVisibility(View.GONE);
                    }


                }
            });
        }


    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

