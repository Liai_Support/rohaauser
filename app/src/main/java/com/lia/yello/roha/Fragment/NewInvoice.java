package com.lia.yello.roha.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.gkemon.XMLtoPDF.PdfGenerator;
import com.gkemon.XMLtoPDF.PdfGeneratorListener;
import com.gkemon.XMLtoPDF.model.FailureResponse;
import com.gkemon.XMLtoPDF.model.SuccessResponse;


import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.lia.yello.roha.R;
import com.lia.yello.roha.adapter.ItemNameRecyclerAdapter;
import com.lia.yello.roha.model.ListorderDatamodel2;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;
import static android.os.Build.VERSION.SDK_INT;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NewInvoice extends Fragment {

    File pictureFileDir;
    View view;
    Button downloadinvoice;
    LinearLayout iswishlinear,pdfview;
    RecyclerView itemrecycler;
    RelativeLayout cdiscount;


    TextView taxidentificationo,orderid,invoicedate,deliveryfees,subtotal,vat,totalvatcal,address,arname,name;
     TextView itemname,price,quantity,itemvat,grandtotal,coupontotal;

    // declaring width and height
    // for our PDF file.
    int pageHeight = 1120;
    int pagewidth = 792;
    public  RecyclerView.Adapter itemadapter;
    private static ArrayList<ListorderDatamodel2> data;
    // creating a bitmap variable
    // for storing our images
    Bitmap bmp, scaledbmp;

    // constant code for runtime permissions
    private static final int PERMISSION_REQUEST_CODE = 200;
    String file_name_path= "";
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {

            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,

    };
    String fName;
    String orderId;
    ImageView qrcodee;
    public Double coste, vattt,finalcost,totalvatcalculated1,couponTotal,tt2,finavat,unitprice,subtotalincludedelivery;
    public String totalamt,deliveryfeess, subtotall,vatt,vatcollected,date,qrcode,itemnamee;
    public String currentdate,ttquantity,cost,totalprice,iswish;
    String full_obj;
    String filename;
    JSONObject invoiceobj;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_new_invoice, container, false);
        address = (TextView) view.findViewById(R.id.invoiceaddress);
        taxidentificationo = (TextView) view.findViewById(R.id.taxno);
        orderid = (TextView) view.findViewById(R.id.orderidrequest);
        invoicedate = (TextView) view.findViewById(R.id.invoivedatetxt);
        qrcodee = (ImageView) view.findViewById(R.id.qrcodee);
        deliveryfees = (TextView) view.findViewById(R.id.deliveryfees);
        subtotal = (TextView) view.findViewById(R.id.subtotalval);
        coupontotal = (TextView) view.findViewById(R.id.coupondiscountval);
        vat = (TextView) view.findViewById(R.id.vat_value);
        totalvatcal = (TextView) view.findViewById(R.id.totalvatcollected);
        arname = (TextView) view.findViewById(R.id.arnamecompany);
        name = (TextView) view.findViewById(R.id.companyname);
        itemname = (TextView) view.findViewById(R.id.itemname);
        quantity = (TextView) view.findViewById(R.id.quantity);
        price = (TextView) view.findViewById(R.id.price);
        itemvat = (TextView) view.findViewById(R.id.itemvat);
        grandtotal = (TextView) view.findViewById(R.id.grandtotal);
        downloadinvoice = (Button) view.findViewById(R.id.btn_invoice);
        iswishlinear = (LinearLayout) view.findViewById(R.id.iswishlinear);
        pdfview = (LinearLayout) view.findViewById(R.id.pdfview);
        itemrecycler = (RecyclerView) view.findViewById(R.id.itemrecycler);
        cdiscount = (RelativeLayout) view.findViewById(R.id.cdiscount);

        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        scaledbmp = Bitmap.createScaledBitmap(bmp, 140, 140, false);

        fName = String.valueOf(System.currentTimeMillis());
        Bundle args = getArguments();
        if(args != null) {

            full_obj = args.getString("full_obj");

            Log.d("dfghyjuikol",""+full_obj);

        }

            try {
                invoiceobj =new JSONObject(full_obj);


                    orderId = invoiceobj.getString("id");
                    vatt = invoiceobj.getString("vat");
                    subtotall = invoiceobj.getString("subtotal");
                    deliveryfeess = invoiceobj.getString("delivery_charge");
                    qrcode = invoiceobj.getString("qr_code");
                    cost = invoiceobj.getString("total_price");
                    ttquantity = invoiceobj.getString("no_of_carton");
                    orderid.setText("DOVM"+orderId);
                    name.setText(StaticInfo.companyname);
                    arname.setText(StaticInfo.arcomapny);
                    deliveryfees.setText(deliveryfeess);
                    totalvatcal.setText(""+(Math.round(Double.parseDouble(invoiceobj.getString("vat")) * 100.0) / 100.0)+ " SAR");
                    address.setText(StaticInfo.Address);
                    taxidentificationo.setText(StaticInfo.TAXNO);
                    invoicedate.setText(invoiceobj.getString("order_date")+invoiceobj.getString("order_time"));
                    grandtotal.setText(invoiceobj.getString("total_price")+"SAR");
                    unitprice = Double.parseDouble(invoiceobj.getString("unit_cost"));

                JSONArray jsarray = new JSONArray(invoiceobj.getString("details"));
                    if (!invoiceobj.getString("discountvalue").equals("")){
                        cdiscount.setVisibility(View.VISIBLE);
                        couponTotal = (Double.parseDouble(invoiceobj.getString("discountvalue"))*jsarray.length()) * unitprice;
                        coupontotal.setText(""+couponTotal+"SAR");
                    }

                     subtotal.setText(invoiceobj.getString("subtotal")+"SAR");
                    subtotalincludedelivery = Double.valueOf(subtotall) + Double.valueOf(deliveryfeess);
                    Double vatamt = Double.valueOf(vatt) /  subtotalincludedelivery * 100;
                    vat.setText(""+vatamt+" % ");

                    Log.d("qrcode",""+qrcode);
                    if (!qrcode.equals("") || !qrcode.isEmpty()) {
                        Picasso.get().load(qrcode).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(qrcodee);
                    }

                    Log.d("wertyuio",""+vatt);


                    Log.d("mnbv",vatt+subtotall);
                    Log.d("mnbv",""+(vatt+subtotall));
                    totalprice=vatt+subtotall;
                    itemrecycler.setVisibility(View.VISIBLE);
                    iswishlinear.setVisibility(View.GONE);
                    itemrecycler.setLayoutManager(new LinearLayoutManager(getContext()));

                //add ItemDecoration
                itemrecycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));


                    data = new ArrayList<ListorderDatamodel2>();
//                    JSONArray jsarray = new JSONArray(invoiceobj.getString("details"));
                    Log.d("rxdcfvghb",""+jsarray);
                    for (int j=0; j<jsarray.length(); j++){
                        JSONObject jsonObject1 = jsarray.getJSONObject(j);
                        Log.d("sneee",""+jsonObject1);
                        ListorderDatamodel2 datamodel=new ListorderDatamodel2(
                                getContext(),
                                jsonObject1.getString("mosque_name"),
                                jsonObject1.getString("delivered_time"),
                                Integer.parseInt(jsonObject1.getString("cart_count")),
                                jsonObject1.getString("status"),
                                jsonObject1.getString("delivered_date"),
                                jsonObject1.getString("units"),
                                jsonObject1.getString("is_mecca"),
                                jsonObject1.getString("place"),
                                jsonObject1.getString("image_path1"),
                                jsonObject1.getString("image_path2"),
                                jsonObject1.getString("image_path3"),
                                jsonObject1.getString("image_path4"),
                                jsonObject1.getString("discountvalue"),
                                jsonObject1.getString("discounttype"),
                                jsonObject1.getString("unit_cost"));
                        data.add(datamodel);

                        Log.d("fgghj",""+jsonObject1.getString("image_path1"));
                        Log.d("sneee1",""+jsonObject1.getString("mosque_name"));
                        Log.d("sneee2",""+jsonObject1.getString("image_path2"));
                        Log.d("sneee3",""+jsonObject1.getString("image_path3"));
                        Log.d("sneee5",""+jsonObject1.getString("place"));
                        Log.d("sneee4",""+data.size());
                    }

                    Log.d("sdefrgtyhujikl;","kanikashanmugam");
                    Log.d("sdefrgtyhujikl;","kanikashanmugam"+data);
                    itemadapter = new ItemNameRecyclerAdapter(getContext(),data,invoiceobj.getString("iswish"), this);
                    itemrecycler.setAdapter(itemadapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }



/*
        Bundle args = getArguments();
        if(args != null) {

            iswish = args.getString("iswish");

            Log.d("dfghyjuikol",""+iswish);

        }


        if (iswish=="true"){
             itemrecycler.setVisibility(View.GONE);
             iswishlinear.setVisibility(View.VISIBLE);
            orderId = args.getString("orderid");
            vatt = args.getString("vat");
            subtotall = args.getString("subtotal");
            deliveryfeess = args.getString("deliveryfees");
            qrcode = args.getString("qrcode");
            itemnamee = args.getString("itemname");
            cost = args.getString("cost");
            ttquantity = args.getString("quantity");
            Log.d("ertyuio",""+orderId);
            Log.d("ertyuio",""+subtotall);
            Log.d("zxcvb",""+itemname);
            Log.d("asdf",""+quantity);
            Log.d("ertyuio",""+deliveryfeess);
            Log.d("ertyuio",""+qrcode);
            orderid.setText("  "+orderId);
            Log.d("vatttt",""+subtotall);
            Log.d("vattt",""+vatt);
            Log.d("vattt",""+Double.valueOf(subtotall)/Double.valueOf(vatt));
           *//* totalvatcalculated1=Double.valueOf(LocalData.getInstance().getSubtotal());
            tt2=Double.valueOf(LocalData.getInstance().getSubtotal());
            finavat=totalvatcalculated1/tt2;*//*


            totalvatcal.setText(""+String.format(Locale.ENGLISH,"%.2f",Double.valueOf(subtotall)/Double.valueOf(vatt))+ " SAR");
            address.setText(StaticInfo.Address);
            taxidentificationo.setText(StaticInfo.TAXNO);
            subtotal.setText(subtotall);
            vat.setText(vatt +" % ");
            Log.d("wertyuio",""+vatt);
            Log.d("mnbv",""+subtotall);

            Log.d("mnbv",vatt+subtotall);
            Log.d("mnbv",""+(vatt+subtotall));
            totalprice=vatt+subtotall;
            deliveryfees.setText(deliveryfeess);
            arname.setText(StaticInfo.arcomapny);
            name.setText(StaticInfo.companyname);
            Picasso.get().load(qrcode).into(qrcodee);
            quantity.setText(ttquantity);
            price.setText(cost);
            coste= Double.valueOf(subtotall);
            vattt= Double.valueOf(vatt);
            finalcost=coste+vattt;
            totalprice= String.valueOf(finalcost);
            itemvat.setText(totalprice);
            itemname.setText("  "+itemnamee);



        }
        else{


            orderId = args.getString("orderid");
            vatt = args.getString("vat");
            subtotall = args.getString("subtotal");
            deliveryfeess = args.getString("deliveryfees");
            qrcode = args.getString("qrcode");
            cost = args.getString("cost");
            ttquantity = args.getString("quantity");
            itemnamee = args.getString("itemname");
            orderid.setText("  "+orderId);
         *//*   totalvatcalculated1=Double.valueOf(LocalData.getInstance().getSubtotal());
            tt2=Double.valueOf(LocalData.getInstance().getSubtotal());
            finavat=totalvatcalculated1/tt2;*//*


            totalvatcal.setText(""+String.format(Locale.ENGLISH,"%.2f",Double.valueOf(subtotall)/Double.valueOf(vatt))+ " SAR");
            address.setText(StaticInfo.Address);
            taxidentificationo.setText(StaticInfo.TAXNO);
            subtotal.setText(subtotall);
            vat.setText(vatt +" % ");
            Log.d("wertyuio",""+vatt);
            Log.d("mnbv",""+subtotall);

            Log.d("mnbv",vatt+subtotall);
            Log.d("mnbv",""+(vatt+subtotall));
            totalprice=vatt+subtotall;
            deliveryfees.setText(deliveryfeess);
            arname.setText(StaticInfo.arcomapny);
            name.setText(StaticInfo.companyname);


            itemrecycler.setVisibility(View.VISIBLE);
            iswishlinear.setVisibility(View.GONE);
            itemrecycler.setLayoutManager(new LinearLayoutManager(getContext()));

            Log.d("sdefrgtyhujikl;","kanikashanmugam");
            Log.d("sdefrgtyhujikl;","kanikashanmugam"+LocalData.getInstance().getOverallMarkersTitle());
            itemadapter = new ItemNameRecyclerAdapter(getContext(),LocalData.getInstance().getOverallMarkersTitle(), this);
            itemrecycler.setAdapter(itemadapter);
        }*/

        if (SDK_INT >= Build.VERSION_CODES.O) {
            currentdate= String.valueOf(LocalDateTime.now());
            Log.d("sedrftyuiop",""+currentdate);
            String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
//            invoicedate.setText(currentDate +" "+currentTime);


        }
        Log.d("getSubtotal",""+LocalData.getInstance().getSubtotal()+"edrftgyhujikolp");
        Log.d("getSubtotal",""+LocalData.getInstance().getSubtotal());
        Log.d("getVat",""+LocalData.getInstance().getVat());
        Log.d("getDeliverycharge",""+LocalData.getInstance().getDeliverycharge());

      //  StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        //StrictMode.setVmPolicy(builder.build());

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }


        downloadinvoice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                downloadinvoice.setVisibility(View.GONE);
                generatePdf(orderId);

            }
        });



        return view;

    }






    private boolean checkPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
            int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        }
    }


    private void requestPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s",getActivity().getPackageName())));
                startActivityForResult(intent, 2296);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, 2296);
            }
        } else {
            //below android 11
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1100);
        }
    }





    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {

                // after requesting permissions we are showing
                // users a toast message of permission granted.
                boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (writeStorage && readStorage) {
                    Toast.makeText(getContext(), "Permission Granted..", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Permission Denined.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }



    public void generatePdf(String orderId) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        Log.d("rootpath",""+root);
        PdfGenerator.getBuilder()
                .setContext(getContext())
                .fromViewSource()
                .fromView(view)
               // .fromViewIDSource()
               // .fromViewID(R.layout.fragment_new_invoice,getActivity(),R.id.pdfview)
                /* "fromLayoutXML()" takes array of layout resources.
                 * You can also invoke "fromLayoutXMLList()" method here which takes list of layout resources instead of array. */
                .setFileName("DOVM"+orderId+"Invoice")
                /* It is file name */
                .setFolderName(root)
                /* It is folder name. If you set the folder name like this pattern (FolderA/FolderB/FolderC), then
                 * FolderA creates first.Then FolderB inside FolderB and also FolderC inside the FolderB and finally
                 * the pdf file named "Test-PDF.pdf" will be store inside the FolderB. */
                .openPDFafterGeneration(true)
                /* It true then the generated pdf will be shown after generated. */
                .build(new PdfGeneratorListener() {
                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        super.onFailure(failureResponse);
                        /* If pdf is not generated by an error then you will findout the reason behind it
                         * from this FailureResponse. */
                    }
                    @Override
                    public void onStartPDFGeneration() {
                        /*When PDF generation begins to start*/
                        downloadinvoice.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFinishPDFGeneration() {
                        /*When PDF generation is finished*/
                        downloadinvoice.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void showLog(String log) {
                        super.showLog(log);
                        /*It shows logs of events inside the pdf generation process*/
                    }

                    @Override
                    public void onSuccess(SuccessResponse response) {
                        super.onSuccess(response);
                        /* If PDF is generated successfully then you will find SuccessResponse
                         * which holds the PdfDocument,File and path (where generated pdf is stored)*/
                        Log.d(TAG, "onSuccess: "+response);

                    }
                });
    }
}






