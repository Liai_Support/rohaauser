package com.lia.yello.roha.adapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.MyOrderFragment;
import com.lia.yello.roha.Fragment.OrderTrackingFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.HomeorderlistDataModel1;
import com.lia.yello.roha.utility.StaticInfo;

import java.util.ArrayList;
import java.util.List;
public class MyOrdersHomeAdapter1 extends RecyclerView.Adapter<MyOrdersHomeAdapter1.MyViewHolder> {
    private MyOrderFragment myOrderFragment;
    private Context mcontext;
    private List<HomeorderlistDataModel1> dataSet;

    public MyOrdersHomeAdapter1(Context context, ArrayList<HomeorderlistDataModel1> data, MyOrderFragment myOrderFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this. myOrderFragment= myOrderFragment;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product,order_id,amount;
        CardView  card;
        ImageView arraw;
        TextView status;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.card = (CardView) itemView.findViewById(R.id.card_main);
            this.product = (TextView) itemView.findViewById(R.id.myorder_txt_product);
            this.amount = (TextView) itemView.findViewById(R.id.amount);
            this.order_id = (TextView) itemView.findViewById(R.id.myorder_txt_id);
            this.status = (TextView) itemView.findViewById(R.id.status);
            this.arraw = (ImageView) itemView.findViewById(R.id.arraw);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_myorder1, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        if(myOrderFragment.sessionManager.getlang().equalsIgnoreCase("ar")){
            holder.arraw.setRotationY(180);
        }
        holder.order_id.setText(""+StaticInfo.order_prefix+"H"+dataSet.get(listPosition).getId());
        holder.status.setText(""+dataSet.get(listPosition).getStatus());
        holder.amount.setText(""+dataSet.get(listPosition).getAmount()+" SAR");
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myOrderFragment.sessionManager.getchoose().equals("rohahome")){
                    Bundle args = new Bundle();
                    args.putString("type","home");
                    args.putString("comefrom","orderpage");
                    args.putInt("oid", dataSet.get(listPosition).getId());
                    args.putString("odate", dataSet.get(listPosition).getOrder_date());
                    args.putString("ddate", dataSet.get(listPosition).getDeliverydate());
                    args.putDouble("osubtotal",dataSet.get(listPosition).getSubtotal());
                    args.putDouble("ovat", dataSet.get(listPosition).getVat());
                    args.putDouble("oamt", dataSet.get(listPosition).getAmount());
                    args.putDouble("odel",dataSet.get(listPosition).getDelivery_charge());
                    args.putString("odiscount",dataSet.get(listPosition).getDiscountvalue());
                    args.putString("odiscounttype",dataSet.get(listPosition).getDiscounttype());
                    args.putString("ocartonname",dataSet.get(listPosition).getCartonname());
                    args.putInt("ocartoncount",dataSet.get(listPosition).getCartoncount());
                    args.putString("ostatus",dataSet.get(listPosition).getStatus());
                    args.putString("orderarray",dataSet.get(listPosition).getDetails().toString());
                    args.putString("paytype", dataSet.get(listPosition).getPayment_type());
                    args.putString("otime", dataSet.get(listPosition).getOrder_time());
                    args.putString("dtime",dataSet.get(listPosition).getDelivered_time());
                    args.putDouble("rating",dataSet.get(listPosition).getRating());
                    args.putString("comment",dataSet.get(listPosition).getComment());
                    args.putString("suggestion",dataSet.get(listPosition).getSuggestion());
                    ((RohahomeDashboradActivity)myOrderFragment.getActivity()).subchangefragment(new OrderTrackingFragment(),args);
                }
                else if(myOrderFragment.sessionManager.getchoose().equals("rohawater")){
                    Bundle args = new Bundle();
                    args.putString("type","home");
                    args.putString("comefrom","orderpage");
                    args.putInt("oid", dataSet.get(listPosition).getId());
                    args.putString("odate", dataSet.get(listPosition).getOrder_date());
                    args.putString("ddate", dataSet.get(listPosition).getDeliverydate());
                    args.putDouble("osubtotal",dataSet.get(listPosition).getSubtotal());
                    args.putDouble("ovat", dataSet.get(listPosition).getVat());
                    args.putDouble("oamt", dataSet.get(listPosition).getAmount());
                    args.putDouble("odel",dataSet.get(listPosition).getDelivery_charge());
                    args.putString("odiscount",dataSet.get(listPosition).getDiscountvalue());
                    args.putString("odiscounttype",dataSet.get(listPosition).getDiscounttype());
                    args.putString("ocartonname",dataSet.get(listPosition).getCartonname());
                    args.putInt("ocartoncount",dataSet.get(listPosition).getCartoncount());
                    args.putString("ostatus",dataSet.get(listPosition).getStatus());
                    args.putString("orderarray",dataSet.get(listPosition).getDetails().toString());
                    args.putString("paytype", dataSet.get(listPosition).getPayment_type());
                    args.putString("otime", dataSet.get(listPosition).getOrder_time());
                    args.putString("dtime",dataSet.get(listPosition).getDelivered_time());
                    args.putDouble("rating",dataSet.get(listPosition).getRating());
                    args.putString("comment",dataSet.get(listPosition).getComment());
                    args.putString("suggestion",dataSet.get(listPosition).getSuggestion());
                    ((RohaWaterDashboardActivity)myOrderFragment.getActivity()).subchangefragment(new OrderTrackingFragment(),args);
                }

            }
        });
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
















