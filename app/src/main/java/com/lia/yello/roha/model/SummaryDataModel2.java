package com.lia.yello.roha.model;

import com.google.android.gms.maps.model.LatLng;

public class SummaryDataModel2 {

    String name;
    Double price;
    int id,place,count,offercount;

    public SummaryDataModel2(String name,int count,Double price,int offercount) {
        this.name = name;
        this.count = count;
        this.price = price;
        this.offercount = offercount;
    }

    public int getOffercount() {
        return offercount;
    }

    public int getCount(){
        return  count;
    }

    public void setCount(int count){
        this.count = count;
    }

    public Double getPrice(){
        return price;
    }

    public void setPrice(Double price){
        this.price = price;
    }

    public  int getPlace(){
        return place;
    }
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

}
