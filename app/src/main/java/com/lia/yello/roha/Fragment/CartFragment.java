package com.lia.yello.roha.Fragment;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.CartAdapter;
import com.lia.yello.roha.model.CartDataModel;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class CartFragment extends Fragment {

    View view;
    SessionManager sessionManager;
    public RecyclerView recyclerView;
    public double total = 0;
    public double vat = 0;
    public double subtotal = 0;
    public int fqty = 0;
    public ArrayList<CartDataModel> data;
    private static RecyclerView.Adapter adapter;
    public TextView ordersubtotal,totalmount,vat_txt;
    RelativeLayout relative_suborder;
    TextView txtvisiblity,placeorder,cancelorder;
    ImageView emptycart;
    public boolean itemnotavailable = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_cart, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        ordersubtotal = (TextView) view.findViewById(R.id.ordersubtotal);
        totalmount = (TextView) view.findViewById(R.id.totalmount);
        vat_txt = (TextView) view.findViewById(R.id.vat_txt);
        placeorder = (TextView) view.findViewById(R.id.placeorder);
        cancelorder = (TextView) view.findViewById(R.id.cancelorder);
        relative_suborder = (RelativeLayout) view.findViewById(R.id.relative_suborder);
        txtvisiblity = (TextView) view.findViewById(R.id.txtvisiblity);
        emptycart = (ImageView) view.findViewById(R.id.emptycart);
        if(sessionManager.getchoose().equals("rohahome")){
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.cart);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        Log.d("xcx", "backsucess");
                        StaticInfo.cartdatachanged = false;
                        ((RohahomeDashboradActivity)getActivity()).localtolive(new RohahomeHomeFragment());
                    /*Intent i = new Intent(getActivity(), RohahomeDashboradActivity.class);
                    startActivity(i);*/
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StaticInfo.cartdatachanged = false;
                    ((RohahomeDashboradActivity)getActivity()).localtolive(new RohahomeHomeFragment());
                }
            });
        }
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        if(sessionManager.getlocalcart().length()!=0){
            ((RohahomeDashboradActivity)getActivity()).localtolive(CartFragment.this);
        }
        else {
            ((RohahomeDashboradActivity)getActivity()).localtolive(CartFragment.this);
        }

        placeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(itemnotavailable == true){
                    sessionManager.snackbarToast(getString(R.string.pleaseremoveunavailableitem),placeorder);
                }
                else {
                    if(fqty>=10){
                        ((RohahomeDashboradActivity)getActivity()).subchangefragment(new RohaHomeOrderSummaryFragment(),null);
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.minimumquantityis10),placeorder);
                    }
                }
            }
        });
        cancelorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticInfo.cartdatachanged = false;
                ((RohahomeDashboradActivity)getActivity()).localtolive(new RohahomeHomeFragment());
                /*Intent i = new Intent(getActivity(), RohahomeDashboradActivity.class);
                startActivity(i); */
            }
        });
        return  view;
    }

  /*  public void addTask(){
        numOfTasks++;
    }

    public void removeTask(){
        numOfTasks--;
    }

    public void allTasksComplete(){
        if(numOfTasks ==0){
            //do what you want to do if all tasks are finished
        }
    }

    class RequestTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... uri) {
            String responseString = "";
            Log.d("scsdgcdc1","sdcsd");
            return responseString;
        }

        @Override
        protected void onPreExecute()
        {
            addTask(); // adds one to task count.
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            removeTask(); // subtracts one from task count.
            allTasksComplete(); // checks to see if all tasks are done...  task count is 0
        }
    }*/

    public void listcart(){
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            listcartrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void listcartrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        txtvisiblity.setVisibility(View.GONE);
                        emptycart.setVisibility(View.GONE);
                        sessionManager.setIsCartempty(false);
                        recyclerView.setVisibility(View.VISIBLE);
                        JSONObject jsonObject=obj.getJSONObject("data");
                        JSONArray data1 = jsonObject.getJSONArray("cart_data");
                        ((RohahomeDashboradActivity)getActivity()).cartBadge.setVisibility(View.VISIBLE);
                        sessionManager.setCartcount(data1.length());
                        ((RohahomeDashboradActivity)getActivity()).cartcount.setText(""+ sessionManager.getCartcount());
                        Log.d("cfghjkkjhgf",""+data1);
                        data = new ArrayList<CartDataModel>();
                        subtotal = 0;
                        total = 0;
                        vat = 0;
                        fqty = 0;
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject1 = data1.getJSONObject(i);
                            CartDataModel datamodel = new CartDataModel(jsonObject1.getInt("cart_id"),jsonObject1.getInt("product_id"),jsonObject1.getString("product_name"),jsonObject1.getString("product_image"),jsonObject1.getInt("quantity"),jsonObject1.getDouble("cost"),jsonObject1.getInt("isstock"),jsonObject1.getDouble("product_price"),jsonObject1.getInt("stock_quantity"));
                            data.add(datamodel);
                            if(jsonObject1.getInt("isstock") == 1){
                                subtotal = subtotal + jsonObject1.getDouble("cost");
                                fqty = fqty + jsonObject1.getInt("quantity");
                            }
                            else {
                                itemnotavailable = true;
                            }
                        }
                        vat = ((subtotal / 100.0f) * StaticInfo.vat);
                        double totalamt = (subtotal + vat);
                        ordersubtotal.setText("" + String.format(Locale.ENGLISH,"%.2f", subtotal)+" SAR");
                        totalmount.setText("" + String.format(Locale.ENGLISH,"%.2f", totalamt)+" SAR");
                        vat_txt.setText("" + String.format(Locale.ENGLISH,"%.2f", vat)+" SAR");

                        adapter = new CartAdapter(getContext(), data, CartFragment.this,((RohahomeDashboradActivity)getActivity()));
                        recyclerView.setAdapter(adapter);

                        ((RohahomeDashboradActivity)getActivity()).livecarttolocal(data1);
                    }
                    else {
                        recyclerView.setVisibility(View.GONE);
                        relative_suborder.setVisibility(View.GONE);
                        txtvisiblity.setVisibility(View.VISIBLE);
                        emptycart.setVisibility(View.VISIBLE);
                        sessionManager.setIsCartempty(true);
                        sessionManager.setCartcount(0);
                        ((RohahomeDashboradActivity)getActivity()).cartBadge.setVisibility(View.GONE);
                        ((RohahomeDashboradActivity)getActivity()).livecarttolocal(new JSONArray());

                        // session.snackbarToast(obj.getString("message"),recyclerView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

}