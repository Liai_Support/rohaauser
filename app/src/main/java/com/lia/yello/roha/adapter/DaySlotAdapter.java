package com.lia.yello.roha.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.RohaHomeOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.DayDetailsDataModel;
import com.lia.yello.roha.model.TimeDetailsDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DaySlotAdapter extends RecyclerView.Adapter<DaySlotAdapter.MyViewHolder> {
    private Context mcontext;
    private List<DayDetailsDataModel> dataSet;
    private RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment;
    private JSONArray jsonArray;
    public ArrayList<TimeDetailsDataModel> data4;
    public int mSelectedItem = -1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
       public TextView  day1,date;
       public LinearLayout linr_date;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.day1 = (TextView) itemView.findViewById(R.id.day1);
            this.date = (TextView) itemView.findViewById(R.id.date_slot);
            this.linr_date = (LinearLayout) itemView.findViewById(R.id.linr_date_slot);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);
            linr_date.setOnClickListener(clickListener);
        }
    }

    public DaySlotAdapter(Context context, List<DayDetailsDataModel>  data, RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment, JSONArray obj1) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohaHomeOrderSummaryFragment = rohaHomeOrderSummaryFragment;
        this.jsonArray = obj1;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_day_slot, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        holder.day1.setText(dataSet.get(listPosition).getDay());
        holder.date.setText(dataSet.get(listPosition).getDate());
        if(listPosition == mSelectedItem){
            holder.day1.setTextColor(Color.WHITE);
            holder.date.setTextColor(Color.WHITE);
            holder.linr_date.setBackgroundColor(Color.parseColor("#00BC06"));
            rohaHomeOrderSummaryFragment.recycler_time_slot.setVisibility(View.VISIBLE);
        }
        else {
            holder.day1.setTextColor(Color.BLACK);
            holder.date.setTextColor(Color.BLACK);
            holder.linr_date.setBackgroundColor(Color.parseColor("#EFFFF4"));
        }
        holder.linr_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.day1.setTextColor(Color.WHITE);
                holder.date.setTextColor(Color.WHITE);
                // to change background color programitacally
                holder.linr_date.setBackgroundColor(Color.parseColor("#00BC06"));
                rohaHomeOrderSummaryFragment.recycler_time_slot.setVisibility(View.VISIBLE);
                try {
                    JSONArray jarray =jsonArray;
                    data4 = new ArrayList<TimeDetailsDataModel>();
                    for (int i=0; i<jarray.length(); i++){
                        JSONObject jsonObject = jarray.getJSONObject(i);
                        if(jsonObject.getInt("date_id") == dataSet.get(listPosition).getId()){
                            JSONArray jsarray = new JSONArray(jsonObject.getString("details"));
                            for (int j=0; j<jsarray.length(); j++){
                                JSONObject jsonObject2 = jsarray.getJSONObject(j);
                                Log.d("zczx",""+jsonObject2.getString("delivery_from"));
                                TimeDetailsDataModel datamodel1 = new TimeDetailsDataModel(jsonObject2.getInt("time_id"), jsonObject2.getString("delivery_from"), jsonObject2.getString("delivery_to"), jsonObject2.getString("session"));
                                data4.add(datamodel1);
                                rohaHomeOrderSummaryFragment.dayid= jsonObject.getInt("date_id");
                            }
                        }
                    }
                    rohaHomeOrderSummaryFragment.adapter3 = new TimeSlotAdapter(mcontext, data4, rohaHomeOrderSummaryFragment);
                    rohaHomeOrderSummaryFragment.recycler_time_slot.setAdapter(rohaHomeOrderSummaryFragment.adapter3);
                    mSelectedItem = listPosition;
                    notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}