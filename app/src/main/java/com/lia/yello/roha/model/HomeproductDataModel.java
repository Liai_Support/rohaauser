package com.lia.yello.roha.model;

import com.google.android.gms.maps.model.LatLng;

public class HomeproductDataModel {
    int id,stockquantity;
    double price;
    String pname,pimage;

    public HomeproductDataModel(int id,String productname,int stockquantity,double price,String image) {

        this.id = id;
        this.pname = productname;
        this.stockquantity = stockquantity;
        this.price = price;
        this.pimage = image;
    }
    public String getPname() {
        return pname;
    }


    public int getId() {
        return id;
    }

    public int getStockquantity(){
        return stockquantity;
    }

    public double getPrice(){
        return price;
    }

    public String getPimage(){
        return pimage;
    }

}
