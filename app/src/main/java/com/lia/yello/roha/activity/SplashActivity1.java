package com.lia.yello.roha.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONObject;

import java.util.Locale;

public class SplashActivity1 extends AppCompatActivity {
    ImageView imageView;
    private SessionManager sessionManager;
    View view;
    String langcode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_splash);
        imageView = (ImageView) findViewById(R.id.splashGif);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        Glide.with(this).asGif().load(R.drawable.logo_dover_gif).listener(new RequestListener<GifDrawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                resource.setLoopCount(1);
                resource.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
                    @Override
                    public void onAnimationEnd(Drawable drawable) {
                        /*Intent i = new Intent(SplashActivity1.this, FlexibleUpdateActivity.class);
                        startActivity(i);
                        SplashActivity1.this.finish();*/
                        Intent i = new Intent(SplashActivity1.this, LanguageActivity.class);
                        startActivity(i);
                        SplashActivity1.this.finish();
                    }
                });
                return false;
            }
        }).into(imageView);

        sessionManager.setIsVersion(false);
        if (sessionManager.getlang().equals("ar")) {
            sessionManager.setAppLocale("ar");
        } else {
            sessionManager.setAppLocale("en");
        }

        String locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = this.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = this.getResources().getConfiguration().locale.getCountry();
        }

       // Log.d("gcgcv1",""+getIntent().hasExtra("data"));
       // Log.d("gcgcv1",""+getIntent().getStringExtra("data"));
       /* if(getIntent().hasExtra("data")==true && !sessionManager.gettoken().equals("")) {
            if (getIntent().getStringExtra("data").equalsIgnoreCase("water")) {
                finish();
                Intent intent = new Intent(this, RohaWaterDashboardActivity.class);
                intent.putExtra("fromnotification", true);
                intent.putExtra("page",""+getIntent().getStringExtra("data"));
                startActivity(intent);
            }
            else if (getIntent().getStringExtra("data").equalsIgnoreCase("home")) {
                finish();
                Intent intent = new Intent(this, RohahomeDashboradActivity.class);
                intent.putExtra("fromnotification", true);
                intent.putExtra("page",""+getIntent().getStringExtra("data"));
                startActivity(intent);
            }
            else if (getIntent().getStringExtra("data").equalsIgnoreCase("offer")) {
                finish();
                Intent intent = new Intent(this, RohahomeDashboradActivity.class);
                intent.putExtra("fromnotification", true);
                intent.putExtra("page",""+getIntent().getStringExtra("data"));
                startActivity(intent);
            }
        }*/
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        createNotificationChannels();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    private void createNotificationChannels() {
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel chatGroupHangUp = new NotificationChannel(
                    "Notification",
                    "Notification",
                    NotificationManager.IMPORTANCE_HIGH
            );
            chatGroupHangUp.enableVibration(true);
            chatGroupHangUp.setSound(uri,attributes);
            notificationManager.createNotificationChannel(chatGroupHangUp);

           /* Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.driver_notification);  //Here is FILE_NAME is the name of file that you want to play
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel channel_all = new NotificationChannel("Notification_Sound", "Notification_Sound", NotificationManager.IMPORTANCE_HIGH);
            channel_all.enableVibration(true);
            channel_all.setSound(sound, attributes); // This is IMPORTANT
            notificationManager.createNotificationChannel(channel_all);*/
        }

    }

}

