package com.lia.yello.roha.model;

import java.util.ArrayList;

public class DayDetailsDataModel {
    String day,date;
    ArrayList<TimeDetailsDataModel> arrayList;
    int id;
    public DayDetailsDataModel(int id, String day,String date){
        this.id=id;
        this.day=day;
        this.date=date;
        this.arrayList = arrayList;
    }

    public int getId() {
        return id;
    }

    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<TimeDetailsDataModel> getArrayList() {
        return arrayList;
    }
}

