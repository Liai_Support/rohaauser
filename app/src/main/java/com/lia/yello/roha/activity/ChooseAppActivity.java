package com.lia.yello.roha.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.lia.yello.roha.R;
import com.lia.yello.roha.databinding.ActiviyChooseappBinding;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class ChooseAppActivity extends AppCompatActivity {
    private SessionManager sessionManager;
    boolean doubleBackToExitPressedOnce = false;
    View view;
    int rCount = 0;
    ActiviyChooseappBinding activiyChooseappBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activiy_chooseapp);
        activiyChooseappBinding = DataBindingUtil.setContentView(this, R.layout.activiy_chooseapp);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        else {
            this.finish();
            startActivity(new Intent(ChooseAppActivity.this, RohaWaterDashboardActivity.class));
        }
       /* activiyChooseappBinding.btnWater.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                sessionManager.setchoose("rohawater");
                activiyChooseappBinding.btnWater.setEnabled(false);
                if (!sessionManager.gettoken().equals("")){
                    finish();
                    startActivity(new Intent(ChooseAppActivity.this, RohaWaterDashboardActivity.class));
                }
                else {
                    activiyChooseappBinding.btnWater.setEnabled(true);
                    sessionManager.settoken("");
                    Snackbar.make(view, getString(R.string.youaredisablefromadminpleasecontactadmin), Snackbar.LENGTH_LONG)
                            .setTextColor(ContextCompat.getColor(ChooseAppActivity.this,R.color.design_default_color_error))
                            .setBackgroundTint(ContextCompat.getColor(ChooseAppActivity.this,R.color.white))
                            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                            .setActionTextColor(Color.BLACK)
                            .setAction(getString(R.string.ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    FirebaseAuth.getInstance().signOut();
                                    sessionManager.settoken("");
                                    sessionManager.setusername("");
                                    sessionManager.setusermail("");
                                    sessionManager.setUserId(0);
                                    sessionManager.setguest(0);
                                    sessionManager.setuserphoneno("");
                                    Intent intent56 = new Intent(ChooseAppActivity.this, LoginNewActivity.class);
                                    intent56.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent56);
                                }
                            })
                            .show();                }
            }
        });
        activiyChooseappBinding.btnHome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                sessionManager.setchoose("rohahome");
                activiyChooseappBinding.btnHome.setEnabled(false);
                if (!sessionManager.gettoken().equals("")) {
                    if (rCount >= 3) {
                        finish();
                        startActivity(new Intent(ChooseAppActivity.this, RohahomeDashboradActivity.class));
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.clientmsg), view);
                        activiyChooseappBinding.btnHome.setEnabled(true);
                    }
                }
                else {
                    sessionManager.settoken("");
                    activiyChooseappBinding.btnHome.setEnabled(true);
                    Snackbar.make(view, getString(R.string.youaredisablefromadminpleasecontactadmin), Snackbar.LENGTH_LONG)
                            .setTextColor(ContextCompat.getColor(ChooseAppActivity.this,R.color.design_default_color_error))
                            .setBackgroundTint(ContextCompat.getColor(ChooseAppActivity.this,R.color.white))
                            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                            .setActionTextColor(Color.BLACK)
                            .setAction(getString(R.string.ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    FirebaseAuth.getInstance().signOut();
                                    sessionManager.settoken("");
                                    sessionManager.setusername("");
                                    sessionManager.setusermail("");
                                    sessionManager.setUserId(0);
                                    sessionManager.setguest(0);
                                    sessionManager.setuserphoneno("");
                                    Intent intent56 = new Intent(ChooseAppActivity.this, LoginNewActivity.class);
                                    intent56.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent56);
                                }
                            })
                            .show();                }
            }
        });
        if (!sessionManager.gettoken().equals("") && sessionManager.isNetworkEnabled() == true) {
            sessionManager.progressdialogshow();
            FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {
                            if (!task.isSuccessful()) {
                                sessionManager.progressdialogdismiss();
                                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                return;
                            }
                            // Get new FCM registration token
                            String token = task.getResult();
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("userid", sessionManager.getUserId());
                                requestbody.put("token", token);
                                requestbody.put("type", "android");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            updatedevicetoken("" + requestbody);
                        }
                    });
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        //activiyChooseappBinding.btnWater.setEnabled(true);
        //activiyChooseappBinding.btnHome.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //rCount++;
    }

    @Override
    public void onBackPressed() {
       /* if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);*/
    }

    private void updatedevicetoken(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("requesttoken", "" + requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.tokenupdateUrl, response1 -> {
               // sessionManager.progressdialogdismiss();
                Log.d("requesttokenresponse", "" + response1);
                try {
                    JSONObject obj = new JSONObject(response1);
                    JSONObject tok = new JSONObject(obj.getString("data"));
                    if(obj.getBoolean("error") == false){
                        if(tok.getString("status").equalsIgnoreCase("inactive")){
                            sessionManager.settoken("");
                            Snackbar.make(view, getString(R.string.youaredisablefromadminpleasecontactadmin), Snackbar.LENGTH_LONG)
                                    .setTextColor(ContextCompat.getColor(ChooseAppActivity.this,R.color.design_default_color_error))
                                    .setBackgroundTint(ContextCompat.getColor(ChooseAppActivity.this,R.color.white))
                                    .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                    .setActionTextColor(Color.BLACK)
                                    .setAction(getString(R.string.ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            FirebaseAuth.getInstance().signOut();
                                            sessionManager.settoken("");
                                            sessionManager.setusername("");
                                            sessionManager.setusermail("");
                                            sessionManager.setUserId(0);
                                            sessionManager.setguest(0);
                                            sessionManager.setuserphoneno("");
                                            Intent intent56 = new Intent(ChooseAppActivity.this, LoginNewActivity.class);
                                            intent56.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent56);
                                        }
                                    })
                                    .show();
                        }
                        else {

                        }
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),view);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    sessionManager.progressdialogdismiss();
                    Log.e("VOLLEY", error.toString());
                    sessionManager.volleyerror(error);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    int id = sessionManager.getUserId();
                    Map<String, String> params = new HashMap<String, String>();
                    //params.put("Content-Type","application/json" );
                    params.put("Authorization", "Bearer " + sessionManager.gettoken());
                    Log.d("param", "" + params);
                    Log.d("id", "" + id);
                    return params;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
