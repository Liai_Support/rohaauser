package com.lia.yello.roha.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.RohaHomeOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.TimeDetailsDataModel;

import java.util.List;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.MyViewHolder> {
    private Context mcontext;
    private List<TimeDetailsDataModel> dataSet;
    private RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment;
    public int mSelectedItem = -1;



    public class MyViewHolder extends RecyclerView.ViewHolder {
       public RadioButton radio_btn_time;
        TextView  timing1,timing2;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.radio_btn_time = (RadioButton) itemView.findViewById(R.id.radio_btn_time);
            this.timing2 = (TextView) itemView.findViewById(R.id.timing2);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);
            radio_btn_time.setOnClickListener(clickListener);
        }


    }

    public TimeSlotAdapter(Context context, List<TimeDetailsDataModel>  data, RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohaHomeOrderSummaryFragment = rohaHomeOrderSummaryFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_time_slot, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        holder.radio_btn_time.setText(dataSet.get(listPosition).getSession());
        holder.timing2.setText("("+dataSet.get(listPosition).getDelivery_from()+" - "+dataSet.get(listPosition).getDelivery_to()+")");
        holder.radio_btn_time.setChecked(listPosition == mSelectedItem);
        if(holder.radio_btn_time.isChecked()){
            rohaHomeOrderSummaryFragment.timeid=dataSet.get(listPosition).getId();
            Log.d("timeeeeid",""+rohaHomeOrderSummaryFragment.timeid);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}