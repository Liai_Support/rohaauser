package com.lia.yello.roha.model;

public class CartDataModel {
    String pname,pimage;
    int cartid,qty,isstock,productid,stockquantity;
    double cost,productprice;
    public CartDataModel(int cartid,int pid,String pname,String pimage,int qty,double cost,int isstock,double productprice,int stockquantity) {
        this.cartid = cartid;
        this.productid = pid;
        this.pname  =pname;
        this.pimage = pimage;
        this.qty = qty;
        this.isstock = isstock;
        this.cost = cost;
        this.productprice = productprice;
        this.stockquantity = stockquantity;

    }

    public int getStockquantity(){
        return stockquantity;
    }

    public  int getProductid(){
        return productid;
    }

    public int getCartid(){
        return cartid;
    }

    public int getIsstock(){
        return isstock;
    }

    public String getPname(){
        return pname;
    }

    public String getPimage(){
        return pimage;
    }

    public double getCost(){
        return cost;
    }

    public int getQty(){
        return qty;
    }

    public double getProductprice(){
        return productprice;
    }
}
