package com.lia.yello.roha.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.adapter.YourwishAdapter;
import com.lia.yello.roha.model.YourwishDatamodel;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
//import com.paytabs.paytabs_sdk.payment.ui.activities.PayTabActivity;
//import com.paytabs.paytabs_sdk.utils.PaymentParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class YourwishFragment extends Fragment implements View.OnClickListener{
    View view;
    private static RecyclerView.Adapter adapter;
    private GridLayoutManager gridLayoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<YourwishDatamodel> data;
    private SessionManager sessionManager;
    public JSONArray wishlistArray = new JSONArray();

    ImageView back;
    MaterialButton next_btn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_yourwish, container, false);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(getActivity());
        }
        ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(""+getString(R.string.ordertype));
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        next_btn = (MaterialButton) view.findViewById(R.id.next_btn);
        gridLayoutManager =new GridLayoutManager(getContext(),2,GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        yourwishrequestJSON();
        sessionManager.progressdialogshow();
        next_btn.setOnClickListener(this);
        return view;
    }

    private void yourwishrequestJSON( ) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
//        final String requestBody = response.toString();
//        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.URLstringwishlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("yourwishresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        Log.d("ghj",""+obj.getString("error"));
                        JSONArray data1 = new JSONArray(obj.getString("data"));
                        Log.d("dsf",""+data1);
                        Log.d("ds33f",""+data1.length());
                        data = new ArrayList<>();
                        for (int i=0; i<data1.length(); i++){
                            JSONObject jsonObject = data1.getJSONObject(i);
                            Log.d("rghjhj",""+jsonObject.getString("title"));
                            YourwishDatamodel datamodel=new YourwishDatamodel(Integer.parseInt(jsonObject.getString("id")),jsonObject.getString("title"),jsonObject.getString("img_path"),jsonObject.getString("icon"));
                            data.add(datamodel);
                        }
                        adapter = new YourwishAdapter(getContext(),data,YourwishFragment.this);
                        recyclerView.setAdapter(adapter);
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("lang", ""+ sessionManager.getlang());
                Log.d("param",""+params);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if(view == back){
            sessionManager.setIsWish(1);
            getActivity().onBackPressed();
            Log.d("ghvbjb","iswish"+ sessionManager.getIsWish());
            /*Intent intent = new Intent(YourwishActivity.this,HomeActivity.class);
            startActivity(intent);*/
        }else if (view == next_btn){
                   Bundle args = new Bundle();
                args.putString("wisharray",""+wishlistArray);
                ((RohaWaterDashboardActivity)getActivity()).subchangefragment(new RohaWaterOrderSummaryFragment(), args);
        }
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //exitByBackKey();
            //progressDialog.cancel();
            getActivity().moveTaskToBack(false);
            sessionManager.setIsWish(1);
            getActivity().onBackPressed();
            Log.d("ghvbjb","iswish"+ sessionManager.getIsWish());
            return true;
        }
        return super.getActivity().onKeyDown(keyCode, event);
    }


}
















