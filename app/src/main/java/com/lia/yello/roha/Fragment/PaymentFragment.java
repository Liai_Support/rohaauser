package com.lia.yello.roha.Fragment;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.PaymentActivity;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.IbanAdapter;
import com.lia.yello.roha.model.IbanDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.lia.yello.roha.utility.VolleyMultipartRequest;
import com.myfatoorah.sdk.entity.executepayment.MFExecutePaymentRequest;
import com.myfatoorah.sdk.entity.initiatesession.MFInitiateSessionResponse;
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse;
import com.myfatoorah.sdk.utils.MFAPILanguage;
import com.myfatoorah.sdk.views.MFResult;
import com.myfatoorah.sdk.views.MFSDK;
import com.myfatoorah.sdk.views.embeddedpayment.MFPaymentCardView;
import com.telr.mobile.sdk.activity.WebviewActivity;
import com.telr.mobile.sdk.entity.request.payment.Address;
import com.telr.mobile.sdk.entity.request.payment.App;
import com.telr.mobile.sdk.entity.request.payment.Billing;
import com.telr.mobile.sdk.entity.request.payment.MobileRequest;
import com.telr.mobile.sdk.entity.request.payment.Name;
import com.telr.mobile.sdk.entity.request.payment.Tran;
import com.telr.mobile.sdk.entity.response.payment.MobileResponse;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import kotlin.Unit;

public class PaymentFragment extends Fragment implements View.OnClickListener {
    View view;
    LinearLayout onlinepay,iban,stcpay,cash,rohaawallet;
    public SessionManager sessionManager;
    ImageView back;
    public String promocode;
    public boolean payiban,ibanaccountnoselect =  false,cod,wallet;
    public int ibannoid;
    String paytype;
    Button btnproceed,btncancel;
    RecyclerView recyclerView;
    TextView totalshow;
    private static ArrayList<IbanDataModel> data1;
    private MFPaymentCardView mfPaymentView;


    public static final String KEY = StaticInfo.KEY;//"s66Mx#BMN5-djBWj"; //"BwxtF~dq9L#xgWZb";//pQ6nP-7rHt@5WRFv        // TODO: Insert your Key here
    public static final String STORE_ID = StaticInfo.STOREID;//"18503";//"21941";   //15996           // TODO: Insert your Store ID here
    public static final boolean isSecurityEnabled = true;      // Mark false to test on simulator, True to test on actual device and Production
    public static final String EMAIL = "girish.spryox@gmail.com" ;//"?????????"  // TODO fill email  // TODO fill email
    //let EMAIL:String = "girish.spryox@gmail.com" //"?????????"  // TODO fill email
    public JSONArray newarray = new JSONArray() ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_payment, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        back = (ImageView) view.findViewById(R.id.back);
        totalshow = (TextView) view.findViewById(R.id.total_show);
        back.setOnClickListener(this);
        getwalletamount();

        if(sessionManager.getchoose().equals("rohahome")){ ;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.paymentoptions);
        }
        if(sessionManager.getchoose().equals("rohawater")){
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.paymentoptions);
        }



        onlinepay = (LinearLayout) view.findViewById(R.id.online_pay);
        iban = (LinearLayout) view.findViewById(R.id.iban_pay);
        stcpay = (LinearLayout) view.findViewById(R.id.stc_pay);
        cash = (LinearLayout) view.findViewById(R.id.cash);
        rohaawallet = (LinearLayout) view.findViewById(R.id.rohaawallet);
        btnproceed = (Button) view.findViewById(R.id.btn_proceed);
        btncancel = (Button) view.findViewById(R.id.btn_cancel);
        mfPaymentView = view.findViewById(R.id.mfPaymentView);

        btncancel.setOnClickListener(this);
        btnproceed.setOnClickListener(this);
        onlinepay.setOnClickListener(this);
        iban.setOnClickListener(this);
        stcpay.setOnClickListener(this);
        cash.setOnClickListener(this);
        rohaawallet.setOnClickListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyler_iban);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        btnproceed.setVisibility(View.GONE);
        btncancel.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
//        initiateSession();
        Log.d("werrtwer",""+LocalData.getInstance().getSubtotal());
        if(sessionManager.getchoose().equals("rohahome")){
            cash.setVisibility(View.INVISIBLE);
            rohaawallet.setVisibility(View.INVISIBLE);
            stcpay.setVisibility(View.INVISIBLE);
            onlinepay.setVisibility(View.VISIBLE);
            iban.setVisibility(View.VISIBLE);
            totalshow.setText(""+getString(R.string.totalamount)+" : "+LocalData.getInstance().getTotalprice()+" SAR");

            LocalData.getInstance().setPaymentamount(LocalData.getInstance().getTotalprice());
            sessionManager.progressdialogshow();
            ibandatarequestJSON();
        }
        else if(sessionManager.getchoose().equals("rohawater")) {
            cash.setVisibility(View.GONE);
            stcpay.setVisibility(View.GONE);
            rohaawallet.setVisibility(View.VISIBLE);
            onlinepay.setVisibility(View.VISIBLE);
            iban.setVisibility(View.VISIBLE);
            LocalData.getInstance().setPaymentamount(LocalData.getInstance().getTotalprice());

            totalshow.setText(""+getString(R.string.totalamount)+" : "+LocalData.getInstance().getTotalprice()+" SAR");
            sessionManager.progressdialogshow();
            ibandatarequestJSON();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

  /*  @Override
    public void onStart() {
        super.onStart();
        Log.d("payment frag onstart","ygvdvdv");
        Log.d("kjcgxhc","sjcb"+LocalData.getInstance().getTransactionid());
        if(!LocalData.getInstance().getTransactionid().equals("") && LocalData.getInstance().getTransactiontype().equalsIgnoreCase("order")){
            if(LocalData.getInstance().getTransactionid().equals("failure")){
                sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                //sessionManager.snackbarToast(getString(R.string.orderfailedtryagain),view);
                LocalData.getInstance().setTransactionid("");
                LocalData.getInstance().setTransactiontype("");
            }
            else {
                if(sessionManager.getchoose().equals("rohahome")){
                    ((RohahomeDashboradActivity) getActivity()).subchangefragment(new SuccessFragment(),null);
                }
                else if(sessionManager.getchoose().equals("rohawater")){
                    ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),null);
                }
            }
        }
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        Log.d("xcvsxxb",""+requestCode+"\n"+resultCode+"\n"+intent);

        if (requestCode == 105 && resultCode == RESULT_OK && intent != null) {
            String paymentMethod = intent.getStringExtra("auth");
            Log.d("zjhjcvg dc",""+paymentMethod);
            if(paymentMethod.equalsIgnoreCase("yes")){
                MobileResponse status = (MobileResponse) intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
                if (status.getAuth() != null) {
                    if(status.getAuth().getStatus().equalsIgnoreCase("A") || status.getAuth().getStatus().equalsIgnoreCase("H")){
                        if(!status.getAuth().getTranref().equals("") && status.getAuth().getTranref() != null){

                            LocalData.getInstance().setTransactionid(status.getAuth().getTranref());
                            Log.d("kjcgxhc","sjcb"+LocalData.getInstance().getTransactionid());
                            if(!LocalData.getInstance().getTransactionid().equals("")){
                                if(LocalData.getInstance().getTransactionid().equals("failure")){
                                    sessionManager.snackbarToast(getString(R.string.paymentfailed)+"- F"+status.getAuth().getMessage(),view);
                                    //sessionManager.snackbarToast(getString(R.string.orderfailedtryagain),view);
                                    UpdatePlaceOrderBody("failure");
                                    LocalData.getInstance().setTransactionid("");
                                    LocalData.getInstance().setTransactiontype("");
                                }
                                else {
                                    if(sessionManager.getchoose().equals("rohahome")){
                                        Bundle args = new Bundle();
                                        args.putBoolean("ibandata",false);
                                        UpdatePlaceOrderBody("success");
                                        ((RohahomeDashboradActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                                    }
                                    else if(sessionManager.getchoose().equals("rohawater")){
                                        Bundle args = new Bundle();
                                        args.putBoolean("ibandata",false);
                                        UpdatePlaceOrderBody("success");
                                        ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                                    }                                }
                            }
                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.paymentfailed)+"-"+status.getAuth().getMessage(),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
                }
            }
            else {
                StatusResponse status = (StatusResponse) intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
                if (status.getAuth() != null) {
                    if(status.getAuth().getStatus().equalsIgnoreCase("A") || status.getAuth().getStatus().equalsIgnoreCase("H")){
                        if(!status.getAuth().getTranref().equals("") && status.getAuth().getTranref() != null){
                            LocalData.getInstance().setTransactionid(status.getAuth().getTranref());
                            Log.d("kjcgxhc","sjcb"+LocalData.getInstance().getTransactionid());
                            if(!LocalData.getInstance().getTransactionid().equals("") ){
                                if(LocalData.getInstance().getTransactionid().equals("failure")){
                                    sessionManager.snackbarToast(getString(R.string.paymentfailed)+"- F"+status.getAuth().getMessage(),view);
                                    //sessionManager.snackbarToast(getString(R.string.orderfailedtryagain),view);
                                    UpdatePlaceOrderBody("failure");
                                    LocalData.getInstance().setTransactionid("");
                                    LocalData.getInstance().setTransactiontype("");
                                }
                                else {
                                    if(sessionManager.getchoose().equals("rohahome")){
                                        Bundle args = new Bundle();
                                        args.putBoolean("ibandata",false);
                                        UpdatePlaceOrderBody("success");
                                        ((RohahomeDashboradActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                                    }
                                    else if(sessionManager.getchoose().equals("rohawater")){
                                        Bundle args = new Bundle();
                                        args.putBoolean("ibandata",false);
                                        UpdatePlaceOrderBody("success");
                                        ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                                    }
                                }
                            }
                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.paymentfailed)+"-"+status.getAuth().getMessage(),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
                }
            }

           /* Intent intent1 = new Intent(getActivity(), SuccessTransationActivity.class);
            intent1.putExtras(intent);
            startActivity(intent1);*/

        }
        else {
             sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
        }
    }


    public void getwalletamount(){
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            getwalletamountJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getwalletamountJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.getwallet, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("walletresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        Log.d("walleterror",""+msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        tok.getString("userid");
                        tok.getString("amount");
                        // show_amt.setText(String.format("%.2f",tok.getDouble("amount"))+" SAR");
                        sessionManager.setwalletamount(Double.parseDouble(String.format(Locale.ENGLISH,"%.2f",tok.getDouble("amount"))));
                        Log.d("fdvdf",""+ sessionManager.getwalletamount());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    private void ibandatarequestJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.URLstringibandata, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ibandataresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("ghj", "" + obj.getString("error"));
                        JSONArray data = new JSONArray(obj.getString("data"));
                        data1 = new ArrayList<IbanDataModel>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = data.getJSONObject(i);
                            IbanDataModel datamodel = new IbanDataModel(jsonObject.getInt("id"),jsonObject.getString("acc_no"), jsonObject.getString("iban_no"),jsonObject.getString("logo"));
                            data1.add(datamodel);
                        }
                        RecyclerView.Adapter adapter1 = new IbanAdapter(getContext(),data1,PaymentFragment.this);
                        recyclerView.setAdapter(adapter1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View view) {
        if(view == onlinepay){
            onlinepay.setBackgroundColor(getResources().getColor(R.color.yellow));
            iban.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            cash.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            rohaawallet.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            stcpay.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            recyclerView.setVisibility(View.GONE);
            payiban = false;
            wallet = false;
            cod = false;
            btnproceed.setVisibility(View.VISIBLE);
            btncancel.setVisibility(View.VISIBLE);
            ibanaccountnoselect = false;
            paytype = "card";
        }
        else if(view == iban){
            payiban = true;
            paytype = "iban";
            wallet = false;
            cod = false;
            iban.setBackgroundColor(getResources().getColor(R.color.yellow));
            onlinepay.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            stcpay.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            cash.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            rohaawallet.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            recyclerView.setVisibility(View.VISIBLE);
            btnproceed.setVisibility(View.VISIBLE);
            btncancel.setVisibility(View.VISIBLE);
        }
        else if(view == stcpay){
            onlinepay.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            iban.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            cash.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            rohaawallet.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            stcpay.setBackgroundColor(getResources().getColor(R.color.yellow));
            recyclerView.setVisibility(View.GONE);
            btnproceed.setVisibility(View.VISIBLE);
            btncancel.setVisibility(View.VISIBLE);
            ibanaccountnoselect = false;
            payiban = false;
            wallet = false;
            cod = false;
            paytype = "stcpay";
        }
        else if(view == cash){
            onlinepay.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            iban.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            rohaawallet.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            cash.setBackgroundColor(getResources().getColor(R.color.yellow));
            recyclerView.setVisibility(View.GONE);
            btnproceed.setVisibility(View.VISIBLE);
            btncancel.setVisibility(View.VISIBLE);
            ibanaccountnoselect = false;
            payiban = false;
            wallet = false;
            cod = true;
            paytype = "cod";
        }
        else if(view == rohaawallet){
            onlinepay.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            cash.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            iban.setBackground(getResources().getDrawable(R.drawable.bg_border_15dp_white));
            rohaawallet.setBackgroundColor(getResources().getColor(R.color.yellow));
            recyclerView.setVisibility(View.GONE);
            btnproceed.setVisibility(View.VISIBLE);
            btncancel.setVisibility(View.VISIBLE);
            ibanaccountnoselect = false;
            cod = false;
            wallet = true;
            payiban = false;
            paytype = "wallet";
        }
        else if(view == back){
            getActivity().onBackPressed();
        }
        else if(view == btncancel){
            getActivity().onBackPressed();
        }
        else if(view == btnproceed){
            if(payiban == false){
                if(sessionManager.getchoose().equals("rohahome")) {
                    if(paytype.equals("wallet")){
                        Log.d(TAG, "onClick: "+LocalData.getInstance());
                        if(sessionManager.getwalletamount() >= Double.parseDouble(LocalData.getInstance().getTotalprice())){
                            LocalData.getInstance().setPaymenttype(paytype);
                            Bundle args = new Bundle();
                            args.putBoolean("ibandata",false);

                            Log.d("wallet","kanika");
                            Log.d("wallet1","kanika"+LocalData.getInstance().getPaymentamount());
                            Log.d("wallet2","kanika");
                            Log.d("wallet3","kanika");
                            Log.d("wallet4","kanika");
                            ((RohahomeDashboradActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                        }
                        else {
                            sessionManager.snackbarToast(getString(R.string.walletislow),view);
                        }
                    }
                    else if(paytype.equals("cod")){
                        LocalData.getInstance().setPaymenttype(paytype);
                        Bundle args = new Bundle();
                        args.putBoolean("ibandata",false);
                        ((RohahomeDashboradActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                    }
                    else if(paytype.equals("card")){
                        LocalData.getInstance().setPaymenttype(paytype);
                        payment();
                    }
                }
                else if(sessionManager.getchoose().equals("rohawater")) {
                    if(paytype.equals("wallet")){
                        Log.d("zcghzbxc",""+LocalData.getInstance().getTotalprice());
                        if(sessionManager.getwalletamount() >= Double.parseDouble(LocalData.getInstance().getTotalprice())){
                            LocalData.getInstance().setPaymenttype(paytype);
                            Bundle args = new Bundle();
                            args.putBoolean("ibandata",false);
                            ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                        }
                        else {
                            sessionManager.snackbarToast(getString(R.string.walletislow),view);
                        }
                    }
                    else if(paytype.equals("cod")){
                        LocalData.getInstance().setPaymenttype(paytype);
                        Bundle args = new Bundle();
                        args.putBoolean("ibandata",false);
                        args.putString("from","dashboard");
                        ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new SuccessFragment(),args);
                    }
                    else if(paytype.equals("card")){
                        LocalData.getInstance().setPaymenttype(paytype);
                        checkPlaceOrderBody();
                        payment();
                    }
                }
            }
            else if(payiban == true){
                if(ibanaccountnoselect == true){
                    if(sessionManager.getchoose().equals("rohahome")){
                        LocalData.getInstance().setPaymenttype(paytype);
                        LocalData.getInstance().setIbanno(String.valueOf(ibannoid));
                        ((RohahomeDashboradActivity) getActivity()).subchangefragment(new IbanFragment(),null);
                    }
                    else if(sessionManager.getchoose().equals("rohawater")){
                        LocalData.getInstance().setPaymenttype(paytype);
                        LocalData.getInstance().setIbanno(String.valueOf(ibannoid));
                        ((RohaWaterDashboardActivity) getActivity()).subchangefragment(new IbanFragment(),null);
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.pleasechoosetheaccount),btnproceed);
                }
            }
        }
    }

    private void checkPlaceOrderBody() {
        sessionManager.progressdialogshow();
        if(sessionManager.getIsWish() == 1){
            try {
                newarray = new JSONArray() ;
                JSONArray conv=LocalData.getInstance().getBodyarray();
                Log.d("hgyfjnbj",""+conv);
                for(int i=0;i<conv.length();i++){
                    JSONObject js =  conv.getJSONObject(i);
                    String mosquename = js.getString("mosquename");
                    byte[] data1 = mosquename.getBytes("UTF-8");
                    String bmosquename = Base64.encodeToString(data1, Base64.NO_WRAP);
                    String placename = js.getString("place");
                    byte[] data11 = placename.getBytes("UTF-8");
                    String bplace = Base64.encodeToString(data11, Base64.NO_WRAP);
                    double lat = js.getDouble("lat");
                    double lng = js.getDouble("lng");
                    int ismeccca = js.getInt("ismecca");
                    int cartcount = js.getInt("cartcount");
                    double productprice = js.getDouble("productprice");
                    String rname = js.getString("receivername");
                    byte[] data2 = rname.getBytes("UTF-8");
                    String brname = Base64.encodeToString(data2, Base64.DEFAULT);
                    String rmno = js.getString("receivermno");
                    byte[] data4 = rmno.getBytes("UTF-8");
                    String brmno = Base64.encodeToString(data4, Base64.DEFAULT);
                    String rdesc = js.getString("receiverdesc");
                    byte[] data3 = rdesc.getBytes("UTF-8");
                    String brdesc = Base64.encodeToString(data3, Base64.DEFAULT);
                    String unit = js.getString("units_qty");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("lat",lat);
                    jsonObject.put("lng",lng);
                    jsonObject.put("ismecca",ismeccca);
                    jsonObject.put("place",bplace);
                    jsonObject.put("mosquename",bmosquename);
                    jsonObject.put("cartcount",cartcount);
                    jsonObject.put("productprice",productprice);
                    jsonObject.put("receivername",brname);
                    jsonObject.put("receivermno",brmno);
                    jsonObject.put("receiverdesc",brdesc);
                    jsonObject.put("units_qty",unit);
                    newarray.put(jsonObject);
                }
                Log.d("bchzxbc",""+newarray);
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            checkPlaceOrderJSON(newarray);
        }
        else if(sessionManager.getIsWish() == 0){
            JSONArray isarray = new JSONArray() ;
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("wish_place", sessionManager.base64convert(LocalData.getInstance().getPlace()));
                isarray.put(requestbody);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            checkPlaceOrderJSON(isarray);
        }
    }



 /*   private void payment() {
       // String profileId = StaticInfo.profileId;
       // String serverKey = StaticInfo.serverKey;
       // String clientKey = StaticInfo.clientKey;
        //live
        String profileId = "55528";
        String serverKey = "SKJNMT99TL-JBKLHDJJB2-NMDJ9KDMD9";
        String clientKey = "CHKMTG-MNDV62-KBHD66-PNQ6H9";

        //test
      //  String profileId = "55181";
      //  String serverKey = "SGJNMT99DR-JBKLHDKBWR-JZ2RJ6MWBH";
      //  String clientKey = "C6KMTG-MNBM62-KBHDK2-6NKB6Q";

        //Log.d("dvd",""+profileId+serverKey+clientKey);
        PaymentSdkLanguageCode locale = PaymentSdkLanguageCode.EN;
        String screenTitle = "Dover";
        String cartId = "123456";
        String cartDesc = "cart description";
        String currency = "SAR";
        double amount = Double.parseDouble(LocalData.getInstance().getTotalprice());
        PaymentSdkTokenise tokeniseType = PaymentSdkTokenise.NONE;
        PaymentSdkTokenFormat tokenFormat = new PaymentSdkTokenFormat.Hex32Format();
        PaymentSdkBillingDetails billingData = new PaymentSdkBillingDetails(
                sessionManager.getccity(),
                "SA",
                sessionManager.getusermail(),
                sessionManager.getusername(),
                sessionManager.getuserphoneno(),
                "state",
                sessionManager.getcaddr(),
                "zip"
        );
        PaymentSdkShippingDetails shippingData = new PaymentSdkShippingDetails(
                sessionManager.getdcity(),
                "SA",
                sessionManager.getusermail(),
                sessionManager.getusername(),
                sessionManager.getuserphoneno(),
                "state",
                sessionManager.getdaddr(),
                "zip"
        );
        PaymentSdkConfigurationDetails configData = new PaymentSdkConfigBuilder(profileId, serverKey, clientKey, amount, currency)
                .setCartDescription(cartDesc)
                .setLanguageCode(locale)
                .setBillingData(billingData)
                .setMerchantCountryCode("SA") // ISO alpha 2
                .setShippingData(shippingData)
                .setCartId(cartId)
                .showBillingInfo(true)
                .showShippingInfo(true)
                .forceShippingInfo(true)
                .setScreenTitle(screenTitle)
                .build();
        PaymentSdkActivity.startCardPayment(getActivity(), configData, this);
    }

    @Override
    public void onError(@NotNull PaymentSdkError paymentSdkError) {
        Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
        Log.d("dfghewe",""+paymentSdkError);
    }

    @Override
    public void onPaymentCancel() {
        Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
        Log.d("dfghewe",""+"paymentSdkError");
    }

    @Override
    public void onPaymentFinish(@NotNull PaymentSdkTransactionDetails paymentSdkTransactionDetails) {
        Log.d("dfgh333",""+paymentSdkTransactionDetails.component8().component1());
        Log.d("dfgh333",""+paymentSdkTransactionDetails.component8().component2());

        Log.d("dfgh33gf3",""+paymentSdkTransactionDetails.component1());
        if(paymentSdkTransactionDetails.component8().component2().equalsIgnoreCase("Authorised") || paymentSdkTransactionDetails.component8().component1().equals(0)){
            Log.d("check1","czxcxv");
            if(paymentSdkTransactionDetails.component1() == null){
                Log.d("check2","czxcxv");
                Toast.makeText(getContext(), "Transaction Id Null", Toast.LENGTH_SHORT).show();
            }
            else {
                Log.d("check2","czxcxv");
                ftid = paymentSdkTransactionDetails.component1();
                if(!ftid.equals("")){
                    if(sessionManager.getchoose().equals("rohahome")){
                        LocalData.getInstance().setTransactionid(ftid);
                        Fragment fragment = new SuccessFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
                    }
                    else {
                        if(sessionManager.getIsWish() == 0) {
                            LocalData.getInstance().setTransactionid(ftid);
                            Fragment fragment = new SuccessFragment();
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
                        }
                        else if(sessionManager.getIsWish() == 1) {
                            LocalData.getInstance().setTransactionid(ftid);
                            Fragment fragment = new SuccessFragment();
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
                        }
                    }
                }
            }
        }
        else {
            Toast.makeText(getContext(), ""+getString(R.string.paymentfailed)+"-"+paymentSdkTransactionDetails.component8().component2(), Toast.LENGTH_SHORT).show();
        }

    }

    private void refundrequest(String amt,String tid) {
        //String s= id.getText().toString();
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("profile_id", StaticInfo.profileId);
            requestbody.put("tran_type", "refund");
            requestbody.put("tran_class", "ecom");
            requestbody.put("cart_id", "123456");
            requestbody.put("cart_currency", "SAR");
            requestbody.put("cart_amount", amt);
            requestbody.put("cart_description", "API VOLLY PROBLEM");
            requestbody.put("tran_ref", tid);
            refundamountrequestJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void refundamountrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.refundrequesturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("refunfresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject result = obj.getJSONObject("payment_result");
                    if(result == null){
                        Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                        Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (result.getString("response_status").equalsIgnoreCase("A")) {
                            Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                            sessionManager.snackbarToast(getString(R.string.refundrequestsuccess), view);
                        } else {
                            Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                            Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                if(error instanceof NoConnectionError){
                    sessionManager.slownetwork(getActivity());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("authorization",StaticInfo.refundserverKey);
                Log.d("vgcd",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }*/


    public void payment(){
   /*     Intent intent = new Intent(getActivity(), WebviewActivity.class);

        //intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(WebviewActivity.EXTRA_MESSAGE, getMobileRequest());
        intent.putExtra(WebviewActivity.SUCCESS_ACTIVTY_CLASS_NAME, "com.lia.yello.roha.activity.SuccessTransationActivity");
        intent.putExtra(WebviewActivity.FAILED_ACTIVTY_CLASS_NAME, "com.lia.yello.roha.activity.FailedTransationActivity");
        intent.putExtra(WebviewActivity.IS_SECURITY_ENABLED, isSecurityEnabled);
        //startActivity(intent);
        startActivityForResult(intent,105);*/
        Intent intent=new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra("from", "order");
        startActivity(intent);
        LocalData.getInstance().setTransactionid("");
        LocalData.getInstance().setTransactiontype("order");
    }

    private MobileRequest getMobileRequest() {
        MobileRequest mobile = new MobileRequest();
        mobile.setStore(STORE_ID);                       // Store ID
        mobile.setKey(KEY);                              // Authentication Key : The Authentication Key will be supplied by Telr as part of the Mobile API setup process after you request that this integration type is enabled for your account. This should not be stored permanently within the App.
        App app = new App();
        app.setId(getActivity().getPackageName());                          // Application installation ID
        app.setName(getString(R.string.app_name));                          // Application name
        app.setUser(String.valueOf(sessionManager.getUserId()));                           // Application user ID : Your reference for the customer/user that is running the App. This should relate to their account within your systems.
//        app.setVersion(BuildConfig.VERSION_NAME);                         // Application version
        app.setSdk(app.getSdk());
        mobile.setApp(app);
        Tran tran = new Tran();
        tran.setTest("0");                              // Test mode : Test mode of zero indicates a live transaction. If this is set to any other value the transaction will be treated as a test.
        tran.setType("sale");                           /* Transaction type
                                                            'auth'   : Seek authorisation from the card issuer for the amount specified. If authorised, the funds will be reserved but will not be debited until such time as a corresponding capture command is made. This is sometimes known as pre-authorisation.
                                                            'sale'   : Immediate purchase request. This has the same effect as would be had by performing an auth transaction followed by a capture transaction for the full amount. No additional capture stage is required.
                                                            'verify' : Confirm that the card details given are valid. No funds are reserved or taken from the card.
                                                        */

        tran.setClazz("paypage");                       // Transaction class only 'paypage' is allowed on mobile, which means 'use the hosted payment page to capture and process the card details'
        tran.setCartid(String.valueOf(new BigInteger(128, new Random()))); //// Transaction cart ID : An example use of the cart ID field would be your own transaction or order reference.
        tran.setDescription(getString(R.string.order)+"-A");         // Transaction description
        tran.setLanguage("en");
        tran.setCurrency("SAR");                        // Transaction currency : Currency must be sent as a 3 character ISO code. A list of currency codes can be found at the end of this document. For voids or refunds, this must match the currency of the original transaction.
        tran.setAmount(LocalData.getInstance().getTotalprice());                         // Transaction amount : The transaction amount must be sent in major units, for example 9 dollars 50 cents must be sent as 9.50 not 950. There must be no currency symbol, and no thousands separators. Thedecimal part must be separated using a dot.
        //tran.setRef();                                // (Optinal) Previous transaction reference : The previous transaction reference is required for any continuous authority transaction. It must contain the reference that was supplied in the response for the original transaction.

        //040023303844  //030023738912
        // tran.setFirstref("030023738912");             // (Optinal) Previous user transaction detail reference : The previous transaction reference is required for any continuous authority transaction. It must contain the reference that was supplied in the response for the original transaction.

        mobile.setTran(tran);
        Billing billing = new Billing();
        Address address = new Address();
        address.setCity(sessionManager.getccity());                       // City : the minimum required details for a transaction to be processed
        address.setCountry(sessionManager.getcountrycodea());                       // Country : Country must be sent as a 2 character ISO code. A list of country codes can be found at the end of this document. the minimum required details for a transaction to be processed
        address.setRegion(sessionManager.getccity());                     // Region
        address.setLine1(sessionManager.getcaddr());// Street address – line 1: the minimum required details for a transaction to be processed
        //address.setLine2("SIT G=Towe");               // (Optinal)
        //address.setLine3("SIT G=Towe");               // (Optinal)
        //address.setZip("SIT G=Towe");                 // (Optinal)
        billing.setAddress(address);
        Name name = new Name();
        name.setFirst(sessionManager.getusername());                          // Forename : the minimum required details for a transaction to be processed
        name.setLast(sessionManager.getusername());                          // Surname : the minimum required details for a transaction to be processed
        name.setTitle(sessionManager.getusername());                           // Title
        billing.setName(name);
        billing.setEmail(sessionManager.getusermail()); //stackfortytwo@gmail.com : the minimum required details for a transaction to be processed.
        billing.setPhone("+"+sessionManager.getcountrycode()+sessionManager.getuserphoneno());
        mobile.setBilling(billing);
        mobile.setCustref("231");
        return mobile;

    }

    private void checkPlaceOrderJSON(JSONArray response) {
        final String requestBody = response.toString();
        Log.d("reques34",requestBody);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.checkplaceorder,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("Response1",""+response.toString());
                        Log.d("Response7",""+new String(String.valueOf(response)));
                        Log.d("Response2",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                sessionManager.setinsertid(jsonObject.getInt("insertid"));
                                Log.d("", "onResponse:insertid "+sessionManager.getinsertid());
//                                fullsucesslin.setVisibility(View.VISIBLE);
//                                success_order_id.setText(""+StaticInfo.order_prefix+"M"+jsonObject.getString("orderId"));
//                                sessionManager.snackbarToast(obj.getString("message"),view);
//                                LocalData.removeAllValues();
                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue",view);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "onErrorResponse: "+error);
                        sessionManager.progressdialogdismiss();
                        sessionManager.snackbarToast(getString(R.string.orderfailed),view);

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type","multipart/form-data;boundary=<calculated when request is sent>" );
//                params.put("Accept", "charset=utf-8");
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("totalcarton",""+LocalData.getInstance().getTotalqty());
                params.put("totalprice",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("iswish",""+ sessionManager.getIsWish());
                params.put("unitsqty",""+LocalData.getInstance().getUnitqty());
                params.put("ismecca",""+ LocalData.getInstance().getIsmeccaIntHeaderArray());
                params.put("place",""+ LocalData.getInstance().getIsmeccaStrHeaderArray());
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());
                Log.d("param",""+params);
                return params;
            }



            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("data", ""+requestBody);
                Log.d("param order",""+params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }

    private void UpdatePlaceOrderBody(String status) {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("insertid", sessionManager.getinsertid());
            if(status.equals("failure")){
                requestbody.put("payment_status","failure" );
            }
            else {
                requestbody.put("payment_status","success" );
            }
            
            UpdatePlaceOrderJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void UpdatePlaceOrderJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("profile", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updateplaceorder, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("updateplaceorderresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    String msg = obj.getString("message");
                    if (obj.getBoolean("error") == true) {

//                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {//                sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                sessionManager.progressdialogdismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
//                int id = sessionManager.getid();
                Map<String, String> params = new HashMap<String, String>();
//                params.put("lang",sessionManager.getlang() );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void initiateSession() {
//        pbLoading.setVisibility(View.VISIBLE);

        /**
         * If you want to use saved card option with embedded payment, send the parameter
         * "customerIdentifier" with a unique value for each customer. This value cannot be used
         * for more than one Customer. Like this:
         */
//        MFInitiateSessionRequest request = new MFInitiateSessionRequest("12332212");
//        MFSDK.INSTANCE.initiateSession(request, (MFResult<MFInitiateSessionResponse> result) -> {

        /**
         * If not, sent it with null. Like this.
         */
        MFSDK.INSTANCE.initiateSession(null, (MFResult<MFInitiateSessionResponse> result) -> {
            if (result instanceof MFResult.Success) {
                Log.d(TAG, "Response: " + new Gson().toJson(((MFResult.Success<MFInitiateSessionResponse>) result).getResponse()));
                mfPaymentView.load(((MFResult.Success<MFInitiateSessionResponse>) result).getResponse());
            }
            if (result instanceof MFResult.Fail) {
                Log.d(TAG, "Fail: " + new Gson().toJson(((MFResult.Fail) result).getError()));
            }

//            pbLoading.setVisibility(View.GONE);

            return Unit.INSTANCE;
        });
    }

    private void payWithEmbeddedPayment() {


        double invoiceAmount = Double.parseDouble("500");
        MFExecutePaymentRequest request = new MFExecutePaymentRequest(invoiceAmount);

        mfPaymentView.pay(
                requireActivity(),
                request,
                MFAPILanguage.EN,
                (String invoiceId) -> {
                    Log.d(TAG, "invoiceId: " + invoiceId);
                    return Unit.INSTANCE;
                },
                (String invoiceId, MFResult<MFGetPaymentStatusResponse> result) -> {
                    if (result instanceof MFResult.Success) {
                        Log.d(TAG, "Response: " + new Gson().toJson(((MFResult.Success<MFGetPaymentStatusResponse>) result).getResponse()));
//                        showAlertDialog("Payment done successfully");
                    }
                    else if (result instanceof MFResult.Fail) {
                        String error = new Gson().toJson(((MFResult.Fail) result).getError());
                        Log.d(TAG, "Fail: " + error);
//                        showAlertDialog(error);
                    }

//                    pbLoading.setVisibility(View.GONE);

                    return Unit.INSTANCE;
                });
    }

}