package com.lia.yello.roha.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.WaterproductDataModel;
import com.lia.yello.roha.utility.LocalData;

import java.util.ArrayList;
import java.util.List;

public class YourwishSummaryAdapter extends RecyclerView.Adapter<YourwishSummaryAdapter.MyViewHolder> {
    private Context mcontext;
    private List<WaterproductDataModel> dataSet;
    private RohaWaterOrderSummaryFragment yourwishSummaryActivity;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView unit_cost,name;
        TextView tot_Cost;
        ImageView pimage;
        CardView linear;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.pname);
            this.unit_cost = (TextView) itemView.findViewById(R.id.unitcost);
            this.tot_Cost = (TextView) itemView.findViewById(R.id.tot_Cost);
            this.linear = (CardView) itemView.findViewById(R.id.card);
            this.pimage = (ImageView) itemView.findViewById(R.id.product_image);

        }
    }

    public YourwishSummaryAdapter(Context context, List<WaterproductDataModel> data, RohaWaterOrderSummaryFragment yourwishSummaryActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.yourwishSummaryActivity = yourwishSummaryActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_waterproductdata, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        final WaterproductDataModel listData = dataSet.get(listPosition);
        Glide.with(mcontext).load(dataSet.get(listPosition).getPimage()).placeholder(R.drawable.defaultimg).into(holder.pimage);
        holder.name.setText(""+dataSet.get(listPosition).getPname());
        holder.unit_cost.setText(dataSet.get(listPosition).gettotqty()+" "+yourwishSummaryActivity.getString(R.string.Carton));
        holder.tot_Cost.setText(dataSet.get(listPosition).gettotcost()+" SAR");


        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bigqty = 0;
                for (int i=0;i<dataSet.size();i++){
                    if(bigqty < dataSet.get(i).gettotqty()){
                        bigqty = dataSet.get(i).gettotqty();
                    }
                }

              LocalData.getInstance().setitemname(dataSet.get(listPosition).getPname());
              LocalData.getInstance().setQuantity(""+dataSet.get(listPosition).gettotqty());
              LocalData.getInstance().setCost(""+dataSet.get(listPosition).getunitcost());
                yourwishSummaryActivity.dynamiccount(dataSet.get(listPosition).getPname(),listData.getMpid(),listData.gettotcost(),listData.getunitcost(),listData.gettotqty(),bigqty);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}