package com.lia.yello.roha.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.OrderTrackingFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.HomeorderlistDataModel2;

import java.util.List;

public class MyOrdersHomeAdapter2  extends RecyclerView.Adapter<MyOrdersHomeAdapter2.MyViewHolder> {
    private Context mcontext;
    private List<HomeorderlistDataModel2> dataSet;
    private OrderTrackingFragment invoiceFragment2;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productname1, productname2,quantity1,subtotal;
        public MyViewHolder(View itemView) {
            super(itemView);
            productname1=(TextView)itemView.findViewById(R.id.txt_productname1);
            productname2=(TextView)itemView.findViewById(R.id.txt_productname2);
            quantity1=(TextView)itemView.findViewById(R.id.txt_qty1);
            subtotal=(TextView)itemView.findViewById(R.id.txt_subtotal);

        }
    }

    public MyOrdersHomeAdapter2(Context context, List<HomeorderlistDataModel2> data, OrderTrackingFragment helpFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.invoiceFragment2 = helpFragment;
    }

    @Override
    public MyOrdersHomeAdapter2.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_summary2, parent, false);
        MyOrdersHomeAdapter2.MyViewHolder myViewHolder = new MyOrdersHomeAdapter2.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {

        if(invoiceFragment2.sessionManager.getlang().equals("ar")){
            holder.subtotal.setBackgroundTintList(ColorStateList.valueOf(mcontext.getResources().getColor(R.color.green)));
            holder.subtotal.setBackground(mcontext.getResources().getDrawable(R.drawable.bg_border_custom1_white));
        }

        holder.productname1.setText(dataSet.get(listPosition).getPname());
        holder.productname2.setText(""+dataSet.get(listPosition).getPsprice()+" SAR");
        holder.quantity1.setText(""+dataSet.get(listPosition).getPqty()+" "+mcontext.getString(R.string.Carton));
        if(dataSet.get(listPosition).getPtprice() == 0.0){
            holder.subtotal.setText(""+mcontext.getString(R.string.free));
        }
        else {
            holder.subtotal.setText(""+dataSet.get(listPosition).getPtprice()+" SAR");
        }

    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

