package com.lia.yello.roha.model;

public class CommonDataModel {
    int id;
    String bname;

    public CommonDataModel(int id, String branchname) {

        this.id = id;
        this.bname = branchname;
    }

    public int getId() {
        return id;
    }

    public String getBname(){
        return bname;
    }

}
