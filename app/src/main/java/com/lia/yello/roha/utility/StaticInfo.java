package com.lia.yello.roha.utility;

public class StaticInfo {

    public static Double vat = 0.0;
    public static Double delivery = 0.0;
    public static String order_prefix = "DOV";
    public static  boolean rohawaterfirst = true;
    public static  boolean rohahomefirst = true;
    public static boolean cartdatachanged = false;
    public static String currentactivityname = "";
    public static boolean welcome = true;
    public static final int PERMISSION_REQUEST_CODE = 200;
    public static final int GPS_REQUEST_CODE = 201;
    public static final String MECCA = "Makkah";
    public static final String MEDINA = "Madina";
    public static final String JEDDAH = "Jeddah";
    public static final String RIYADH = "Riyadh";
    public static final String googlesigninwebclientid = "1003591081634-putc7jciejjjv330scd8fqghj90bsiuq.apps.googleusercontent.com";
//    public static final String googlemapapikey = "AIzaSyAX7o-0VreaS0Gz9jM22oaCzZfF0KiETBU";
    public static final String googlemapapikey = "AIzaSyDZW9EY2uJmSRk3OZ_-970CrH_hRa9jFiA";
    public static final String facebook_app_id = "941260073283328";
    public static final String fb_login_protocol_scheme = "fb941260073283328";
//
     public static String BaseUrl = "http://staging.doverapp.me/rohaapi/customer_ver2/";
     public static String BaseUrl2 = "http://staging.doverapp.me/rohaapi/customer_ver3/";
     public static String BaseUrl3 = "http://staging.doverapp.me/rohaapi/customer_ver4/";
     public static String BaseUrl7 = "http://staging.doverapp.me/rohaapi/customer_ver8/";
//  public static String BaseUrl = "http://doverapp.me/rohaapi/customer_ver2/";
//  public static String BaseUrl2 = "http://doverapp.me/rohaapi/customer_ver3/";
//  public static String BaseUrl3 = "http://staging.doverapp.me/rohaapi/customer_ver4/";
  public static String BaseUrl4 = "http://staging.doverapp.me/rohaapi/customer_ver5/";
  public static String BaseUrl5 = "http://staging.doverapp.me/rohaapi/customer_ver6/";
    public static String URLstringlogin = BaseUrl+"custlogin";
    public static String UnregisteredToken = BaseUrl5+"unregisteredUserToken";
    public static String URLstringnewlogin = BaseUrl+"customerlogin";
    public static String URLstringchecksociallogin = BaseUrl+"checksocialmedialogin";
    public static String URLstringsocialsignup = BaseUrl+"customersocialsignup";
    public static String URLstringsignup = BaseUrl4+"customersignup";
    public static String URLstringcheckphno = BaseUrl+"checkphoneno";
    public static String URLstringcheckemailid = BaseUrl+"checkemail";
    public static String URLstringprofileupdate = BaseUrl+"updateprofile";
    public static String URLstringforgetpassword = BaseUrl+"forgotpassword";
    public static String URLstringresetpassword = BaseUrl+"resetpassword";
    public static String URLstringviewprofile = BaseUrl+"viewprofile";
    public static String URLstringwaterproductdata = BaseUrl+"waterproductdata";
    public static String URLstringhomeproductdata = BaseUrl+"homeproductdata";
    public static String URLstringibandata = BaseUrl+"ibandet";
    public static String URLstringwishlist = BaseUrl2+"yourwishlist";
    //public static String URLstringplaceorder = "https://rohaa.com/customer/placeorder";
    public static String URLstringplaceorderwater = BaseUrl7+"placeorder_ver2";
    public static String URLstringplaceorderhome = BaseUrl2+"placeorder_home";
    public static String URLstringorderlist = BaseUrl2+"orderlist";
    public static String tokenupdateUrl = BaseUrl+"updateUserToken";
    public static String commonvalues = BaseUrl+"constantvalues";
    public static String bannervalues = BaseUrl+"listbanner";
    public static String addwallet = BaseUrl+"addwallet";
    public static String getwallet = BaseUrl+"getwallet";
    public static String addaddress = BaseUrl+"add_address";
    public static String listaddress = BaseUrl+"list_address";
    public static String deleteaddress = BaseUrl+"deleteaddress";
    public static String updateaddress = BaseUrl+"update_address";
    public static String home_orderlist = BaseUrl2+"home_orderlist";
    public static String list_wallethistory = BaseUrl+"list_wallethistory";
    public static String listoffers = BaseUrl+"listoffers";
    public static String validate_offers = BaseUrl+"validateoffer";
    public static String review = BaseUrl+"rating";
    public static String timeslot = BaseUrl+"delivery_info";
    public static String notification = BaseUrl+"listnotification";
    public static String clearnotification = BaseUrl+"clearnotification";
    public static String list_faq = BaseUrl+"faq";
    public static String offerstatus = BaseUrl+"updateofferstatus";
    public static String verifyotp = BaseUrl+"verifyOtp";
    public static String removesignup = BaseUrl+"removeSignup";
    public static String updatepassword = BaseUrl+"updatepassword";
    public static String addcart = BaseUrl+"add_cart";
    public static String updatecart = BaseUrl+"update_cart";
    public static String deletecart = BaseUrl+"delete_cart";
    public static String deleteallcart = BaseUrl+"remove_cart";
    public static String listcart = BaseUrl+"list_cart";
    public static String updateaddressid = BaseUrl+"updateaddressid";
    public static String URLupdatetransactionid = BaseUrl+"updatetransactionid";
    public static String cancelorder = BaseUrl+"cancelorder";
    public static String mapdata = "https://doverapp.me/rohaapi/customer_ver5/get_mapdata";


    //old paytab credintial
    public static String payment_api = "GSL9Mj0XJoxpYohYG90vKmK8RZAxQRdwKZGwiGu252r3rJ17S1DDooLgnlOed1KX0ruj7Zlc1NgswhApjd2HCOXZaMenNYlOpQ8x";
    public static String payment_mail = "hanaidreesi100@gmail.co";

    //new paytab credintial
    //test
    //public static String profileId = "55181";
    //public static String serverKey = "SGJNMT99DR-JBKLHDKBWR-JZ2RJ6MWBH";
    //public static String clientKey = "C6KMTG-MNBM62-KBHDK2-6NKB6Q";
    //live
    //public static String profileId = "55528";
    //public static String serverKey = "SKJNMT99TL-JBKLHDJJB2-NMDJ9KDMD9";
    //public static String clientKey = "CHKMTG-MNDV62-KBHD66-PNQ6H9";
    //public static String refundserverKey = "SHJNMT99LJ-JBBB2MHGDH-2NT6MM9BKZ";
    //public static String refundrequesturl = "https://secure.paytabs.sa/payment/request";

    //telr credential
    public static String STOREID = "26069";
    public static String KEY = "F2fF5-dJ9g^kGgk4";
    public static String MODE = "0";

//    for tracking the online mode payment -only for card

    public static String checkplaceorder = BaseUrl+"checkplaceorder";
    public static String updateplaceorder = BaseUrl+"updateplaceorder";


    // staticinfo for comman value

    public static String TAXNO = "";
    public static String Address = "";
    public static String companyname = "";
    public static String arcomapny = "";
}
