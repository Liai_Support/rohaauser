package com.lia.yello.roha.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.HomeorderlistDataModel2;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class InvoiceFragment2 extends Fragment {
    View view;
    String date,type,status;
    int id;
    Double vat,subtotal,amount,deliverycharge;
    String discount;
    TextView order_date,discounttxtview,order_subtotal,order_vat,total_amt,order_id,delivary,discounttxt;
    Button review;
    String comefrom,comment,suggestions;
    Double rating;
    EditText ratetxt;
    RatingBar ratingBar;
    LinearLayout reviewlayout;
    private static ArrayList<HomeorderlistDataModel2> data;
    String orderarray,dicounttype,cartonname;
    int cartoncount;
    RecyclerView recycler_orderlist;
    JSONArray jsonArray;
    public SessionManager sessionManager;
    boolean iswater = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_invoice2, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);

        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
           /* OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }));
            }*/
           /* ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });*/
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
        }

        order_date=(TextView)view.findViewById(R.id.invoice_status_date);
        order_subtotal=(TextView)view.findViewById(R.id.nvoice_status_subtotal);
        order_vat=(TextView)view.findViewById(R.id.nvoice_status_vat);
        order_id=(TextView)view.findViewById(R.id.invoice_status_id);
        total_amt=(TextView)view.findViewById(R.id.invoice_status_amt);
        delivary = (TextView) view.findViewById(R.id.nvoice_delivery);
        discounttxt = (TextView) view.findViewById(R.id.discount);
        discounttxtview = (TextView) view.findViewById(R.id.discount_txt);

        ratetxt = (EditText) view.findViewById(R.id.ratetxt);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        reviewlayout = (LinearLayout) view.findViewById(R.id.reviewlayout);
        review = (Button) view.findViewById(R.id.review_myorder);
        recycler_orderlist = (RecyclerView) view.findViewById(R.id.recycler_orderlist);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getContext());
        recycler_orderlist.setLayoutManager(layoutManager2);

        Bundle args = getArguments();
        comefrom = args.getString("comefrom");
        date = args.getString("odate");
        subtotal =  args.getDouble("osubtotal");
        id = args.getInt("oid");
        vat = args.getDouble("ovat");
        amount = args.getDouble("oamt");
        deliverycharge = args.getDouble("odel");
        discount = args.getString("odiscount");
        dicounttype = args.getString("odiscounttype");
        cartonname = args.getString("ocartonname");
        cartoncount = args.getInt("ocartoncount");
        type = args.getString("type");
        status = args.getString("ostatus");
        rating = args.getDouble("rating");
        suggestions = args.getString("suggestion");
        comment = args.getString("comment");
        orderarray = args.getString("orderarray");

        order_date.setText(date);
        order_subtotal.setText(""+subtotal+" SAR");
        order_vat.setText(""+vat+" SAR");
        total_amt.setText(""+amount+" SAR");
        order_id.setText(""+ StaticInfo.order_prefix+"H"+id);
        delivary.setText(""+deliverycharge+" SAR");
        if(dicounttype.equalsIgnoreCase("Carton")){
            discounttxtview.setText(""+getString(R.string.offer));
            discounttxt.setText(""+cartoncount+" "+cartonname);
        }
        else {
            discounttxtview.setText(""+getString(R.string.discount));
            discounttxt.setText(""+discount+" SAR");
        }


        if(!comefrom.equalsIgnoreCase("invoice")){
            recycler_orderlist.setVisibility(View.GONE);
            try {
                jsonArray = new JSONArray(orderarray);
                Log.d("status2",""+jsonArray);
                data = new ArrayList<HomeorderlistDataModel2>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HomeorderlistDataModel2 datamodel = new HomeorderlistDataModel2(jsonObject.getInt("product_id"), jsonObject.getString("product_name"),jsonObject.getString("product_image"),jsonObject.getDouble("product_price"), jsonObject.getInt("product_quantity"), jsonObject.getDouble("product_amount"));
                    data.add(datamodel);
                }
               // adapter1 = new MyOrdersHomeAdapter2(getContext(), data, InvoiceFragment2.this);
                //recycler_orderlist.setAdapter(adapter1);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (status.equals("completed")){
                review.setVisibility(View.GONE);
                if(rating == null || rating == 0){
                    reviewlayout.setVisibility(View.VISIBLE);
                }
                else {
                    reviewlayout.setVisibility(View.GONE);
                }
            }
            else {
                reviewlayout.setVisibility(View.GONE);
            }
        }
        else {
            recycler_orderlist.setVisibility(View.GONE);
        }



        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratetxt.getText().toString().equals("") || ratingBar.getRating()==0){
                    sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                }else{
                    float rating = ratingBar.getRating();
                    String review = ratetxt.getText().toString();
                    String suggestion = ratetxt.getText().toString();
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("order_id",id);
                        requestbody.put("cust_id", sessionManager.getUserId());
                        requestbody.put("rate", rating);
                        requestbody.put("comment", review);
                        requestbody.put("suggestion", suggestion);
                        requestbody.put("type", "home");
                        reviewJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return view;
    }

    private void reviewJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request123",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.review, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("reviewresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    //    Fragment fragment = new RohahomeHomeFragment();
                     //   getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                    }
                    else {
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

}