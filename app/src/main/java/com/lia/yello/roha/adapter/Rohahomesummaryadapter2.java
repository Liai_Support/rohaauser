package com.lia.yello.roha.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.RohaHomeOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.CartDataModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class Rohahomesummaryadapter2  extends RecyclerView.Adapter<Rohahomesummaryadapter2.MyViewHolder> {
    private Context mcontext;
    private List<CartDataModel> dataSet;
    private RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productname1, productname2,quantity1,subtotal;
        CardView card;
        public MyViewHolder(View itemView) {
            super(itemView);
            productname1=(TextView)itemView.findViewById(R.id.txt_productname1);
            productname2=(TextView)itemView.findViewById(R.id.txt_productname2);
            quantity1=(TextView)itemView.findViewById(R.id.txt_qty1);
            subtotal=(TextView)itemView.findViewById(R.id.txt_subtotal);
            card = (CardView)itemView.findViewById(R.id.card1_cart6);

        }
    }

    public Rohahomesummaryadapter2(Context context, List<CartDataModel> data, RohaHomeOrderSummaryFragment helpFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohaHomeOrderSummaryFragment = helpFragment;
    }

    @Override
    public Rohahomesummaryadapter2.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_summary2, parent, false);
        Rohahomesummaryadapter2.MyViewHolder myViewHolder = new Rohahomesummaryadapter2.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {

        if(rohaHomeOrderSummaryFragment.sessionManager.getlang().equals("ar")){
            holder.subtotal.setBackgroundTintList(ColorStateList.valueOf(mcontext.getResources().getColor(R.color.green)));
            holder.subtotal.setBackground(mcontext.getResources().getDrawable(R.drawable.bg_border_custom1_white));
        }

        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1 = rohaHomeOrderSummaryFragment.sessionManager.getlocalcart().getJSONObject(listPosition);
            holder.productname1.setText(""+jsonObject1.getString("productname"));
            holder.productname2.setText(""+jsonObject1.getDouble("productprice")+" SAR");
            holder.quantity1.setText(""+jsonObject1.getInt("productquantity")+" "+mcontext.getString(R.string.Carton));
            holder.subtotal.setText(""+(jsonObject1.getDouble("productquantity")*jsonObject1.getInt("productprice"))+" SAR");
        } catch (JSONException e) {
            e.printStackTrace();
        }
      /*  if(dataSet.get(listPosition).getIsstock() == 0){
            holder.card.setAlpha(0.4f);
            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sessionManager.snackbarToast(mcontext.getString(R.string.stocknotavailable),holder.card);
                }
            });
        }*/

    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

