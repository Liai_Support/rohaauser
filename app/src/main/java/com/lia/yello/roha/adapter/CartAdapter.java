package com.lia.yello.roha.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lia.yello.roha.Fragment.CartFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.CartDataModel;
import com.lia.yello.roha.utility.StaticInfo;

import java.util.List;
import java.util.Locale;

public class CartAdapter  extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    private Context mcontext;
    private List<CartDataModel> dataSet;
    private CartFragment cartFragment;
    private RohahomeDashboradActivity rohahomeDashboradActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, txtquantity, cost;
        CardView cardview;
        ImageView menuimage, imgminus, imgplus, delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.txtquantity = (TextView) itemView.findViewById(R.id.txtquantity);
            this.menuimage = (ImageView) itemView.findViewById(R.id.menuimage);
            this.imgminus = (ImageView) itemView.findViewById(R.id.imgminus);
            this.imgplus = (ImageView) itemView.findViewById(R.id.imgplus);
            this.cost = (TextView) itemView.findViewById(R.id.cost);
            this.delete = (ImageView) itemView.findViewById(R.id.delete);
            this.cardview = (CardView) itemView.findViewById(R.id.cardview1_orders);
        }
    }

    public CartAdapter(Context context, List<CartDataModel> data, CartFragment helpFragment, RohahomeDashboradActivity rohahomeDashboradActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.cartFragment = helpFragment;
        this.rohahomeDashboradActivity = rohahomeDashboradActivity;
    }

    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_cart, parent, false);
        CartAdapter.MyViewHolder myViewHolder = new CartAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        Glide.with(mcontext).load(dataSet.get(listPosition).getPimage()).placeholder(R.drawable.defaultimg).into(holder.menuimage);
        holder.title.setText(dataSet.get(listPosition).getPname());
        holder.txtquantity.setText("" + dataSet.get(listPosition).getQty());
        holder.cost.setText("" + dataSet.get(listPosition).getProductprice() + " SAR");

        if (dataSet.get(listPosition).getIsstock() == 0) {
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StaticInfo.cartdatachanged = true;
                    rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(),0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), 0, false, false, true);
                    dataSet.remove(listPosition);
                    notifyDataSetChanged();
                    if(dataSet.size()==0){
                        rohahomeDashboradActivity.sessionManager.setCartcount(0);
                        rohahomeDashboradActivity.cartBadge.setVisibility(View.GONE);
                        Fragment fragment = new CartFragment();
                        rohahomeDashboradActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                    }
                   /* JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id",dataSet.get(listPosition).getCartid());
                        deletecartrequestJSON(requestbody);
                        sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }
            });
            holder.cardview.setAlpha(0.4f);
            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rohahomeDashboradActivity.sessionManager.snackbarToast(mcontext.getString(R.string.itemnotavailable), holder.cardview);
                }
            });
        } else {
            holder.imgplus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dataSet.get(listPosition).getStockquantity() > Integer.parseInt(holder.txtquantity.getText().toString())) {
                        StaticInfo.cartdatachanged = true;
                        rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(),0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), Integer.parseInt(holder.txtquantity.getText().toString()) + 1, false, true, false);
                        holder.txtquantity.setText("" + (Integer.parseInt(holder.txtquantity.getText().toString()) + 1));
                        cartFragment.subtotal = cartFragment.subtotal + dataSet.get(listPosition).getProductprice();
                        cartFragment.fqty = cartFragment.fqty + 1;
                        cartFragment.vat = ((cartFragment.subtotal / 100.0f) * StaticInfo.vat);
                        double totalamt = (cartFragment.subtotal + cartFragment.vat);
                        cartFragment.ordersubtotal.setText("" + String.format(Locale.ENGLISH, "%.2f", cartFragment.subtotal) + " SAR");
                        cartFragment.totalmount.setText("" + String.format(Locale.ENGLISH, "%.2f", totalamt) + " SAR");
                        cartFragment.vat_txt.setText("" + String.format(Locale.ENGLISH, "%.2f", cartFragment.vat) + " SAR");

                /*  //  holder.txtquantity.setText(""+(dataSet.get(listPosition).getQty()+1));
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id",dataSet.get(listPosition).getCartid());
                        requestbody.put("quantity",dataSet.get(listPosition).getQty()+1);
                        requestbody.put("cost",(dataSet.get(listPosition).getQty()+1)*dataSet.get(listPosition).getProductprice());
                        updatecartrequestJSON(requestbody);
                       // sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    } else {
                        rohahomeDashboradActivity.sessionManager.snackbarToast(mcontext.getString(R.string.stocknotavailable), holder.imgplus);
                    }
                }
            });
            holder.imgminus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dataSet.get(listPosition).getQty() <= 1) {
                        // session.snackbarToast(cartFragment.getString(R.string.minimumquantityis10),holder.imgminus);
                    } else {
                        StaticInfo.cartdatachanged = true;
                        rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(),0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), Integer.parseInt(holder.txtquantity.getText().toString()) - 1, false, true, false);
                        holder.txtquantity.setText("" + (Integer.parseInt(holder.txtquantity.getText().toString()) - 1));
                        cartFragment.subtotal = cartFragment.subtotal - dataSet.get(listPosition).getProductprice();
                        cartFragment.fqty = cartFragment.fqty - 1;
                        cartFragment.vat = ((cartFragment.subtotal / 100.0f) * StaticInfo.vat);
                        double totalamt = (cartFragment.subtotal + cartFragment.vat);
                        cartFragment.ordersubtotal.setText("" + String.format(Locale.ENGLISH, "%.2f", cartFragment.subtotal) + " SAR");
                        cartFragment.totalmount.setText("" + String.format(Locale.ENGLISH, "%.2f", totalamt) + " SAR");
                        cartFragment.vat_txt.setText("" + String.format(Locale.ENGLISH, "%.2f", cartFragment.vat) + " SAR");

                 /*  // holder.txtquantity.setText(""+(dataSet.get(listPosition).getQty()-1));
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id",dataSet.get(listPosition).getCartid());
                        requestbody.put("quantity",dataSet.get(listPosition).getQty()-1);
                        requestbody.put("cost",(dataSet.get(listPosition).getQty()-1)*dataSet.get(listPosition).getProductprice());
                        updatecartrequestJSON(requestbody);
                       // sessionManager.progressdialogshow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    }
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StaticInfo.cartdatachanged = true;
                    rohahomeDashboradActivity.localcartfunc(dataSet.get(listPosition).getPname(),0, dataSet.get(listPosition).getProductid(), dataSet.get(listPosition).getProductprice(), 0, false, false, true);
                    cartFragment.subtotal = cartFragment.subtotal - (dataSet.get(listPosition).getProductprice() * Integer.parseInt(holder.txtquantity.getText().toString()));
                    cartFragment.fqty = cartFragment.fqty - Integer.parseInt(holder.txtquantity.getText().toString());
                    cartFragment.vat = ((cartFragment.subtotal / 100.0f) * StaticInfo.vat);
                    double totalamt = (cartFragment.subtotal + cartFragment.vat);
                    cartFragment.ordersubtotal.setText("" + String.format(Locale.ENGLISH, "%.2f", cartFragment.subtotal) + " SAR");
                    cartFragment.totalmount.setText("" + String.format(Locale.ENGLISH, "%.2f", totalamt) + " SAR");
                    cartFragment.vat_txt.setText("" + String.format(Locale.ENGLISH, "%.2f", cartFragment.vat) + " SAR");
                    dataSet.remove(listPosition);
                    notifyDataSetChanged();
                    if(dataSet.size()==0){
                        rohahomeDashboradActivity.sessionManager.setCartcount(0);
                        rohahomeDashboradActivity.cartBadge.setVisibility(View.GONE);
                        Fragment fragment = new CartFragment();
                        rohahomeDashboradActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                    }
               /* JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("id",dataSet.get(listPosition).getCartid());
                    deletecartrequestJSON(requestbody);
                   // sessionManager.progressdialogshow();
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

