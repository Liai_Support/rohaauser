package com.lia.yello.roha.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.lia.yello.roha.Fragment.RohahomeHomeFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.HomeproductDataModel;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeproductdataAdapter extends RecyclerView.Adapter<HomeproductdataAdapter.MyViewHolder> {
    private Context mcontext;
    private List<HomeproductDataModel> dataSet;
    private RohahomeDashboradActivity rohahomeDashboardActivity;
    RohahomeHomeFragment rohahomeHomeFragment;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView pname;
        TextView tot_Cost,count;
        public ImageView next,pimage,addcart,plus,minus;
        LinearLayout linear;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.pname = (TextView) itemView.findViewById(R.id.pname);
            this.tot_Cost = (TextView) itemView.findViewById(R.id.tot_Cost);
            this.linear = (LinearLayout) itemView.findViewById(R.id.linear);
            this.pimage = (ImageView) itemView.findViewById(R.id.pimage);
            this.addcart = (ImageView) itemView.findViewById(R.id.addcart);
            this.plus = (ImageView) itemView.findViewById(R.id.plus);
            this.minus = (ImageView) itemView.findViewById(R.id.minus);
            this.count = (TextView) itemView.findViewById(R.id.count);

        }
    }

    public HomeproductdataAdapter(Context context, List<HomeproductDataModel> data, FragmentActivity activity, RohahomeHomeFragment rohahomeHomeFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohahomeDashboardActivity = (RohahomeDashboradActivity) activity;
        this.rohahomeHomeFragment = rohahomeHomeFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_homeproductdata, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        if(!dataSet.get(listPosition).getPname().equalsIgnoreCase("330ml×20pcs")){
            holder.pname.setText(dataSet.get(listPosition).getPname());
            holder.tot_Cost.setText(dataSet.get(listPosition).getPrice()+" SAR");
            Glide.with(mcontext).load(dataSet.get(listPosition).getPimage()).placeholder(R.drawable.defaultimg).into(holder.pimage);
            if(dataSet.get(listPosition).getStockquantity()==0 || dataSet.get(listPosition).getStockquantity()<10){
                holder.linear.setAlpha(0.4f);
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rohahomeDashboardActivity.sessionManager.snackbarToast(mcontext.getString(R.string.stocknotavailable),holder.linear);
                    }
                });
            }
            else {
                holder.plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.parseInt(holder.count.getText().toString()) == 0 && dataSet.get(listPosition).getStockquantity()>Integer.parseInt(holder.count.getText().toString())) {
                            JSONObject requestbody = new JSONObject();
                            /* requestbody.put("productid", dataSet.get(listPosition).getId());
                             requestbody.put("userid", rohahomeDashboardActivity.sessionManager.getUserId());
                             requestbody.put("branchid", rohahomeDashboardActivity.branchid);
                             requestbody.put("quantity", "1");
                             requestbody.put("cost", (dataSet.get(listPosition).getPrice() * 1));
                             addtocartrequestJSON(requestbody, holder.count);*/
                            StaticInfo.cartdatachanged = true;
                            rohahomeDashboardActivity.localcartfunc(dataSet.get(listPosition).getPname(),0,dataSet.get(listPosition).getId(),dataSet.get(listPosition).getPrice(),1,true,false,false);
                            holder.count.setText("1");
                        }
                        else if(dataSet.get(listPosition).getStockquantity() > Integer.parseInt(holder.count.getText().toString())) {
                        /*
                            for (int i = 0; i < rohahomeDashboardActivity.sessionManager.getlocalcart().length(); i++) {
                                JSONObject jsonObject1 = null;
                                try {
                                    jsonObject1 = rohahomeDashboardActivity.sessionManager.getlocalcart().getJSONObject(i);
                                    if (jsonObject1.getInt("productid") == dataSet.get(listPosition).getId()) {
                                        JSONObject requestbody = new JSONObject();
                                        try {
                                            requestbody.put("id", jsonObject1.getInt("cart_id"));
                                            requestbody.put("quantity", Integer.parseInt(holder.count.getText().toString()) + 1);
                                            requestbody.put("cost", (Integer.parseInt(holder.count.getText().toString()) + 1) * dataSet.get(listPosition).getPrice());
                                            updatecartrequestJSON(requestbody, holder.count, true);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
*/
                            StaticInfo.cartdatachanged = true;
                            rohahomeDashboardActivity.localcartfunc(dataSet.get(listPosition).getPname(),0,dataSet.get(listPosition).getId(),dataSet.get(listPosition).getPrice(),Integer.parseInt(holder.count.getText().toString())+1,false,true,false);
                            holder.count.setText(""+(Integer.parseInt(holder.count.getText().toString())+1));
                        }
                        else {
                            rohahomeDashboardActivity.sessionManager.snackbarToast(mcontext.getString(R.string.stocknotavailable),holder.linear);
                        }
                    }
                });
                holder.minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(Integer.parseInt(holder.count.getText().toString()) != 0){
                            if(Integer.parseInt(holder.count.getText().toString()) == 1){
                            /*
                            for (int i = 0; i < rohahomeDashboardActivity.sessionManager.getlocalcart().length(); i++) {
                                JSONObject jsonObject1 = null;
                                try {
                                    jsonObject1 = rohahomeDashboardActivity.sessionManager.getlocalcart().getJSONObject(i);
                                    if (jsonObject1.getInt("productid")==dataSet.get(listPosition).getId()) {
                                        JSONObject requestbody = new JSONObject();
                                        try {
                                            requestbody.put("id",jsonObject1.getInt("cart_id"));
                                            deletecartrequestJSON(requestbody,holder.count);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
*/
                                StaticInfo.cartdatachanged = true;
                                rohahomeDashboardActivity.localcartfunc(dataSet.get(listPosition).getPname(),0,dataSet.get(listPosition).getId(),dataSet.get(listPosition).getPrice(),0,false,false,true);
                                holder.count.setText("0");
                            }
                            else {
                            /*
                                                        for (int i = 0; i < rohahomeDashboardActivity.sessionManager.getlocalcart().length(); i++) {
                                                            JSONObject jsonObject1 = null;
                                                            try {
                                                                jsonObject1 = rohahomeDashboardActivity.sessionManager.getlocalcart().getJSONObject(i);
                                                                if (jsonObject1.getInt("productid")==dataSet.get(listPosition).getId()) {
                                                                    JSONObject requestbody = new JSONObject();
                                                                    try {
                                                                        requestbody.put("id",jsonObject1.getInt("cart_id"));
                                                                        requestbody.put("quantity",Integer.parseInt(holder.count.getText().toString())-1);
                                                                        requestbody.put("cost",(Integer.parseInt(holder.count.getText().toString())-1)*dataSet.get(listPosition).getPrice());
                                                                        updatecartrequestJSON(requestbody,holder.count,false);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                            catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                            */
                                StaticInfo.cartdatachanged = true;
                                rohahomeDashboardActivity.localcartfunc(dataSet.get(listPosition).getPname(),0,dataSet.get(listPosition).getId(),dataSet.get(listPosition).getPrice(),Integer.parseInt(holder.count.getText().toString())-1,false,true,false);
                                holder.count.setText(""+(Integer.parseInt(holder.count.getText().toString())-1));
                            }
                        }
                    }
                });
            }

            if(rohahomeHomeFragment.sessionManager.getlocalcart()!=null && rohahomeHomeFragment.sessionManager.getlocalcart().length()!=0){
                for (int i = 0; i < rohahomeHomeFragment.sessionManager.getlocalcart().length(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    try {
                        jsonObject1 = rohahomeHomeFragment.sessionManager.getlocalcart().getJSONObject(i);
                        if (jsonObject1.getInt("productid")==dataSet.get(listPosition).getId()) {
                            holder.addcart.setImageTintList(ColorStateList.valueOf(Color.GREEN));
                            holder.count.setText(""+jsonObject1.getInt("productquantity"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else {
            holder.linear.setVisibility(View.GONE);
        }


    }

    private void addtocartrequestJSON(JSONObject response, TextView counttxt) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.addcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("addtocartresponse", ">>" + response);
              //  rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        //  rohahomeDashboardActivity.listcart();
                        counttxt.setText("1");
                        rohahomeDashboardActivity.listcart();

                        //addcart.setImageTintList(ColorStateList.valueOf(Color.GREEN));
                        // rohahomeDashboardActivity.session.snackbarToast(obj.getString("message"),rohahomeDashboardActivity.botm_nav);
                    }
                    else {
                        counttxt.setText("1");
                        rohahomeDashboardActivity.sessionManager.snackbarToast(obj.getString("message"),rohahomeDashboardActivity.botm_nav);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
               // rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                rohahomeDashboardActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void deletecartrequestJSON(JSONObject response, TextView counttxt) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deletecart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                //rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        counttxt.setText("0");
                        //addcart.setImageTintList(ColorStateList.valueOf(Color.WHITE));
                        rohahomeDashboardActivity.listcart();
                    }
                    else {
                        rohahomeDashboardActivity.sessionManager.snackbarToast(obj.getString("message"),counttxt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                //rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                rohahomeDashboardActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void updatecartrequestJSON(JSONObject response,TextView counttxt,Boolean plus) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updatecart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                //rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        if(plus == true){
                            counttxt.setText(""+(Integer.parseInt(counttxt.getText().toString())+1));
                        }
                        else {
                            counttxt.setText("" + (Integer.parseInt(counttxt.getText().toString()) - 1));
                        }
                    }
                    else {
                        rohahomeDashboardActivity.sessionManager.snackbarToast(obj.getString("message"),counttxt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                //rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                rohahomeDashboardActivity.sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}