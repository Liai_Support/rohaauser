package com.lia.yello.roha.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener {
    Animation animFadeIn;
    private SessionManager sessionManager;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        sessionManager.setIsVersion(false);
        Log.d("gcgcv1",""+getIntent().hasExtra("data"));
        Log.d("gcgcv1",""+getIntent().getStringExtra("data"));
        if(getIntent().hasExtra("data")==true && !sessionManager.gettoken().equals("")) {
            if (getIntent().getStringExtra("data").equalsIgnoreCase("water")) {
                finish();
                Intent intent = new Intent(this, RohaWaterDashboardActivity.class);
                intent.putExtra("fromnotification", true);
                intent.putExtra("page",""+getIntent().getStringExtra("data"));
                startActivity(intent);
            } else if (getIntent().getStringExtra("data").equalsIgnoreCase("home")) {
                finish();
                Intent intent = new Intent(this, RohahomeDashboradActivity.class);
                intent.putExtra("fromnotification", true);
                intent.putExtra("page",""+getIntent().getStringExtra("data"));
                startActivity(intent);
            }else if (getIntent().getStringExtra("data").equalsIgnoreCase("offer")) {
                finish();
                Intent intent = new Intent(this, RohahomeDashboradActivity.class);
                intent.putExtra("fromnotification", true);
                intent.putExtra("page",""+getIntent().getStringExtra("data"));
                startActivity(intent);
            }
        }
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
        }
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fade_in);
        animFadeIn.setAnimationListener(this);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(animFadeIn);
      //  startService(new Intent(getBaseContext(), OnClearFromRecentService.class));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(getIntent().hasExtra("data")==true) {
            if (getIntent().getStringExtra("data").equalsIgnoreCase("water")) {
                finish();
                Intent intent1 = new Intent(this, RohaWaterDashboardActivity.class);
                intent1.putExtra("fromnotification", true);
                startActivity(intent1);
            } else if (getIntent().getStringExtra("data").equalsIgnoreCase("home")) {
                finish();
                Intent intent2 = new Intent(this, RohahomeDashboradActivity.class);
                intent2.putExtra("fromnotification", true);
                startActivity(intent2);
            }
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public void onAnimationStart(Animation animation) {
        //under Implementation
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Intent i = new Intent(SplashActivity.this, LanguageActivity.class);
        startActivity(i);
        this.finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        //under Implementation
    }


    private void profileviewrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.UnregisteredToken, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        String msg = obj.getString("data");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


}
