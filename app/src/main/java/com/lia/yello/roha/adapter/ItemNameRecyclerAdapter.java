package com.lia.yello.roha.adapter;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.HelpFragment;
import com.lia.yello.roha.Fragment.NewInvoice;
import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.FaqDataModel;
import com.lia.yello.roha.model.ListorderDatamodel2;
import com.lia.yello.roha.model.SummaryDataModel1;
import com.lia.yello.roha.utility.LocalData;

import java.util.ArrayList;
import java.util.List;

public class ItemNameRecyclerAdapter extends RecyclerView.Adapter<ItemNameRecyclerAdapter.MyViewHolder> {
private Context mcontext;
    public ArrayList<String> noverallmarkerstitle =LocalData.getInstance().getOverallMarkersTitle();

private NewInvoice newInvoice;
    private List<ListorderDatamodel2> dataSet;
    String iswish;


public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView name, quantity,price,reclervat;
    public MyViewHolder(View itemView) {
        super(itemView);
        this.name = (TextView) itemView.findViewById(R.id.itemname);
        this.quantity = (TextView) itemView.findViewById(R.id.quantity);
        this.price = (TextView) itemView.findViewById(R.id.price);
        this.reclervat = (TextView) itemView.findViewById(R.id.reclervat);
    }
}
    public ItemNameRecyclerAdapter(Context context, ArrayList<ListorderDatamodel2> data, String iswish, NewInvoice newInvoice) {
        this.dataSet = data;
        this.mcontext = context;
        this.iswish = iswish;
        this.newInvoice = newInvoice;
    }



    @Override
    public ItemNameRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemrecycler, parent, false);
        ItemNameRecyclerAdapter.MyViewHolder myViewHolder = new ItemNameRecyclerAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemNameRecyclerAdapter.MyViewHolder holder, int listPosition) {
        Log.d(TAG, "onBindViewHolder: "+listPosition);

            holder.name.setText("Water and Delivery" );
            holder.quantity.setText("" + dataSet.get(listPosition).getCartoncount() + " " + mcontext.getString(R.string.quantity));


        Log.d("wertyuiop","nvbvbbvb");

        holder.price.setText(dataSet.get(listPosition).getUnitprice());
        if (dataSet.get(listPosition).getDiscountvalue().equals("")) {
            Double total = Double.valueOf(dataSet.get(listPosition).getUnitprice()) * Double.valueOf(dataSet.get(listPosition).getCartoncount()) ;
            holder.reclervat.setText(""+total);

        }else{
            Double total = Double.valueOf(dataSet.get(listPosition).getUnitprice()) * Double.valueOf(dataSet.get(listPosition).getCartoncount());

            holder.reclervat.setText(""+total);
        }
        // itemname.setText(itemnamee);
       /* newInvoice.coste= Double.valueOf(newInvoice.subtotall);
        newInvoice.vattt= Double.valueOf(newInvoice.vatt);
        holder.quantity.setText(newInvoice.ttquantity);

        newInvoice.finalcost=newInvoice.coste+newInvoice.vattt;
        newInvoice.totalprice= String.valueOf(newInvoice.finalcost);
        holder.reclervat.setText(newInvoice.totalprice);*/


    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}


