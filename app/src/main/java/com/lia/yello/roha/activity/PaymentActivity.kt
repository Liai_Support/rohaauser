package com.lia.yello.roha.activity

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.button.MaterialButton
import com.google.gson.Gson
import com.lia.yello.roha.Fragment.SuccessFragment
import com.lia.yello.roha.R
import com.lia.yello.roha.utility.LocalData
import com.lia.yello.roha.utility.SessionManager
import com.lia.yello.roha.utility.StaticInfo
import com.moyasar.android.sdk.PaymentConfig
import com.moyasar.android.sdk.PaymentResult
import com.moyasar.android.sdk.PaymentSheet
import com.moyasar.android.sdk.payment.models.Payment
import com.myfatoorah.sdk.entity.executepayment.MFExecutePaymentRequest
import com.myfatoorah.sdk.entity.initiatesession.MFInitiateSessionRequest
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse
import com.myfatoorah.sdk.utils.MFAPILanguage
import com.myfatoorah.sdk.utils.MFCountry
import com.myfatoorah.sdk.utils.MFEnvironment
import com.myfatoorah.sdk.views.MFResult
import com.myfatoorah.sdk.views.MFSDK
import com.myfatoorah.sdk.views.embeddedpayment.MFPaymentCardView
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*


class PaymentActivity : AppCompatActivity() {
    var paymentSheet: PaymentSheet? = null
    private var sessionManager: SessionManager? = null
    var view: View? = null
     var spinner: ProgressBar? = null

    var from: String = ""
    val payAmout: Double = LocalData.getInstance().paymentamount.toDouble()*100
    val config = PaymentConfig(
        amount = payAmout.toInt(),
        currency = "SAR",
        description = "Order Payment",
        apiKey = "pk_live_XuG25DzPQviW5WXiY42ukaN5QoM9ffFpmcXauRHu"
    )
    private lateinit var successFragment: SuccessFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        view = findViewById<View>(R.id.container) as ConstraintLayout

        spinner = findViewById<View>(R.id.progressBar1) as ProgressBar

        sessionManager = SessionManager(this, this, view)
        //Log.d(TAG, "onCreate: $pay")
        // set up your My Fatoorah Merchant details
        MFSDK.init("rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL",
            MFCountry.SAUDI_ARABIA,
            MFEnvironment.TEST)
        // You can custom your action bar, but this is optional not required to set this line
        MFSDK.setUpActionBar("Dover", R.color.colorPrimary, R.color.white, true)
        // To hide action bar
        // MFSDK.setUpActionBar(isShowToolBar = false)
    val request = MFInitiateSessionRequest( customerIdentifier = sessionManager!!.userId.toString() )
//    MFSDK.initiateSession(request)
      val  cardview = findViewById<View>(R.id.mfPaymentView) as MFPaymentCardView
        MFSDK.initiateSession(request) {
            when (it) {
                is MFResult.Success -> {
                    cardview.load(it.response)
                    spinner!!.visibility = View.GONE
                }
                is MFResult.Fail -> {
                    Log.d(TAG, "Fail: " + Gson().toJson(it.error))
                }
                else -> {
                    Log.d(TAG, "Fail: ")
                }
            }
        }
        // initiatePayment

        /*val request = MFInitiatePaymentRequest(0.100, MFCurrencyISO.SAUDI_ARABIA_SAR)
        MFSDK.initiatePayment(
            request,
            MFAPILanguage.EN
        ) { result: MFResult<MFInitiatePaymentResponse> ->
            when (result) {
                is MFResult.Success ->
                    Log.d(TAG, "Response: " + Gson().toJson(result.response))
                is MFResult.Fail ->
                    Log.d(TAG, "Fail: " + Gson().toJson(result.error))
                else -> { Log.d(TAG, "Fail: " )}
            }
        }*/



        val ok= findViewById<View>(R.id.ok) as MaterialButton
        val pay= findViewById<View>(R.id.pay) as MaterialButton
        val cancel= findViewById<View>(R.id.cancel) as MaterialButton
        sessionManager = SessionManager(this, this, view)
        getIntentValues()
//        paymentSheet = PaymentSheet(this, { handlePaymentResult(it) }, config)
//        paymentSheet!!.present()
        ok.setOnClickListener {
            paymentSheet!!.present()
        }
        pay.setOnClickListener {
            spinner!!.visibility = View.VISIBLE

            val request = MFExecutePaymentRequest(payAmout)


            Log.d(TAG, "onCreate: "+sessionManager!!.getlang().toString().uppercase())
            cardview.pay(
                this,
                request,
                MFAPILanguage.EN,
                onInvoiceCreated = {
                    Log.d(TAG, "invoiceId: $it")
                }
            ) { invoiceId: String, result: MFResult<MFGetPaymentStatusResponse> ->
                spinner!!.visibility = View.GONE

                when (result) {
                    is MFResult.Success -> {
                        Log.d(TAG, "Response: " + Gson().toJson(result.response))
                        if (from == "order") {
                            LocalData.getInstance().transactionid =
                                result.response.invoiceTransactions!![0].paymentId
                            UpdatePlaceOrderBody("success")

                        }else if (from == "wallet"){
                            LocalData.getInstance().transactionid = result.response.invoiceTransactions!![0].paymentId
                            addmoneytowallet(LocalData.getInstance().paymentamount,
                                LocalData.getInstance().transactionid)
                        }
                    }
                    is MFResult.Fail -> {
                        // Handle error
                        val mainIntent = Intent(this,
                            FailedActivity::class.java)
                        if (from.equals("order")){
                            mainIntent.putExtra("from","order")
                        }else if (from == "wallet"){
                            mainIntent.putExtra("from","wallet")
                        }
                        startActivity(mainIntent)
                        Log.d(TAG, "Fail: " + Gson().toJson(result.error))
                    }
                    else -> { Log.d(TAG, "Fail: ") }
                }
                Log.d(TAG, "invoiceId: $invoiceId")
            }
        }
        cancel.setOnClickListener {
            onBackPressed()
        }


    }
    private fun getIntentValues() {

        intent.extras?.let {

            from = it.getString("from").toString()

        }
    }



        private fun UpdatePlaceOrderBody(status: String) {
            val requestbody = JSONObject()
            try {
                requestbody.put("insertid", sessionManager!!.getinsertid())
                if (status == "failure") {
                    requestbody.put("payment_status", "failure")
                } else {
                    requestbody.put("payment_status", "success")
                }
                UpdatePlaceOrderJSON(requestbody)
                sessionManager!!.progressdialogshow()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        private fun UpdatePlaceOrderJSON(response: JSONObject) {
            val requestQueue = Volley.newRequestQueue(this)
            val requestBody = response.toString()
            Log.d("profile", response.toString())
            val stringRequest: StringRequest =
                object : StringRequest(Method.POST, StaticInfo.updateplaceorder,
                    Response.Listener { response ->
                        sessionManager?.progressdialogdismiss()
                        Log.d("updateplaceorderresponse", ">>$response")
                        try {
                            val obj = JSONObject(response)
                            val msg = obj.getString("message")
                            if (obj.getBoolean("error") == true) {
                                 Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                            } else {
                                val intent = Intent(this, CardSucessActivity::class.java)
                                val bundle = Bundle()
                                bundle.putBoolean("ibandata", true)
                                Log.d(TAG, "handleCompletedPayment: " + bundle)
                                startActivity(intent, bundle)                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error -> //                sessionManager.volleyerror(error);
                        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                        sessionManager!!.progressdialogdismiss()
                    }) {
                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
    //                int id = sessionManager.getid();
                        //                params.put("lang",sessionManager.getlang() );
                        return HashMap()
                    }

                    override fun getBodyContentType(): String {
                        return "application/json; charset=utf-8"
                    }

                    @Throws(AuthFailureError::class)
                    override fun getBody(): ByteArray? {
                         try {
                            return if (requestBody == null) null else requestBody.toByteArray(charset("utf-8"))
                        } catch (uee: UnsupportedEncodingException) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                                requestBody,
                                "utf-8")
                             return null
                        }

                    }
                }
            stringRequest.retryPolicy =
                DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            Log.d("asdsdjkxdk", "" + stringRequest)
            requestQueue.add(stringRequest)
        }

        private fun addmoneytowallet(amt: String, tid: String) {
            //String s= id.getText().toString();
            val requestbody = JSONObject()
            try {
                requestbody.put("amount", amt)
                requestbody.put("userid", sessionManager!!.userId)
                requestbody.put("transaction_id", tid)
                addamountrequestJSON(requestbody)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        private fun addamountrequestJSON(requestbody: JSONObject) {
            val requestQueue = Volley.newRequestQueue(this)
            val requestBody = requestbody.toString()
            Log.d("reques34", requestbody.toString())
            val stringRequest: StringRequest = object : StringRequest(Method.POST, StaticInfo.addwallet,
                Response.Listener { response ->
                    LocalData.getInstance().paymentamount = ""
                    LocalData.getInstance().transactionid = ""
                    LocalData.getInstance().transactiontype = ""
                    Log.d("walletresponse", ">>$response")
                    try {
                        val obj = JSONObject(response)
                        if (obj.getBoolean("error") == true) {
                            val msg = obj.getString("message")
                            // refundrequest(amt,transactionid);
                            Log.d("walleterror", "" + msg)
                        } else {
                            val tok = JSONObject(obj.getString("data"))
    //                        show_amt.setText(String.format(Locale.ENGLISH,
    //                            "%.2f",
    //                            tok.getDouble("amount")) + " SAR")
                            sessionManager!!.setwalletamount(String.format(Locale.ENGLISH,
                                "%.2f",
                                tok.getDouble("amount")).toDouble())
                            // sessionManager.showNotification3(getString(R.string.app_name),getString(R.string.walletisupdated));
                            getwalletamount()
    //                        getwallethistory()
                            /* JSONObject requestbody = new JSONObject();
                                  try {
                                      requestbody.put("userid", session.getUserId());
                                      getwalletamountJSON(requestbody);
                                  } catch (JSONException e) {
                                      e.printStackTrace();
                                  }*/
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                    LocalData.getInstance().paymentamount = ""
                    LocalData.getInstance().transactionid = ""
                    LocalData.getInstance().transactiontype = ""
                    //refundrequest(amt,transactionid);
                    sessionManager!!.volleyerror(error)
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val id = sessionManager!!.userId
                    //params.put("Content-Type","application/json" );
                    return java.util.HashMap()
                }

                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                @Throws(AuthFailureError::class)
                override fun getBody(): ByteArray? {
                    try {
                        return if (requestBody == null) null else requestBody.toByteArray(charset("utf-8"))
                    } catch (uee: UnsupportedEncodingException) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody,
                            "utf-8")
                        return null
                    }

                }
            }
            stringRequest.retryPolicy =
                DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            Log.d("asdsdjkxdk", "" + stringRequest)
            requestQueue.add(stringRequest)
        }

        private fun getwalletamountJSON(requestbody: JSONObject) {
            val requestQueue = Volley.newRequestQueue(this)
            val requestBody = requestbody.toString()
            Log.d("reques34", requestbody.toString())
            val stringRequest: StringRequest = object : StringRequest(Method.POST, StaticInfo.getwallet,
                Response.Listener { response ->
                    Log.d("walletresponse", ">>$response")
                    try {
                        val obj = JSONObject(response)
                        if (obj.getBoolean("error") == true) {
                            val msg = obj.getString("message")
                            Log.d("walleterror", "" + msg)
                        } else {
                            val msg = obj.getString("data")
                            val tok = JSONObject(msg)
                            tok.getString("userid")
                            tok.getString("amount")
    //                        show_amt.setText(String.format(Locale.ENGLISH,
    //                            "%.2f",
    //                            tok.getDouble("amount")) + " SAR")
                            sessionManager!!.setwalletamount(String.format(Locale.ENGLISH,
                                "%.2f",
                                tok.getDouble("amount")).toDouble())
                            Log.d("fdvdf", "" + sessionManager!!.getwalletamount())
                            finish()

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                    sessionManager!!.volleyerror(error)
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val id = sessionManager!!.userId
                    //params.put("Content-Type","application/json" );
                    return java.util.HashMap()
                }

                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                @Throws(AuthFailureError::class)
                override fun getBody(): ByteArray? {
                    try {
                        return if (requestBody == null) null else requestBody.toByteArray(charset("utf-8"))
                    } catch (uee: UnsupportedEncodingException) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody,
                            "utf-8")
                        return null
                    }

                }
            }
            stringRequest.retryPolicy =
                DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            Log.d("asdsdjkxdk", "" + stringRequest)
            requestQueue.add(stringRequest)
        }

    /*  private void payment(String amt) {
     //  String profileId = StaticInfo.profileId;
      //  String serverKey = StaticInfo.serverKey;
      //  String clientKey = StaticInfo.clientKey;
        //live
        String profileId = "55528";
        String serverKey = "SKJNMT99TL-JBKLHDJJB2-NMDJ9KDMD9";
        String clientKey = "CHKMTG-MNDV62-KBHD66-PNQ6H9";

        //test
      //  String profileId = "55181";
       // String serverKey = "SGJNMT99DR-JBKLHDKBWR-JZ2RJ6MWBH";
       // String clientKey = "C6KMTG-MNBM62-KBHDK2-6NKB6Q";

        //Log.d("dvd",""+profileId+serverKey+clientKey);
        PaymentSdkLanguageCode locale = PaymentSdkLanguageCode.EN;
        String screenTitle = "Dover";
        String cartId = "123456";
        String cartDesc = "Add Wallet Amount";
        String currency = "SAR";
        double amount = Double.parseDouble(amt);
        PaymentSdkTokenise tokeniseType = PaymentSdkTokenise.NONE;
        PaymentSdkTokenFormat tokenFormat = new PaymentSdkTokenFormat.Hex32Format();
        PaymentSdkTransactionType transType = PaymentSdkTransactionType.SALE;
        List<PaymentSdkApms> type = Collections.singletonList(PaymentSdkApms.STC_PAY);
        PaymentSdkBillingDetails billingData = new PaymentSdkBillingDetails(
                sessionManager.getccity(),
                "SA",
                sessionManager.getusermail(),
                sessionManager.getusername(),
                sessionManager.getuserphoneno(),
                "state",
                sessionManager.getcaddr(),
                "zip"
        );
        PaymentSdkShippingDetails shippingData = new PaymentSdkShippingDetails(
                sessionManager.getccity(),
                "SA",
                sessionManager.getusermail(),
                sessionManager.getusername(),
                sessionManager.getuserphoneno(),
                "state",
                sessionManager.getcaddr(),
                "zip"
        );
        PaymentSdkConfigurationDetails configData = new PaymentSdkConfigBuilder(profileId, serverKey, clientKey, amount, currency)
                .setCartDescription(cartDesc)
                .setLanguageCode(locale)
                .setBillingData(billingData)
                .setMerchantCountryCode("SA") // ISO alpha 2
                .setShippingData(shippingData)
                .setCartId(cartId)
               // .setAlternativePaymentMethods(type)
                .showBillingInfo(false)
                .showShippingInfo(false)
                .forceShippingInfo(false)
                .setScreenTitle(screenTitle)
                .build();
        Log.d("dcd",""+configData);
        PaymentSdkActivity.startCardPayment(getActivity(), configData, this);
       // PaymentSdkActivity.startAlternativePaymentMethods(getActivity(), configData, this);

    }
    @Override
    public void onError(@NotNull PaymentSdkError paymentSdkError) {
        Log.d("dfghewe",""+paymentSdkError);
        Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onPaymentCancel() {
        Log.d("dfghewe",""+"paymentSdkError");
        Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onPaymentFinish(@NotNull PaymentSdkTransactionDetails paymentSdkTransactionDetails) {
        Log.d("dfgh333",""+paymentSdkTransactionDetails.component8().component1());
        Log.d("dfgh333",""+paymentSdkTransactionDetails.component8().component2());

        Log.d("dfgh33gf3",""+paymentSdkTransactionDetails.component1());
        if(paymentSdkTransactionDetails.component8().component2().equalsIgnoreCase("Authorised") || paymentSdkTransactionDetails.component8().component1().equals(0)){
            if(paymentSdkTransactionDetails.component1() == null){
                Toast.makeText(getContext(), "Transaction Id Null", Toast.LENGTH_SHORT).show();
            }
            else {
                transactionid = paymentSdkTransactionDetails.component1();
               // refundrequest(amt,transactionid);
                addmoneytowallet(amt,paymentSdkTransactionDetails.component1());
            }
        }
        else {
            Toast.makeText(getContext(), ""+getString(R.string.paymentfailed)+"-"+paymentSdkTransactionDetails.component8().component2(), Toast.LENGTH_SHORT).show();
        }
    }

    private void refundrequest(String amt,String tid) {
        //String s= id.getText().toString();
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("profile_id", StaticInfo.profileId);
            requestbody.put("tran_type", "refund");
            requestbody.put("tran_class", "ecom");
            requestbody.put("cart_id", "123456");
            requestbody.put("cart_currency", "SAR");
            requestbody.put("cart_amount", amt);
            requestbody.put("cart_description", "API VOLLY PROBLEM");
            requestbody.put("tran_ref", tid);
            refundamountrequestJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void refundamountrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.refundrequesturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("refunfresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject result = obj.getJSONObject("payment_result");
                    if(result == null){
                        Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                        Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (result.getString("response_status").equalsIgnoreCase("A")) {
                            Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                            sessionManager.snackbarToast(getString(R.string.refundrequestsuccess), add_money);
                        } else {
                            Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                            Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                if(error instanceof NoConnectionError){
                    sessionManager.slownetwork(getActivity());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("authorization",StaticInfo.refundserverKey);
                Log.d("vgcd",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }*/
        fun getwalletamount() {
            val requestbody = JSONObject()
            try {
                requestbody.put("userid", sessionManager!!.userId)
                getwalletamountJSON(requestbody)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }





    override fun onBackPressed() {
        val mainIntent = Intent(this,
            RohaWaterDashboardActivity::class.java)
        startActivity(mainIntent)
    }

    fun failedActivity(){
        val mainIntent = Intent(this,
            FailedActivity::class.java)
        startActivity(mainIntent)
    }


}


