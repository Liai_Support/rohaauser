package com.lia.yello.roha.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.SummaryDataModel1;
import com.lia.yello.roha.model.SummaryDataModel2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RohawaterSummaryAdapter1 extends RecyclerView.Adapter<RohawaterSummaryAdapter1.MyViewHolder> {
    private Context mcontext;
    private List<SummaryDataModel1> dataSet;
    public boolean isClickable = true;
    public ArrayList<String> nbranchname = new ArrayList<>();
    public ArrayList<Integer> nvaluecount = new ArrayList<>();
    private RohaWaterOrderSummaryFragment rohaWaterOrderSummaryFragment;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        ImageView plus,minus,close;
        TextView value;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.plus = (ImageView) itemView.findViewById(R.id.plus);
            this.minus = (ImageView) itemView.findViewById(R.id.minus);
            this.value = (TextView) itemView.findViewById(R.id.value);
            this.close = (ImageView) itemView.findViewById(R.id.close);
        }
    }

    public RohawaterSummaryAdapter1(Context context, List<SummaryDataModel1> data, RohaWaterOrderSummaryFragment rohaWaterOrderSummaryFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.rohaWaterOrderSummaryFragment = rohaWaterOrderSummaryFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_water_summary1, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        final JSONObject mosquedetobject = new JSONObject();
        final TextView textViewName = holder.textViewName;
        textViewName.setText(dataSet.get(listPosition).getTitle());
        final int balance = rohaWaterOrderSummaryFragment.totcartonqty % dataSet.size();
        final int value = rohaWaterOrderSummaryFragment.totcartonqty / dataSet.size();
        if (listPosition==0){
            String bname = dataSet.get(listPosition).getBname();
            int bcount = 0;
            for(int i=0;i<dataSet.size();i++){
                if(dataSet.get(i).getBname().equalsIgnoreCase(bname)){
                    bcount++;
                }
            }
            if(dataSet.get(listPosition).getBstock() >= ((value*bcount)+balance)){
                holder.value.setText(String.valueOf(value+balance));
                rohaWaterOrderSummaryFragment.FinaltotCartqty = rohaWaterOrderSummaryFragment.FinaltotCartqty+Integer.parseInt(holder.value.getText().toString());
                nbranchname.add(dataSet.get(listPosition).getBname());
                nvaluecount.add(Integer.parseInt(holder.value.getText().toString()));
                dataSet.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
            }
            else {
                int nvalue = dataSet.get(listPosition).getBstock()/bcount;
                holder.value.setText(String.valueOf(nvalue));
                rohaWaterOrderSummaryFragment.FinaltotCartqty = rohaWaterOrderSummaryFragment.FinaltotCartqty+Integer.parseInt(holder.value.getText().toString());
                nbranchname.add(dataSet.get(listPosition).getBname());
                nvaluecount.add(Integer.parseInt(holder.value.getText().toString()));
                dataSet.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
            }
        }
        else {
            String bname = dataSet.get(listPosition).getBname();
            int bcount = 0;
            for(int i=0;i<dataSet.size();i++){
                if(dataSet.get(i).getBname().equalsIgnoreCase(bname)){
                    bcount++;
                }
            }
            if(dataSet.get(listPosition).getBstock() >= value*bcount){
                holder.value.setText(String.valueOf(value));
                rohaWaterOrderSummaryFragment.FinaltotCartqty = rohaWaterOrderSummaryFragment.FinaltotCartqty+Integer.parseInt(holder.value.getText().toString());
                nbranchname.add(dataSet.get(listPosition).getBname());
                nvaluecount.add(Integer.parseInt(holder.value.getText().toString()));
                dataSet.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
            }
            else {
                int nvalue = dataSet.get(listPosition).getBstock()/bcount;
                holder.value.setText(String.valueOf(nvalue));
                rohaWaterOrderSummaryFragment.FinaltotCartqty = rohaWaterOrderSummaryFragment.FinaltotCartqty+Integer.parseInt(holder.value.getText().toString());
                nbranchname.add(dataSet.get(listPosition).getBname());
                nvaluecount.add(Integer.parseInt(holder.value.getText().toString()));
                dataSet.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
            }
        }

        if (rohaWaterOrderSummaryFragment.sessionManager.getIsWish() == 0){
            holder.close.setVisibility(View.GONE);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isClickable == true){
                    String bname = dataSet.get(listPosition).getBname();
                    int tvalue = 0;
                    int bcount = 0;
                    for(int i=0;i<nbranchname.size();i++){
                        if(nbranchname.get(i).equalsIgnoreCase(bname)){
                            bcount++;
                            tvalue = tvalue+nvaluecount.get(i);
                        }
                    }
                    if(dataSet.get(listPosition).getBstock() > tvalue){
                        int count = Integer.parseInt(holder.value.getText().toString().trim());
                        holder.value.setText(String.valueOf(count+1));
                        rohaWaterOrderSummaryFragment.data1.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
                        nvaluecount.set(listPosition,Integer.parseInt(holder.value.getText().toString()));
                        rohaWaterOrderSummaryFragment.FinaltotCartqty++;
                        rohaWaterOrderSummaryFragment.calc();
                        rohaWaterOrderSummaryFragment.secondlay();
                    }
                    else {
                        if(rohaWaterOrderSummaryFragment.sessionManager.getIsWish() == 1){
                            rohaWaterOrderSummaryFragment.sessionManager.snackbarToast(mcontext.getString(R.string.maximumstockvalueisreachedin)+" "+bname,holder.plus);
                        }
                        else {
                            rohaWaterOrderSummaryFragment.sessionManager.snackbarToast(mcontext.getString(R.string.maximumstockvalueisreached),holder.plus);
                        }
                    }
                }
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isClickable == true) {
                    int count = Integer.parseInt(holder.value.getText().toString().trim());
                    if (count > 10) {
                        holder.value.setText(String.valueOf(count-1));
                        rohaWaterOrderSummaryFragment.data1.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
                        nvaluecount.set(listPosition,Integer.parseInt(holder.value.getText().toString()));
                        rohaWaterOrderSummaryFragment.FinaltotCartqty--;
                        rohaWaterOrderSummaryFragment.calc();
                        rohaWaterOrderSummaryFragment.secondlay();
                    }
                    else {
                        rohaWaterOrderSummaryFragment.sessionManager.snackbarToast(mcontext.getString(R.string.minimumquantityis10),holder.plus);
                    }
                }
            }
        });
        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isClickable == true) {
                    if (dataSet.size() > 1) {
                        rohaWaterOrderSummaryFragment.deletedbranch.add(dataSet.get(listPosition).getBname());
                        rohaWaterOrderSummaryFragment.noverallmarkersLatLng.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkerstitle.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkersbranchid.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkersbranchname.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkersbranchstock.remove(listPosition);
                        rohaWaterOrderSummaryFragment.overallmarkersLatLng = rohaWaterOrderSummaryFragment.noverallmarkersLatLng;
                        rohaWaterOrderSummaryFragment.overallmarkerstitle = rohaWaterOrderSummaryFragment.noverallmarkerstitle;
                        rohaWaterOrderSummaryFragment.overallmarkersbranchid = rohaWaterOrderSummaryFragment.noverallmarkersbranchid;
                        rohaWaterOrderSummaryFragment.overallmarkersbranchname = rohaWaterOrderSummaryFragment.noverallmarkersbranchname;
                        rohaWaterOrderSummaryFragment.overallmarkersbranchstock = rohaWaterOrderSummaryFragment.noverallmarkersbranchstock;
                        rohaWaterOrderSummaryFragment.setvalue();
                    } else {
                        rohaWaterOrderSummaryFragment.deletedbranch.add(dataSet.get(listPosition).getBname());
                        rohaWaterOrderSummaryFragment.noverallmarkersLatLng.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkerstitle.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkersbranchid.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkersbranchname.remove(listPosition);
                        rohaWaterOrderSummaryFragment.noverallmarkersbranchstock.remove(listPosition);
                        rohaWaterOrderSummaryFragment.overallmarkersLatLng = rohaWaterOrderSummaryFragment.noverallmarkersLatLng;
                        rohaWaterOrderSummaryFragment.overallmarkerstitle = rohaWaterOrderSummaryFragment.noverallmarkerstitle;
                        rohaWaterOrderSummaryFragment.overallmarkersbranchid = rohaWaterOrderSummaryFragment.noverallmarkersbranchid;
                        rohaWaterOrderSummaryFragment.overallmarkersbranchname = rohaWaterOrderSummaryFragment.noverallmarkersbranchname;
                        rohaWaterOrderSummaryFragment.overallmarkersbranchstock = rohaWaterOrderSummaryFragment.noverallmarkersbranchstock;
                       // rohaWaterOrderSummaryFragment.setvalue();
                        rohaWaterOrderSummaryFragment.getActivity().onBackPressed();
                    }
                }
            }
        });

        /*rohaWaterOrderSummaryFragment.data2.add(new SummaryDataModel2(dataSet.get(listPosition).getTitle(),Integer.parseInt(holder.value.getText().toString()),Integer.parseInt(holder.value.getText().toString())*rohaWaterOrderSummaryFragment.FinalUnitCost,0));
        Log.d("hvbhcxv",""+rohaWaterOrderSummaryFragment.data2.size());
        if(rohaWaterOrderSummaryFragment.noverallmarkerstitle.size() == (listPosition+1)){
            rohaWaterOrderSummaryFragment.adapter2 = new RohawaterSummaryAdapter2(mcontext,rohaWaterOrderSummaryFragment.data2, rohaWaterOrderSummaryFragment);
            rohaWaterOrderSummaryFragment.recyclerView2.setAdapter(rohaWaterOrderSummaryFragment.adapter2);
        }
        */
        if(dataSet.size()==1){
            holder.close.setVisibility(View.INVISIBLE);
        }
        rohaWaterOrderSummaryFragment.data1.get(listPosition).setQty(Integer.parseInt(holder.value.getText().toString()));
        rohaWaterOrderSummaryFragment.secondlay();
        rohaWaterOrderSummaryFragment.calc();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}