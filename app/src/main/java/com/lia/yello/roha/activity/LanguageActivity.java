package com.lia.yello.roha.activity;

import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.lia.yello.roha.BuildConfig;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.CountryData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import co.mobiwise.materialintro.MaterialIntroConfiguration;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.ContentValues.TAG;

import org.json.JSONException;
import org.json.JSONObject;

public class LanguageActivity extends AppCompatActivity {
    private SessionManager sessionManager;
    private TextView arabic, english;
    private static final String INTRO_CARD = "material_intro";
    String currentVersion,onlinever;
    private static final String SHOWCASE_ID = "simple example";
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        view = (FrameLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        Log.i("lang tag", Locale.getDefault().getLanguage() );
        if (sessionManager.isNetworkEnabled() == false) {
            sessionManager.noNetworkDialog(this);
        }
        if(!sessionManager.gettoken().equals("")){
            FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                return;
                            }
                            else {
                                // Get new FCM registration token
                                String token = task.getResult();
                                JSONObject requestbody1 = new JSONObject();
                                try {

                                    requestbody1.put("token", token);
                                    requestbody1.put("type", "android");

                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                updateunRegistereddevicetoken(requestbody1);
                                JSONObject requestbody = new JSONObject();
                                try {
                                    requestbody.put("userid", sessionManager.getUserId());
                                    requestbody.put("token", token);
                                    requestbody.put("type", "android");
                                    updatedevicetoken(requestbody);
                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        }
        if (Locale.getDefault().getLanguage()== "ar" || Locale.getDefault().getLanguage().equals("ar"))  {

                //sessionManager.progressdialogshow();
                sessionManager.setAppLocale("ar");
                Intent intent=new Intent(LanguageActivity.this, LoginNewActivity.class);
                startActivity(intent);
                //sessionManager.progressdialogdismiss();

        }
        else{
                //sessionManager.progressdialogshow();
                sessionManager.setAppLocale("en");
                Intent intent=new Intent(LanguageActivity.this, LoginNewActivity.class);
                startActivity(intent);
                //sessionManager.progressdialogdismiss();

        }

      /*  if (sessionManager.isNetworkEnabled() == false) {
            sessionManager.noNetworkDialog(this);
        }
        else {
            configrequestJSON();
            isCheck();
            isUpdate();
            arabic = (TextView) findViewById(R.id.arabic);
            english = (TextView) findViewById(R.id.english);
            arabic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isCheck() == true){
                        arabic.setEnabled(false);
                        //sessionManager.progressdialogshow();
                        sessionManager.setAppLocale("ar");
                        Intent intent=new Intent(LanguageActivity.this, LoginNewActivity.class);
                        startActivity(intent);
                        //sessionManager.progressdialogdismiss();
                    }
                }
            });
            english.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isCheck() == true){
                        english.setEnabled(false);
                        //sessionManager.progressdialogshow();
                        sessionManager.setAppLocale("en");
                        Intent intent=new Intent(LanguageActivity.this, LoginNewActivity.class);
                        startActivity(intent);
                        //sessionManager.progressdialogdismiss();
                    }
                }
            });
            if(!sessionManager.gettoken().equals("")){
                FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                    return;
                                }
                                else {
                                    // Get new FCM registration token
                                    String token = task.getResult();
                                    JSONObject requestbody1 = new JSONObject();
                                    try {

                                        requestbody1.put("token", token);
                                        requestbody1.put("type", "android");

                                        sessionManager.progressdialogshow();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    updateunRegistereddevicetoken(requestbody1);
                                    JSONObject requestbody = new JSONObject();
                                    try {
                                        requestbody.put("userid", sessionManager.getUserId());
                                        requestbody.put("token", token);
                                        requestbody.put("type", "android");
                                        updatedevicetoken(requestbody);
                                        sessionManager.progressdialogshow();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            }
        }*/
        /*ViewTarget target = new ViewTarget(R.id.english, this);
        new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(target)
                .setContentTitle(R.string.orderdate)
                .setContentText(R.string.orderdate)
                .setStyle(R.style.CustomShowcaseTheme2)
               // .setShowcaseEventListener(this)
                //.replaceEndButton(R.layout.view_custom_button)
                .build();*/

        // showIntro(english, INTRO_CARD, "Please Select your Language Here!!!");
        // presentShowcaseView(1000); // one second delay
    }

    public void isUpdate(){
        commondatarequestJSON();
    }

    private void commondatarequestJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.commonvalues, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("commondataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        sessionManager.progressdialogdismiss();
                        JSONObject obj1 = new JSONObject(obj.getString("data"));
                        StaticInfo.delivery = obj1.getDouble("delivery_charges");
                        StaticInfo.vat = obj1.getDouble("vat");
                        StaticInfo.order_prefix = obj1.getString("order_prefix");
                        StaticInfo.STOREID = obj1.getString("payment_storeid");
                        StaticInfo.KEY = obj1.getString("payment_key");
                        StaticInfo.MODE = obj1.getString("payment_mode");
                        //StaticInfo.payment_api = obj1.getString("payment_api");
                        //StaticInfo.payment_mail = obj1.getString("payment_mail");
                        //StaticInfo.profileId = obj1.getString("profileid");
                        //StaticInfo.serverKey = obj1.getString("serverkey");
                        //StaticInfo.clientKey = obj1.getString("clientkey");
                        if(obj1.getJSONArray("branches").length()!=0 && obj1.getJSONArray("branches")!=null){
                            sessionManager.setbrancharray(obj1.getJSONArray("branches").toString());
                        }
                        else {
                            sessionManager.setbrancharray("");
                        }

                        String ver = obj1.getString("androidappversion");
                        try {
                            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        Log.d("update", "Current version " + currentVersion + "playstore version " + ver);
                        if (Float.valueOf(currentVersion) < Float.valueOf(ver)) {
                            sessionManager.setIsVersion(true);
                            sessionManager.versiondialog();
                        }
                        else {
                            sessionManager.setIsVersion(false);
                        }
                    }
                    else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void updatedevicetoken(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject jsonBody = response;
        final String requestcartBody = jsonBody.toString();
        Log.d("requesttoken", "" + requestcartBody);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.tokenupdateUrl, response1 -> {
            sessionManager.progressdialogdismiss();
            Log.d("requesttokenresponse", "" + response1);
            try {
                JSONObject obj = new JSONObject(response1);
                JSONObject tok = new JSONObject(obj.getString("data"));
                if(obj.getBoolean("error") == false){
                    if(tok.getString("status").equalsIgnoreCase("inactive")){
                        sessionManager.settoken("");
                        Snackbar.make(view, getString(R.string.youaredisablefromadminpleasecontactadmin), Snackbar.LENGTH_LONG)
                                .setTextColor(ContextCompat.getColor(LanguageActivity.this,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(LanguageActivity.this,R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setActionTextColor(Color.BLACK)
                                .setAction(getString(R.string.ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        FirebaseAuth.getInstance().signOut();
                                        sessionManager.settoken("");
                                        sessionManager.setusername("");
                                        sessionManager.setusermail("");
                                        sessionManager.setUserId(0);
                                        sessionManager.setguest(0);
                                        sessionManager.setuserphoneno("");
                                        Intent intent56 = new Intent(LanguageActivity.this, LoginNewActivity.class);
                                        intent56.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent56);
                                    }
                                })
                                .show();
                    }
                    else {

                    }
                }
                else {
                    sessionManager.snackbarToast(obj.getString("message"),view);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }


    public Boolean isCheck(){
        if(!sessionManager.isPermissionsEnabled()){
            sessionManager.permissionsEnableRequest();
            return false;
        }
        else if(!sessionManager.isGpsEnabled()){
            sessionManager.gpsEnableRequest();
            return false;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case StaticInfo.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted && storageAccepted && cameraAccepted) {
                        //all permissions granted
                        if(!sessionManager.isGpsEnabled()){
                            sessionManager.gpsEnableRequest();
                        }
                        /*showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE},
                                                            PERMISSION_REQUEST_CODE);

                                                }
                                            }
                                        });*/
                    }
                    else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(CAMERA) || shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                sessionManager.permissionsEnableRequest();
                                return;
                            }
                            else {
                                sessionManager.displayManuallyEnablePermissionsDialog();
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case StaticInfo.GPS_REQUEST_CODE:
                if(resultCode != RESULT_OK){
                    //gps enabled succefully
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//        arabic.setEnabled(true);
//        english.setEnabled(true);
    }


    private void configrequestJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://ip-api.com/json", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("configdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("status").equalsIgnoreCase("success")) {
                        sessionManager.setcountrycodea(obj.getString("countryCode"));
                    }
                    else {
                       // sessionManager.setcountrycodea("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void presentShowcaseView(int withDelay) {
        new MaterialShowcaseView.Builder(this)
                .setTarget(english)
                // .setShape(new RectangleShape())
                .setDismissOnTouch(true)
                .withRectangleShape(true)
                .setTitleText("Hello")
                .setDismissText("GOT IT")
                .setContentText("This is some amazing feature you should know about")
                .setDelay(withDelay) // optional but starting animations immediately in onCreate can make them choppy
                .singleUse(SHOWCASE_ID) // provide a unique ID used to ensure it is only shown once
//                .useFadeAnimation() // remove comment if you want to use fade animations for Lollipop & up
                .show();
    }

    private void showIntro(View view, String usageId, String text) {
        MaterialIntroConfiguration config = new MaterialIntroConfiguration();
        config.setDelayMillis(200);
        config.setFadeAnimationEnabled(true);
        config.setColorTextViewInfo(Color.RED);
        // config.setMaskColor(Color.WHITE);
        config.setPadding(50);
        new MaterialIntroView.Builder(this)
                .enableDotAnimation(true)
                .enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setTextColor(Color.YELLOW)
                .performClick(true)
                .setInfoText(text)
                .setTarget(view)
                .setShape(ShapeType.RECTANGLE)
                // .setIdempotent(true)
                .setConfiguration(config)
                .setUsageId(usageId) //THIS SHOULD BE UNIQUE ID
                .show();

    }



    private void updateunRegistereddevicetoken(JSONObject response) {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject jsonBody = response;
        final String requestcartBody = jsonBody.toString();
        Log.d("requesttoken", "" + requestcartBody);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.UnregisteredToken, response1 -> {
            // sessionManager.progressdialogdismiss();
            Log.d("requesttokenresponse", "" + response1);
            try {
                JSONObject obj = new JSONObject(response1);
                JSONObject tok = new JSONObject(obj.getString("data"));
                if(obj.getBoolean("error") == false){


                }
                else {
                    sessionManager.snackbarToast(obj.getString("message"),view);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","multipart/form-data;boundary=<calculated when request is sent>" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }

}
