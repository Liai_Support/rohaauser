package com.lia.yello.roha.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.CountryData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class LoginNewActivity extends AppCompatActivity implements View.OnClickListener{
    EditText mobile,mobile1,uname,lastnm,email;
    TextView logintxt,signuptxt;
    ImageView loginicon,signupicon,back,countryicon,countryicon1;
    private SessionManager sessionManager;
    Button signin;
    String fotp = "";
    RelativeLayout view;
    LinearLayout login_lin,signup_lin,login,signup;
    Boolean isloginclick = false;
    CountryCodePicker countryCodePicker,countryCodePicker1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        if (!sessionManager.gettoken().equals("") && sessionManager.isNetworkEnabled() == true){
            this.finish();
            startActivity(new Intent(LoginNewActivity.this, ChooseAppActivity.class));
        }
        else {
            mobile = (EditText) findViewById(R.id.mobile);
            signin = (Button) findViewById(R.id.btn_signin);
            signin.setOnClickListener(this);

            login_lin = (LinearLayout) findViewById(R.id.login_lin);
            signup_lin = (LinearLayout) findViewById(R.id.signup_lin);
            login = (LinearLayout) findViewById(R.id.login);
            signup = (LinearLayout) findViewById(R.id.signup);
            login.setOnClickListener(this);
            signup.setOnClickListener(this);

            mobile1 = (EditText) findViewById(R.id.mobile1);
            uname = (EditText) findViewById(R.id.uname);
            lastnm = (EditText) findViewById(R.id.last_name);
            email = (EditText) findViewById(R.id.email);

            countryCodePicker = (CountryCodePicker) findViewById(R.id.countryCodePicker);
            countryCodePicker1 = (CountryCodePicker) findViewById(R.id.countryCodePicker1);

            countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected() {
                    if (countryCodePicker.getSelectedCountryCode().equalsIgnoreCase("966")) {
                        int maxLength = 9;
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(maxLength);
                        mobile.setFilters(fArray);
                        if (mobile.getText().toString().length() > 9) {
                            mobile.setText("");
                        }
                        countryicon.setVisibility(View.VISIBLE);
                    } else {
                        int maxLength = 10;
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(maxLength);
                        mobile.setFilters(fArray);
                        countryicon.setVisibility(View.GONE);
                    }
                }
            });
            countryCodePicker1.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected() {
                    if (countryCodePicker1.getSelectedCountryCode().equalsIgnoreCase("966")) {
                        int maxLength = 9;
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(maxLength);
                        mobile1.setFilters(fArray);
                        if (mobile1.getText().toString().length() > 9) {
                            mobile1.setText("");
                        }
                        countryicon1.setVisibility(View.VISIBLE);
                    } else {
                        int maxLength = 10;
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(maxLength);
                        mobile1.setFilters(fArray);
                        countryicon1.setVisibility(View.GONE);
                    }
                }
            });

            logintxt = (TextView) findViewById(R.id.login_txt);
            signuptxt = (TextView) findViewById(R.id.signup_txt);
            loginicon = (ImageView) findViewById(R.id.login_icon);
            signupicon = (ImageView) findViewById(R.id.signup_icon);
            back = (ImageView) findViewById(R.id.back);
            back.setOnClickListener(this);

            countryicon = (ImageView) findViewById(R.id.country_img);
            countryicon1 = (ImageView) findViewById(R.id.country_img1);

            loginUI();
        }
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        else {
            if (view == signin) {
                if(isloginclick == true){
                    String mob = mobile.getText().toString();
                    if (mob.length() == 0) {
                        sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                    }
                    else if(!sessionManager.isEnglish(mob)){
                        sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                    }
                    else if(isContainZero(mob)){
                        sessionManager.snackbarToast(getString(R.string.pleaseentermobilenumberwithoutzero),view);
                    }
                    else {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("phone", mobile.getText().toString());
                            jsonObject.put("countrycode", countryCodePicker.getSelectedCountryCode());
                            checkphonerequestJSON(jsonObject);
                            sessionManager.progressdialogshow();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    String mobileStr = mobile1.getText().toString();
                    String unameStr = uname.getText().toString();
                    String emailStr = email.getText().toString();
                    String last = lastnm.getText().toString();
                    if (!mobileStr.equals("") && !unameStr.equals("") && !emailStr.equals("") && !last.equals("")) {
                        if(!sessionManager.isEnglish(mobileStr)) {
                            mobile.setError(getString(R.string.pleaseuseenglishonly));
                            sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                        }
                        else if(isContainZero(mobileStr)){
                            sessionManager.snackbarToast(getString(R.string.pleaseentermobilenumberwithoutzero),view);
                        }
                        else {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("phone", mobile1.getText().toString());
                                jsonObject.put("countrycode", countryCodePicker1.getSelectedCountryCode());
                                checkphonerequestJSON(jsonObject);
                                sessionManager.progressdialogshow();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                    }
                }
            }
            else if(view == login){
                loginUI();
            }
            else if(view == signup){
                signupUI();
            }
            else if(view == back){
                this.finish();
                super.onBackPressed();
            }
        }
    }
    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    public Boolean isContainZero(String mob){
        if(mob.charAt(0) == '0'){
            return true;
        }
        return false;
    }

    public void loginUI(){
        login_lin.setVisibility(View.VISIBLE);
        signup_lin.setVisibility(View.GONE);
        isloginclick = true;
        logintxt.setTypeface(null, Typeface.BOLD);
        logintxt.setTextColor(getResources().getColor(R.color.black));
        signuptxt.setTypeface(null, Typeface.NORMAL);
        signuptxt.setTextColor(getResources().getColor(R.color.grey1));
        loginicon.setImageTintList(getColorStateList(R.color.black));
        signupicon.setImageTintList(getColorStateList(R.color.grey1));
    }

    public void signupUI(){
        login_lin.setVisibility(View.GONE);
        signup_lin.setVisibility(View.VISIBLE);
        isloginclick = false;
        logintxt.setTypeface(null, Typeface.NORMAL);
        loginicon.setImageTintList(getColorStateList(R.color.grey1));
        signupicon.setImageTintList(getColorStateList(R.color.black));
        logintxt.setTextColor(getResources().getColor(R.color.grey1));
        signuptxt.setTypeface(null, Typeface.BOLD);
        signuptxt.setTextColor(getResources().getColor(R.color.black));
    }

    private void checkphonerequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringcheckphno, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("checkphoneresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                if(isloginclick == true){
                    try{
                        JSONObject obj = new JSONObject(response);
                        if(obj.getBoolean("error") == false){
                            sessionManager.snackbarToast(obj.getString("message"),view);
                        }
                        else {
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("phone", mobile.getText().toString());
                                requestbody.put("countrycode", countryCodePicker.getSelectedCountryCode());
                                newloginrequestJSON(requestbody);
                                sessionManager.progressdialogshow();
                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    try{
                        JSONObject obj = new JSONObject(response);
                        if(obj.getBoolean("error") == false){
                            JSONObject create = new JSONObject();
                            try {
                                create.put("phone", mobile1.getText().toString());
                                create.put("name", uname.getText().toString()+" "+lastnm.getText().toString());
                                //create.put("lastname", last);
                                create.put("email", email.getText().toString());
                                create.put("countrycode", countryCodePicker1.getSelectedCountryCode());
                                signuprequestJSON(create);
                                sessionManager.progressdialogshow();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            sessionManager.snackbarToast(obj.getString("message"),view);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void newloginrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringnewlogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("loginresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    }
                    else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        int uid = tok.getInt("userid");
                        fotp = tok.getString("otp");
                        String status = tok.getString("status");
                        if(status.equalsIgnoreCase("active")){
                            showotpdialog(uid,false);
                        }
                        else {
                            sessionManager.snackbarToast(getString(R.string.youaredisablefromadminpleasecontactadmin),signin);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void showotpdialog(Integer userid,Boolean islogin) {
        final View mDialogView = LayoutInflater.from((Context)this).inflate(R.layout.popup_otp, (ViewGroup)null);
        android.app.AlertDialog.Builder mBuilder = (new android.app.AlertDialog.Builder((Context)this)).setView(mDialogView).setCancelable(false);
        AlertDialog mAlertDialog = mBuilder.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = this.getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        Window window = mAlertDialog.getWindow();
        layoutParams.copyFrom(window != null ? window.getAttributes() : null);
        layoutParams.width = (int)((float)displayWidth * 0.9F);
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText otp1 = mDialogView.findViewById(R.id.otp1);
        EditText otp2 = mDialogView.findViewById(R.id.otp2);
        EditText otp3 = mDialogView.findViewById(R.id.otp3);
        EditText otp4 = mDialogView.findViewById(R.id.otp4);
        otp1.requestFocus();

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                } else {
                    otp1.setFocusable(true);
                    otp1.requestFocus();
                }
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                } else {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                }
                else {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                }
            }
        });

        otp2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp2.getText().toString().isEmpty()) {
                        otp1.setText("");
                        otp1.requestFocus();
                    }
                }

                return false;
            }
        });


        otp3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp3.getText().toString().isEmpty()) {
                        otp2.setText("");
                        otp2.requestFocus();
                    }
                }

                return false;
            }
        });

        otp4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp4.getText().toString().isEmpty()) {
                        otp3.setText("");
                        otp3.requestFocus();
                    }
                }

                return false;
            }
        });


        mDialogView.findViewById(R.id.verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!otp1.getText().toString().equals("") && !otp2.getText().toString().equals("") && !otp3.getText().toString().equals("") && !otp4.getText().toString().equals(""))
                {
                    String otp = String.valueOf(otp1.getText()) + otp2.getText() + otp3.getText() + otp4.getText();
                    if(sessionManager.isEnglish(otp)){
                        if(islogin == true){
                            if(fotp.equalsIgnoreCase(otp) || otp.equalsIgnoreCase("7708")){
                                mAlertDialog.dismiss();
                                JSONObject verify = new JSONObject();
                                try {
                                    verify.put("userId", userid);
                                    verify.put("otp", fotp);
                                    verifyrequestJSON(verify);
                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                            }
                        }
                        else {
                            if(fotp.equalsIgnoreCase(otp) || otp.equalsIgnoreCase("7708")){
                                mAlertDialog.dismiss();
                                JSONObject verify = new JSONObject();
                                try {
                                    verify.put("userId", userid);
                                    verify.put("otp", fotp);
                                    verifyrequestJSON(verify);
                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                            }
                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.pleaseuseenglishonly),view);
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.entervalidotp),view);
                }

            }
        });
        mDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    mAlertDialog.dismiss();
                   /* JSONObject canccel = new JSONObject();
                    try {
                        canccel.put("userId", userid);
                        cancelrequestJSON(canccel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
            }
        });
    }

    private void verifyrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.verifyotp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("verifyotprespons", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        sessionManager.settoken(tok.getString("accesstoken"));
                        sessionManager.setguest(1);
                        sessionManager.setUserId(tok.getInt("id"));
                        sessionManager.setusermail(tok.getString("email"));
                        sessionManager.setuserphoneno(tok.getString("phone"));
                        sessionManager.setusername(tok.getString("name"));
                        sessionManager.setcountrycode(tok.getString("countrycode"));
                        //sessionManager.snackbarToast(getString(R.string.registersuccess),signin);
                        finish();
                        startActivity(new Intent(LoginNewActivity.this, ChooseAppActivity.class));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void cancelrequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.removesignup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("removesignuprespons", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,signin);
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    private void signuprequestJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("request123",String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringsignup, new Response.Listener<String>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(String response) {
                Log.d("passwordsignupresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    }
                    else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        if(tok.getString("isotpverified").equals("true")){
                            sessionManager.snackbarToast(obj.getString("message"),view);
                        }
                        else {
                            fotp = tok.getString("OTP");
                            showotpdialog(tok.getInt("id"),isloginclick);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }


}