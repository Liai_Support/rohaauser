package com.lia.yello.roha.Fragment;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.Gravity;
import android.view.WindowManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.IOException;
import java.util.List;


import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.utility.SessionManager;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AddaddressMapFragment extends Fragment implements View.OnClickListener {
    View view;
    SupportMapFragment mapFragment;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private FusedLocationProviderClient client;
    GoogleMap gmap;
    TextView addr,popup_adress;
    Button save;
    LatLng latLng;
    int faid,fbranchid;
    String faddr = "", fcity = "", address,fatype;
    double flat, flng;
    boolean update;
    double lat, lng;
    SessionManager sessionManager;
    Button change_loc;
    ImageView close;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_map_addaddress, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);

        ((RohahomeDashboradActivity) getActivity()).botm_nav.setVisibility(View.GONE);
        ((RohahomeDashboradActivity) getActivity()).home_bottom_icon.setVisibility(View.GONE);
        ((RohahomeDashboradActivity) getActivity()).profile_txt.setText(""+getString(R.string.addaddress));

        addr = (TextView) view.findViewById(R.id.txt_address);
        save = (Button) view.findViewById(R.id.save);
        save.setOnClickListener(this);
        client = LocationServices.getFusedLocationProviderClient(getContext());

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        Dexter.withContext(getApplicationContext())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        getmylocation();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();
        Bundle args = getArguments();
        update = args.getBoolean("update");

        if (update == true) {
            faid = args.getInt("faid");
            address = args.get("faddr").toString();
            lat = args.getDouble("flat");
            lng = args.getDouble("flng");
            fatype = args.getString("fatype");
            addr.setText("" + address);
            fcity = args.getString("fcity");
            fbranchid = args.getInt("fbranchid");
        }
        else {
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
        }

        return view;

    }

    public void getmylocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        if (update == true) {
                            latLng = new LatLng(lat, lng);
                        } else {
                            latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        }
                       /* MarkerOptions markerOptions=new MarkerOptions().position(latLng).title("You are here...!!");
                        googleMap.addMarker(markerOptions);*/
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                        gmap = googleMap;
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        gmap.setMyLocationEnabled(true);
                        gmap.setOnCameraIdleListener(onCameraIdleListener);
                        gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                // gmap.animateCamera(CameraUpdateFactory.zoomBy(17));
                                gmap.animateCamera(CameraUpdateFactory.zoomIn());
                            }
                        });
                       /* Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getContext(), Locale.getDefault());
                        try {
                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName();
                            Log.d("kkkkkjhbvkk",""+addresses);
                            Log.d("kkkkkkk",""+address);
                            Log.d("kkkkkk",""+city);
                            Log.d("kkkkk",""+state);
                            Log.d("kkkdfvdfkkk",""+country);
                            Log.d("kfvfkkkk",""+postalCode);
                            Log.d("kkkkdvk",""+knownName);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                      /*  gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                // creating marker
                                MarkerOptions markerOptions = new MarkerOptions();

                                // set marker position
                                markerOptions.position(latLng);

                                // set Lattitude & longitude on marker

                                markerOptions.title(latLng.latitude + ":" + latLng.longitude);
                                Geocoder geocoder;
                                List<Address> addresses;
                                geocoder = new Geocoder(getContext(), Locale.getDefault());
                                try {
                                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                                    if(addresses!=null){
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();
                                        Log.d("kkkkkkk",""+address);
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                gmap.clear();
                                gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                                gmap.addMarker(markerOptions);
                            }
                        });*/
                    }

                });
            }
        });
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng latLng = gmap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addresses != null && addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();
                        if (!addresses.isEmpty() && !country.isEmpty()) {
                            addr.setText(address + "  " + country);
                        }
                        addr.setText("" + address);
                        faddr = address;
                        flat = latLng.latitude;
                        flng = latLng.longitude;
                        if (city != null) {
                            fcity = sessionManager.getcityname(city);
                        }
                        else {
                            fcity = "";
                           // Toast.makeText(getApplicationContext(),"We could not find that location..",Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };

    }


    /*=@Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(onCameraIdleListener);

    }*/

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == save) {
            if (update == true) {
                if (sessionManager.isbranchAvailable(fcity) != 0) {
                    fbranchid = sessionManager.isbranchAvailable(fcity);
                    ((RohahomeDashboradActivity) getActivity()).botm_nav.setVisibility(View.VISIBLE);
                    ((RohahomeDashboradActivity) getActivity()).home_bottom_icon.setVisibility(View.VISIBLE);
                    Bundle args = new Bundle();
                    args.putInt("faid", faid);
                    args.putString("faddr", faddr);
                    args.putString("fcity", fcity);
                    args.putDouble("flat", flat);
                    args.putDouble("flng", flng);
                    args.putInt("fbranchid", fbranchid);
                    args.putString("fatype",fatype);
                    args.putBoolean("update", true);
                    ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddAddressFragment(),args);
                }
                else{
                    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                    View promptView = layoutInflater.inflate(R.layout.popup_location_invalid, null);
                    final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();

                    change_loc = (Button)promptView.findViewById(R.id.btn_chng_loc);
                    popup_adress = (TextView) promptView.findViewById(R.id.popup_address);
                    close = (ImageView) promptView.findViewById(R.id.popup_close_icon);

                    change_loc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertD.dismiss();
                        }
                    });
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertD.dismiss();
                        }
                    });
                    popup_adress.setText(faddr);
                    alertD.setView(promptView);
                    alertD.setCancelable(true);
                    alertD.show();
                    alertD.getWindow().setGravity(Gravity.BOTTOM);
                    alertD.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                    layoutParams.copyFrom(alertD.getWindow().getAttributes());
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    alertD.getWindow().setAttributes(layoutParams);
                }
            }
            else {
                if (sessionManager.isbranchAvailable(fcity) != 0) {
                    fbranchid = sessionManager.isbranchAvailable(fcity);
                    ((RohahomeDashboradActivity) getActivity()).botm_nav.setVisibility(View.VISIBLE);
                    ((RohahomeDashboradActivity) getActivity()).home_bottom_icon.setVisibility(View.VISIBLE);
                    Bundle args = new Bundle();
                    args.putString("faddr", faddr);
                    args.putString("fcity", fcity);
                    args.putInt("fbranchid", fbranchid);
                    args.putDouble("flat", flat);
                    args.putDouble("flng", flng);
                    args.putBoolean("update", false);
                    ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddAddressFragment(),args);
                }
                else {
                    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                    View promptView = layoutInflater.inflate(R.layout.popup_location_invalid, null);
                    final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();

                    change_loc = (Button)promptView.findViewById(R.id.btn_chng_loc);
                    popup_adress = (TextView) promptView.findViewById(R.id.popup_address);
                    close = (ImageView) promptView.findViewById(R.id.popup_close_icon);

                    change_loc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertD.dismiss();
                        }
                    });
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertD.dismiss();
                        }
                    });
                    popup_adress.setText(faddr);
                    alertD.setView(promptView);
                    alertD.setCancelable(true);
                    alertD.show();
                    alertD.getWindow().setGravity(Gravity.BOTTOM);
                    alertD.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                    layoutParams.copyFrom(alertD.getWindow().getAttributes());
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    alertD.getWindow().setAttributes(layoutParams);
                }
            }
        }
    }
}












