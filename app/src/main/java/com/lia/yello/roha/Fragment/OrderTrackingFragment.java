package com.lia.yello.roha.Fragment;

import static com.facebook.login.widget.ProfilePictureView.TAG;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.MyOrdersHomeAdapter2;
import com.lia.yello.roha.adapter.MyOrdersWaterAdapter2;
import com.lia.yello.roha.model.HomeorderlistDataModel2;
import com.lia.yello.roha.model.ListorderDatamodel2;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OrderTrackingFragment extends Fragment implements View.OnClickListener{
    View view;
    public int donationid;
    public int iswish;
    String jsonArray;
    public String orderdate,paytype,status,deliverydate,orderarray,timestampSt,comment,suggestions;
    public Double amount,rating,subtotal;
    public String comefrom;
    TextView orderid,moredetails,amount_txt, ordreddate,paymenttype,timestamporder,timestapmdeliver,deliverydateB;
    Button rohahome,rohawater;
    ImageView img_orderplaced,img_outfordelivery,img_pendingforattaesting,img_delivered;
    public Bundle args = new Bundle();

    public SessionManager sessionManager;
    RecyclerView recyclerView;
    private static ArrayList<ListorderDatamodel2> data;
    private static ArrayList<HomeorderlistDataModel2> data1;
    private static RecyclerView.Adapter adapter;
    TextView deliveryfee,vat,subtotalval;
    Double deliveryfeeSt;
    Double vatSt;
    String deliverdtimeSt,orderedtimeSt,full_obj;
    CardView blackcard;
    LinearLayout linearLayout,delivery_lay1;
    View delivery_lay2;
    Boolean iswater = false;
    MaterialButton invoice_btn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order_tracking_new, container, false);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
           /* OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }));
            }*/
           /* ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });*/
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
        }

        rohahome = (Button) view.findViewById(R.id.txt_roha_home);
        rohawater = (Button) view.findViewById(R.id.txt_roha_water);
        orderid = (TextView) view.findViewById(R.id.orderid);
        amount_txt = (TextView) view.findViewById(R.id.amount);
        ordreddate = (TextView) view.findViewById(R.id.ordereddate);
        paymenttype = (TextView) view.findViewById(R.id.paymenttype);

        delivery_lay1 = (LinearLayout) view.findViewById(R.id.delivery_lay1);
        delivery_lay2 = (View) view.findViewById(R.id.delivery_lay2);
        deliveryfee = (TextView) view.findViewById(R.id.delivaryfee);
        vat = (TextView) view.findViewById(R.id.vat_percent);
        subtotalval = (TextView) view.findViewById(R.id.subtotal_val);
        timestamporder = (TextView) view.findViewById(R.id.timestamporder);
        timestapmdeliver = (TextView) view.findViewById(R.id.timestampdelivered);
        deliverydateB = (TextView) view.findViewById(R.id.deliverydateB);
        blackcard = (CardView) view.findViewById(R.id.blackcard);

        img_orderplaced = (ImageView) view.findViewById(R.id.img_orderplaced);
        img_outfordelivery = (ImageView) view.findViewById(R.id.img_outfordelivery);
        img_pendingforattaesting = (ImageView) view.findViewById(R.id.img_pendingforattaesting);
        img_delivered = (ImageView) view.findViewById(R.id.img_delivered);
        moredetails = (TextView) view.findViewById(R.id.moredetails);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        moredetails.setOnClickListener(this);
        linearLayout = (LinearLayout) view.findViewById(R.id.lin_top);
        linearLayout.setVisibility(View.GONE);
        invoice_btn = (MaterialButton) view.findViewById(R.id.invoice_btn);

        invoice_btn.setOnClickListener(this);

        args = getArguments();
        comefrom = args.getString("type");
        if(comefrom.equals("water")){
            moredetails.setVisibility(View.GONE);
            donationid = args.getInt("donationid",0);
            iswish = args.getInt("iswish",0);
            orderdate = args.getString("orderdate");


            /*String my_date = args.getString("orderdate");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date strDate = null;
            try {
                strDate = sdf.parse(my_date);
                Log.d(TAG, "onCreateView: Date"+sdf.parse(my_date) +"-----"+new Date() );

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (new Date().after(strDate)) {
                invoice_btn.setVisibility(View.VISIBLE);
            }
            else{
                invoice_btn.setVisibility(View.GONE);
            }*/


            deliverydate = args.getString("deliverydate");
            deliverdtimeSt = args.getString("delivered_time");
            orderedtimeSt = args.getString("order_time");
            paytype = args.getString("paytype");
            subtotal = args.getDouble("subtotal");
            amount = args.getDouble("amount");
            status = args.getString("status");
            deliveryfeeSt = args.getDouble("delivery_charge");
            vatSt = args.getDouble("vat");
            timestampSt=args.getString("time_from");
            rating = args.getDouble("rating");
            suggestions = args.getString("suggestion");
            comment = args.getString("comment");
            jsonArray = args.getString("arraay");
            full_obj = args.getString("full_obj");

            amount_txt.setText(""+String.format(Locale.ENGLISH,"%.2f",amount)+" "+getString(R.string.sar));
            ordreddate.setText(""+orderdate);
            timestamporder.setText(""+orderedtimeSt);
            deliverydateB.setText(""+deliverydate);
            timestapmdeliver.setText(""+deliverdtimeSt);
            paymenttype.setText(""+paytype);
            if(deliveryfeeSt == 0){
                delivery_lay1.setVisibility(View.GONE);
                delivery_lay2.setVisibility(View.GONE);
            }
            else {
                deliveryfee.setText(""+String.format(Locale.ENGLISH,"%.2f",deliveryfeeSt)+" "+getString(R.string.sar));
            }
            vat.setText(""+String.format(Locale.ENGLISH,"%.2f",vatSt)+" "+getString(R.string.sar));
            subtotalval.setText(""+String.format(Locale.ENGLISH,"%.2f",subtotal)+" "+getString(R.string.sar));

            rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
            rohahome.setTextColor(Color.WHITE);
            rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            rohawater.setTextColor(Color.BLACK);

            if(iswish == 0){
                orderid.setText(""+getString(R.string.orderid)+" : "+StaticInfo.order_prefix+donationid);
                blackcard.setVisibility(View.GONE);
                if(status.equalsIgnoreCase(getString(R.string.pending)) || status.equalsIgnoreCase(getString(R.string.hold))){
                    img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                }
                else if(status.equalsIgnoreCase("assigned") || status.equalsIgnoreCase(getString(R.string.outfordelivery))){
                    img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                    img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                }
                else if(status.equalsIgnoreCase(getString(R.string.completed))){
                    img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                    img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                    img_pendingforattaesting.setImageDrawable(getResources().getDrawable(R.drawable.pendingforattesting_yellow));
                    img_delivered.setImageDrawable(getResources().getDrawable(R.drawable.delivered_yellow));
                    blackcard.setVisibility(View.VISIBLE);
                }
            }
            else {
                orderid.setText(""+getString(R.string.orderid)+" : "+StaticInfo.order_prefix+"M"+donationid);
                blackcard.setVisibility(View.GONE);
                if(status.equalsIgnoreCase(getString(R.string.pending)) || status.equalsIgnoreCase(getString(R.string.hold))){
                    img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                }
                else if(status.equalsIgnoreCase("assigned") || status.equalsIgnoreCase(getString(R.string.outfordelivery))){
                    img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                    img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                }
                else if(status.equalsIgnoreCase(getString(R.string.completed))){
                    img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                    img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                    img_pendingforattaesting.setImageDrawable(getResources().getDrawable(R.drawable.pendingforattesting_yellow));
                    img_delivered.setImageDrawable(getResources().getDrawable(R.drawable.delivered_yellow));
                }
            }

            try {
                JSONArray jarray = new JSONArray(jsonArray);
                data = new ArrayList<ListorderDatamodel2>();
                for (int i=0; i<jarray.length(); i++){
                    JSONObject jsonObject = jarray.getJSONObject(i);
                    if(jsonObject.getInt("id") == donationid){
                        JSONArray jsarray = new JSONArray(jsonObject.getString("details"));
                        Log.d("rxdcfvghb",""+jsarray);
                        for (int j=0; j<jsarray.length(); j++){
                            JSONObject jsonObject1 = jsarray.getJSONObject(j);
                            Log.d("sneee",""+jsonObject1);
                            ListorderDatamodel2 datamodel=new ListorderDatamodel2(
                                    getContext(),
                                    jsonObject1.getString("mosque_name"),
                                    jsonObject1.getString("delivered_time"),
                                    Integer.parseInt(jsonObject1.getString("cart_count")),
                                    jsonObject1.getString("status"),
                                    jsonObject1.getString("delivered_date"),
                                    jsonObject1.getString("units"),
                                    jsonObject1.getString("is_mecca"),
                                    jsonObject1.getString("place"),
                                    jsonObject1.getString("image_path1"),
                                    jsonObject1.getString("image_path2"),
                                    jsonObject1.getString("image_path3"),
                                    jsonObject1.getString("image_path4"),
                                    jsonObject1.getString("discountvalue"),
                                    jsonObject1.getString("discounttype"),
                                    jsonObject1.getString("unit_cost"));
                            data.add(datamodel);

                            Log.d("fgghj",""+jsonObject1.getString("image_path1"));
                            Log.d("sneee1",""+jsonObject1.getString("mosque_name"));
                            Log.d("sneee2",""+jsonObject1.getString("image_path2"));
                            Log.d("sneee3",""+jsonObject1.getString("image_path3"));
                            Log.d("sneee5",""+jsonObject1.getString("place"));
                            Log.d("sneee4",""+jsonObject1.getString("image_path4"));
                        }
                        adapter = new MyOrdersWaterAdapter2(getContext(),data,OrderTrackingFragment.this);
                        recyclerView.setAdapter(adapter);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else if(comefrom.equals("home")){
            blackcard.setVisibility(View.GONE);
            donationid = args.getInt("oid",0);
            orderdate = args.getString("odate");
            orderedtimeSt = args.getString("otime");
            deliverydate = args.getString("ddate");
            deliverdtimeSt = args.getString("dtime");
            paytype = args.getString("paytype");
            amount = args.getDouble("oamt");
            subtotal = args.getDouble("osubtotal");
            status = args.getString("ostatus");
            orderarray = args.getString("orderarray");
            deliveryfeeSt = args.getDouble("odel");
            vatSt = args.getDouble("ovat");
            rating = args.getDouble("rating");
            suggestions = args.getString("suggestion");
            comment = args.getString("comment");

            orderid.setText(""+getString(R.string.orderid)+" : "+StaticInfo.order_prefix+"H"+donationid);
            amount_txt.setText(""+String.format(Locale.ENGLISH,"%.2f",amount)+" "+getString(R.string.sar));
            ordreddate.setText(""+orderdate);
            timestamporder.setText(""+orderedtimeSt);
            deliverydateB.setText(""+deliverydate);
            timestapmdeliver.setText(""+deliverdtimeSt);
            vat.setText(""+String.format(Locale.ENGLISH,"%.2f",vatSt)+" "+getString(R.string.sar));
            if(deliveryfeeSt == 0 || deliveryfeeSt == 0.0){
                delivery_lay1.setVisibility(View.GONE);
                delivery_lay2.setVisibility(View.GONE);
            }
            else {
                deliveryfee.setText(""+String.format(Locale.ENGLISH,"%.2f",deliveryfeeSt)+" "+getString(R.string.sar));
            }
            paymenttype.setText(""+paytype);
            subtotalval.setText(""+String.format(Locale.ENGLISH,"%.2f",subtotal)+" "+getString(R.string.sar));

            rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
            rohawater.setTextColor(Color.WHITE);
            rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            rohahome.setTextColor(Color.BLACK);

            if(status.equalsIgnoreCase("pending") || status.equalsIgnoreCase("hold")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
            }
            else if(status.equalsIgnoreCase("out for delivery") || status.equalsIgnoreCase("assigned")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
            }
            else if(status.equalsIgnoreCase("completed")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                img_pendingforattaesting.setImageDrawable(getResources().getDrawable(R.drawable.pendingforattesting_yellow));
                img_delivered.setImageDrawable(getResources().getDrawable(R.drawable.delivered_yellow));
                blackcard.setVisibility(View.VISIBLE);
            }


            try {
                JSONArray jsonArray = new JSONArray(orderarray);
                Log.d("status2",""+jsonArray);
                data1 = new ArrayList<HomeorderlistDataModel2>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HomeorderlistDataModel2 datamodel = new HomeorderlistDataModel2(jsonObject.getInt("product_id"), jsonObject.getString("product_name"),jsonObject.getString("product_image"),jsonObject.getDouble("product_price"), jsonObject.getInt("product_quantity"), jsonObject.getDouble("product_amount"));
                    data1.add(datamodel);
                }
                adapter = new MyOrdersHomeAdapter2(getContext(), data1, OrderTrackingFragment.this);
                recyclerView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view == moredetails){
            if(comefrom.equals("home")){
                if(sessionManager.getchoose().equals("rohahome")){
                    ((RohahomeDashboradActivity)getActivity()).subchangefragment(new InvoiceFragment2(),args);
                }
                else if(sessionManager.getchoose().equals("rohawater")){
                    ((RohaWaterDashboardActivity)getActivity()).subchangefragment(new InvoiceFragment2(),args);
                }
            }
        }else if (view  == invoice_btn){
            Bundle args = new Bundle();
            args.putString("full_obj",full_obj);
            ((RohaWaterDashboardActivity)getActivity()).subchangefragment(new NewInvoice(),args);

        }
    }
}