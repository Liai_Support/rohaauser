package com.lia.yello.roha.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.ChooseAppActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.HomebannerAdapter;
import com.lia.yello.roha.adapter.HomeproductdataAdapter;
import com.lia.yello.roha.model.BannerDataModel;
import com.lia.yello.roha.model.HomeproductDataModel;
import com.lia.yello.roha.model.OfferDataModel;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.facebook.FacebookSdk.getApplicationContext;


public class RohahomeHomeFragment extends Fragment implements FragmentManager.OnBackStackChangedListener
{
    View view;
    private static RecyclerView.Adapter adapter1;
    public ArrayList<HomeproductDataModel> data = new ArrayList<HomeproductDataModel>();
    public SessionManager sessionManager;
    ArrayList<BannerDataModel> data1 = new ArrayList<BannerDataModel>();
    ArrayList<OfferDataModel> offerdata = new ArrayList<OfferDataModel>();
    int position;
    Timer timer;
    TimerTask timerTask;
    public RecyclerView recyclerView1,recyclerView2;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;

    int currentPage = 0;
    final long DELAY_MS = 5000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 8000; // time in milliseconds between successive task executions.
    private int dotscount;
    private ImageView[] dots;
    RohahomeDashboradActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_roha_home_home, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        activity = ((RohahomeDashboradActivity)getActivity());
        activity.currentfragment = RohahomeHomeFragment.this;
        StaticInfo.rohahomefirst = false;
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Log.d("xcx", "backsucess");
                   // activity.localtolive(new Fragment());
                    getActivity().finish();
                    Intent i = new Intent(getActivity(), ChooseAppActivity.class);
                    startActivity(i);
                }
            }));
        }
        sliderDotspanel = view.findViewById(R.id.SliderDots);
        viewPager = view.findViewById(R.id.viewpager);
        recyclerView2=(RecyclerView)view.findViewById(R.id.recycler_home_frag2);
        //HorizontalLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        //recyclerView2.setLayoutManager(HorizontalLayout);
        recyclerView2.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView2.setItemAnimator(new DefaultItemAnimator());

        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("branchid", activity.branchid);
            productdatarequestJSON(requestbody);
            sessionManager.progressdialogshow();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject requestbody1 = new JSONObject();
            requestbody1.put("offerfor","home");
            offerdatarequestJSON(requestbody1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    private void offerdatarequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("offerreques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listoffers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("offerdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        JSONArray ojsonArray = obj.getJSONArray("data");
                        for(int i=0;i<ojsonArray.length();i++) {
                            JSONObject jsonObject = ojsonArray.getJSONObject(i);
                            OfferDataModel datamodel = new OfferDataModel(jsonObject.getString("carton_name"),jsonObject.getInt("id"), jsonObject.getString("name"), jsonObject.getString("image"), jsonObject.getString("promocode"), jsonObject.getString("valid_from"), jsonObject.getString("valid_to"), jsonObject.getDouble("value"),jsonObject.getString("type"),jsonObject.getDouble("minimum_order_amount"));
                            offerdata.add(datamodel);
                            bannerdatarequestJSON();
                        }
                        /*RecyclerView.Adapter adapter1 = new OffersAdapter(getContext(),data,OffersFragment.this);
                        recyclerView.setAdapter(adapter1);*/

                    } else {
                        bannerdatarequestJSON();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                bannerdatarequestJSON();
                if(error instanceof NoConnectionError){
                    //session.slownetwork(RohawaterDashboardActivity.this);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("lang", sessionManager.getlang());
                Log.d("param", "" + params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    private void runAutoScrollBanner() {
        if (timer == null && timerTask == null) {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (position == Integer.MAX_VALUE) {
                        position = Integer.MAX_VALUE /Integer.MAX_VALUE;
                        recyclerView1.scrollToPosition(position);
                        recyclerView1.smoothScrollBy(3, 4);
                    } else {
                        position++;
                        recyclerView1.smoothScrollToPosition(position);
                    }
                }
            };
            timer.schedule(timerTask, 1000,1000);
        }
    }

    private void bannerdatarequestJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.bannervalues, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("bannerdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                       // progressDialog.dismiss();
                        JSONArray bjsonArray = obj.getJSONArray("data");
                        for(int i=0;i<bjsonArray.length();i++) {
                            JSONObject jsonObject = bjsonArray.getJSONObject(i);
                            BannerDataModel datamodel = new BannerDataModel(jsonObject.getInt("id"), jsonObject.getString("description"), jsonObject.getString("image"));
                            data1.add(datamodel);
                        }
                        autoscrolladapter();
                    }
                    else {
                        autoscrolladapter();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                autoscrolladapter();
                if(error instanceof NoConnectionError){
                  //  progressDialog.dismiss();
                    //session.slownetwork(RohawaterDashboardActivity.this);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("lang", sessionManager.getlang());
                Log.d("param", "" + params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    private void autoscrolladapter(){
        if(data1.size()==0 && offerdata.size()==0){
            BannerDataModel datamodel = new BannerDataModel(0, "default", "default");
            data1.add(datamodel);
        }
        //  PagerAdapter adapter3 = new HomebannerAdapter(getActivity(),imageId,imagesName);
        PagerAdapter adapter3 = new HomebannerAdapter(getActivity(),data1,offerdata);
        viewPager.setAdapter(adapter3);

        dotscount = adapter3.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){

            dots[i] = new ImageView(getContext());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_border_non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_border_active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_border_non_active_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_border_active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == dotscount) {
                    currentPage = -1;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }


    private void productdatarequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringhomeproductdata, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("productdataresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("dvdxv",""+obj.getJSONArray("data"));
                        JSONArray data1 = obj.getJSONArray("data");
                        data = new ArrayList<HomeproductDataModel>();
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject = data1.getJSONObject(i);
                            HomeproductDataModel datamodel = new HomeproductDataModel(jsonObject.getInt("master_product_id"), jsonObject.getString("product_name"), jsonObject.getInt("stockquantity"),jsonObject.getDouble("price"),jsonObject.getString("pimage"));
                            data.add(datamodel);
                        }
                        adapter1 = new HomeproductdataAdapter(getContext(), data, getActivity(),RohahomeHomeFragment.this);
                        recyclerView2.setAdapter(adapter1);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onBackStackChanged() {
        getActivity().finish();
        Intent i = new Intent(getContext(), ChooseAppActivity.class);
        startActivity(i);
    }

}