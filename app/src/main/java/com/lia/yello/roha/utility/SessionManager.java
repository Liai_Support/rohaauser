package com.lia.yello.roha.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.LanguageActivity;
import com.lia.yello.roha.activity.SplashActivity1;
import com.lia.yello.roha.model.CountryData;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class SessionManager {

    private SharedPreferences pref;
    private static final String PREF_NAME = "RohaPref";
    SharedPreferences.Editor editor;

    Context context;
    ProgressDialog progressDialog;
    View view;
    Activity activity;
    JSONArray j = new JSONArray();


    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_NETWORK = "IsNetwork";
    private static final String IS_VERSION = "IsVersion";
    private static final String IS_LOCATION = "IsLocation";
    private static final String IS_CARTEMPTY = "IsCartempty";
    private static final String IS_OFFER = "IsOffernotify";
    private static final String IS_GPS = "IsGps";
    public static final String KEY_NAME = "userId";
    public static final String ibanaccountnoselect = "ibanaccountnoselect";
    public static final Integer ibannoid = 0;

    public LocationManager locationManager;
    String currentVersion;


    public SessionManager(Activity activity,Context context,View view) {
        this.context = context;
        this.view = view;
        this.activity = activity;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setAppLocale(String localeCode) {
        Resources resources = activity.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else {
            configuration.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(configuration, displayMetrics);
        setlang(localeCode);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
    public boolean isIban(){
        return pref.getBoolean(ibanaccountnoselect, false);
    }

    public boolean isNetworkEnabled(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return pref.getBoolean(IS_NETWORK,connected);
        }  catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return pref.getBoolean(IS_NETWORK,connected);
    }

      /*private boolean haveNetworkWithwifi(){
        boolean have_WIFI= false;
        boolean have_MobileData = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for(NetworkInfo info:networkInfos){
            if (info.getTypeName().equalsIgnoreCase("WIFI"))if (info.isConnected())have_WIFI=true;
            if (info.getTypeName().equalsIgnoreCase("MOBILE DATA"))if (info.isConnected())have_MobileData=true;
        }
        return have_WIFI||have_MobileData;
    }*/

    public boolean isGpsEnabled(){
        locationManager = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        }
        else {
            return true;
        }
    }

    public void gpsEnableRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        SettingsClient settingsClient = LocationServices.getSettingsClient(activity);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
        task.addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // Add polylines to the map.
                // Polylines are useful to show a route or some other connection between points.
                // Position the map's camera near Alice Springs in the center of Australia,
                // and set the zoom factor so most of Australia shows on the screen.
                setIsGps(true);
            }
        });
        task.addOnFailureListener(activity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        setIsGps(false);
                       /* if(isgps1() == false){
                            nogps1(activity);
                        }*/
                        resolvable.startResolutionForResult(activity, StaticInfo.GPS_REQUEST_CODE);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }


    public void setIsGps(boolean isGps){
        editor.putBoolean(IS_GPS, isGps);
        editor.commit();
    }

    public boolean getIsGps(){
        boolean gps = pref.getBoolean(IS_GPS,true);
        return gps;
    }

    public void displayManuallyEnableGpsDialog() {
        Intent intent = new Intent(
                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(intent);
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(
                context);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    public boolean isPermissionsEnabled() {
        int result1 = ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
    }

    public void permissionsEnableRequest() {
        ActivityCompat.requestPermissions(activity, new String[]{ACCESS_FINE_LOCATION,CAMERA,WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE}, StaticInfo.PERMISSION_REQUEST_CODE);
    }

    public void displayManuallyEnablePermissionsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("We need to access Location for performing necessary task. Please permit the permission through "
                + "Settings screen.\n\nSelect App Permissions -> Enable permission(Location,Storage)");
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.permitmanually), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton(context.getString(R.string.cancel), null);
        builder.show();
    }

    public void settoken(String token) {
        pref.edit().putString("token", token).commit();
    }

    public String gettoken() {
        String token = pref.getString("token","");
        return token;
    }


    public void setusername(String username) {
        pref.edit().putString("username", username).commit();
    }

    public String getusername() {
        String username = pref.getString("username","");
        return username;
    }

    public void setbrancharray(String jsonArray){
        pref.edit().putString("barray", jsonArray).commit();
    }

    public JSONArray getbrancharray() {
        String strJson = pref.getString("barray","");//second parameter is necessary ie.,Value to return if this preference does not exist.
        try {
            j = new JSONArray(strJson);
            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return j;
    }

    public int isbranchAvailable(String branch){
        int branchid = 0;
        for(int i=0;i<getbrancharray().length();i++){
            try {
                if(getbrancharray().getJSONObject(i).getString("branchname").equalsIgnoreCase(branch)){
                    branchid = getbrancharray().getJSONObject(i).getInt("id");
                    return branchid;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return branchid;
    }

    public String getbranchname(int branchid){
        String branchname = "";
        for(int i=0;i<getbrancharray().length();i++){
            try {
                if(getbrancharray().getJSONObject(i).getInt("id") == branchid){
                    if(getlang().equalsIgnoreCase("en")){
                        branchname = getbrancharray().getJSONObject(i).getString("branchname");
                        return branchname;
                    }
                    else if(getlang().equalsIgnoreCase("ar")) {
                        branchname = getbrancharray().getJSONObject(i).getString("ar_branchname");
                        return branchname;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return branchname;
    }

    public String getcityname(String city){
        String city1 = "";
        if (city.equalsIgnoreCase("مكة المكرمة") || city.equalsIgnoreCase("مكه") || city.equalsIgnoreCase("Mecca") || city.equalsIgnoreCase("Makkah") || city.equalsIgnoreCase("Mekkah") || city.equalsIgnoreCase("Makka")) {
            city1 = StaticInfo.MECCA;
            return city1;
        }
        else if (city.equalsIgnoreCase("المدينة المنورة") || city.equalsIgnoreCase("Medina") || city.equalsIgnoreCase("Madina") || city.equalsIgnoreCase("Madinah") || city.equalsIgnoreCase("Medinah")) {
            city1 = StaticInfo.MEDINA;
            return city1;
        }
        else if (city.equalsIgnoreCase("جدة") || city.equalsIgnoreCase("Jeddah")) {
            city1 = StaticInfo.JEDDAH;
            return city1;
        }
        else if (city.equalsIgnoreCase("Riyadh")){
            city1 = StaticInfo.RIYADH;
            return city1;
        }
        else {
            city1 = city;
            return city1;
        }
    }

    public void setlocalcart(String jsonArray){
        pref.edit().putString("carray", jsonArray).commit();
    }

    public JSONArray getlocalcart(){
        Gson gson = new Gson();
        String json = pref.getString("carray", "");
        //JSONArray tasks = gson.fromJson(json, (Type) new JSONArray());
        try {
            JSONArray jsonArray = new JSONArray(json);
            j = jsonArray;
            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return j;
    }

    public void setdbranchid(int dbid) {
        pref.edit().putInt("dbid", dbid).commit();
    }

    public int getdbranchid() {
        int dbid = pref.getInt("dbid",0);
        return dbid;
    }

    public void setcbranchid(int cbid) {
        pref.edit().putInt("cbid", cbid).commit();
    }

    public int getcbranchid() {
        int cbid = pref.getInt("cbid",0);
        return cbid;
    }

    public void setdlat(String dlat) {
        pref.edit().putString("dlat", dlat).commit();
    }

    public String getdlat() {
        String dlat = pref.getString("dlat","");
        return dlat;
    }

    public void setdlng(String dlng) {
        pref.edit().putString("dlng", dlng).commit();
    }

    public String getdlng() {
        String dlng = pref.getString("dlng","");
        return dlng;
    }

    public void setdcity(String dcity) {
        pref.edit().putString("dcity", dcity).commit();
    }
    public String getdcity() {
        String dcity = pref.getString("dcity","");
        return dcity;
    }

    public void setdaddr(String daddr) {
        pref.edit().putString("daddr", daddr).commit();
    }

    public String getdaddr() {
        String daddr = pref.getString("daddr","");
        return daddr;
    }


    public void setclat(String clat) {
        pref.edit().putString("clat", clat).commit();
    }

    public String getclat() {
        String clat = pref.getString("clat","");
        return clat;
    }

    public void setclng(String clng) {
        pref.edit().putString("clng", clng).commit();
    }

    public String getclng() {
        String clng = pref.getString("clng","");
        return clng;
    }

    public void setccity(String ccity) {
        pref.edit().putString("ccity", ccity).commit();
    }
    public String getccity() {
        String ccity = pref.getString("ccity","default");
        return ccity;
    }

    public void setcaddr(String caddr) {
        pref.edit().putString("caddr", caddr).commit();
    }

    public String getcaddr() {
        String caddr = pref.getString("caddr","default");
        return caddr;
    }


    public void setuserphoneno(String userphoneno) {
        pref.edit().putString("userphoneno", userphoneno).commit();
    }

    public String getuserphoneno() {
        String userphoneno = pref.getString("userphoneno","");
        return userphoneno;
    }

    public void setcountrycode(String usercountrycode) {
        pref.edit().putString("usercountrycode", usercountrycode).commit();
        phonecodetocountrycode(usercountrycode);
    }

    public String getcountrycode() {
        String usercountrycode = pref.getString("usercountrycode","966");
        return usercountrycode;
    }

    public void setcountrycodea(String usercountrycodea) {
        pref.edit().putString("usercountrycodea", usercountrycodea).commit();
    }

    public String getcountrycodea() {
        String usercountrycodea = pref.getString("usercountrycodea","AE");
        return usercountrycodea;
    }

    public void setusermail(String usermail) {
        pref.edit().putString("usermail", usermail).commit();
    }

    public String getusermail() {
        String usermail = pref.getString("usermail","");
        return usermail;
    }

    public void setunitsqty(String unit) {
        pref.edit().putString("unit", unit).commit();
    }

    public String getunitsqty() {
        String unit = pref.getString("unit","");
        return unit;
    }
    public void setUserId(int userId) {
        pref.edit().putInt("userId", userId).commit();
    }

    public int getUserId() {
        int userId = pref.getInt("userId",0);
        return userId;
    }

    public void setCartcount(int cartcount) {
        pref.edit().putInt("cartcount", cartcount).commit();
    }

    public int getCartcount() {
        int cartcount = pref.getInt("cartcount",0);
        return cartcount;
    }

    public void setwalletamount(double wamount) {
        pref.edit().putFloat("wallet", (float) wamount).commit();
    }

    public double getwalletamount() {
        double userId = pref.getFloat("wallet",0);
        return userId;
    }

    public void setIsWish(int isWish) {
        pref.edit().putInt("isWish", isWish).commit();
    }

    public int getIsWish() {
        int isWish = pref.getInt("isWish",0);
        return isWish;
    }

    public void setIsMecca(int isMecca) {
        pref.edit().putInt("isMecca", isMecca).commit();
    }

    public int getIsMecca() {
        int isMecca = pref.getInt("isMecca",0);
        return isMecca;
    }

  /*  public void setIsMeccaAvailable(int isMecca) {
        pref.edit().putInt("isMecca", isMecca).commit();
    }

    public int getIsMeccaAvailable() {
        int isMecca = pref.getInt("isMecca",0);
        return isMecca;
    }

    public void setIsRiyadhAvailable(int isRiyadh) {
        pref.edit().putInt("isRiyadh", isRiyadh).commit();
    }

    public int getIsRiyadhAvailable() {
        int isRiyadh = pref.getInt("isRiyadh",0);
        return isRiyadh;
    }

    public void setIsMedinaAvailable(int isMedina) {
        pref.edit().putInt("isMedina", isMedina).commit();
    }

    public int getIsMedinaAvailable() {
        int isMedina = pref.getInt("isMedina",0);
        return isMedina;
    }

    public void setIsJeddahAvailable(int isJeddah) {
        pref.edit().putInt("isJeddah", isJeddah).commit();
    }

    public int getIsJeddahAvailable() {
        int isJeddah = pref.getInt("isJeddah",0);
        return isJeddah;
    }*/

    public void setlang(String Lang) {
        pref.edit().putString("Lang", Lang).commit();
    }

    public String getlang() {
        String Lang = pref.getString("Lang","ar");
        return Lang;
    }

    public void setdob(String dob) {
        pref.edit().putString("Dob", dob).commit();
    }

    public String getdob() {
        String Dob = pref.getString("Dob","");
        return Dob;
    }
    public void setaddress(String address) {
        pref.edit().putString("address", address).commit();
    }

    public String getaddress() {
        String address = pref.getString("address","");
        return address;
    }

    public void setprofilepic(String profilepic) {
        pref.edit().putString("profilepic", profilepic).commit();
    }

    public String getprofilepic() {
        String profilepic = pref.getString("profilepic","");
        return profilepic;
    }

    public void setseletedaddressid(int aid) {
        pref.edit().putInt("aid", aid).commit();
    }

    public int getseletedaddressid() {
        int aid = pref.getInt("aid",0);
        return aid;
    }


    public void setgender(String Gender) {
        pref.edit().putString("Gender", Gender).commit();
    }

    public String getgender() {
        String Gender = pref.getString("Gender","");
        return Gender;
    }

    public void setguest(int guest) {
        pref.edit().putInt("guest", guest).commit();
    }

    public int getguest() {
        int guest = pref.getInt("guest",0);
        return guest;
    }

    public void setchoose(String choose) {
        pref.edit().putString("choose", choose).commit();
    }
    public String getchoose() {
        String choose = pref.getString("choose","");
        return choose;
    }


    public void noNetworkDialog(final Activity activity){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(context);
        // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.popup_no_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.popup_no_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
               /* if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }*/
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetworkEnabled() == true){
                    alertDialog.dismiss();
                    Intent intent = new Intent(context,activity.getClass());
                    context.startActivity(intent);
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }

    public void volleyerror(VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
            snackbarToast(context.getString(R.string.timeouterror),view);
        } else if (error instanceof AuthFailureError) {
            // Error indicating that there was an Authentication Failure while performing the request
            snackbarToast(context.getString(R.string.authfailureerror),view);
        } else if (error instanceof ServerError) {
            //Indicates that the server responded with a error response
            snackbarToast(context.getString(R.string.servererror),view);
        } else if (error instanceof NetworkError) {
            //Indicates that there was network error while performing the request
            snackbarToast(context.getString(R.string.networkerror),view);
        } else if (error instanceof ParseError) {
            // Indicates that the server response could not be parsed
            snackbarToast(context.getString(R.string.parseerror),view);
        }
    }

    public void slownetwork(final Activity activity){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(context);
        // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.popup_slow_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.popup_slow_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
               /* if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }*/
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetworkEnabled() == true){
                    alertDialog.dismiss();
                    Intent intent = new Intent(context,activity.getClass());
                    context.startActivity(intent);
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }

    public void setIscurrentlocationselected(boolean isLocation){
        editor.putBoolean(IS_LOCATION, isLocation);
        editor.commit();
    }

    public boolean iscurrentlocationselected(){
        return pref.getBoolean(IS_LOCATION,false);
    }

    public void setIsCartempty(boolean isCartempty){
        editor.putBoolean(IS_CARTEMPTY, isCartempty);
        editor.commit();
    }

    public boolean isCartempty(){
        return pref.getBoolean(IS_CARTEMPTY,false);
    }

    public void setoffernotify(boolean isoffernotify){
        editor.putBoolean(IS_OFFER, isoffernotify);
        editor.commit();
    }

    public boolean isoffernotify(){
        return pref.getBoolean(IS_OFFER,true);
    }

    public void versiondialog(){
        //show anything
        //Uncomment the below code to Set the message and title from the strings.xml file
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //Setting message manually and performing action on button click
        builder.setMessage(context.getString(R.string.newversionisavailable))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.update), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                        //myWebLink.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.lia.yello.roha"));
                        myWebLink.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Dover"));
                        context.startActivity(myWebLink);
                        activity.finish();
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                        setIsVersion(false);
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("");
        alert.show();
    }

    public void setIsVersion(boolean isVersion){
        editor.putBoolean(IS_VERSION, isVersion);
        editor.commit();
    }

    public boolean getIsVersion(){
        return pref.getBoolean(IS_VERSION,false);
    }

    @SuppressLint("WrongConstant")
    public void snackbarToast(String msg, View view){
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(context,R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(context,R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show();
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void progressdialogshow(){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }

        progressDialog = new ProgressDialog(context,R.style.MyDialogstyle);
        progressDialog.setTitle("");
        progressDialog.setMessage(context.getString(R.string.pleasewait));
        progressDialog.setCancelable(false);

        if(progressDialog.isShowing() == false){
            Log.d("dopencountt","1");
            progressDialog.show();
        }
    }

    public void progressdialogdismiss() {
        if (progressDialog != null) {
            if (progressDialog.isShowing() == true) {
                Log.d("dclosecountt", "1");
                progressDialog.dismiss();
            }
        }
    }

    public void showNotification3(String title,String body){
        //sessionManager = new SessionManager(context);
        Intent intent = new Intent(context, SplashActivity1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Default";
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setContentIntent(null);
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        manager.notify(0, builder.build());
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), okListener)
                .setNegativeButton(context.getString(R.string.cancel), null)
                .create()
                .show();
    }

    public String base64convert(String content){
        byte[] data = new byte[0];
        try {
            data = content.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String fcontent = Base64.encodeToString(data, Base64.NO_WRAP);
        return fcontent;
    }


    public void phonecodetocountrycode(String phonecode){
        String countryname = "";
        for (int i = 0; i< CountryData.countryAreaCodes.length; i++){
            if(CountryData.countryAreaCodes[i].equalsIgnoreCase(phonecode)){
                countryname = CountryData.countryNames[i];
                break;
            }
        }

        // Get all country codes in a string array.
        String[] isoCountryCodes = Locale.getISOCountries();
        // Iterate through all country codes:
        for (String code : isoCountryCodes) {
            // Create a locale using each country code
            Locale locale = new Locale("", code);
            // Get country name for each code.
            String countryfullname = locale.getDisplayCountry();
            String name = locale.getCountry();
            if(countryfullname.equalsIgnoreCase(countryname))
            {
                setcountrycodea(code);
                break;
            }
        }

        Log.d("jbkjkjk",""+getcountrycodea());
    }

    public boolean isEnglish(String string){
        String pattern = "^[a-zA-Z0-9]*$";
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$"; //not working for validation
        String regex1 = "[^a-zA-Z0-9]+"; //only contains alpha numeric or special symbol
        String languagename = Locale.getDefault().getDisplayLanguage();
        String country = Locale.getDefault().getCountry();
        if (string.matches(pattern)) {
            // is English
            return true;
        }
        return false;
    }

    public boolean isEnglish1(String str){
        str = str.toLowerCase();
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char ch = charArray[i];
            if (!(ch >= 'a' && ch <= 'z' && ch >= 0 && ch <= 9)) {
                return false;
            }
        }
        return true;
    }




    public void setinsertid(int insertid) {
        pref.edit().putInt("insertid", insertid).commit();
    }

    public int getinsertid() {
        int insertid = pref.getInt("insertid",0);
        return insertid;
    }

}

