package com.lia.yello.roha.Fragment;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.OffersAdapter;
import com.lia.yello.roha.model.OfferDataModel;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OffersFragment extends Fragment implements View.OnClickListener {
    View view;
    ArrayList<OfferDataModel> data;
    RecyclerView recyclerView;
    SessionManager sessionManager;
    boolean iswater = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_offers, container, false);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_offers);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(getActivity());
        }
        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.offers);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
            try {
                JSONObject requestbody = new JSONObject();
                requestbody.put("offerfor","home");
                offerdatarequestJSON(requestbody);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.offers);
             try {
                JSONObject requestbody = new JSONObject();
                requestbody.put("offerfor","water");
                offerdatarequestJSON(requestbody);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return view;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater") && iswater==true){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }
        }
    }

    private void offerdatarequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("offerreques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listoffers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("offerdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        JSONArray ojsonArray = obj.getJSONArray("data");
                        data = new ArrayList<OfferDataModel>();
                        for(int i=0;i<ojsonArray.length();i++) {
                            JSONObject jsonObject = ojsonArray.getJSONObject(i);
                            OfferDataModel datamodel = new OfferDataModel(jsonObject.getString("carton_name"),jsonObject.getInt("id"), jsonObject.getString("name"), jsonObject.getString("image"), jsonObject.getString("promocode"), jsonObject.getString("valid_from"), jsonObject.getString("valid_to"), jsonObject.getDouble("value"),jsonObject.getString("type"),jsonObject.getDouble("minimum_order_amount"));
                            data.add(datamodel);
                        }
                        RecyclerView.Adapter adapter1 = new OffersAdapter(getContext(),data,OffersFragment.this);
                        recyclerView.setAdapter(adapter1);

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                if(error instanceof NoConnectionError){
                    //session.slownetwork(RohawaterDashboardActivity.this);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("lang", sessionManager.getlang());
                Log.d("param", "" + params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

}