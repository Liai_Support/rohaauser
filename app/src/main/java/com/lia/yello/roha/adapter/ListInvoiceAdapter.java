package com.lia.yello.roha.adapter;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.InvoiceFragment1;
import com.lia.yello.roha.Fragment.InvoiceFragment2;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.HomeorderlistDataModel1;
import com.lia.yello.roha.utility.StaticInfo;

import java.util.List;
public class ListInvoiceAdapter extends RecyclerView.Adapter<ListInvoiceAdapter.MyViewHolder> {
    private InvoiceFragment1 invoiceFragment1;

    private Context mcontext;
    private List<HomeorderlistDataModel1> dataSet;

    public ListInvoiceAdapter(Context context, List<HomeorderlistDataModel1> data, InvoiceFragment1 invoiceFragment1) {
        this.dataSet = data;
        this.mcontext = context;
        this.invoiceFragment1 = invoiceFragment1;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product,date,order_id;
        CardView  invoice_card;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.product = (TextView) itemView.findViewById(R.id.product);
            this.date = (TextView) itemView.findViewById(R.id.ordereddate);
            this.order_id = (TextView) itemView.findViewById(R.id.invoice_order_id);
            this.invoice_card = (CardView) itemView.findViewById(R.id.invoice_card);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_invoice, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,  final int listPosition) {
        holder.product.setText(""+StaticInfo.order_prefix+"H"+dataSet.get(listPosition).getId());
        holder.date.setText(""+dataSet.get(listPosition).getOrder_date());
        holder.order_id.setText(""+ StaticInfo.order_prefix+"H"+dataSet.get(listPosition).getId());
        holder.invoice_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invoiceFragment1.sessionManager.getchoose().equals("rohahome")){
                    Bundle args = new Bundle();
                    args.putString("comefrom","invoice");
                    args.putString("type","home");
                    args.putInt("oid", dataSet.get(listPosition).getId());
                    args.putString("odate", dataSet.get(listPosition).getOrder_date());
                    args.putDouble("osubtotal",dataSet.get(listPosition).getSubtotal());
                    args.putDouble("ovat", dataSet.get(listPosition).getVat());
                    args.putDouble("oamt", dataSet.get(listPosition).getAmount());
                    args.putDouble("odel",dataSet.get(listPosition).getDelivery_charge());
                    args.putString("odiscount",dataSet.get(listPosition).getDiscountvalue());
                    args.putString("odiscounttype",dataSet.get(listPosition).getDiscounttype());
                    args.putString("ocartonname",dataSet.get(listPosition).getCartonname());
                    args.putInt("ocartoncount",dataSet.get(listPosition).getCartoncount());
                    args.putString("ostatus",dataSet.get(listPosition).getStatus());
                    args.putString("orderarray",dataSet.get(listPosition).getDetails().toString());
                    args.putString("otime", dataSet.get(listPosition).getOrder_time());
                    ((RohahomeDashboradActivity)invoiceFragment1.getActivity()).subchangefragment(new InvoiceFragment2(),args);
                }
                else if(invoiceFragment1.sessionManager.getchoose().equals("rohawater")){
                    Bundle args = new Bundle();
                    args.putString("comefrom","invoice");
                    args.putString("type","home");
                    args.putInt("oid", dataSet.get(listPosition).getId());
                    args.putString("odate", dataSet.get(listPosition).getOrder_date());
                    args.putDouble("osubtotal",dataSet.get(listPosition).getSubtotal());
                    args.putDouble("ovat", dataSet.get(listPosition).getVat());
                    args.putDouble("oamt", dataSet.get(listPosition).getAmount());
                    args.putDouble("odel",dataSet.get(listPosition).getDelivery_charge());
                    args.putString("odiscount",dataSet.get(listPosition).getDiscountvalue());
                    args.putString("odiscounttype",dataSet.get(listPosition).getDiscounttype());
                    args.putString("ocartonname",dataSet.get(listPosition).getCartonname());
                    args.putInt("ocartoncount",dataSet.get(listPosition).getCartoncount());
                    args.putString("ostatus",dataSet.get(listPosition).getStatus());
                    args.putString("orderarray",dataSet.get(listPosition).getDetails().toString());
                    ((RohaWaterDashboardActivity)invoiceFragment1.getActivity()).subchangefragment(new InvoiceFragment2(),args);
                }

            }
        });
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
















