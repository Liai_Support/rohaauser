package com.lia.yello.roha.Fragment;
import static android.app.Activity.RESULT_OK;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.lia.yello.roha.BuildConfig;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.PaymentActivity;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.WalletAdapter;
import com.lia.yello.roha.model.WalletDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import com.telr.mobile.sdk.activity.WebviewActivity;
import com.telr.mobile.sdk.entity.request.payment.Address;
import com.telr.mobile.sdk.entity.request.payment.App;
import com.telr.mobile.sdk.entity.request.payment.Billing;
import com.telr.mobile.sdk.entity.request.payment.MobileRequest;
import com.telr.mobile.sdk.entity.request.payment.Name;
import com.telr.mobile.sdk.entity.request.payment.Tran;
import com.telr.mobile.sdk.entity.response.payment.MobileResponse;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

/*
import com.paytabs.paytabs_sdk.payment.ui.activities.PayTabActivity;
import com.paytabs.paytabs_sdk.utils.PaymentParams;
*/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class WalletFragment extends Fragment implements View.OnClickListener   {
    View view;
    Button  add_money,add,btn_cancel;
    private SessionManager sessionManager;
    private static RecyclerView recyclerView;
    private static ArrayList<WalletDataModel> data;
    private static RecyclerView.Adapter adapter1;
    EditText amount;
    TextView show_amt;
    boolean iswater = false;

    public static final String KEY = StaticInfo.KEY;//"s66Mx#BMN5-djBWj"; //"BwxtF~dq9L#xgWZb";//pQ6nP-7rHt@5WRFv        // TODO: Insert your Key here
    public static final String STORE_ID = StaticInfo.STOREID;//"18503";//"21941";   //15996           // TODO: Insert your Store ID here
    public static final boolean isSecurityEnabled = true;      // Mark false to test on simulator, True to test on actual device and Production
    public static final String EMAIL = "girish.spryox@gmail.com" ;//"?????????"  // TODO fill email  // TODO fill email
    //let EMAIL:String = "girish.spryox@gmail.com" //"?????????"  // TODO fill email

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallet, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(getActivity());
        }
        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.wallet);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });

        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.wallet);
        }
        recyclerView = (RecyclerView) view.findViewById(R.id.wallet_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        //ids
        getwalletamount();
        getwallethistory();
        add_money = (Button) view.findViewById(R.id.btn_update);
        add = (Button) view.findViewById(R.id.btn_add);
        show_amt = (TextView) view.findViewById(R.id.txt_show_amt);
        // id = (TextView) view.findViewById(R.id.id_wallet);
        add_money.setOnClickListener(this);
        // sessionManager.showNotification3("Test","Welcome");

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == add_money) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View promptView = layoutInflater.inflate(R.layout.popup_card_wallet_addmoney, null);
            final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();
            add = (Button)promptView.findViewById(R.id.btn_add);
            btn_cancel = (Button)promptView.findViewById(R.id.btn_cancel);
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertD.dismiss();
                }
            });
            amount=(EditText)promptView.findViewById(R.id.amount);
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!amount.getText().toString().equals("") && Double.parseDouble(amount.getText().toString())!=0.0) {
                        LocalData.getInstance().setPaymentamount(amount.getText().toString());
                        payment();
                        // addmoneytowallet(amt);
                        alertD.dismiss();
                    }else{
                        snackbarToast(getString(R.string.entervalidamount));
                    }
                }
            });
            alertD.dismiss();
            alertD.setView(promptView);
            alertD.setCancelable(true);
            alertD.show();
            alertD.getWindow().setGravity(Gravity.CENTER);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(alertD.getWindow().getAttributes());
            alertD.getWindow().setAttributes(layoutParams);

            // disallow cancel of AlertDialog on click of back button and outside touch
            /*dialog.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        dialog.dismiss();
                    }
                    return true;
                }
            });*/
        }
    }


    public void payment(){
 /*       Intent intent = new Intent(getActivity(), WebviewActivity.class);

        //intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(WebviewActivity.EXTRA_MESSAGE, getMobileRequest());
        intent.putExtra(WebviewActivity.SUCCESS_ACTIVTY_CLASS_NAME, "com.lia.yello.roha.activity.SuccessTransationActivity");
        intent.putExtra(WebviewActivity.FAILED_ACTIVTY_CLASS_NAME, "com.lia.yello.roha.activity.FailedTransationActivity");
        intent.putExtra(WebviewActivity.IS_SECURITY_ENABLED, isSecurityEnabled);
        //getActivity().startActivity(intent);
        startActivityForResult(intent,105);
        LocalData.getInstance().setTransactionid("");
        LocalData.getInstance().setTransactiontype("wallet");*/
        Intent intent=new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra("from", "wallet");
        startActivity(intent);
        LocalData.getInstance().setTransactionid("");
        LocalData.getInstance().setTransactiontype("wallet");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        Log.d("xcvsxxb",""+requestCode+"\n"+resultCode+"\n"+intent);

        if (requestCode == 105 && resultCode == RESULT_OK && intent != null) {
            String paymentMethod = intent.getStringExtra("auth");
            Log.d("zjhjcvg dc",""+paymentMethod);
            if(paymentMethod.equalsIgnoreCase("yes")){
                MobileResponse status = (MobileResponse) intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
                if (status.getAuth() != null) {
                    if(status.getAuth().getStatus().equalsIgnoreCase("A") || status.getAuth().getStatus().equalsIgnoreCase("H")){
                        if(!status.getAuth().getTranref().equals("") && status.getAuth().getTranref() != null){
                            LocalData.getInstance().setTransactionid(status.getAuth().getTranref());
                            Log.d("kjcgxhc","sjcb"+LocalData.getInstance().getTransactionid());
                            if(!LocalData.getInstance().getTransactionid().equals("") ){
                                if(LocalData.getInstance().getTransactionid().equals("failure")){
                                    sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                                    LocalData.getInstance().setTransactionid("");
                                    LocalData.getInstance().setTransactiontype("");
                                }
                                else {
                                    addmoneytowallet(LocalData.getInstance().getPaymentamount(), LocalData.getInstance().getTransactionid());
                                }
                            }
                        }
                    }
                    else {
                        sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
                    }
                }
            }
            else {
                StatusResponse status = (StatusResponse) intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
                if (status.getAuth() != null) {
                    if(status.getAuth().getStatus().equalsIgnoreCase("A") || status.getAuth().getStatus().equalsIgnoreCase("H")){
                        if(!status.getAuth().getTranref().equals("") && status.getAuth().getTranref() != null){
                            LocalData.getInstance().setTransactionid(status.getAuth().getTranref());
                            Log.d("kjcgxhc","sjcb"+LocalData.getInstance().getTransactionid());
                            if(!LocalData.getInstance().getTransactionid().equals("") ){
                                if(LocalData.getInstance().getTransactionid().equals("failure")){
                                    sessionManager.snackbarToast(status.getAuth().message+"-"+getString(R.string.paymentfailed),view);
                                    LocalData.getInstance().setTransactionid("");
                                    LocalData.getInstance().setTransactiontype("");
                                }
                                else {
                                    addmoneytowallet(LocalData.getInstance().getPaymentamount(), LocalData.getInstance().getTransactionid());
                                }
                            }
                        }
                    }
                    else {
                        sessionManager.snackbarToast(status.getAuth().message+"-"+getString(R.string.paymentfailed),view);
                        LocalData.getInstance().setTransactionid("");
                        LocalData.getInstance().setTransactiontype("");
                    }
                }
            }

           /* Intent intent1 = new Intent(getActivity(), SuccessTransationActivity.class);
            intent1.putExtras(intent);
            startActivity(intent1);*/
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater") && iswater==true){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }
        }
    }


    @Override
    public void onResume() {
        getwalletamount();
        getwallethistory();
        super.onResume();

    }

   /* @Override
    public void onStart() {
        super.onStart();
        Log.d("wallet frag onstart","ygvdvdv");
        Log.d("kjcgxhc","sjcb"+LocalData.getInstance().getTransactionid());
        if(!LocalData.getInstance().getTransactionid().equals("") && LocalData.getInstance().getTransactiontype().equalsIgnoreCase("wallet")){
            if(LocalData.getInstance().getTransactionid().equals("failure")){
                sessionManager.snackbarToast(getString(R.string.paymentfailed),view);
                LocalData.getInstance().setTransactionid("");
                LocalData.getInstance().setTransactiontype("");
            }
            else {
                addmoneytowallet(LocalData.getInstance().getPaymentamount(), LocalData.getInstance().getTransactionid());
            }
        }
    }*/

    private MobileRequest getMobileRequest() {
        MobileRequest mobile = new MobileRequest();
        mobile.setStore(STORE_ID);                       // Store ID
        mobile.setKey(KEY);                              // Authentication Key : The Authentication Key will be supplied by Telr as part of the Mobile API setup process after you request that this integration type is enabled for your account. This should not be stored permanently within the App.
        App app = new App();
        app.setId(getActivity().getPackageName());                          // Application installation ID
        app.setName(getString(R.string.app_name));                          // Application name
        app.setUser(String.valueOf(sessionManager.getUserId()));                           // Application user ID : Your reference for the customer/user that is running the App. This should relate to their account within your systems.
        app.setVersion(BuildConfig.VERSION_NAME);                         // Application version
        app.setSdk(app.getSdk());
        mobile.setApp(app);
        Tran tran = new Tran();
        tran.setTest("0");                              // Test mode : Test mode of zero indicates a live transaction. If this is set to any other value the transaction will be treated as a test.
        tran.setType("Sale");                           /* Transaction type
                                                            'auth'   : Seek authorisation from the card issuer for the amount specified. If authorised, the funds will be reserved but will not be debited until such time as a corresponding capture command is made. This is sometimes known as pre-authorisation.
                                                            'sale'   : Immediate purchase request. This has the same effect as would be had by performing an auth transaction followed by a capture transaction for the full amount. No additional capture stage is required.
                                                            'verify' : Confirm that the card details given are valid. No funds are reserved or taken from the card.
                                                        */

        tran.setClazz("paypage");                       // Transaction class only 'paypage' is allowed on mobile, which means 'use the hosted payment page to capture and process the card details'
        tran.setCartid(String.valueOf(new BigInteger(128, new Random()))); //// Transaction cart ID : An example use of the cart ID field would be your own transaction or order reference.
        tran.setDescription(getString(R.string.wallet)+"-A");         // Transaction description
        tran.setLanguage("en");
        tran.setCurrency("SAR");                        // Transaction currency : Currency must be sent as a 3 character ISO code. A list of currency codes can be found at the end of this document. For voids or refunds, this must match the currency of the original transaction.
        tran.setAmount(LocalData.getInstance().getPaymentamount());                         // Transaction amount : The transaction amount must be sent in major units, for example 9 dollars 50 cents must be sent as 9.50 not 950. There must be no currency symbol, and no thousands separators. Thedecimal part must be separated using a dot.
        //tran.setRef();                                // (Optinal) Previous transaction reference : The previous transaction reference is required for any continuous authority transaction. It must contain the reference that was supplied in the response for the original transaction.

        //040023303844  //030023738912
        // tran.setFirstref("030023738912");             // (Optinal) Previous user transaction detail reference : The previous transaction reference is required for any continuous authority transaction. It must contain the reference that was supplied in the response for the original transaction.

        mobile.setTran(tran);
        Billing billing = new Billing();
        Address address = new Address();
        address.setCity(sessionManager.getccity());                       // City : the minimum required details for a transaction to be processed
        address.setCountry(sessionManager.getcountrycodea());                       // Country : Country must be sent as a 2 character ISO code. A list of country codes can be found at the end of this document. the minimum required details for a transaction to be processed
        address.setRegion(sessionManager.getccity());                     // Region
        address.setLine1(sessionManager.getcaddr());// Street address – line 1: the minimum required details for a transaction to be processed
        //address.setLine2("SIT G=Towe");               // (Optinal)
        //address.setLine3("SIT G=Towe");               // (Optinal)
        //address.setZip("SIT G=Towe");                 // (Optinal)
        billing.setAddress(address);
        Name name = new Name();
        name.setFirst(sessionManager.getusername());                          // Forename : the minimum required details for a transaction to be processed
        name.setLast(sessionManager.getusername());                          // Surname : the minimum required details for a transaction to be processed
        name.setTitle(sessionManager.getusername());                           // Title
        billing.setName(name);
        billing.setEmail(sessionManager.getusermail()); //stackfortytwo@gmail.com : the minimum required details for a transaction to be processed.
        billing.setPhone("+"+sessionManager.getcountrycode()+sessionManager.getuserphoneno());
        mobile.setBilling(billing);
        mobile.setCustref("231");
        return mobile;
    }


  /*  private void payment(String amt) {
     //  String profileId = StaticInfo.profileId;
      //  String serverKey = StaticInfo.serverKey;
      //  String clientKey = StaticInfo.clientKey;
        //live
        String profileId = "55528";
        String serverKey = "SKJNMT99TL-JBKLHDJJB2-NMDJ9KDMD9";
        String clientKey = "CHKMTG-MNDV62-KBHD66-PNQ6H9";

        //test
      //  String profileId = "55181";
       // String serverKey = "SGJNMT99DR-JBKLHDKBWR-JZ2RJ6MWBH";
       // String clientKey = "C6KMTG-MNBM62-KBHDK2-6NKB6Q";

        //Log.d("dvd",""+profileId+serverKey+clientKey);
        PaymentSdkLanguageCode locale = PaymentSdkLanguageCode.EN;
        String screenTitle = "Dover";
        String cartId = "123456";
        String cartDesc = "Add Wallet Amount";
        String currency = "SAR";
        double amount = Double.parseDouble(amt);
        PaymentSdkTokenise tokeniseType = PaymentSdkTokenise.NONE;
        PaymentSdkTokenFormat tokenFormat = new PaymentSdkTokenFormat.Hex32Format();
        PaymentSdkTransactionType transType = PaymentSdkTransactionType.SALE;
        List<PaymentSdkApms> type = Collections.singletonList(PaymentSdkApms.STC_PAY);
        PaymentSdkBillingDetails billingData = new PaymentSdkBillingDetails(
                sessionManager.getccity(),
                "SA",
                sessionManager.getusermail(),
                sessionManager.getusername(),
                sessionManager.getuserphoneno(),
                "state",
                sessionManager.getcaddr(),
                "zip"
        );
        PaymentSdkShippingDetails shippingData = new PaymentSdkShippingDetails(
                sessionManager.getccity(),
                "SA",
                sessionManager.getusermail(),
                sessionManager.getusername(),
                sessionManager.getuserphoneno(),
                "state",
                sessionManager.getcaddr(),
                "zip"
        );
        PaymentSdkConfigurationDetails configData = new PaymentSdkConfigBuilder(profileId, serverKey, clientKey, amount, currency)
                .setCartDescription(cartDesc)
                .setLanguageCode(locale)
                .setBillingData(billingData)
                .setMerchantCountryCode("SA") // ISO alpha 2
                .setShippingData(shippingData)
                .setCartId(cartId)
               // .setAlternativePaymentMethods(type)
                .showBillingInfo(false)
                .showShippingInfo(false)
                .forceShippingInfo(false)
                .setScreenTitle(screenTitle)
                .build();
        Log.d("dcd",""+configData);
        PaymentSdkActivity.startCardPayment(getActivity(), configData, this);
       // PaymentSdkActivity.startAlternativePaymentMethods(getActivity(), configData, this);

    }
    @Override
    public void onError(@NotNull PaymentSdkError paymentSdkError) {
        Log.d("dfghewe",""+paymentSdkError);
        Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onPaymentCancel() {
        Log.d("dfghewe",""+"paymentSdkError");
        Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onPaymentFinish(@NotNull PaymentSdkTransactionDetails paymentSdkTransactionDetails) {
        Log.d("dfgh333",""+paymentSdkTransactionDetails.component8().component1());
        Log.d("dfgh333",""+paymentSdkTransactionDetails.component8().component2());

        Log.d("dfgh33gf3",""+paymentSdkTransactionDetails.component1());
        if(paymentSdkTransactionDetails.component8().component2().equalsIgnoreCase("Authorised") || paymentSdkTransactionDetails.component8().component1().equals(0)){
            if(paymentSdkTransactionDetails.component1() == null){
                Toast.makeText(getContext(), "Transaction Id Null", Toast.LENGTH_SHORT).show();
            }
            else {
                transactionid = paymentSdkTransactionDetails.component1();
               // refundrequest(amt,transactionid);
                addmoneytowallet(amt,paymentSdkTransactionDetails.component1());
            }
        }
        else {
            Toast.makeText(getContext(), ""+getString(R.string.paymentfailed)+"-"+paymentSdkTransactionDetails.component8().component2(), Toast.LENGTH_SHORT).show();
        }
    }

    private void refundrequest(String amt,String tid) {
        //String s= id.getText().toString();
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("profile_id", StaticInfo.profileId);
            requestbody.put("tran_type", "refund");
            requestbody.put("tran_class", "ecom");
            requestbody.put("cart_id", "123456");
            requestbody.put("cart_currency", "SAR");
            requestbody.put("cart_amount", amt);
            requestbody.put("cart_description", "API VOLLY PROBLEM");
            requestbody.put("tran_ref", tid);
            refundamountrequestJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void refundamountrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.refundrequesturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("refunfresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject result = obj.getJSONObject("payment_result");
                    if(result == null){
                        Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                        Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (result.getString("response_status").equalsIgnoreCase("A")) {
                            Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                            sessionManager.snackbarToast(getString(R.string.refundrequestsuccess), add_money);
                        } else {
                            Toast.makeText(getContext(), R.string.paymentsuccess, Toast.LENGTH_LONG).show();
                            Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Toast.makeText(getContext(), "Refund Failure", Toast.LENGTH_LONG).show();
                if(error instanceof NoConnectionError){
                    sessionManager.slownetwork(getActivity());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("authorization",StaticInfo.refundserverKey);
                Log.d("vgcd",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }*/


    public void getwalletamount(){
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            getwalletamountJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getwallethistory(){
        JSONObject responsebody = new JSONObject();
        try {
            responsebody.put("userid", sessionManager.getUserId());
            wallethistoryrequestJSON(responsebody);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


   /* private void payment(String amt) {
        Intent in = new Intent(getActivity(), PayTabActivity.class);
        in.putExtra(PaymentParams.MERCHANT_EMAIL,StaticInfo.payment_mail); //this a demo account for testing the sdk
        in.putExtra(PaymentParams.SECRET_KEY,StaticInfo.payment_api.trim());//Add your Secret Key Here
     *//*   in.putExtra(PaymentParams.MERCHANT_EMAIL,"rhegazy@paytabs.com"); //this a demo account for testing the sdk
        in.putExtra(PaymentParams.SECRET_KEY,"BIueZNfPLblJnMmPYARDEoP5x1WqseI3XciX0yNLJ8v7URXTrOw6dmbKn8bQnTUk6ch6L5SudnC8fz2HozNBVZlj7w9uq4Pwg7D1");//Add your Secret Key Here
    *//*    in.putExtra(PaymentParams.LANGUAGE,session.getlang());
        in.putExtra(PaymentParams.TRANSACTION_TITLE, "Test Paytabs android library");
        in.putExtra(PaymentParams.AMOUNT, Double.parseDouble(""+amt));
        in.putExtra(PaymentParams.CURRENCY_CODE, "SAR");
        in.putExtra(PaymentParams.CUSTOMER_PHONE_NUMBER, ""+session.getuserphoneno());
        in.putExtra(PaymentParams.CUSTOMER_EMAIL, ""+session.getusermail());
        in.putExtra(PaymentParams.ORDER_ID, "123456");
        in.putExtra(PaymentParams.PRODUCT_NAME, "Product 1, Product 2");
//Billing Address
        in.putExtra(PaymentParams.ADDRESS_BILLING, "Flat 1,Building 123, Road 2345");
        in.putExtra(PaymentParams.CITY_BILLING, "Manama");
        in.putExtra(PaymentParams.STATE_BILLING, "Manama");
        in.putExtra(PaymentParams.COUNTRY_BILLING, "BHR");
        in.putExtra(PaymentParams.POSTAL_CODE_BILLING, "00973"); //Put Country Phone code if Postal code not available '00973'
//Shipping Address
        in.putExtra(PaymentParams.ADDRESS_SHIPPING, "Flat 1,Building 123, Road 2345");
        in.putExtra(PaymentParams.CITY_SHIPPING, "Manama");
        in.putExtra(PaymentParams.STATE_SHIPPING, "Manama");
        in.putExtra(PaymentParams.COUNTRY_SHIPPING, "BHR");
        in.putExtra(PaymentParams.POSTAL_CODE_SHIPPING, "00973"); //Put Country Phone code if Postal code not available '00973'
//Payment Page Style
        in.putExtra(PaymentParams.PAY_BUTTON_COLOR, "#ffc600");
//Tokenization
        in.putExtra(PaymentParams.IS_TOKENIZATION, false);
        //in.putExtra(PaymentParams.IS_PREAUTH,false);
        startActivityForResult(in, PaymentParams.PAYMENT_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("mailid",""+StaticInfo.payment_mail);
        Log.d("key",""+StaticInfo.payment_api);
        Log.d("requestcode", "" + requestCode);
        Log.d("resultcode", "" + resultCode);
        Log.d("sdcvcfxb", "" + data);
       // Log.d("sxb", "" +data.toString() + " " + bundleToString(data.getExtras()));

        if (resultCode == RESULT_OK && requestCode == PaymentParams.PAYMENT_REQUEST_CODE) {
            Log.e("responsecode", data.getStringExtra(PaymentParams.RESPONSE_CODE));
            Log.e("transactionid", data.getStringExtra(PaymentParams.TRANSACTION_ID));
            Log.d("token", "" + data.getStringExtra(PaymentParams.TOKEN));
            Log.d("mailid1",""+data.getStringExtra(PaymentParams.MERCHANT_EMAIL));
            Log.d("key1",""+data.getStringExtra(PaymentParams.SECRET_KEY));

            if ((data.hasExtra(PaymentParams.TOKEN) && !data.getStringExtra(PaymentParams.TOKEN).isEmpty()) || !data.getStringExtra(PaymentParams.TRANSACTION_ID).isEmpty()) {
                //   Log.e("Tag", data.getStringExtra(PaymentParams.TOKEN));
                //  Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_EMAIL));
                //  Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_PASSWORD));
               *//* if(data.getStringExtra(PaymentParams.RESPONSE_CODE) == 100){
                    Intent intent = new Intent(ReceiverDetailsActivity.this,SuceessActivity.class);
                    startActivity(intent);
                }*//*
                addmoneytowallet(amt, data.getStringExtra(PaymentParams.TRANSACTION_ID));
            } else {
                Toast.makeText(getContext(), R.string.paymentfailed, Toast.LENGTH_SHORT).show();
            }
        }
    }*/

    public static String bundleToString(Bundle bundle) {
        StringBuilder out = new StringBuilder("Bundle[");

        if (bundle == null) {
            out.append("null");
        } else {
            boolean first = true;
            for (String key : bundle.keySet()) {
                if (!first) {
                    out.append(", ");
                }

                out.append(key).append('=');

                Object value = bundle.get(key);

                if (value instanceof int[]) {
                    out.append(Arrays.toString((int[]) value));
                } else if (value instanceof byte[]) {
                    out.append(Arrays.toString((byte[]) value));
                } else if (value instanceof boolean[]) {
                    out.append(Arrays.toString((boolean[]) value));
                } else if (value instanceof short[]) {
                    out.append(Arrays.toString((short[]) value));
                } else if (value instanceof long[]) {
                    out.append(Arrays.toString((long[]) value));
                } else if (value instanceof float[]) {
                    out.append(Arrays.toString((float[]) value));
                } else if (value instanceof double[]) {
                    out.append(Arrays.toString((double[]) value));
                } else if (value instanceof String[]) {
                    out.append(Arrays.toString((String[]) value));
                } else if (value instanceof CharSequence[]) {
                    out.append(Arrays.toString((CharSequence[]) value));
                } else if (value instanceof Parcelable[]) {
                    out.append(Arrays.toString((Parcelable[]) value));
                } else if (value instanceof Bundle) {
                    out.append(bundleToString((Bundle) value));
                } else {
                    out.append(value);
                }

                first = false;
            }
        }

        out.append("]");
        return out.toString();
    }

    private void addmoneytowallet(String amt,String tid) {
        //String s= id.getText().toString();
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("amount", amt);
            requestbody.put("userid", sessionManager.getUserId());
            requestbody.put("transaction_id", tid);
            addamountrequestJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addamountrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.addwallet, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LocalData.getInstance().setPaymentamount("");
                LocalData.getInstance().setTransactionid("");
                LocalData.getInstance().setTransactiontype("");
                Log.d("walletresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                       // refundrequest(amt,transactionid);
                        Log.d("walleterror",""+msg);
                    } else {
                        JSONObject tok = new JSONObject(obj.getString("data"));

                        show_amt.setText(String.format(Locale.ENGLISH,"%.2f",tok.getDouble("amount"))+" SAR");
                        sessionManager.setwalletamount(Double.parseDouble(String.format(Locale.ENGLISH,"%.2f",tok.getDouble("amount"))));
                       // sessionManager.showNotification3(getString(R.string.app_name),getString(R.string.walletisupdated));
                        getwalletamount();
                        getwallethistory();
                       /* JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("userid", session.getUserId());
                            getwalletamountJSON(requestbody);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                LocalData.getInstance().setPaymentamount("");
                LocalData.getInstance().setTransactionid("");
                LocalData.getInstance().setTransactiontype("");
                //refundrequest(amt,transactionid);
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void getwalletamountJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.getwallet, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("walletresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        Log.d("walleterror",""+msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        tok.getString("userid");
                        tok.getString("amount");
                        show_amt.setText(String.format(Locale.ENGLISH,"%.2f",tok.getDouble("amount"))+" SAR");
                        sessionManager.setwalletamount(Double.parseDouble(String.format(Locale.ENGLISH,"%.2f",tok.getDouble("amount"))));
                        Log.d("fdvdf",""+ sessionManager.getwalletamount());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void snackbarToast(String msg){
        Snackbar snackbar ;
        add = (Button) view.findViewById(R.id.btn_add);

        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(getContext(),R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(getContext(),R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show();

    }

    private void wallethistoryrequestJSON(JSONObject responsebody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = responsebody.toString();
        Log.d("reques34", String.valueOf(responsebody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.list_wallethistory, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("wallet_history", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        JSONArray data1 = obj.getJSONArray("data");
                        data = new ArrayList<WalletDataModel>();
                        if(data1.length()>0){
                            for (int i = 0; i < data1.length(); i++) {
                                JSONObject jsonObject = data1.getJSONObject(i);
                                WalletDataModel datamodel = new WalletDataModel(jsonObject.getInt("id"),jsonObject.getString("order_id"),jsonObject.getString("order_for"),jsonObject.getString("iswish"),jsonObject.getString("operation"),jsonObject.getString("amount"),jsonObject.getString("transaction_id"),jsonObject.getString("date"));
                                data.add(datamodel);
                            }
                            adapter1 = new WalletAdapter(getContext(), data);
                            recyclerView.setAdapter(adapter1);
                        }
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }



}