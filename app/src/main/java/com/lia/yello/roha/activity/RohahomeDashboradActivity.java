package com.lia.yello.roha.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.lia.yello.roha.Fragment.CartFragment;
import com.lia.yello.roha.Fragment.AddaddressMapFragment;
import com.lia.yello.roha.Fragment.HelpFragment;
import com.lia.yello.roha.Fragment.MyOrderFragment;
import com.lia.yello.roha.Fragment.NotificationFragment;
import com.lia.yello.roha.Fragment.OffersFragment;
import com.lia.yello.roha.Fragment.InvoiceFragment1;
import com.lia.yello.roha.Fragment.PaymentFragment;
import com.lia.yello.roha.Fragment.ProfileFragment;
import com.lia.yello.roha.Fragment.RohaHomeOrderSummaryFragment;
import com.lia.yello.roha.Fragment.RohahomeHomeFragment;
import com.lia.yello.roha.Fragment.PaymentOptionsFragment;
import com.lia.yello.roha.Fragment.WalletFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.adapter.RohaHomeListAddressAdapter;
import com.lia.yello.roha.model.AddressListDataModel;
import com.lia.yello.roha.model.CommonDataModel;
import com.lia.yello.roha.model.GetAddressModel;
import com.lia.yello.roha.model.ListOfferDataModel;
import com.lia.yello.roha.model.Prediction;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.View.VISIBLE;

public class RohahomeDashboradActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, SimpleGestureFilter.SimpleGestureListener {
    public SessionManager sessionManager;
    View view;
    public LinearLayout navheader;
    public View headerview;
    public NavigationView navigationView;
    public int branchid = 0;
    JSONArray brancharray = new JSONArray();
    JSONArray addressarray = new JSONArray();
    public JSONArray cartarray = new JSONArray();
    public DrawerLayout drawerLayout;
    public Fragment currentfragment = new Fragment();
    public ArrayList<AddressListDataModel> data2;
    public ImageView nav_close, home_bottom_icon, side_nav_icon, nav_profile_pic, facebook, twitter, instagram;
    public TextView name, wallet_img, txt_loc, get_addrs, version;
    public TextView profile_txt, txt_delv_add, txt_edit_profile, clear_notification;
    Fragment fragment = new Fragment();
    private static ArrayList<CommonDataModel> data1;
    private static ArrayList<ListOfferDataModel> data3;
    public RecyclerView.Adapter adapter1;
    RecyclerView recyclerView1;
    public BottomNavigationView botm_nav;
    public RelativeLayout relativeLayout;
    EditText add_address;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private SimpleGestureFilter detector;
    public ImageView notification, popclose, add, delete,back;
    public EditText currentlocation;
    public BottomSheetBehavior mBottomSheetBehavior;
    public View cartBadge;
    public TextView cartcount;
    public JSONArray localcart = new JSONArray();
    ProgressBar progressBar;
    RelativeLayout relative;
    Boolean fromnotification = false;
    String page = null;
    String city = "";
    public String  langfrom = "p";
    Fragment tfragment = new Fragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roha_home_dashboard);
        view = (CoordinatorLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        sessionManager.setchoose("rohahome");
        StaticInfo.rohahomefirst = true;
        langfrom = getIntent().getStringExtra("langfrom");
        fromnotification = getIntent().getBooleanExtra("fromnotification", false);
        page = getIntent().getStringExtra("page");

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        detector = new SimpleGestureFilter(this, this);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(this);
        }

        relativeLayout = findViewById(R.id.relvhome);
        home_bottom_icon = (ImageView) findViewById(R.id.home_bottom);
        side_nav_icon = (ImageView) findViewById(R.id.homeee);
        back = (ImageView) findViewById(R.id.back);
        wallet_img = (TextView) findViewById(R.id.txtdeladd);
        txt_loc = (TextView) findViewById(R.id.txtloc);
        get_addrs = (TextView) findViewById(R.id.txtdelvadd);
        name = findViewById(R.id.uname);
        botm_nav = findViewById(R.id.bottom_navigation_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        profile_txt = (TextView) findViewById(R.id.Profile_txt);
        txt_delv_add = (TextView) findViewById(R.id.txtdelvadd);
        clear_notification = (TextView) findViewById(R.id.clear_notification);
        notification = (ImageView) findViewById(R.id.notification);
        currentlocation = (EditText) findViewById(R.id.edittxt_current_location);
        add_address = (EditText) findViewById(R.id.add_address_image);
        relative = (RelativeLayout) findViewById(R.id.relative_nav_home);
        popclose = (ImageView) findViewById(R.id.close_pop);
        facebook = (ImageView) findViewById(R.id.facebook);
        version = (TextView) findViewById(R.id.version);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.setEnabled(false);
        progressBar.setVisibility(View.GONE);
        instagram = (ImageView) findViewById(R.id.instagram);
        twitter = (ImageView) findViewById(R.id.twitter);
        recyclerView1 = (RecyclerView) findViewById(R.id.address_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(layoutManager);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        ScrollView nav_scrool = (ScrollView) findViewById(R.id.scroll);
        RelativeLayout nav_relative = (RelativeLayout) findViewById(R.id.relative_nav_home);
        if(sessionManager.getlang().equalsIgnoreCase("ar")){
            nav_scrool.setBackgroundResource(R.drawable.bg_border_left_top_bottom_30dp_white);
            nav_relative.setBackgroundResource(R.drawable.bg_border_left_top_bottom_30dp_white);
            navigationView.setBackgroundResource(R.drawable.bg_border_top_left_30dp_white);
            back.setRotationY(180);
        }


        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.moveapp);
        menuItem.setTitle(R.string.dovermasjid);
        navheader();

        txt_loc.setOnClickListener(this);
        side_nav_icon.setOnClickListener(this);
        home_bottom_icon.setOnClickListener(this);
        get_addrs.setOnClickListener(this);
        popclose.setOnClickListener(this);
        notification.setOnClickListener(this);
        currentlocation.setOnClickListener(this);
        add_address.setOnClickListener(this);
        relative.setOnClickListener(this);
        facebook.setOnClickListener(this);
        instagram.setOnClickListener(this);
        twitter.setOnClickListener(this);
        version.setOnClickListener(this);
        back.setOnClickListener(this);
        botm_nav.getMenu().getItem(0).setTitle(getString(R.string.cart));
        botm_nav.getMenu().getItem(0).setIcon(getResources().getDrawable(R.drawable.cart_icon));
        botm_nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(StaticInfo.rohahomefirst == false) {
                    switch (item.getItemId()) {
                        case R.id.btm_profile:
                            if (item.isCheckable() == false) {
                                navigationchangefragment(new ProfileFragment(), 4, 0);
                            }
                            break;
                        case R.id.btm_cart:
                            if (item.isCheckable() == false) {
                                navigationchangefragment(new CartFragment(), 0, 1);
                            }
                            break;
                        case R.id.btm_wallet:
                            if (item.isCheckable() == false) {
                                navigationchangefragment(new WalletFragment(), 1, 4);
                            }
                            break;
                        case R.id.btm_offers:
                            if (item.isCheckable() == false) {
                                navigationchangefragment(new OffersFragment(), 3, 3);
                            }
                            break;
                    }
                    relativeLayout.setBackground(getResources().getDrawable(R.drawable.bg_border_bottom_left_right_15dp_white));
                    relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                }
                return true;
            }
        });
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) botm_nav.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(0);
        cartBadge = LayoutInflater.from(this).inflate(R.layout.cartbadgeicon, menuView, false);
        itemView.addView(cartBadge);
        cartBadge.setVisibility(VISIBLE);
        cartcount = (TextView) cartBadge.findViewById(R.id.cart_badge);

        if (langfrom != null && !langfrom.equals("")) {
            if (langfrom.equals("profile")) {
                profilefragmentload();
            }
        }
        else if (fromnotification == true) {
            if (page.equalsIgnoreCase("home")) {
                myorderfragmentload();
            } else if (page.equalsIgnoreCase("offer")) {
                fragment = new OffersFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
            }
        }
        else {
            LocalData.removeAllValues();
            getprofile();
            listcart();
            listaddress(false);
        }
        //  version.setText("Version : "+BuildConfig.VERSION_NAME);
        try {
            version.setText("" + getString(R.string.version) + " : " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void homefragmentload() {
        for (int i = 0; i < botm_nav.getMenu().size(); i++) {
            botm_nav.getMenu().getItem(i).setCheckable(false);
        }
        for(int j=0;j < navigationView.getMenu().size();j++){
            navigationView.getMenu().getItem(j).setCheckable(false);
        }
        side_nav_icon.setVisibility(View.VISIBLE);
        back.setVisibility(View.GONE);
        txt_delv_add.setVisibility(View.VISIBLE);
        txt_loc.setVisibility(View.VISIBLE);
        notification.setVisibility(View.VISIBLE);
        profile_txt.setVisibility(View.GONE);
        botm_nav.setVisibility(VISIBLE);
        home_bottom_icon.setVisibility(VISIBLE);
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.yellow_update));
        langfrom = "home";
        fragment = new RohahomeHomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        back.setOnClickListener(this::onClick);
    }
    public void profilefragmentload() {
        navigationchangefragment(new ProfileFragment(),4,0);
    }
    public void myorderfragmentload() {
        navigationchangefragment(new MyOrderFragment(),-1,2);
    }

    public void navigationchangefragment(Fragment fragment, int bnavid, int snavid){
        for (int i = 0; i < botm_nav.getMenu().size(); i++) {
            botm_nav.getMenu().getItem(i).setCheckable(false);
        }
        for(int j=0;j < navigationView.getMenu().size();j++){
            navigationView.getMenu().getItem(j).setCheckable(false);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment,fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        txt_delv_add.setVisibility(View.GONE);
        txt_loc.setVisibility(View.GONE);
        profile_txt.setVisibility(View.VISIBLE);
        side_nav_icon.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        notification.setVisibility(View.GONE);
        if(bnavid != -1 && snavid != -1){
            botm_nav.getMenu().getItem(bnavid).setCheckable(true);
            botm_nav.getMenu().getItem(bnavid).setChecked(true);
            navigationView.getMenu().getItem(snavid).setCheckable(true);
            navigationView.setCheckedItem(navigationView.getMenu().getItem(snavid).getItemId());
        }
        else if(snavid != -1){
            navigationView.getMenu().getItem(snavid).setCheckable(true);
            navigationView.setCheckedItem(navigationView.getMenu().getItem(snavid).getItemId());
        }
    }
    public void subchangefragment(Fragment fragment, Bundle bundle){
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment,fragment.getClass().getSimpleName()).addToBackStack(fragment.getClass().getSimpleName()).commit();
        txt_delv_add.setVisibility(View.GONE);
        txt_loc.setVisibility(View.GONE);
        profile_txt.setVisibility(View.VISIBLE);
        side_nav_icon.setVisibility(View.GONE);
        notification.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(this::onClick);
    }
    public void removeallfragments(){
        for(int i=0;i<getSupportFragmentManager().getBackStackEntryCount();i++){
            getSupportFragmentManager().popBackStack();
        }
        homefragmentload();
    }
    public void navigationselectclear(){
        for (int i = 0; i < botm_nav.getMenu().size(); i++) {
            botm_nav.getMenu().getItem(i).setCheckable(false);
        }
        for(int j=0;j < navigationView.getMenu().size();j++){
            navigationView.getMenu().getItem(j).setCheckable(false);
        }
    }

    public void livecarttolocal(JSONArray cartarray) {
        localcart = new JSONArray();
        sessionManager.setlocalcart(localcart.toString());
        if (cartarray.length() != 0) {
            for (int i = 0; i < cartarray.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = cartarray.getJSONObject(i);
                    localcartfunc(jsonObject.getString("product_name"),0, jsonObject.getInt("product_id"), jsonObject.getDouble("product_price"), jsonObject.getInt("quantity"), true, false, false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void localcartfunc(String producttname,int cartid, int producid, double productprice, int quantity, boolean add, boolean update, boolean delete) {
        if (localcart.length() == 0 || add == true) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("productname",producttname);
                jsonObject.put("cartid", 0);
                jsonObject.put("productid", producid);
                jsonObject.put("productprice", productprice);
                jsonObject.put("productquantity", quantity);
                localcart.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (localcart.length() > 0 && update == true) {
            for (int i = 0; i < localcart.length(); i++) {
                try {
                    if (localcart.getJSONObject(i).getInt("productid") == producid) {
                        localcart.getJSONObject(i).put("cartid",0);
                        localcart.getJSONObject(i).put("productname",producttname);
                        localcart.getJSONObject(i).put("productid", producid);
                        localcart.getJSONObject(i).put("productprice", productprice);
                        localcart.getJSONObject(i).put("productquantity", quantity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (localcart.length() > 0 && delete == true) {
            for (int i = 0; i < localcart.length(); i++) {
                try {
                    if (localcart.getJSONObject(i).getInt("productid") == producid) {
                        localcart.remove(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Gson gson = new Gson();
        String json = gson.toJson(localcart);
        sessionManager.setlocalcart(localcart.toString());
        Log.d("hgcvcxzc2", "" + sessionManager.getlocalcart());
        carticon();
    }
    public void carticon() {
        if (sessionManager.getlocalcart().length() == 0) {
            cartBadge.setVisibility(View.GONE);
            sessionManager.setCartcount(sessionManager.getlocalcart().length());
            cartcount.setText("" + sessionManager.getlocalcart().length());
            sessionManager.setIsCartempty(true);
        } else {
            cartBadge.setVisibility(View.VISIBLE);
            sessionManager.setCartcount(sessionManager.getlocalcart().length());
            cartcount.setText("" + sessionManager.getlocalcart().length());
            sessionManager.setIsCartempty(false);
        }

    }
    public void localtolive(Fragment cartFragment) {
        StaticInfo.cartdatachanged = false;
        tfragment = cartFragment;
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            emptycartrequestJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void emptycartrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deleteallcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("emptycartresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        if (sessionManager.getlocalcart().length() != 0) {
                            for (int i = 0; i < sessionManager.getlocalcart().length(); i++) {
                                JSONObject requestbody = new JSONObject();
                                requestbody.put("productid", sessionManager.getlocalcart().getJSONObject(i).getInt("productid"));
                                requestbody.put("userid", sessionManager.getUserId());
                                requestbody.put("branchid", branchid);
                                requestbody.put("quantity", sessionManager.getlocalcart().getJSONObject(i).getInt("productquantity"));
                                requestbody.put("cost", ((sessionManager.getlocalcart().getJSONObject(i).getInt("productprice")) * (sessionManager.getlocalcart().getJSONObject(i).getInt("productquantity"))));
                                addtocartrequestJSON(requestbody, i + 1);
                            }
                        }
                        else {
                            if (tfragment.getClass().toString().equalsIgnoreCase(new CartFragment().getClass().toString())) {
                                CartFragment cartFragment = (CartFragment) tfragment;
                                cartFragment.listcart();
                            }
                            else if(tfragment.getClass().toString().equalsIgnoreCase(new RohaHomeOrderSummaryFragment().getClass().toString())){
                                RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment = (RohaHomeOrderSummaryFragment) tfragment;
                                if(rohaHomeOrderSummaryFragment.procced.isEnabled() == true){
                                    rohaHomeOrderSummaryFragment.listcart();
                                }
                                else {
                                    rohaHomeOrderSummaryFragment.proceedclick();
                                }
                            }
                            else if (tfragment.getClass().toString().equalsIgnoreCase(new RohahomeHomeFragment().getClass().toString())) {
                                homefragmentload();
                                /*RohahomeDashboradActivity.this.finish();
                                Intent intent = new Intent(RohahomeDashboradActivity.this, RohahomeDashboradActivity.class);
                                startActivity(intent);*/
                            }
                        }
                    } else {
                        //sessionManager.snackbarToast(obj.getString("message"),relativeLayout);
                        if (sessionManager.getlocalcart().length() != 0) {
                            for (int i = 0; i < sessionManager.getlocalcart().length(); i++) {
                                JSONObject requestbody = new JSONObject();
                                requestbody.put("productid", sessionManager.getlocalcart().getJSONObject(i).getInt("productid"));
                                requestbody.put("userid", sessionManager.getUserId());
                                requestbody.put("branchid", branchid);
                                requestbody.put("quantity", sessionManager.getlocalcart().getJSONObject(i).getInt("productquantity"));
                                requestbody.put("cost", ((sessionManager.getlocalcart().getJSONObject(i).getInt("productprice")) * (localcart.getJSONObject(i).getInt("productquantity"))));
                                addtocartrequestJSON(requestbody, i + 1);
                            }
                        } else {
                            if (tfragment.getClass().toString().equalsIgnoreCase(new CartFragment().getClass().toString())) {
                                CartFragment cartFragment = (CartFragment) tfragment;
                                cartFragment.listcart();
                            }
                            else if(tfragment.getClass().toString().equalsIgnoreCase(new RohaHomeOrderSummaryFragment().getClass().toString())){
                                RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment = (RohaHomeOrderSummaryFragment) tfragment;
                                if(rohaHomeOrderSummaryFragment.procced.isEnabled() == true){
                                    rohaHomeOrderSummaryFragment.listcart();
                                }
                                else {
                                    rohaHomeOrderSummaryFragment.proceedclick();
                                }                            }
                            else if (tfragment.getClass().toString().equalsIgnoreCase(new RohahomeHomeFragment().getClass().toString())) {
                                homefragmentload();
                               /* RohahomeDashboradActivity.this.finish();
                                Intent intent = new Intent(RohahomeDashboradActivity.this, RohahomeDashboradActivity.class);
                                startActivity(intent);*/
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                //session.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    public void addtocartrequestJSON(JSONObject response, int length) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.addcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("addtocartresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        if (tfragment.getClass().toString().equalsIgnoreCase(new CartFragment().getClass().toString()) && length == sessionManager.getlocalcart().length()) {
                            CartFragment cartFragment = (CartFragment) tfragment;
                            cartFragment.listcart();
                        }
                        else if(tfragment.getClass().toString().equalsIgnoreCase(new RohaHomeOrderSummaryFragment().getClass().toString()) && length == sessionManager.getlocalcart().length()){
                            RohaHomeOrderSummaryFragment rohaHomeOrderSummaryFragment = (RohaHomeOrderSummaryFragment) tfragment;
                            if(rohaHomeOrderSummaryFragment.procced.isEnabled() == true){
                                rohaHomeOrderSummaryFragment.listcart();
                            }
                            else {
                                rohaHomeOrderSummaryFragment.proceedclick();
                            }                        }
                        else if (tfragment.getClass().toString().equalsIgnoreCase(new RohahomeHomeFragment().getClass().toString()) && length == sessionManager.getlocalcart().length()) {
                            homefragmentload();
                            /* RohahomeDashboradActivity.this.finish();
                            Intent intent = new Intent(RohahomeDashboradActivity.this, RohahomeDashboradActivity.class);
                            startActivity(intent);*/
                        }
                        else {

                        }
                    } else {
                        sessionManager.snackbarToast(obj.getString("message"), relativeLayout);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                // rohahomeDashboardActivity.sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void getprofile() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getUserId());
            profileviewrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void profileviewrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringviewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);

                        sessionManager.setusername(tok.getString("name"));
                        sessionManager.setusermail(tok.getString("email"));
                        sessionManager.setcountrycode(tok.getString("countrycode"));
                        sessionManager.setuserphoneno(tok.getString("phone"));
                        sessionManager.setdob(tok.getString("dob"));
                        sessionManager.setgender(tok.getString("gender"));
                        sessionManager.setaddress(tok.getString("address"));
                        sessionManager.setprofilepic(tok.getString("image"));
                        Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(nav_profile_pic);

                        int id = tok.getInt("user_address");
                        Log.d("hzcvxhc",""+id);
                        if (id == 0) {
                            sessionManager.setseletedaddressid(0);
                            sessionManager.setIscurrentlocationselected(true);
                        }
                        else {
                            sessionManager.setseletedaddressid(id);
                            sessionManager.setIscurrentlocationselected(false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void listcart() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            listcartrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void listcartrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        sessionManager.setIsCartempty(false);
                        JSONObject jsonObject = obj.getJSONObject("data");
                        cartarray = jsonObject.getJSONArray("cart_data");
                        livecarttolocal(cartarray);
                        cartBadge.setVisibility(View.VISIBLE);
                        sessionManager.setCartcount(cartarray.length());
                        cartcount.setText("" + cartarray.length());
                    } else {
                        sessionManager.setIsCartempty(true);
                        cartarray = new JSONArray();
                        cartBadge.setVisibility(View.GONE);
                        sessionManager.setCartcount(0);
                        livecarttolocal(cartarray);
                        // session.snackbarToast(obj.getString("message"),recyclerView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void listaddress(boolean isdelete) {
        JSONObject requestbody1 = new JSONObject();
        try {
            requestbody1.put("userid", sessionManager.getUserId());
            listaddressrequestJSON(requestbody1,isdelete);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void listaddressrequestJSON(JSONObject requestbody,boolean isdelete) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listaddressdataresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("dvdxv", "" + obj.getJSONArray("data"));
                        JSONArray data1 = obj.getJSONArray("data");
                        addressarray = obj.getJSONArray("data");
                        data2 = new ArrayList<AddressListDataModel>();
                        if (data1.length() == 0) {
                            adapter1 = new RohaHomeListAddressAdapter(getApplicationContext(), data2, RohahomeDashboradActivity.this);
                            recyclerView1.setAdapter(adapter1);
                        }
                        else {
                            for (int i = 0; i < data1.length(); i++) {
                                JSONObject jsonObject = data1.getJSONObject(i);
                                AddressListDataModel datamodel = new AddressListDataModel(jsonObject.getString("addresstype"), jsonObject.getInt("branchid"), jsonObject.getString("address"), jsonObject.getString("city"), jsonObject.getInt("id"), jsonObject.getDouble("lat"), jsonObject.getDouble("lng"));
                                data2.add(datamodel);
                            }
                            adapter1 = new RohaHomeListAddressAdapter(getApplicationContext(), data2, RohahomeDashboradActivity.this);
                            recyclerView1.setAdapter(adapter1);
                        }
                        if(isdelete == false){
                            commondatarequestJSON();
                        }
                    }
                    else {
                        data2 = new ArrayList<AddressListDataModel>();
                        adapter1 = new RohaHomeListAddressAdapter(getApplicationContext(), data2, RohahomeDashboradActivity.this);
                        recyclerView1.setAdapter(adapter1);
                        if(isdelete == false){
                            commondatarequestJSON();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void updateaddressidrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updateaddressid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        //String msg = obj.getString("data");
                        // JSONObject tok = new JSONObject(msg);
                        sessionManager.setseletedaddressid(0);
                        // showLocation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                // params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                // Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    // for bottom swipe up
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {
        String str = "";
        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                str = "Swipe Right";
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                str = "Swipe Left";
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                str = "Swipe Down";
              /*  RelativeLayout popup_loc1 = (RelativeLayout) findViewById(R.id.popup_loc);
                popup_loc1.setVisibility(View.GONE);*/
                //  botm_nav.setOnNavigationItemSelectedListener(MenuItem::isEnabled);

                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;
            case SimpleGestureFilter.SWIPE_UP:

               /* str = "Swipe Up";
                popup_loc2  = (RelativeLayout) findViewById(R.id.popup_loc);
                popup_loc2.setVisibility(View.VISIBLE);
              // botm_nav.setOnNavigationItemSelectedListener(null);*/
                //mBottomSheetBehavior.setPeekHeight(400);
                //    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                break;


        }

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {
        //   Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }


    private void commondatarequestJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.commonvalues, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("commondataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("ghj", "" + obj.getString("error"));
                        JSONObject obj1 = new JSONObject(obj.getString("data"));
                        StaticInfo.delivery = obj1.getDouble("delivery_charges");
                        StaticInfo.vat = obj1.getDouble("vat");
                        StaticInfo.order_prefix = obj1.getString("order_prefix");
                        StaticInfo.STOREID = obj1.getString("payment_storeid");
                        StaticInfo.KEY = obj1.getString("payment_key");
                        StaticInfo.MODE = obj1.getString("payment_mode");
                        //StaticInfo.payment_api = obj1.getString("payment_api");
                        //StaticInfo.payment_mail = obj1.getString("payment_mail");
                        //StaticInfo.profileId = obj1.getString("profileid");
                        //StaticInfo.serverKey = obj1.getString("serverkey");
                        //StaticInfo.clientKey = obj1.getString("clientkey");
                        if(obj1.getJSONArray("branches").length()!=0 && obj1.getJSONArray("branches")!=null){
                            sessionManager.setbrancharray(obj1.getJSONArray("branches").toString());
                        }
                        else {
                            sessionManager.setbrancharray("");
                        }
                        brancharray = obj1.getJSONArray("branches");
                        data1 = new ArrayList<CommonDataModel>();
                        for (int i = 0; i < brancharray.length(); i++) {
                            JSONObject jsonObject = brancharray.getJSONObject(i);
                            CommonDataModel datamodel = new CommonDataModel(jsonObject.getInt("id"), jsonObject.getString("branchname"));
                            data1.add(datamodel);
                        }
                        showLocation();
                    }
                    else {
                        sessionManager.snackbarToast("api issue",view);
                        sessionManager.setbrancharray("");
                        showLocation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void navheader() {
        Log.d("chvh", "" + sessionManager.getprofilepic());
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerview = navigationView.getHeaderView(0);
        navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);
        nav_close = (ImageView) navheader.findViewById(R.id.nav_close);
        nav_profile_pic = (ImageView) navheader.findViewById(R.id.img_header);
        if (!sessionManager.getprofilepic().equals("")) {
            Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(nav_profile_pic);
        }

        // using for image set
      /*  if(session.getprofilepic()!=null){
            Picasso.get().load(session.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(nav_profile_pic);
        }*/
        txt_edit_profile = (TextView) navheader.findViewById(R.id.edit_profile_txt);
        name = (TextView) navheader.findViewById(R.id.uname);
        name.setText(sessionManager.getusername());
        nav_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        txt_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                navigationchangefragment(new ProfileFragment(),4,0);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(StaticInfo.rohahomefirst == false) {
            switch (item.getItemId()) {
                case R.id.profilee:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new ProfileFragment(), 4, 0);
                    }
                    break;
                case R.id.wallet:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new WalletFragment(), 1, 4);
                    }
                    break;
                case R.id.cart:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new CartFragment(), 0, 1);
                    }
                    break;
                case R.id.orders:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new MyOrderFragment(), -1, 2);
                    }
                    break;
                case R.id.offers:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new OffersFragment(), -1, 3);
                    }
                    break;
                case R.id.moveapp:
                    //sessionManager.snackbarToast(getString(R.string.clientmsg),relativeLayout);
                    this.finish();
                    Intent intent = new Intent(RohahomeDashboradActivity.this, RohaWaterDashboardActivity.class);
                    startActivity(intent);
                    break;
                case R.id.credit_card:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new PaymentOptionsFragment(), -1, 5);
                    }
                    break;
                case R.id.invoices:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new InvoiceFragment1(), 3, 7);
                    }
                    break;
                case R.id.help:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new HelpFragment(), -1, 8);
                    }
                    break;
                case R.id.reset:
                    Intent intent1 = new Intent(RohahomeDashboradActivity.this, ResetPasswordActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.signoff:
                    FirebaseAuth.getInstance().signOut();
                    sessionManager.settoken("");
                    sessionManager.setusername("");
                    sessionManager.setusermail("");
                    sessionManager.setUserId(0);
                    sessionManager.setguest(0);
                    sessionManager.setuserphoneno("");
                    Intent intent56 = new Intent(RohahomeDashboradActivity.this, LoginNewActivity.class);
                    intent56.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent56);
                    break;

            }
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.bg_border_bottom_left_right_15dp_white));
            relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            botm_nav.setVisibility(VISIBLE);
            home_bottom_icon.setVisibility(VISIBLE);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            drawerLayout.closeDrawer(GravityCompat.START);
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == home_bottom_icon) {
            StaticInfo.cartdatachanged = true;
            localtolive(new RohahomeHomeFragment());
        }
        else if (v == popclose) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
        else if (v == side_nav_icon) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            drawerLayout.openDrawer(GravityCompat.START);
            botm_nav.setClickable(false);
        }
        else if (v == txt_loc) {
            // mBottomSheetBehavior.setPeekHeight(250,true);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            //mBottomSheetBehavior.setPeekHeight(350);
        }
        else if (v == add_address) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.bg_border_bottom_left_right_15dp_white));
            relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            Bundle args = new Bundle();
            args.putBoolean("update", false);
            subchangefragment(new AddaddressMapFragment(),args);
        }
        else if (v == notification) {
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.bg_border_bottom_left_right_15dp_white));
            relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            navigationchangefragment(new NotificationFragment(),-1,-1);
        }
        else if (v == currentlocation) {
           /* session.setseletedaddressid(0);
            showLocation();*/
            sessionManager.setIscurrentlocationselected(true);
            showLocation();
        }
        else if (v == relative) {
            botm_nav.setClickable(false);
        }
        else if (v == facebook) {
            Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
            myWebLink.setData(Uri.parse("https://www.facebook.com/rohaa.r.9"));
            startActivity(myWebLink);
        }
        else if (v == twitter) {

        }
        else if (v == instagram) {

        }
        else if (v == version) {
            // sessionManager.checkVersionAvailable();
        }
        else if (v == back) {
            getSupportFragmentManager().popBackStack();
        }
    }

    // for fetching current location
    private void showLocation() {
        locationListener();
        getLastKnownLocation();
    }

    private void locationListener() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval((20 * 1000));
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Log.d("xcvgsdcv","sdcsdcsd"+locationResult);
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        if(StaticInfo.rohahomefirst == true) {
                            getLastKnownLocation();
                        }
                    }
                }

            }

        };
    }

    private void getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.d("cbdhcsd",""+location);
                if (location != null) {
                    // searchPlaces(String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
                    LocalData.LocationAddress locationAddress = new LocalData.LocationAddress();
                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(), getApplicationContext(), new GeocoderHandler());

                } else {
                    if (ActivityCompat.checkSelfPermission(RohahomeDashboradActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RohahomeDashboradActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    fusedLocationProviderClient.requestLocationUpdates(
                            locationRequest,
                            locationCallback,
                            Looper.getMainLooper()
                    );
                }

            }
        });
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String currentCity = "";
            String currentAddress = "";
            String lat = "";
            String lng = "";
            int pincode;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    Log.d("gvbhnjmds",""+bundle);
                    Log.d("gvbhnjbvcmds",""+message.getData().toString());
                    currentAddress = bundle.getString("address");
                    currentCity = bundle.getString("city");
                    lat = bundle.getString("lat");
                    lng = bundle.getString("lng");
                    // pincode = Integer.parseInt(bundle.getString("pincode"));
                    break;
                default:
                    currentCity = "";
                    currentAddress = "";
                    lat = "";
                    lng = "";
                    pincode = 0;
            }
            if(currentCity != null && !currentCity.equals("")) {
                city = sessionManager.getcityname(currentCity);
                currentlocation.setText(""+currentAddress);

                if (sessionManager.getseletedaddressid() == 0 || sessionManager.iscurrentlocationselected() == true) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    if(sessionManager.isbranchAvailable(city) != 0){
                        if(sessionManager.isCartempty() == true){
                            branchid = sessionManager.isbranchAvailable(city);
                            sessionManager.setdlat("" + lat);
                            sessionManager.setdlng("" + lng);
                            sessionManager.setdaddr("" + currentAddress);
                            sessionManager.setdcity("" + city);
                            sessionManager.setdbranchid(branchid);
                            sessionManager.setIscurrentlocationselected(true);

                            sessionManager.setclat("" + lat);
                            sessionManager.setclng("" + lng);
                            sessionManager.setcaddr("" + currentAddress);
                            sessionManager.setccity("" + city);
                            sessionManager.setcbranchid(branchid);
                            txt_loc.setText(""+currentAddress);
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("userid", sessionManager.getUserId());
                                requestbody.put("addressid", 0);
                                updateaddressidrequestJSON(requestbody);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            homefragmentload();
                        }
                        else {
                            if (sessionManager.getdcity().equalsIgnoreCase(city)) {
                                branchid = sessionManager.isbranchAvailable(city);
                                sessionManager.setdlat("" + lat);
                                sessionManager.setdlng("" + lng);
                                sessionManager.setdaddr("" + currentAddress);
                                sessionManager.setdcity("" + city);
                                sessionManager.setdbranchid(branchid);
                                sessionManager.setIscurrentlocationselected(true);

                                sessionManager.setclat("" + lat);
                                sessionManager.setclng("" + lng);
                                sessionManager.setcaddr("" + currentAddress);
                                sessionManager.setccity("" + city);
                                sessionManager.setcbranchid(branchid);
                                txt_loc.setText(""+currentAddress);

                                JSONObject requestbody = new JSONObject();
                                try {
                                    requestbody.put("userid", sessionManager.getUserId());
                                    requestbody.put("addressid", 0);
                                    updateaddressidrequestJSON(requestbody);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                homefragmentload();
                            }
                            else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RohahomeDashboradActivity.this);
                                builder.setTitle(getString(R.string.alert));
                                String finalCurrentAddress = currentAddress;
                                String finalLat = lat;
                                String finalLng = lng;
                                builder.setMessage("Your Current Delivery address is " + sessionManager.getdcity() + " if you change your cart products will delete automatically are you sure to continue")
                                        .setCancelable(true)
                                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //do things
                                                dialog.dismiss();
                                            }
                                        })
                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //do things;
                                                JSONObject requestbody = new JSONObject();
                                                try {
                                                    requestbody.put("userid", sessionManager.getUserId());
                                                    emptycartrequestJSON(requestbody, city, finalLat, finalLng, finalCurrentAddress);
                                                    dialog.dismiss();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                final AlertDialog dialog = builder.create();
                                dialog.show(); //show() should be called before dialog.getButton().

                                final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                                positiveButton.setBackgroundColor(Color.TRANSPARENT);
                                //positiveButton.setTextColor(Color.BLACK);
                                LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                                positiveButtonLL.gravity = Gravity.CENTER;
                                positiveButton.setLayoutParams(positiveButtonLL);
                            }
                        }
                    }
                    else {
                        if(sessionManager.isCartempty() == true){
                            if (sessionManager.getseletedaddressid() == 0) {
                                branchid = 0;
                                homefragmentload();
                            }
                            noserviceDialog(city);
                        }
                        else {
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("userid", sessionManager.getUserId());
                                emptycartrequestJSON1(requestbody);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                else if (sessionManager.iscurrentlocationselected() == false || sessionManager.getseletedaddressid() != 0) {
                    sessionManager.setclat("" + lat);
                    sessionManager.setclng("" + lng);
                    sessionManager.setcaddr("" + currentAddress);
                    sessionManager.setccity("" + city);
                    sessionManager.setcbranchid(0);

                    if (addressarray.length() != 0) {
                        for (int i = 0; i < addressarray.length(); i++) {
                            JSONObject jsonObject1 = null;
                            try {
                                jsonObject1 = addressarray.getJSONObject(i);
                                if (jsonObject1.getInt("id") == sessionManager.getseletedaddressid()) {
                                    if(sessionManager.isbranchAvailable(jsonObject1.getString("city")) != 0){
                                        branchid = jsonObject1.getInt("branchid");
                                        sessionManager.setdlat("" + jsonObject1.getDouble("lat"));
                                        sessionManager.setdlng("" + jsonObject1.getDouble("lng"));
                                        sessionManager.setdaddr("" + jsonObject1.getString("address"));
                                        sessionManager.setdcity("" + jsonObject1.getString("city"));
                                        sessionManager.setdbranchid(branchid);
                                        sessionManager.setIscurrentlocationselected(false);
                                        txt_loc.setText("" + jsonObject1.getString("address"));
                                        homefragmentload();
                                    }
                                    else {
                                        if(sessionManager.isCartempty() == true){
                                            branchid = 0;
                                            homefragmentload();
                                            noserviceDialog(city);
                                        }
                                        else {
                                            JSONObject requestbody = new JSONObject();
                                            try {
                                                requestbody.put("userid", sessionManager.getUserId());
                                                emptycartrequestJSON1(requestbody);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            else {
                Toast.makeText(getApplicationContext(),"We could not find your city..",Toast.LENGTH_SHORT).show();
            }

        }

    }

    public void searchPlaces(String lat,String lng){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBodyurl = "https://maps.googleapis.com/maps/api/geocode/json?"+"latlng=" + lat + "," + lng + "&sensor=true&key=" +StaticInfo.googlemapapikey;
        Log.d("reques34sds", requestBodyurl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestBodyurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("mapaddressresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    Gson gson = new Gson();
                    GetAddressModel getAddressModel = gson.fromJson(obj.toString(), GetAddressModel.class);
                    List<Prediction> value = getAddressModel.getPredictions();
                    if (value.size() != 0) {
                        if (value.get(0).getAddressComponents() != null && value.get(0).getAddressComponents().size() != 0) {
                            String currentAddress = "";
                            if (value.get(0).getAddressComponents().size() >= 2) {
                                currentAddress = value.get(0).getAddressComponents().get(0).getLong_name() + "," + value.get(0).getAddressComponents().get(1).getLong_name();
                                city = value.get(0).getAddressComponents().get(3).getShort_name();
                            } else if (value.get(0).getAddressComponents().size() >= 1) {
                                currentAddress = value.get(0).getAddressComponents().get(0).getLong_name();
                            }


                        }
                        else {
                            // Toast.makeText(RohahomeDashboradActivity.this, R.string.locationerror, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        // Toast.makeText(RohahomeDashboradActivity.this, R.string.locationerror, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void noserviceDialog(String city){
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        AlertDialog.Builder builder = new AlertDialog.Builder(RohahomeDashboradActivity.this);
        builder.setMessage(getString(R.string.noservice1) + " " + city)
                .setCancelable(true)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things;
                        dialog.dismiss();
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show(); //show() should be called before dialog.getButton().

        final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setBackgroundColor(Color.TRANSPARENT);
        //positiveButton.setTextColor(Color.BLACK);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.gravity = Gravity.CENTER;
        positiveButton.setLayoutParams(positiveButtonLL);
    }

    private void emptycartrequestJSON(JSONObject response, String city,String lat,String lng,String currentaddress) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deleteallcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("emptycartresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        branchid = sessionManager.isbranchAvailable(city);
                        sessionManager.setdlat(""+lat);
                        sessionManager.setdlng(""+lng);
                        sessionManager.setdaddr(""+currentaddress);
                        sessionManager.setdcity(""+city);
                        sessionManager.setdbranchid(branchid);
                        sessionManager.setIscurrentlocationselected(true);

                        sessionManager.setclat(""+lat);
                        sessionManager.setclng(""+lng);
                        sessionManager.setcaddr(""+currentaddress);
                        sessionManager.setccity(""+city);
                        sessionManager.setcbranchid(branchid);
                        txt_loc.setText(""+currentaddress);
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("userid", sessionManager.getUserId());
                            requestbody.put("addressid", 0);
                            updateaddressidrequestJSON(requestbody);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        homefragmentload();
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),botm_nav);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                //session.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void emptycartrequestJSON1(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.deleteallcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("emptycartresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        finish();
                        Intent intent = new Intent(RohahomeDashboradActivity.this,RohahomeDashboradActivity.class);
                        startActivity(intent);
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                //session.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        StaticInfo.currentactivityname = getClass().getName();
        if (sessionManager.getlang().equals("ar")) {
            sessionManager.setAppLocale("ar");
        } else {
            sessionManager.setAppLocale("en");
        }
    }

    //use for home button and recent button press
    @Override
    protected void onUserLeaveHint() {
        if(StaticInfo.cartdatachanged == true){
            Log.d("vcgshc","vdxvxv");
            localtolive(new Fragment());
            // Toast.makeText(this,"Home press",Toast.LENGTH_LONG).show();
        }
        super.onUserLeaveHint();
    }

   /* //use for home button and recent button press
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getEventType() != AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED || event.getClassName() == null)
            return;

        String className = String.valueOf(event.getClassName());

        if (className.equals("com.android.internal.policy.impl.RecentApplicationsDialog")
                || className.equals("com.android.systemui.recent.RecentsActivity")
                || className.equals("com.android.systemui.recents.RecentsActivity")){
            //Recent button was pressed. Do something.
            Toast.makeText(RohahomeDashboradActivity.this,"Home buton pressed1",Toast.LENGTH_LONG).show();
        }
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        Log.d("xcvxb12",""+requestCode+".."+resultCode+".."+intent);
        if (requestCode == 105 && resultCode == RESULT_OK) {
            if(LocalData.getInstance().getTransactiontype().equalsIgnoreCase("wallet")){
                WalletFragment walletFragment = new WalletFragment();
                walletFragment.onActivityResult(requestCode, resultCode, intent);
            }
            else if(LocalData.getInstance().getTransactiontype().equalsIgnoreCase("order")){
                PaymentFragment paymentFragment = new PaymentFragment();
                paymentFragment.onActivityResult(requestCode, resultCode, intent);
            }
        }
    }

}