package com.lia.yello.roha.model;

import com.google.android.gms.maps.model.LatLng;

public class WaterproductDataModel {
    String pname,pimage;
    LatLng latlng;
    int id,tot_Qty,mpid,stockquantity;
    Double tot_Cost,Unit_Cost,vat,delivery_charges;
    String payment_api,payment_mail,order_prefix;

    public WaterproductDataModel(String pimage,String name,int id,int mproductid, int tot_Qty, Double tot_Cost, Double Unit_Cost,int stockquantity) {
        this.pname = name;
        this.pimage = pimage;
        this.latlng = latlng;
        this.id = id;
        this.mpid = mproductid;
        this.vat = vat;
        this.tot_Cost = tot_Cost;
        this.tot_Qty = tot_Qty;
        this.Unit_Cost = Unit_Cost;
        this.stockquantity = stockquantity;
        this.delivery_charges = delivery_charges;
        this.payment_api = payment_api;
        this.payment_mail = payment_mail;
        this.order_prefix = order_prefix;
    }

    public String getPimage(){
        return pimage;
    }

    public int getStockquantity(){
        return stockquantity;
    }

    public String getPname() {
        return pname;
    }

    public int getMpid(){
        return mpid;
    }
    public String getPayment_api(){
        return payment_api;
    }

    public String getPayment_mail(){
        return payment_mail;
    }

    public String getOrder_prefix(){
        return order_prefix;
    }
    public LatLng getLatlng() {
        return latlng;
    }

    public int getId() {
        return id;
    }
    public Double getvat() {
        return vat;
    }
    public Double getdeliverycharge() {
        return delivery_charges;
    }


    public Double getunitcost() {
        return Unit_Cost;
    }
    public int gettotqty() {
        return tot_Qty;
    }
    public Double gettotcost() {
        return tot_Cost;
    }

}
