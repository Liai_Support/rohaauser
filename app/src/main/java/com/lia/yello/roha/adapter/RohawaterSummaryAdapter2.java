package com.lia.yello.roha.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.SummaryDataModel2;

import java.util.List;
import java.util.Locale;

public class RohawaterSummaryAdapter2 extends RecyclerView.Adapter<RohawaterSummaryAdapter2.MyViewHolder> {
    private Context mcontext;
    private List<SummaryDataModel2> dataSet;
    public RohaWaterOrderSummaryFragment summaryActivity;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName,sumcorton,sumprice,unitqty;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.sumcorton = (TextView) itemView.findViewById(R.id.sum_cortons);
            this.sumprice = (TextView) itemView.findViewById(R.id.sum_price);
            this.unitqty = (TextView) itemView.findViewById(R.id.unitqty);

        }
    }

    public RohawaterSummaryAdapter2(Context context, List<SummaryDataModel2> data, RohaWaterOrderSummaryFragment summaryActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.summaryActivity = summaryActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_water_summary2, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        Log.d("cvxc",""+dataSet.size());
        if(summaryActivity.sessionManager.getlang().equals("ar")){
            holder.sumprice.setBackgroundTintList(ColorStateList.valueOf(summaryActivity.getResources().getColor(R.color.yellow_update)));
            holder.sumprice.setBackground(summaryActivity.getResources().getDrawable(R.drawable.bg_border_custom1_white));
        }
        holder.textViewName.setText(dataSet.get(listPosition).getName());
        holder.unitqty.setText(""+summaryActivity.unitsqty);
        if(dataSet.get(listPosition).getOffercount()!=0){
            holder.sumcorton.setText(String.valueOf(dataSet.get(listPosition).getCount())+"+"+dataSet.get(listPosition).getOffercount()+" "+summaryActivity.getString(R.string.Carton));
        }
        else {
            holder.sumcorton.setText(String.valueOf(dataSet.get(listPosition).getCount())+" "+summaryActivity.getString(R.string.Carton));
        }
        holder.sumprice.setText("" + String.format(Locale.ENGLISH,"%.2f", dataSet.get(listPosition).getPrice()) + " SAR");
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}