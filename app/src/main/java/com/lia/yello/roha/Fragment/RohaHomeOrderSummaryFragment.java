package com.lia.yello.roha.Fragment;

import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.DaySlotAdapter;
import com.lia.yello.roha.adapter.RohahomeSummaryListAddressAdapter;
import com.lia.yello.roha.adapter.Rohahomesummaryadapter1;
import com.lia.yello.roha.adapter.Rohahomesummaryadapter2;
import com.lia.yello.roha.model.AddressListDataModel;
import com.lia.yello.roha.model.CartDataModel;
import com.lia.yello.roha.model.DayDetailsDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RohaHomeOrderSummaryFragment extends Fragment implements View.OnClickListener {
    View view;
    public TextView addmore,productname,productname1,quantity1,productname2,quantity,subtotal1,txt_vat,txt_delv_charges,txt_total
            ,subtotal,txt_discount,txt_timeofdelivery1,txt_payment_method1,txt_promo_code1,txt_add_note1,txt_delivery_loc1,txt_timeofdelivery2,txt_payment_method2,txt_promo_code2,txt_add_note2,txt_delivery_loc2,add_address,txt_promocode;
    int count = 10,mpid;
    ImageView plus,minus,card_close,image;
    CardView cardView1;
    public Button procced,cancle,confirm_item,apply;
    int fqty;
    double ftotal;
    RelativeLayout relv_time,relv_pay_method,relv_promo_code,relv_add_note,relv_dilv,relv_summary;
    public SessionManager sessionManager;
    int stockquantity;
    double price,fsubtotal,fvat,fdelivery;
    private static RecyclerView.Adapter adapter1;
    private static RecyclerView.Adapter adapter2;
    public static RecyclerView.Adapter adapter3;
    public RecyclerView recyclerlocation,recycler_day_slot,recycler_time_slot;
    public ArrayList<AddressListDataModel> data;
    public ArrayList<DayDetailsDataModel> data3;
    public  String faddr = "";
    RadioButton cash,wallet,swipemachine,online;
    public  double flat,flng;
    public int bid;
    private RadioGroup radioGroup;
    public RadioButton clocation;
    String paymenttype = "";
    public int discount_id=0,dayid=10,timeid=0;
    public String discount_type = "0";
    public String dicount_value = "0";
    public  String addnote_txt, fcity,promocodee;
    LinearLayout linr_delivery,linear_promocode,linr_below_cnfrm;
    View delivery_view,promo_view;
    Fragment fragment=null;
    EditText promocode,addnote_edittxt;
    public RecyclerView recycler1,recycler2;
    public ArrayList<CartDataModel> data5;
    private static Rohahomesummaryadapter1 adapter4;
    private static RecyclerView.Adapter adapter5;
    public double ntotal = 0;
    public double nvat = 0;
    public double nsubtotal = 0;
    public double ndelivery = 0;
    public int nfqty = 0;
    public int nstock = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_home_order_summary, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);

        ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.summary);

        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Log.d("xcx", "backsucess");
                    StaticInfo.cartdatachanged = false;
                    getActivity().onBackPressed();
                }
            }));
        }
        ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticInfo.cartdatachanged = false;
                getActivity().onBackPressed();
            }
        });

        recycler1 = (RecyclerView) view.findViewById(R.id.recycler1);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
        recycler1.setLayoutManager(layoutManager1);

        recycler2 = (RecyclerView) view.findViewById(R.id.recycler2);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getContext());
        recycler2.setLayoutManager(layoutManager2);

        fcity = sessionManager.getdcity();
        getwalletamount();
        linr_delivery=(LinearLayout)view.findViewById(R.id.linr3_cart6);
        linear_promocode=(LinearLayout)view.findViewById(R.id.linr4_cart7);
        promo_view=(View)view.findViewById(R.id.view_cart7);

        apply=(Button) view.findViewById(R.id.btn_apply);
        promocode=(EditText)view.findViewById(R.id.promocode);

        clocation =(RadioButton) view.findViewById(R.id.clocation);
        radioGroup = (RadioGroup)view.findViewById(R.id.radioGroup);
        cash = (RadioButton)view.findViewById(R.id.radio_cash);
        swipemachine = (RadioButton)view.findViewById(R.id.radio_swipemachine);
        wallet = (RadioButton)view.findViewById(R.id.radio_wallet);
        online = (RadioButton)view.findViewById(R.id.radio_online);


        // textview id
        addmore = (TextView)view.findViewById(R.id.txt2_cart1);
        productname=(TextView)view.findViewById(R.id.txt_productname);
        productname1=(TextView)view.findViewById(R.id.txt_productname1);
        productname2=(TextView)view.findViewById(R.id.txt_productname2);
        add_address=(TextView)view.findViewById(R.id.add_adress_clk);
        quantity=(TextView)view.findViewById(R.id.txt_qty);
        quantity1=(TextView)view.findViewById(R.id.txt_qty1);
        txt_vat=(TextView)view.findViewById(R.id.txt_vat);
        txt_delv_charges=(TextView)view.findViewById(R.id.txt_delv_charges);
        subtotal=(TextView)view.findViewById(R.id.txt_subtotal);
        subtotal1=(TextView)view.findViewById(R.id.txt_subtotal1);
        txt_total=(TextView)view.findViewById(R.id.txt_total);
        procced = (Button)view.findViewById(R.id.btn_proceed);
        procced.setEnabled(true);
        cancle = (Button)view.findViewById(R.id.btn_cancel);
        confirm_item = (Button)view.findViewById(R.id.btn_confrm_itm);
        txt_promocode = (TextView) view.findViewById(R.id.txt_promocode);
        txt_discount = (TextView) view.findViewById(R.id.txt_discount);

        //  Image view id
        plus=(ImageView) view.findViewById(R.id.plus);
        minus=(ImageView) view.findViewById(R.id.minus);
        card_close=(ImageView) view.findViewById(R.id.card_close);
        cardView1=(CardView)view.findViewById(R.id.cardview1_orders);
        image=(ImageView)view.findViewById(R.id.img_card);


        //  id of txtview yellow txt
        txt_delivery_loc1 = (TextView)view.findViewById(R.id.txt_delivery_loc1);
        txt_timeofdelivery1 = (TextView)view.findViewById(R.id.txt_timeofdelivery1);
        txt_payment_method1 = (TextView)view.findViewById(R.id.txt_payment_method1);
        txt_promo_code1 = (TextView)view.findViewById(R.id.txt_promo_code1);
        txt_add_note1 = (TextView)view.findViewById(R.id.txt_add_note1);

        txt_delivery_loc2 = (TextView)view.findViewById(R.id.txt_delivery_loc2);
        txt_timeofdelivery2 = (TextView)view.findViewById(R.id.txt_timeofdelivery2);
        txt_payment_method2 = (TextView)view.findViewById(R.id.txt_payment_method2);
        txt_promo_code2 = (TextView)view.findViewById(R.id.txt_promo_code2);
        txt_add_note2 = (TextView)view.findViewById(R.id.txt_add_note2);


        // id of relative layout
        relv_time=(RelativeLayout)view.findViewById(R.id.revlay_time);
        relv_pay_method=(RelativeLayout)view.findViewById(R.id.relv_payment);
        relv_promo_code=(RelativeLayout)view.findViewById(R.id.relv_promo_code);
        relv_add_note=(RelativeLayout)view.findViewById(R.id.relv_add_note);
        relv_dilv=(RelativeLayout)view.findViewById(R.id.relv_dilv_loc_order);
        relv_summary=(RelativeLayout)view.findViewById(R.id.relative_total_suummay);
        linr_below_cnfrm=(LinearLayout)view.findViewById(R.id.linear_below_cnfrm);

        addnote_edittxt=(EditText)view.findViewById(R.id.addnote_edittxt);

        delivery_view=(View)view.findViewById(R.id.view_cart6);
        promo_view=(View)view.findViewById(R.id.view_cart7);

        txt_delivery_loc1.setText(""+ sessionManager.getdaddr());
        txt_delivery_loc2.setText(""+ sessionManager.getdaddr());
        faddr = sessionManager.getdaddr();
        flat = Double.parseDouble(sessionManager.getdlat());
        flng = Double.parseDouble(sessionManager.getdlng());
        bid = sessionManager.getdbranchid();
        fcity = sessionManager.getdcity();
        if(sessionManager.iscurrentlocationselected() == true){
            clocation.setChecked(true);
        }
        if(sessionManager.getlang().equals("ar")){
            subtotal.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
            subtotal.setBackground(getResources().getDrawable(R.drawable.bg_border_custom1_white));
        }
        addnote_edittxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                addnote_txt=addnote_edittxt.getText().toString();
                Log.d("jnjkmoiidoi",""+addnote_txt);

            }

            @Override
            public void afterTextChanged(Editable s) {
                addnote_txt=addnote_edittxt.getText().toString();
                Log.d("jnjkmoiidoi",""+addnote_txt);
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                int selectedId=radioGroup.getCheckedRadioButtonId();
                RadioButton rb=(RadioButton)view.findViewById(checkedId);
                if(cash.isChecked()){
                    paymenttype = "cod";
                    txt_payment_method1.setText(""+rb.getText().toString());
                    txt_payment_method2.setText(""+rb.getText().toString());

                }
                else if(swipemachine.isChecked()){
                    paymenttype = "swipemachine";
                    txt_payment_method1.setText(""+rb.getText().toString());
                    txt_payment_method2.setText(""+rb.getText().toString());

                }
                else if(wallet.isChecked()){
                    paymenttype = "wallet";
                    txt_payment_method1.setText(""+rb.getText().toString());
                    txt_payment_method2.setText(""+rb.getText().toString());

                }
                else if(online.isChecked()){
                    paymenttype = "card";
                    txt_payment_method1.setText(""+rb.getText().toString());
                    txt_payment_method2.setText(""+rb.getText().toString());

                }
            }
        });



        //listcart();
        if(sessionManager.getlocalcart().length()!=0){
            ((RohahomeDashboradActivity)getActivity()).localtolive(RohaHomeOrderSummaryFragment.this);
        }
        else {
            ((RohahomeDashboradActivity)getActivity()).localtolive(RohaHomeOrderSummaryFragment.this);
        }
        linr_delivery.setVisibility(View.GONE);
        delivery_view.setVisibility(View.GONE);

        // data coming from adapter
      /*  Bundle args = getArguments();
        pname = args.get("pname").toString();
        stockquantity = Integer.parseInt(args.get("stockquantity").toString());
        price = Integer.parseInt(args.get("price").toString());
        pimg = args.get("image").toString();
        mpid = Integer.parseInt(args.get("mpid").toString());*/


     /*  // Picasso.get().load(pimg).placeholder(R.mipmap.ic_launcher_foreground).error(R.mipmap.ic_launcher_round).into(image);
        Glide.with(getContext()).load(pimg).placeholder(R.drawable.defaultimg).into(image);

        quantity.setText("10");
        quantity1.setText("10"+" Cartons");
        fqty = count;
        productname.setText(pname);
        productname1.setText(pname);
        productname2.setText(pname);
        double tot = price*count;
        fsubtotal = tot;
        double Finalvatvatvalue = (tot / 100.0f) * StaticInfo.vat;
        fvat = Finalvatvatvalue;
        txt_vat.setText(""+String.format("%.2f", Finalvatvatvalue)+" SAR");
        double delivery = StaticInfo.delivery;
        fdelivery = delivery;
        txt_delv_charges.setText(""+String.format("%.2f", StaticInfo.delivery)+ " SAR");

        fdelivery = 0;
        linr_delivery.setVisibility(View.GONE);
        delivery_view.setVisibility(View.GONE);

        subtotal.setText(""+String.format("%.2f", tot)+" SAR");
        subtotal1.setText(""+String.format("%.2f", tot)+" SAR");
        double total=tot+Finalvatvatvalue+fdelivery;
        ftotal = total;
        txt_total.setText(""+String.format("%.2f", total)+" SAR");

*/

        // click listner
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        txt_timeofdelivery1.setOnClickListener(this);
        txt_payment_method1.setOnClickListener(this);
        txt_promo_code1.setOnClickListener(this);
        txt_add_note1.setOnClickListener(this);
        txt_delivery_loc1.setOnClickListener(this);
        txt_timeofdelivery2.setOnClickListener(this);
        txt_payment_method2.setOnClickListener(this);
        txt_promo_code2.setOnClickListener(this);
        txt_add_note2.setOnClickListener(this);
        txt_delivery_loc2.setOnClickListener(this);
        relv_time.setOnClickListener(this);
        relv_pay_method.setOnClickListener(this);
        relv_promo_code.setOnClickListener(this);
        relv_add_note.setOnClickListener(this);
        relv_dilv.setOnClickListener(this);
        procced.setOnClickListener(this);
        cancle.setOnClickListener(this);
        confirm_item.setOnClickListener(this);
        add_address.setOnClickListener(this);
        apply.setOnClickListener(this);
        addmore.setOnClickListener(this);
        //  addnote_edittxt.addTextChangedListener(this);


        recyclerlocation =(RecyclerView)view.findViewById(R.id.recycler_location);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerlocation.setLayoutManager(layoutManager);
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            locationrequestJSON(requestbody);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        clocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setIscurrentlocationselected(true);
                clocation.setChecked(true);
                txt_delivery_loc1.setText(""+ sessionManager.getcaddr());
                txt_delivery_loc2.setText(""+ sessionManager.getcaddr());
                faddr = sessionManager.getcaddr();
                flat = Double.parseDouble(sessionManager.getclat());
                flng = Double.parseDouble(sessionManager.getclng());
                bid = sessionManager.getcbranchid();
                fcity = sessionManager.getccity();
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("userid", sessionManager.getUserId());
                    locationrequestJSON(requestbody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        recycler_day_slot=(RecyclerView)view.findViewById(R.id.recycler_day_slot);
        recycler_day_slot.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));

        recycler_time_slot=(RecyclerView)view.findViewById(R.id.recycler_time_slot);
        recycler_time_slot.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        timedetailsJSON();
        return view;

    }

    public void listcart(){
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            listcartrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void listcartrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listcart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listcartresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        nsubtotal = 0;
                        nvat = 0;
                        ntotal = 0;
                        ndelivery = 0;
                        JSONObject jsonObject=obj.getJSONObject("data");
                        JSONArray data1 = jsonObject.getJSONArray("cart_data");
                        Log.d("cfghjkkjhgf",""+data1);
                        ((RohahomeDashboradActivity)getActivity()).cartBadge.setVisibility(View.VISIBLE);
                        sessionManager.setCartcount(data1.length());
                        ((RohahomeDashboradActivity)getActivity()).livecarttolocal(data1);
                        ((RohahomeDashboradActivity)getActivity()).cartcount.setText(""+ sessionManager.getCartcount());
                        data5 = new ArrayList<CartDataModel>();
                        nsubtotal = 0;
                        ntotal = 0;
                        nvat = 0;
                        nfqty = 0;
                        nstock = 0;
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject1 = data1.getJSONObject(i);
                            CartDataModel datamodel = new CartDataModel(jsonObject1.getInt("cart_id"),jsonObject1.getInt("product_id"),jsonObject1.getString("product_name"),jsonObject1.getString("product_image"),jsonObject1.getInt("quantity"),jsonObject1.getDouble("cost"),jsonObject1.getInt("isstock"),jsonObject1.getDouble("product_price"),jsonObject1.getInt("stock_quantity"));
                            data5.add(datamodel);
                            if(jsonObject1.getInt("isstock") == 1) {
                                nstock = jsonObject1.getInt("stock_quantity");
                                nsubtotal = nsubtotal + jsonObject1.getDouble("cost");
                                nfqty = nfqty + jsonObject1.getInt("quantity");
                            }
                        }
                        nvat = ((nsubtotal / 100.0f) * StaticInfo.vat);
                        double totalamt = (nsubtotal + nvat);
                        ntotal = totalamt;
                        subtotal1.setText("" + String.format(Locale.ENGLISH,"%.2f", nsubtotal)+" SAR");
                        txt_total.setText(""+getString(R.string.total)+" : " + String.format(Locale.ENGLISH,"%.2f", totalamt)+" SAR");
                        txt_vat.setText("" + String.format(Locale.ENGLISH,"%.2f", nvat)+" SAR");

                        adapter4 = new Rohahomesummaryadapter1(getContext(), data5, RohaHomeOrderSummaryFragment.this,((RohahomeDashboradActivity)getActivity()));
                        recycler1.setAdapter(adapter4);
                        recycleradapter2();
                    }
                    else {
                        nsubtotal = 0;
                        nvat = 0;
                        ntotal = 0;
                        ndelivery = 0;
                        nstock = 0;
                        sessionManager.setCartcount(0);
                        ((RohahomeDashboradActivity)getActivity()).cartBadge.setVisibility(View.GONE);
                        fragment = new CartFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void recycleradapter2(){
        adapter5 = new Rohahomesummaryadapter2(getContext(), data5, RohaHomeOrderSummaryFragment.this);
        recycler2.setAdapter(adapter5);
    }


    @Override
    public void onClick(View v) {
        if(v==plus){
            Log.d("dvfv",""+stockquantity);
            if(stockquantity > Integer.parseInt(quantity.getText().toString())){
                count++;
                quantity.setText(""+count);
                quantity1.setText(""+count+" cartons");
                fqty = count;
                double tot = price*count;
                fsubtotal = tot;
                double Finalvatvatvalue = (tot / 100.0f) * StaticInfo.vat;
                fvat =Finalvatvatvalue;
                txt_vat.setText(""+String.format("%.2f", Finalvatvatvalue)+" SAR");
                // double delivery = StaticInfo.delivery;
                fdelivery = StaticInfo.delivery;
                if(count>=10){
                    fdelivery = 0;
                    linr_delivery.setVisibility(View.GONE);
                    delivery_view.setVisibility(View.GONE);
                }
                txt_delv_charges.setText(""+String.format("%.2f", StaticInfo.delivery)+ " SAR");
                subtotal.setText(""+String.format("%.2f", tot)+" SAR");
                subtotal1.setText(""+String.format("%.2f", tot)+" SAR");
                double total=tot+Finalvatvatvalue+fdelivery;
                txt_total.setText(""+getString(R.string.total)+" : " +String.format("%.2f", total)+" SAR");
                ftotal = total;
            }
            else {
                // snackbarToast("Stock limit is reached");
                Log.d("no stoke is limit","");
            }
        }
        else if(v==minus){
            if(Integer.parseInt(quantity.getText().toString()) > 10){
                count--;
                quantity.setText(""+count);
                quantity1.setText(""+count+" cartons");
                fqty = count;
                double tot = price*count;
                fsubtotal = tot;
                double Finalvatvatvalue = (tot / 100.0f) * StaticInfo.vat;
                fvat = Finalvatvatvalue;
                txt_vat.setText(""+String.format("%.2f", Finalvatvatvalue)+" SAR");
                fdelivery = StaticInfo.delivery;
                if(count>=10){
                    linr_delivery.setVisibility(View.GONE);
                    delivery_view.setVisibility(View.GONE);
                    fdelivery = 0;
                }
                else {
                    linr_delivery.setVisibility(View.VISIBLE);
                    delivery_view.setVisibility(View.VISIBLE);
                    fdelivery = StaticInfo.delivery;
                }
                txt_delv_charges.setText(""+String.format("%.2f", StaticInfo.delivery)+ " SAR");
                subtotal.setText(""+String.format("%.2f", tot)+" SAR");
                subtotal1.setText(""+String.format("%.2f", tot)+" SAR");
                double total=tot+Finalvatvatvalue+fdelivery;
                txt_total.setText(""+getString(R.string.total)+" : " +String.format("%.2f", total)+" SAR");
                ftotal = total;

            }
            else {
                //snackbarToast("Minimum order carton is 10");
            }
        }
        else if(v==txt_timeofdelivery1){
            txt_timeofdelivery1.setVisibility(View.GONE);
            relv_time.setVisibility(View.VISIBLE);
        }
        else if(v==txt_timeofdelivery2){
            relv_time.setVisibility(View.GONE);
            txt_timeofdelivery1.setVisibility(View.VISIBLE);
        }
        else if(v==addmore){
            ((RohahomeDashboradActivity)getActivity()).homefragmentload();
        }
        else if(v==txt_payment_method1){
            txt_payment_method1.setVisibility(View.GONE);
            relv_pay_method.setVisibility(View.VISIBLE);
        }
        else if(v==txt_payment_method2){
            relv_pay_method.setVisibility(View.GONE);
            txt_payment_method1.setVisibility(View.VISIBLE);

        }
        else if(v==txt_promo_code1){
            txt_promo_code1.setVisibility(View.GONE);
            relv_promo_code.setVisibility(View.VISIBLE);
        }
        else if(v==txt_promo_code2){
            relv_promo_code.setVisibility(View.GONE);
            txt_promo_code1.setVisibility(View.VISIBLE);

        }
        else if(v==txt_add_note1){
            txt_add_note1.setVisibility(View.GONE);
            relv_add_note.setVisibility(View.VISIBLE);
        }
        else if(v==txt_add_note2){
            relv_add_note.setVisibility(View.GONE);
            txt_add_note1.setVisibility(View.VISIBLE);

            addnote_txt=addnote_edittxt.getText().toString();


        }
        else if(v==txt_delivery_loc1){
            txt_delivery_loc1.setVisibility(View.GONE);
            relv_dilv.setVisibility(View.VISIBLE);
        }
        else if(v==txt_delivery_loc2){
            relv_dilv.setVisibility(View.GONE);
            txt_delivery_loc1.setVisibility(View.VISIBLE);
        }
        else if(v==procced){
            if(nfqty<10){
                sessionManager.snackbarToast(getString(R.string.minimumquantityis10),procced);
            }
            else if(!fcity.equals(sessionManager.getdcity())){
                sessionManager.snackbarToast(""+getString(R.string.hereyoucanchangethelocationonlythatbelongsto)+" "+ sessionManager.getdcity(),view);
            }
            else if(timeid==0 || dayid==10){
                sessionManager.snackbarToast(getString(R.string.pleasechoosedeliverytime),procced);
            }
            else if(paymenttype.equals("")){
                sessionManager.snackbarToast(getString(R.string.pleasechoosepaymentmethod),procced);
            }
            else {
                procced.setEnabled(false);
                ((RohahomeDashboradActivity)getActivity()).localtolive(RohaHomeOrderSummaryFragment.this);
            }
        }
        else if(v==cancle){
            StaticInfo.cartdatachanged = false;
            getActivity().onBackPressed();
        }
        else if(v==add_address){
            Bundle args = new Bundle();
            args.putBoolean("update", false);
            ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddaddressMapFragment(),args);
        }
        else if(v==confirm_item){

            // relv_summary.setVisibility(View.VISIBLE);

            linr_below_cnfrm.setVisibility(View.VISIBLE);

        }
        else if(v==apply){
            if (promocode.getText().toString().equals("")){
                sessionManager.snackbarToast(getString(R.string.enterpromocode),view);
            }
            else {
                promocodee = promocode.getText().toString().replaceAll("\\s", "");
                Log.d("promocodeeee",""+promocodee);
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("userid", sessionManager.getUserId());
                    requestbody.put("promocode", promocodee);
                    requestbody.put("offerfor", "home");
                    validoffersJSON(requestbody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void proceedclick(){
        Log.d("vhxv","dcdc");
        procced.setEnabled(true);
        if (!faddr.equals("") && !paymenttype.equals("") && timeid!=0 && dayid!=10) {
            if(paymenttype.equals("wallet")){
                if(sessionManager.getwalletamount() >= ntotal){
                    storedata();
                    Bundle args = new Bundle();
                    args.putBoolean("ibandata",false);
                    ((RohahomeDashboradActivity)getActivity()).subchangefragment(new SuccessFragment(),null);
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.walletislow),view);
                }
            }
            else if(paymenttype.equals("cod") || paymenttype.equals("swipemachine")){
                storedata();
                Bundle args = new Bundle();
                args.putBoolean("ibandata",false);
                ((RohahomeDashboradActivity)getActivity()).subchangefragment(new SuccessFragment(),args);
            }
            else if(paymenttype.equals("card")){
                storedata();
                ((RohahomeDashboradActivity)getActivity()).subchangefragment(new PaymentFragment(),null);
            }
        }
        else {
            sessionManager.snackbarToast(getString(R.string.enterallfields),view);
        }

    }

    private void storedata(){
        LocalData.getInstance().setTotalqty(String.valueOf(nfqty));

        LocalData.getInstance().setTotalprice(String.format(Locale.ENGLISH,"%.2f",ntotal));
        LocalData.getInstance().setVat(String.format(Locale.ENGLISH,"%.2f",nvat));
        LocalData.getInstance().setDeliverycharge(String.format(Locale.ENGLISH,"%.2f",ndelivery));
        LocalData.getInstance().setBranchid(String.valueOf(bid));
        LocalData.getInstance().setSubtotal(String.format(Locale.ENGLISH,"%.2f",nsubtotal));
        LocalData.getInstance().setProductid(String.valueOf(mpid));
        LocalData.getInstance().setOrderaddress(faddr);
        LocalData.getInstance().setTransactionid("");
        LocalData.getInstance().setPaymenttype(paymenttype);
        LocalData.getInstance().setOrderlat(String.valueOf(flat));
        LocalData.getInstance().setOrderlng(String.valueOf(flng));
        LocalData.getInstance().setDayid(String.valueOf(dayid));
        LocalData.getInstance().setTimeid(String.valueOf(timeid));
        LocalData.getInstance().setDiscountid(String.valueOf(discount_id));
        LocalData.getInstance().setDiscountvalue(String.valueOf(dicount_value));
        LocalData.getInstance().setDiscounttype(String.valueOf(discount_type));
        LocalData.getInstance().setNotes(addnote_edittxt.getText().toString());

    }

    private void locationrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("locationdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("dvdxv",""+obj.getJSONArray("data"));
                        JSONArray data1 = obj.getJSONArray("data");
                        data = new ArrayList<AddressListDataModel>();
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject = data1.getJSONObject(i);
                            AddressListDataModel datamodel = new AddressListDataModel(jsonObject.getString("addresstype"),jsonObject.getInt("branchid"),jsonObject.getString("address"),jsonObject.getString("city"),jsonObject.getInt("id"),jsonObject.getDouble("lat"),jsonObject.getDouble("lng"));
                            data.add(datamodel);

                        }
                        adapter1 = new RohahomeSummaryListAddressAdapter(getContext(), data, RohaHomeOrderSummaryFragment.this);
                        recyclerlocation.setAdapter(adapter1);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void validoffersJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.validate_offers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("validoffersdataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("data");
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        //  Log.d("hghghghghg",""+obj.getJSONObject("data"));
                        // JSONObject jsonObject = obj.getJSONObject("data");
                        if(jsonObject.getDouble("minimum_offer_amount") <= ntotal){
                            if(jsonObject.getString("type").equals("SAR")){
                                Double s=Double.parseDouble(jsonObject.getString("discount"));
                                Log.d("jxbhdbchgdc",""+s);
                                sessionManager.snackbarToast(getString(R.string.promocodeapplied),apply);
                                linear_promocode.setVisibility(View.VISIBLE);
                                promo_view.setVisibility(View.VISIBLE);
                                txt_promocode.setText(""+String.format(Locale.ENGLISH,"%.2f", s)+" SAR");
                                ntotal = ntotal-s;
                                dicount_value = String.valueOf(s);
                                discount_type = jsonObject.getString("type");
                                txt_total.setText(""+getString(R.string.total)+" : " +String.format(Locale.ENGLISH,"%.2f", ntotal)+" SAR");
                                //subtotal.setText(""+total+"SAR");
                                apply.setEnabled(false);
                                adapter4.isClickable = false;
                                relv_promo_code.setVisibility(View.GONE);
                                txt_promo_code1.setText(getString(R.string.promocodeapplied));
                                txt_promo_code1.setVisibility(View.VISIBLE);
                                txt_promo_code1.setEnabled(false);
                                discount_id=jsonObject.getInt("id");
                                // ftotal=total;
                            }
                            else if(jsonObject.getString("type").equals("Percentage")){
                                Double sss=Double.parseDouble(jsonObject.getString("discount"));
                                sessionManager.snackbarToast(getString(R.string.promocodeapplied),apply);
                                Double dd=(ntotal / sss);
                                linear_promocode.setVisibility(View.VISIBLE);
                                promo_view.setVisibility(View.VISIBLE);
                                txt_promocode.setText(""+String.format(Locale.ENGLISH,"%.2f", dd)+" SAR");
                                ntotal = ntotal-dd;
                                dicount_value = String.valueOf(dd);
                                discount_type = jsonObject.getString("type");
                                txt_total.setText(""+getString(R.string.total)+" : " +String.format(Locale.ENGLISH,"%.2f", ntotal)+" SAR");
                                //subtotal.setText(""+total+"SAR");
                                apply.setEnabled(false);
                                adapter4.isClickable = false;
                                relv_promo_code.setVisibility(View.GONE);
                                txt_promo_code1.setVisibility(View.VISIBLE);
                                txt_promo_code1.setEnabled(false);
                                discount_id=jsonObject.getInt("id");
                                txt_promo_code1.setText(getString(R.string.promocodeapplied));
                            }
                            else if(jsonObject.getString("type").equalsIgnoreCase("Carton")){
                                if(nstock >= nfqty+(int) jsonObject.getDouble("discount")){
                                    sessionManager.snackbarToast(getString(R.string.promocodeapplied),apply);
                                    txt_discount.setText(getString(R.string.offer));
                                    linear_promocode.setVisibility(View.VISIBLE);
                                    promo_view.setVisibility(View.VISIBLE);
                                    txt_promocode.setText(""+(int) jsonObject.getDouble("discount")+" - "+jsonObject.getString("carton_name"));
                                    apply.setEnabled(false);
                                    adapter4.isClickable = false;
                                    relv_promo_code.setVisibility(View.GONE);
                                    txt_promo_code1.setText(getString(R.string.promocodeapplied));
                                    txt_promo_code1.setVisibility(View.VISIBLE);
                                    txt_promo_code1.setEnabled(false);
                                    discount_id=jsonObject.getInt("id");
                                    discount_type = jsonObject.getString("type");
                                    dicount_value = String.valueOf((int) jsonObject.getDouble("discount"));
                                }
                                else {
                                    sessionManager.snackbarToast(getString(R.string.stocknotavailable),apply);
                                }
                            }
                        }
                        else {
                            sessionManager.snackbarToast(String.valueOf(getString(R.string.thisofferonlyapplicableorderamountabove)+" "+jsonObject.getDouble("minimum_offer_amount")+" SAR"),apply);
                        }
                    }
                    else {
                        sessionManager.snackbarToast(obj.getString("message"),apply);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void getwalletamount(){
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            getwalletamountJSON(requestbody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void getwalletamountJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.getwallet, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("walletresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        Log.d("walleterror",""+msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        tok.getString("userid");
                        tok.getString("amount");
                        // show_amt.setText(String.format("%.2f",tok.getDouble("amount"))+" SAR");
                        sessionManager.setwalletamount(Double.parseDouble(String.format("%.2f",tok.getDouble("amount"))));
                        Log.d("fdvdf",""+ sessionManager.getwalletamount());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void timedetailsJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.timeslot, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("timedetailsresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("dsfrft", "" + obj.getJSONArray("data"));
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        JSONArray data1 = obj.getJSONArray("data");
                        data3 = new ArrayList<DayDetailsDataModel>();
                        for(int a=0;a< data1.length(); a++){
                            JSONObject jsonObject = data1.getJSONObject(a);
                            Log.d("123",""+jsonObject.getString("date_id"));
                            Log.d("1234",""+jsonObject.getJSONArray("details"));
                            JSONArray jsonArray = jsonObject.getJSONArray("details");
                            DayDetailsDataModel datamodel = new DayDetailsDataModel(jsonObject.getInt("date_id"), jsonObject.getString("day"), jsonObject.getString("date"));
                            data3.add(datamodel);
                            int date_id= jsonObject.getInt("date_id");
                            Log.d("bvaszxdcfvgh",""+date_id);




                            /*for (int j =0;j<jsonArray.length();j++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(j);
                                TimeDetailsDataModel datamodel1 = new TimeDetailsDataModel(jsonObject2.getInt("id"), jsonObject2.getString("delivery_from"), jsonObject2.getString("delivery_to"), jsonObject2.getString("session"));
                                data4.add(datamodel1);
                            }
                            adapter3 = new TimeSlotAdapter(getContext(), data4, RohaHomeOrderSummaryFragment.this);
                            recycler_time_slot.setAdapter(adapter3);*/
                        }
                        adapter2 = new DaySlotAdapter(getContext(), data3, RohaHomeOrderSummaryFragment.this,obj1);
                        recycler_day_slot.setAdapter(adapter2);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                if(error instanceof NoConnectionError){
                    //session.slownetwork(RohawaterDashboardActivity.this);
                }
            }
        }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("lang", session.getlang());
                Log.d("param", "" + params);
                return params;
            }*/

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
}