package com.lia.yello.roha.model;

import java.util.ArrayList;

public class TimeDetailsDataModel {
    String delivery_from,delivery_to,session;
    int id;
    public TimeDetailsDataModel(int id,String delivery_from,String delivery_to,String session){
        this.id=id;
        this.delivery_from=delivery_from;
        this.delivery_to=delivery_to;
        this.session=session;

    }

    public int getId() {
        return id;
    }

    public String getDelivery_from() {
        return delivery_from;
    }

    public String getDelivery_to() {
        return delivery_to;
    }

    public String getSession() {
        return session;
    }
}
