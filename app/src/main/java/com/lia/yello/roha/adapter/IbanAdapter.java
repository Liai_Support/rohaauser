package com.lia.yello.roha.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.PaymentFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.IbanDataModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class IbanAdapter extends RecyclerView.Adapter<IbanAdapter.MyViewHolder> {
    private Context mcontext;
    private List<IbanDataModel> dataSet;
    private PaymentFragment paymentActivity;
    public int mSelectedItem = -1;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView ibanno,accountno;
        ImageView img;
        RadioButton radioButton;
        ImageView acccopy,ibancopy;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.ibanno = (TextView) itemView.findViewById(R.id.iban_no);
            this.accountno = (TextView) itemView.findViewById(R.id.account_no);
            this.img = (ImageView) itemView.findViewById(R.id.img);
            this.radioButton = (RadioButton) itemView.findViewById(R.id.radio);
            this.acccopy = (ImageView) itemView.findViewById(R.id.acc_copy);
            this.ibancopy = (ImageView) itemView.findViewById(R.id.iban_copy);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    ibancopy.setVisibility(View.GONE);
                    acccopy.setVisibility(View.GONE);
                }
            };
            itemView.setOnClickListener(clickListener);
            radioButton.setOnClickListener(clickListener);
        }
    }

    public IbanAdapter(Context context,List<IbanDataModel>  data, PaymentFragment paymentActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.paymentActivity = paymentActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_iban, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        if(paymentActivity.sessionManager.getlang().equals("ar")){
            holder.accountno.setGravity(View.FOCUS_RIGHT);
            holder.ibanno.setGravity(View.FOCUS_RIGHT);
            holder.ibanno.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        }
        holder.ibanno.setText(""+String.valueOf(dataSet.get(listPosition).getIbanno()));
        holder.accountno.setText(""+String.valueOf(dataSet.get(listPosition).getAccountno()));
        Picasso.get().load(dataSet.get(listPosition).getImg()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(holder.img);

        holder.radioButton.setChecked(listPosition == mSelectedItem);
        holder.acccopy.setVisibility(View.GONE);
        holder.ibancopy.setVisibility(View.GONE);

        if(holder.radioButton.isChecked()){
            String df = String.valueOf(dataSet.get(listPosition).getId());
            holder.acccopy.setVisibility(View.VISIBLE);
            holder.ibancopy.setVisibility(View.VISIBLE);
            paymentActivity.ibanaccountnoselect = true;
            paymentActivity.ibannoid = dataSet.get(listPosition).getId();
        }
        holder.acccopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) mcontext.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", dataSet.get(listPosition).getAccountno());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mcontext,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
            }
        });

        holder.ibancopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) mcontext.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", dataSet.get(listPosition).getIbanno());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mcontext,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}