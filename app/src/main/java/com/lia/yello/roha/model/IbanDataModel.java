package com.lia.yello.roha.model;

public class IbanDataModel {
    String img,ibanno,accountno;
    int id;
    public IbanDataModel(int id,String accountno,String ibanno,String img) {
        this.img = img;
        this.id  =id;
        this.ibanno = ibanno;
        this.accountno = accountno;

    }

    public int getId(){
        return id;
    }

    public String getImg(){
        return img;
    }

    public String getIbanno(){
        return ibanno;
    }
    public String getAccountno(){
        return accountno;
    }
}
