package com.lia.yello.roha.utility;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocalData {
    private LocalData() {

    }

    private static LocalData instance = null;

    public static LocalData getInstance() {
        if (instance == null) {
            instance = new LocalData();
        }

        return instance;
    }

    public static void removeAllValues() {
        instance = new LocalData();
    }

    private String dayid = "";
    private String timeid = "";
    private String totalqty = "";
    private String subtotal = "";
    private String vat = "";
    private String deliverycharge = "";
    private String discountid = "";
    private String discountamount = "";
    private String discounttype = "";
    private String totalprice = "";
    private String transactionid = "";
    private String transactiontype = "";
    private String paymenttype = "";
    private String branchid = "";
    private String productid = "";
    private String place = "";
    private String wishlistplace = "";
    private String notes = "";
    private String ibanno = "";
    private String unitqty = "";

    private JSONArray bodyarray = new JSONArray();

    private String orderaddress = "";
    private String orderlat = "";
    private String orderlng = "";

    private String paymentamount = "";
    private String itemname = "";
    private String cost = "";
    private String quantity = "";

    private ArrayList<LatLng> markerlatlng = new ArrayList<>();
    private ArrayList<JSONObject> wishlistArray = new ArrayList<JSONObject>();
    private ArrayList<String> markertitle = new ArrayList<>();
    private ArrayList<String> markerplace = new ArrayList<>();
    private ArrayList<Integer> markerplaceid = new ArrayList<>();
    private ArrayList<Integer> ismeccaint = new ArrayList<>();
    private ArrayList<Integer> wishlistId = new ArrayList<>();
    private ArrayList<String> ismeccastr = new ArrayList<>();


    private ArrayList<String> fragpage = new ArrayList<>();

    public void setFragpage(ArrayList<String> fragpage) {
        this.fragpage = fragpage;
    }

    public ArrayList<String> getFragpage() {
        return fragpage;
    }

    public void setPaymentamount(String paymentamount){
        this.paymentamount = paymentamount;
    }

    public String getPaymentamount() {
        return paymentamount;
    }


    public void setOverallMarkersbranchid(ArrayList<Integer> markerplaceid) {
        this.markerplaceid = markerplaceid;
    }

    public ArrayList<Integer> getOverallMarkersbranchid() {
        return markerplaceid;
    }

    public void setOverallMarkersbranchname(ArrayList<String> markerplace) {
        this.markerplace = markerplace;
    }

    public ArrayList<String> getOverallMarkersbranchname() {
        return markerplace;
    }

    public void setIsmeccaIntHeaderArray(ArrayList<Integer> ismeccaint) {
        this.ismeccaint = ismeccaint;
    }

    public ArrayList<Integer> getIsmeccaIntHeaderArray() {
        return ismeccaint;
    }

    public void setIsmeccaStrHeaderArray(ArrayList<String> ismeccastr) {
        this.ismeccastr = ismeccastr;
    }


    public ArrayList<String> getIsmeccaStrHeaderArray() {
        return ismeccastr;
    }

    public void setOverallMarkersTitle(ArrayList<String> markertitle) {
        this.markertitle = markertitle;
    }

    public ArrayList<String> getOverallMarkersTitle() {
        return markertitle;
    }

    public void setOverallMarkersLatlng(ArrayList<LatLng> markerlatlng) {
        this.markerlatlng = markerlatlng;
    }

    public ArrayList<LatLng> getOverallMarkersLatlng() {
        return markerlatlng;
    }

    public JSONArray getBodyarray() {
        //Log.d("jhbjhget","hghh");
        return bodyarray;
    }

    public void setBodyarray(JSONArray bodyarray) {
        //Log.d("jhbjh","hghh");
        this.bodyarray = bodyarray;
    }

    public String getUnitqty() {
        return unitqty;
    }

    public void setUnitqty(String unitqty) {
        this.unitqty = unitqty;
    }





    public String getDayid() {
        return dayid;
    }

    public void setDayid(String dayid) {
        this.dayid = dayid;
    }

    public String getTimeid() {
        return timeid;
    }

    public void setTimeid(String timeid) {
        this.timeid = timeid;
    }

    public String getTotalqty() {
        return totalqty;
    }

    public void setTotalqty(String totalqty) {
        this.totalqty = totalqty;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }
    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }
    public String getDeliverycharge() {
        return deliverycharge;
    }

    public void setDeliverycharge(String deliverycharge) {
        this.deliverycharge = deliverycharge;
    }
    public String getDiscountid() {
        return discountid;
    }

    public void setDiscountid(String discountid) {
        this.discountid = discountid;
    }
    public String getDiscountvalue() {
        return discountamount;
    }

    public void setDiscountvalue(String discountamount) {
        this.discountamount = discountamount;
    }

    public String getDiscounttype() {
        return discounttype;
    }

    public void setDiscounttype(String discounttype) {
        this.discounttype = discounttype;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }
    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }



    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }



    // newly added


    public String getitemname() {
        return itemname;
    }

    public void setitemname(String itemname) {
        this.itemname = itemname;
    }


    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }
    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getWishlistplace() {
        return wishlistplace;
    }

    public void setWishlistplace(String wishlistplace) {
        this.wishlistplace = wishlistplace;
    }

    public ArrayList<Integer> getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(ArrayList<Integer> wishlistId) {
        this.wishlistId = wishlistId;
    }

    public ArrayList<JSONObject> getWishlistArray() {
        return wishlistArray;
    }

    public void setWishlistArray(ArrayList<JSONObject> wishlistArray) {
        this.wishlistArray = wishlistArray;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getOrderaddress() {
        return orderaddress;
    }

    public void setOrderaddress(String orderaddress) {
        this.orderaddress = orderaddress;
    }

    public String getOrderlat() {
        return orderlat;
    }

    public void setOrderlat(String orderlat) {
        this.orderlat = orderlat;
    }
    public String getOrderlng() {
        return orderlng;
    }

    public void setOrderlng(String orderlng) {
        this.orderlng = orderlng;
    }

    public String getIbanno() {
        return ibanno;
    }

    public void setIbanno(String ibanno) {
        this.ibanno = ibanno;
    }


    public static class LocationAddress {
        private static final String TAG = "LocationAddress";

        public static void getAddressFromLocation(final double latitude, final double longitude,
                                                  final Context context, final Handler handler) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
                    String result = null;
                    String city = null;
                    String pin = null;
                    try {
                        List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                        if (addressList != null && addressList.size() > 0) {
                            Address address = addressList.get(0);
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                sb.append(address.getAddressLine(i)).append("\n");
                            }
                            sb.append(address.getLocality()).append("\n");
                            sb.append(address.getPostalCode()).append("\n");
                            sb.append(address.getCountryName());
                            city = String.valueOf(address.getLocality());
                            pin = String.valueOf(address.getPostalCode());
                            //result = sb.toString();
                            result = addressList.get(0).getAddressLine(0);
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Unable connect to Geocoder", e);
                    } finally {
                        Message message = Message.obtain();
                        message.setTarget(handler);
                        if (result != null) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                           // result = "Latitude: " + latitude + " Longitude: " + longitude + "\n\nAddress:\n" + result;
                            bundle.putString("lat", String.valueOf(latitude));
                            bundle.putString("lng", String.valueOf(latitude));
                            bundle.putString("address", result);
                            bundle.putString("city",city);
                            bundle.putString("pincode",pin);
                            message.setData(bundle);
                        } else {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n Unable to get address for this lat-long.";
                            bundle.putString("address", result);
                            message.setData(bundle);
                        }
                        message.sendToTarget();
                    }
                }
            };
            thread.start();
        }
    }
}
