//package com.lia.yello.roha.activity
//
//import android.os.Bundle
//import android.util.Log
//import android.view.View
//import android.widget.*
//import androidx.appcompat.app.AppCompatActivity
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.android.volley.DefaultRetryPolicy
//import com.android.volley.Response
//import com.android.volley.toolbox.StringRequest
//import com.android.volley.toolbox.Volley
//import com.lia.yello.roha.Fragment.PaymentFragment
//import com.lia.yello.roha.R
//import com.lia.yello.roha.adapter.IbanAdapter
//import com.lia.yello.roha.model.IbanDataModel
//import com.lia.yello.roha.utility.LocalData
//import com.lia.yello.roha.utility.SessionManager
//import com.lia.yello.roha.utility.StaticInfo
//import com.moyasar.android.sdk.PaymentConfig
//import com.moyasar.android.sdk.PaymentResult
//import com.moyasar.android.sdk.PaymentSheet
//import com.moyasar.android.sdk.payment.models.Payment
//import org.json.JSONArray
//import org.json.JSONObject
//
//public class PaymentActivity1 : AppCompatActivity(),View.OnClickListener {
//    var paymentSheet: PaymentSheet? = null
//    val config = PaymentConfig(
//        amount = 100,
//        currency = "SAR",
//        description = "Sample Android SDK Payment",
//        apiKey = "pk_test_vcFUHJDBwiyjsh73khFuPpTnRPY4gp2aaYdNddY3"
//    )
//
//    var view: View? = null
//
//    var sessionManager: SessionManager? = null
//    var back: ImageView? = null
//    var promocode: String? = null
//    var payiban = false
//    public  var ibanaccountnoselect:kotlin.Boolean = false
//    var cod:Boolean = false
//    var wallet:Boolean = false
//    var ibannoid = 0
//    var paytype: String? = null
//    var btnproceed: Button? = null
//    var btncancel:android.widget.Button? = null
//    var recyclerView: RecyclerView? = null
//    var totalshow: TextView? = null
//    private var data1: ArrayList<IbanDataModel>? = null
//
//
//    val KEY =
//        StaticInfo.KEY //"s66Mx#BMN5-djBWj"; //"BwxtF~dq9L#xgWZb";//pQ6nP-7rHt@5WRFv        // TODO: Insert your Key here
//
//    val STORE_ID =
//        StaticInfo.STOREID //"18503";//"21941";   //15996           // TODO: Insert your Store ID here
//
//    val isSecurityEnabled =
//        true // Mark false to test on simulator, True to test on actual device and Production
//
//    val EMAIL = "girish.spryox@gmail.com" //"?????????"  // TODO fill email  // TODO fill email
//
//    //let EMAIL:String = "girish.spryox@gmail.com" //"?????????"  // TODO fill email
//    var newarray = JSONArray()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.fragment_payment)
//        view = findViewById<View>(R.id.root) as RelativeLayout
//
//        sessionManager = SessionManager(this, this,view)
////        back =findViewById<View>(R.id.back) as ImageView
////        totalshow = findViewById<View>(R.id.total_show) as TextView
//        back!!.setOnClickListener(this)
////        getwalletamount()
//
//        val onlinepay = findViewById<LinearLayout>(R.id.online_pay)
//        val iban = findViewById<LinearLayout>(R.id.iban_pay)
//        val stcpay = findViewById<LinearLayout>(R.id.stc_pay)
//        val cash = findViewById<LinearLayout>(R.id.cash)
//        val rohaawallet = findViewById<LinearLayout>(R.id.rohaawallet)
//        val btnproceed = findViewById<Button>(R.id.btn_proceed)
//        val btncancel = findViewById<Button>(R.id.btn_cancel)
//        val recyclerView = findViewById<RecyclerView>(R.id.recyler_iban)
//        val layoutManager = LinearLayoutManager(this)
//        recyclerView.layoutManager = layoutManager
//        btncancel.setOnClickListener(this)
//        btnproceed.setOnClickListener(this)
//        onlinepay.setOnClickListener(this)
//        iban.setOnClickListener(this)
//        stcpay.setOnClickListener(this)
//        cash.setOnClickListener(this)
//        rohaawallet.setOnClickListener(this)
//        btnproceed.visibility = View.GONE
//        btncancel.visibility = View.GONE
//        recyclerView.visibility = View.GONE
//        if (sessionManager!!.getchoose() == "rohahome") {
//            cash.visibility = View.INVISIBLE
//            rohaawallet.visibility = View.INVISIBLE
//            stcpay.visibility = View.INVISIBLE
//            onlinepay.visibility = View.VISIBLE
//            iban.visibility = View.VISIBLE
//            totalshow!!.text =
//                "" + getString(R.string.totalamount) + " : " + LocalData.getInstance().totalprice + " SAR"
//            sessionManager!!.progressdialogshow()
//            ibandatarequestJSON()
//        } else if (sessionManager!!.getchoose() == "rohawater") {
//            cash.visibility = View.GONE
//            stcpay.visibility = View.GONE
//            rohaawallet.visibility = View.VISIBLE
//            onlinepay.visibility = View.VISIBLE
//            iban.visibility = View.VISIBLE
//            totalshow!!.text =
//                "" + getString(R.string.totalamount) + " : " + LocalData.getInstance().totalprice + " SAR"
//            sessionManager!!.progressdialogshow()
//            ibandatarequestJSON()
//        }
//
//
//
//
//        paymentSheet = PaymentSheet(this, { handlePaymentResult(it) }, config)
//
//        val donateBtn = findViewById<Button>(R.id.button)
//        donateBtn.setOnClickListener {
//            paymentSheet!!.present()
//        }
//    }
//    override fun onResume() {
//        super.onResume()
//    }
//
//    private fun ibandatarequestJSON() {
//        val requestQueue = Volley.newRequestQueue(this)
//        val stringRequest: StringRequest =
//            object : StringRequest(Method.GET, StaticInfo.URLstringibandata,
//                Response.Listener { response ->
//                    Log.d("ibandataresponse", ">>$response")
//                    sessionManager!!.progressdialogdismiss()
//                    try {
//                        val obj = JSONObject(response)
//                        if (obj.getBoolean("error") == false) {
//                            Log.d("ghj", "" + obj.getString("error"))
//                            val data = JSONArray(obj.getString("data"))
//                            data1 = java.util.ArrayList()
//                            for (i in 0 until data.length()) {
//                                val jsonObject = data.getJSONObject(i)
//                                val datamodel = IbanDataModel(jsonObject.getInt("id"),
//                                    jsonObject.getString("acc_no"),
//                                    jsonObject.getString("iban_no"),
//                                    jsonObject.getString("logo"))
//                               data1!!.add(datamodel)
//                            }
//                            val adapter1: RecyclerView.Adapter<*> = IbanAdapter(this,
//                                data1,
//                                this)
//                            recyclerView!!.adapter = adapter1
//                        }
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                },
//                Response.ErrorListener { error ->
//                    Log.e("VOLLEY", error.toString())
//                    sessionManager!!.progressdialogdismiss()
//                    sessionManager!!.volleyerror(error)
//                }) {
//                override fun getBodyContentType(): String {
//                    return "application/json; charset=utf-8"
//                }
//            }
//        stringRequest.retryPolicy =
//            DefaultRetryPolicy(10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
//        Log.d("asdsdjkxdk", "" + stringRequest)
//        requestQueue.add(stringRequest)
//    }
//
//
//    fun handlePaymentResult(result: PaymentResult) {
//        when (result) {
//            is PaymentResult.Completed -> {
//                handleCompletedPayment(result.payment);
//            }
//            is PaymentResult.Failed -> {
//                // Handle error
//                var error = result.error;
//            }
//            PaymentResult.Canceled -> {
//                // User has canceled the payment
//            }
//        }
//    }
//
//    fun handleCompletedPayment(payment: Payment) {
//        when (payment.status) {
//            "paid" -> { /* Handle successful payment */ }
//            "failed" -> {
//                var errorMessage = payment.source["message"]
//                /* Handle failed payment */
//            }
//            else -> { /* Handle other statuses */ }
//        }
//    }
//
//    override fun onClick(v: View?) {
//
//    }
//}