package com.lia.yello.roha.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    EditText cpass, npass, cnpass;
    Button reset;
    private SessionManager sessionManager;
    ImageView back;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this, this, view);
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(this);
        }
        cpass = (EditText) findViewById(R.id.cpass);
        npass = (EditText) findViewById(R.id.npass);
        cnpass = (EditText) findViewById(R.id.cnpass);
        reset = (Button) findViewById(R.id.reset);
        reset.setOnClickListener(this);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(this);
        } else {
            if (view == reset) {
                String specialcharreg = ".*[@#!$%^&+=].*";
                String uppercasereg = ".*[A-Z].*";
                String numberreg = ".*[0-9].*";
                String cpassword = String.valueOf(cpass.getText().toString());
                String npassword = String.valueOf(npass.getText().toString());
                String cnpassword = String.valueOf(cnpass.getText().toString());
                if (!npassword.equals(cnpassword)) {
                    npass.setError(getString(R.string.enterpasswordcorrectly));
                    cnpass.setError(getString(R.string.enterpasswordcorrectly));
                    sessionManager.snackbarToast(getString(R.string.passwordandconfirmpasswordmismatch), view);
                } else if (cpass.getText().toString().trim().length() == 0 || npass.getText().toString().trim().length() == 0 || cnpass.getText().toString().trim().length() == 0) {
                    cpass.setError(getString(R.string.enterallfields));
                    npass.setError(getString(R.string.enterallfields));
                    cnpass.setError(getString(R.string.enterallfields));
                    sessionManager.snackbarToast(getString(R.string.enterallfields), view);
                }
           /* else if ((npassword.length() < 8) || (!npassword.matches(specialcharreg)) || (!npassword.matches(uppercasereg)) || (!npassword.matches(numberreg))) {
                npass.setError(getString(R.string.passmusthavesymbol));
                cnpass.setError(getString(R.string.passmusthavesymbol));
                //Snackbar.make(view,"Password must have 8 Characters,One UpperCase,Numeric,Special Symbol",Snackbar.LENGTH_SHORT).show();
                snackbarToast(getString(R.string.passmusthavesymbol));
            }*/
                else if (cpassword.equals(npassword)) {
                    sessionManager.snackbarToast(getString(R.string.currentpasswordandnewpasswordissame),view);
                    npass.setError(getString(R.string.enterpasswordcorrectly));
                    cnpass.setError(getString(R.string.enterpasswordcorrectly));
                } else {
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("userid", sessionManager.getUserId());
                        requestbody.put("oldpassword", cpassword);
                        requestbody.put("password", npassword);
                        resetrequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (view == back) {
                onBackPressed();
            }
        }
    }

    private void resetrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringresetpassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("passwordresetresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(getString(R.string.currentpasswordisincorrect),view);
                        cpass.setError(getString(R.string.enterpasswordcorrectly));
                    } else {
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}