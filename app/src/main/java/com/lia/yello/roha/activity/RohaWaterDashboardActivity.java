package com.lia.yello.roha.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.lia.yello.roha.Chat.MainActivity;
import com.lia.yello.roha.Fragment.CartFragment;
import com.lia.yello.roha.Fragment.HelpFragment;
import com.lia.yello.roha.Fragment.InvoiceFragment1;
import com.lia.yello.roha.Fragment.MyOrderFragment;
import com.lia.yello.roha.Fragment.NewInvoice;
import com.lia.yello.roha.Fragment.NotificationFragment;
import com.lia.yello.roha.Fragment.OffersFragment;
import com.lia.yello.roha.Fragment.PaymentFragment;
import com.lia.yello.roha.Fragment.PaymentOptionsFragment;
import com.lia.yello.roha.Fragment.ProfileFragment;
import com.lia.yello.roha.Fragment.RohaWaterOrderSummaryFragment;
import com.lia.yello.roha.Fragment.RohawaterHomeFragment;
import com.lia.yello.roha.Fragment.WalletFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.AddressListDataModel;
import com.lia.yello.roha.model.CommonDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.squareup.picasso.Picasso;
import com.telr.mobile.sdk.activity.WebviewActivity;
import com.telr.mobile.sdk.entity.response.payment.MobileResponse;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RohaWaterDashboardActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener,View.OnClickListener{
    public SessionManager sessionManager;
    public LinearLayout navheader;
    public View headerview;
    public NavigationView navigationView;
    public int exitcount1 = 0;
    public int exitcount2 = 0;
    public DrawerLayout drawerLayout;
    public ArrayList<AddressListDataModel> data2;
    public ImageView nav_close, home_bottom_icon, side_nav_icon,back, nav_profile_pic,facebook,twitter,instagram;
    public TextView name, wallet_img, txt_loc, get_addrs,version;
    public TextView profile_txt,txt_delv_add,txt_edit_profile;
    public Fragment fragment = new Fragment();
    public  RecyclerView.Adapter adapter1;
    public BottomNavigationView botm_nav;
    RelativeLayout relativeLayout;
    public RohawaterHomeFragment rohawaterHomeFragment = new RohawaterHomeFragment();
    public ProgressBar progressBar;
    public String langfrom = "p";
    ImageView notification,popclose,add,delete;
    EditText currentlocation;
    Boolean fromnotification = false;
    View view;
    boolean doubleBackToExitPressedOnce = false;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roha_water_dashboard);
        view = (DrawerLayout) findViewById(R.id.drawer);
        sessionManager = new SessionManager(this,this,view);
        sessionManager.setchoose("rohawater");

        langfrom = getIntent().getStringExtra("langfrom");
        fromnotification = getIntent().getBooleanExtra("fromnotification",false);

        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(this);
        }

        relativeLayout = findViewById(R.id.relvhome);
        home_bottom_icon = (ImageView) findViewById(R.id.home_bottom);
        side_nav_icon = (ImageView) findViewById(R.id.homeee);
        back = (ImageView) findViewById(R.id.back);
        wallet_img = (TextView) findViewById(R.id.txtdeladd);
        txt_loc = (TextView) findViewById(R.id.txtloc);
        get_addrs = (TextView) findViewById(R.id.txtdelvadd);
        name = findViewById(R.id.uname);
        botm_nav = findViewById(R.id.bottom_navigation_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        profile_txt = (TextView) findViewById(R.id.Profile_txt);
        txt_delv_add = (TextView) findViewById(R.id.txtdelvadd);
        notification = (ImageView) findViewById(R.id.notification);
        currentlocation = (EditText)findViewById(R.id.edittxt_current_location);
        facebook = (ImageView) findViewById(R.id.facebook);
        instagram = (ImageView) findViewById(R.id.instagram);
        twitter = (ImageView) findViewById(R.id.twitter);
        version = (TextView) findViewById(R.id.version);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        //Glide.with(this).load(R.drawable.home_icon1).placeholder(R.drawable.home_icon1).into(home_bottom);
        //home_bottom.setBackground(getResources().getDrawable(R.drawable.bg_border_custom_home1));

        Glide.with(this).load(R.drawable.water_home_icon).placeholder(R.drawable.water_home_icon).into(home_bottom_icon);
        home_bottom_icon.setBackground(getResources().getDrawable(R.drawable.bg_border_custom_home1));


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        ScrollView nav_scrool = (ScrollView) findViewById(R.id.scroll);
        RelativeLayout nav_relative = (RelativeLayout) findViewById(R.id.relative_nav_home);
        if(sessionManager.getlang().equalsIgnoreCase("ar")){
            nav_scrool.setBackgroundResource(R.drawable.bg_border_left_top_bottom_30dp_white);
            nav_relative.setBackgroundResource(R.drawable.bg_border_left_top_bottom_30dp_white);
            navigationView.setBackgroundResource(R.drawable.bg_border_top_left_30dp_white);
            back.setRotationY(180);
        }
        navigationView.setNavigationItemSelectedListener(this);
        //navigationView.setCheckedItem(R.id.share);
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.moveapp);
        menuItem.setTitle(R.string.doverhome);
        MenuItem menuItem1 = menu.findItem(R.id.cart);
        menuItem1.setVisible(false);
        navheader();

        txt_loc.setOnClickListener(this);
        side_nav_icon.setOnClickListener(this);
        back.setOnClickListener(this);
        home_bottom_icon.setOnClickListener(this);
        notification.setOnClickListener(this);
        facebook.setOnClickListener(this);
        instagram.setOnClickListener(this);
        twitter.setOnClickListener(this);
        version.setOnClickListener(this);
        botm_nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Log.d("xvhjvxv1",""+item.isChecked());
                Log.d("xvhjvxv2",""+item.isCheckable());
                if(StaticInfo.rohawaterfirst == false){
                    switch (item.getItemId()) {
                        case R.id.btm_profile:
                            if(item.isCheckable() == false){
                                navigationchangefragment(new ProfileFragment(),4,0);
                            }
                            break;
                        case R.id.btm_cart:
                            if(item.isCheckable() == false) {
                                navigationchangefragment(new MyOrderFragment(),0,2);
                            }
                            break;
                        case R.id.btm_wallet:
                            if(item.isCheckable() == false) {
                                navigationchangefragment(new WalletFragment(),1,4);
                            }
                            break;
                        case R.id.btm_offers:
                            if(item.isCheckable() == false) {
                                navigationchangefragment(new OffersFragment(),3,3);
                            }
                            break;
                    }
                }
                //relativeLayout.setVisibility(View.GONE);
                return true;
            }
        });

        if(langfrom!=null){
            if(langfrom.equals("profile")){
                profilefragmentload();
            }

        }
        else if(fromnotification==true){
            myorderfragmentload();
        }
        else{
            LocalData.removeAllValues();
            getprofile();
            sessionManager.progressdialogshow();
            commondatarequestJSON();
        }
        try {
            //  version.setText("Version : "+BuildConfig.VERSION_NAME);
            version.setText(""+getString(R.string.version)+" : "+getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void profilefragmentload() {
        navigationchangefragment(new ProfileFragment(),4,0);
    }

    public void myorderfragmentload() {
        navigationchangefragment(new MyOrderFragment(),0,2);
    }

    public void homefragmentload() {
        langfrom = "home";
        for (int i = 0; i < botm_nav.getMenu().size(); i++) {
            botm_nav.getMenu().getItem(i).setCheckable(false);
        }
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setCheckable(false);
        }
        txt_delv_add.setVisibility(View.GONE);
        txt_loc.setVisibility(View.GONE);
        profile_txt.setVisibility(View.VISIBLE);
        profile_txt.setText(""+getString(R.string.ordertype));
        side_nav_icon.setVisibility(View.VISIBLE);
        back.setVisibility(View.GONE);
        rohawaterHomeFragment = new RohawaterHomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, rohawaterHomeFragment,rohawaterHomeFragment.getClass().getSimpleName()).commit();
    }

    public void getprofile() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getUserId());
            profileviewrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void profileviewrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringviewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);

                        sessionManager.setusername(tok.getString("name"));
                        sessionManager.setusermail(tok.getString("email"));
                        sessionManager.setcountrycode(tok.getString("countrycode"));
                        sessionManager.setuserphoneno(tok.getString("phone"));
                        sessionManager.setdob(tok.getString("dob"));
                        sessionManager.setgender(tok.getString("gender"));
                        sessionManager.setaddress(tok.getString("address"));
                        sessionManager.setprofilepic(tok.getString("image"));
                        Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(nav_profile_pic);

                        int id = tok.getInt("user_address");
                        Log.d("hzcvxhc",""+id);
                       /* if (id == 0) {
                            sessionManager.setseletedaddressid(0);
                            sessionManager.setIscurrentlocationselected(true);
                        }
                        else {
                            sessionManager.setseletedaddressid(id);
                            sessionManager.setIscurrentlocationselected(false);
                        }*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void commondatarequestJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.commonvalues, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("commondataresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        sessionManager.progressdialogdismiss();
                        JSONObject obj1 = new JSONObject(obj.getString("data"));
                        StaticInfo.delivery = obj1.getDouble("delivery_charges");
                        StaticInfo.vat = obj1.getDouble("vat");
                        StaticInfo.order_prefix = obj1.getString("order_prefix");
                        StaticInfo.STOREID = obj1.getString("payment_storeid");
                        StaticInfo.KEY = obj1.getString("payment_key");
                        StaticInfo.MODE = obj1.getString("payment_mode");
                        StaticInfo.TAXNO = obj1.getString("tax_no");
                        StaticInfo.Address = obj1.getString("address");
                        StaticInfo.companyname = obj1.getString("company_name");
                        StaticInfo.arcomapny = obj1.getString("ar_company_name");

                        Log.d("wertyuiop",""+StaticInfo.TAXNO);
                        Log.d("wertyuiop",""+StaticInfo.Address);
                        Log.d("wertyuiop",""+StaticInfo.companyname);
                        Log.d("wertyuiop",""+StaticInfo.arcomapny);
                        //StaticInfo.payment_api = obj1.getString("payment_api");
                        //StaticInfo.payment_mail = obj1.getString("payment_mail");
                        //StaticInfo.profileId = obj1.getString("profileid");
                        //StaticInfo.serverKey = obj1.getString("serverkey");
                        //StaticInfo.clientKey = obj1.getString("clientkey");
                        if(obj1.getJSONArray("branches").length()!=0 && obj1.getJSONArray("branches")!=null){
                            sessionManager.setbrancharray(obj1.getJSONArray("branches").toString());
                        }
                        else {
                            sessionManager.setbrancharray("");
                        }
                        Log.d("dscdnvj",""+sessionManager.getbrancharray());
                        homefragmentload();
                    }
                    else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(StaticInfo.rohawaterfirst == false) {
            switch (item.getItemId()) {
                case R.id.profilee:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new ProfileFragment(), 4, 0);
                    }
                    break;
                case R.id.wallet:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new WalletFragment(), 1, 4);
                    }
                    break;
                case R.id.cart:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new CartFragment(), 0, 1);
                    }
                    break;
                case R.id.orders:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new MyOrderFragment(), 0, 2);
                    }
                    break;
                case R.id.offers:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new OffersFragment(), -1, 3);
                    }
                    break;
                case R.id.moveapp:
                    sessionManager.snackbarToast(getString(R.string.clientmsg), relativeLayout);
               /* this.finish();
                Intent intent = new Intent(RohaWaterDashboardActivity.this, RohahomeDashboradActivity.class);
                startActivity(intent);*/
                    break;
                case R.id.credit_card:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new PaymentOptionsFragment(), -1, 5);
                    }
                    break;
                case R.id.invoices:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new InvoiceFragment1(), 3, 7);
                    }
                    break;
                case R.id.help:
                    if (item.isCheckable() == false) {
                        navigationchangefragment(new HelpFragment(), -1, 8);
                    }
                    break;
                case R.id.reset:
                    Intent intent1 = new Intent(RohaWaterDashboardActivity.this, ResetPasswordActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.signoff:
                    FirebaseAuth.getInstance().signOut();
                    sessionManager.settoken("");
                    sessionManager.setusername("");
                    sessionManager.setusermail("");
                    sessionManager.setUserId(0);
                    sessionManager.setguest(0);
                    sessionManager.setuserphoneno("");
                    Intent intent56 = new Intent(RohaWaterDashboardActivity.this, LoginNewActivity.class);
                    intent56.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent56);
                    break;
                case R.id.language:
                    if(sessionManager.getlang()=="ar"){
                        sessionManager.setAppLocale("en");
                    }else{
                        sessionManager.setAppLocale("ar");
                    }
                    startActivity(new Intent(RohaWaterDashboardActivity.this, RohaWaterDashboardActivity.class));

                    break;

            }
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    public void navheader() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerview = navigationView.getHeaderView(0);
        navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);
        nav_close = (ImageView) navheader.findViewById(R.id.nav_close);
        nav_profile_pic = (ImageView) navheader.findViewById(R.id.img_header);
        if(!sessionManager.getprofilepic().equals("")){
            Picasso.get().load(sessionManager.getprofilepic()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(nav_profile_pic);
        }
        txt_edit_profile= (TextView) navheader.findViewById(R.id.edit_profile_txt);
        name = (TextView) navheader.findViewById(R.id.uname);
        name.setText(sessionManager.getusername());
        nav_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        txt_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                navigationchangefragment(new ProfileFragment(),4,0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        StaticInfo.currentactivityname = getClass().getName();
        if (sessionManager.getlang().equals("ar")) {
            sessionManager.setAppLocale("ar");
        } else {
            sessionManager.setAppLocale("en");
        }
    }

    @Override
    public void onClick(View v) {
        if (v == home_bottom_icon) {
            this.finish();
            Intent intent = new Intent(RohaWaterDashboardActivity.this,RohaWaterDashboardActivity.class);
            startActivity(intent);
        }
        else if (v == side_nav_icon) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
        else if(v == back){
            onBackPressed();
        }
        else if(v==notification){
            navigationchangefragment(new NotificationFragment(),-1,-1);
        }
        else if(v == facebook){
            Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
            myWebLink.setData(Uri.parse("https://www.facebook.com/rohaa.r.9"));
            startActivity(myWebLink);
        }
        else if (v == version) {
           // sessionManager.checkVersionAvailable();
        }
        else if(v == twitter){

        }
        else if(v == instagram){

        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            Log.d("scbsdc2",""+getSupportFragmentManager().getBackStackEntryCount());
            Log.d("fvgbnm3",""+exitcount1);
            Log.d("fvgbnm4",""+exitcount2);
            if(getSupportFragmentManager().getBackStackEntryCount() > 0){
                if(langfrom.equalsIgnoreCase("profile")){
                    this.finish();
                    Intent intent = new Intent(this,RohaWaterDashboardActivity.class);
                    startActivity(intent);
                }
                else {
                    if(getSupportFragmentManager().getBackStackEntryCount() == exitcount1){
                        if(isfragmentpresent() == true){
                            int v = (exitcount1-exitcount2);
                            removeallfragments(v);
                            side_nav_icon.setVisibility(View.GONE);
                            back.setVisibility(View.VISIBLE);
                            notification.setVisibility(View.GONE);
                        }
                        else {
                            removeallfragments(exitcount1);
                            side_nav_icon.setVisibility(View.VISIBLE);
                            back.setVisibility(View.GONE);
                            notification.setVisibility(View.VISIBLE);
                        }
                    }
                    else {
                        removesinglefragment();
                    }
                }
            }
            else {
                /*this.finish();
                Intent i = new Intent(this, ChooseAppActivity.class);
                startActivity(i);*/
                if (doubleBackToExitPressedOnce) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    startActivity(intent);
                    return;
                }

                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);
            }
        }
    }

    public void navigationchangefragment(Fragment fragment, int bnavid, int snavid){
        for (int i = 0; i < botm_nav.getMenu().size(); i++) {
            botm_nav.getMenu().getItem(i).setCheckable(false);
        }
        for(int j=0;j < navigationView.getMenu().size();j++){
            navigationView.getMenu().getItem(j).setCheckable(false);
        }

        exitcount1 = getSupportFragmentManager().getBackStackEntryCount()+1;
        getSupportFragmentManager().beginTransaction().add(R.id.container, fragment,fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        LocalData.getInstance().getFragpage().add(profile_txt.getText().toString());
        side_nav_icon.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        notification.setVisibility(View.GONE);
        txt_delv_add.setVisibility(View.GONE);
        txt_loc.setVisibility(View.GONE);
        profile_txt.setVisibility(View.VISIBLE);
        progressBar.setEnabled(false);
        progressBar.setVisibility(View.GONE);
        if(bnavid != -1 && snavid != -1){
            botm_nav.getMenu().getItem(bnavid).setCheckable(true);
            botm_nav.getMenu().getItem(bnavid).setChecked(true);
            navigationView.getMenu().getItem(snavid).setCheckable(true);
            navigationView.setCheckedItem(navigationView.getMenu().getItem(snavid).getItemId());
        }
        else if(snavid != -1){
            navigationView.getMenu().getItem(snavid).setCheckable(true);
            navigationView.setCheckedItem(navigationView.getMenu().getItem(snavid).getItemId());
        }
    }
    public  void subchangefragment(Fragment fragment, Bundle bundle){
        exitcount2++;
        LocalData.getInstance().getFragpage().add(profile_txt.getText().toString());
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.container,fragment,fragment.getClass().getSimpleName()).addToBackStack(fragment.getClass().getSimpleName()).commit();
        txt_delv_add.setVisibility(View.GONE);
        txt_loc.setVisibility(View.GONE);
        profile_txt.setVisibility(View.VISIBLE);
        side_nav_icon.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        notification.setVisibility(View.GONE);

    }
    public Boolean isfragmentpresent(){
        for (int i=0;i<getSupportFragmentManager().getFragments().size();i++){
            if(getSupportFragmentManager().getFragments().get(i).getTag().equals(RohaWaterOrderSummaryFragment.class.getSimpleName())){
                return true;
            }
        }
        return false;
    }
    void removeallfragments(int v){
        for (int i = 0; i < v; i++) {
            getSupportFragmentManager().popBackStack();
            //getSupportFragmentManager().beginTransaction().remove(fragarray.get(i)).commit();
            if (LocalData.getInstance().getFragpage().size() != 0) {
                int f = LocalData.getInstance().getFragpage().size();
                profile_txt.setText("" + LocalData.getInstance().getFragpage().get(f - 1));
                LocalData.getInstance().getFragpage().remove(f - 1);
            }
        }
        exitcount1 = 0;
    }
    void removesinglefragment(){
        if (LocalData.getInstance().getFragpage().size() > 0) {
            getSupportFragmentManager().popBackStack();
            exitcount2--;
            //super.onBackPressed();
            int i = LocalData.getInstance().getFragpage().size();
            profile_txt.setText("" + LocalData.getInstance().getFragpage().get(i - 1));
            LocalData.getInstance().getFragpage().remove(i - 1);
            if(getSupportFragmentManager().getBackStackEntryCount() == 1){
                side_nav_icon.setVisibility(View.VISIBLE);
                back.setVisibility(View.GONE);
                notification.setVisibility(View.VISIBLE);
            }
            else {
                side_nav_icon.setVisibility(View.GONE);
                back.setVisibility(View.VISIBLE);
                notification.setVisibility(View.GONE);
            }
        }
        else {
            this.finish();
            Intent i = new Intent(this, RohaWaterDashboardActivity.class);
            startActivity(i);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        Log.d("xcvxb12",""+requestCode+".."+resultCode+".."+intent);
        if (requestCode == 105 && resultCode == RESULT_OK) {
            if(LocalData.getInstance().getTransactiontype().equalsIgnoreCase("wallet")){
                WalletFragment walletFragment = new WalletFragment();
                walletFragment.onActivityResult(requestCode, resultCode, intent);
            }
            else if(LocalData.getInstance().getTransactiontype().equalsIgnoreCase("order")){
                PaymentFragment paymentFragment = new PaymentFragment();
                paymentFragment.onActivityResult(requestCode, resultCode, intent);
            }
        }
    }


}