package com.lia.yello.roha.model;

public class ListOfferDataModel  {
    String name,promocode,valid_from,valid_to,percentage;
    int id;
    public ListOfferDataModel(int id,String name,String promocode,String valid_from,String valid_to,String percentage){
        this.id=id;
        this.name=name;
        this.promocode=promocode;
        this.valid_from=valid_from;
        this.valid_to=valid_to;
        this.percentage=percentage;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPromocode() {
        return promocode;
    }

    public String getValid_from() {
        return valid_from;
    }

    public String getValid_to() {
        return valid_to;
    }

    public String getPercentage() {
        return percentage;
    }
}

