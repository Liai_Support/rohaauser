package com.lia.yello.roha.Fragment;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AddAddressFragment extends Fragment implements View.OnClickListener {
    GoogleMap gmap;
    SupportMapFragment mapFragment;
    private FusedLocationProviderClient client;
    View view;
    String address,fcity;
    double lat,lng;
    int faid,fbranchid;
    TextView addr,addr1,txt_add_address;
    EditText building_name,door_no;
    TextView spinner_edit_txt,txt_home,txt_office,txt_others,backmap;
    RelativeLayout dropDown;
    Button confirm_btn;
    SessionManager sessionManager;
    String add_type;
    EditText edt_others;
    TextInputLayout lay_others;
    boolean update,home,office;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_address, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        ((RohahomeDashboradActivity) getActivity()).profile_txt.setText(""+getString(R.string.address));
        client = LocationServices.getFusedLocationProviderClient(getContext());
        addr = (TextView) view.findViewById(R.id.txt_save_loc);
        addr1 = (TextView) view.findViewById(R.id.addr1);
        building_name = (EditText) view.findViewById(R.id.building_name);
        door_no = (EditText) view.findViewById(R.id.door_no);


        // for add address drop down id
        backmap = (TextView)view.findViewById(R.id.gobackmap);
        spinner_edit_txt = (TextView) view.findViewById(R.id.dropdown_edit_txt);
        txt_home = (TextView) view.findViewById(R.id.txt_home);
        txt_office = (TextView) view.findViewById(R.id.txt_office);
        txt_others = (TextView) view.findViewById(R.id.txt_others);
        txt_add_address = (TextView) view.findViewById(R.id.txt_add_address);
        edt_others = (EditText) view.findViewById(R.id.othertag) ;
        lay_others = (TextInputLayout) view.findViewById(R.id.othertag_lay);
        dropDown=(RelativeLayout)view.findViewById(R.id.dropdown);
        confirm_btn=(Button)view.findViewById(R.id.btn_confirm);
        confirm_btn.setOnClickListener(this);

        // click listeners
        txt_add_address.setOnClickListener(this);
        spinner_edit_txt.setOnClickListener(this);
        txt_home.setOnClickListener(this);
        txt_office.setOnClickListener(this);
        txt_others.setOnClickListener(this);
        backmap.setOnClickListener(this);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);
        Dexter.withContext(getApplicationContext())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        getmylocation();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();

        Bundle args = getArguments();
        faid = args.getInt("faid");
        address = args.get("faddr").toString();
        lat = args.getDouble("flat");
        lng = args.getDouble("flng");
        fcity = args.getString("fcity");
        fbranchid = args.getInt("fbranchid");
        addr.setText(""+address);
        addr1.setText(""+address);
        addr1.setHint(""+address);
        update = args.getBoolean("update");
        add_type = args.getString("fatype");
        if(add_type != null){
            spinner_edit_txt.setText(""+add_type);
            edt_others.setText(""+add_type);
        }

        if(update == true){
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
        }
        listaddress();
        return view;
    }

    public void listaddress(){
        sessionManager.progressdialogshow();
        JSONObject requestbody1 = new JSONObject();
        try {
            requestbody1.put("userid", sessionManager.getUserId());
            listaddressrequestJSON(requestbody1);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void listaddressrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.listaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("listaddressdataresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("dvdxv",""+obj.getJSONArray("data"));
                        JSONArray data1 = obj.getJSONArray("data");
                        Log.d("jaafzyauj",""+data1.length());
                        if (data1.length()==0){

                        }
                        else{
                            for (int i = 0; i < data1.length(); i++) {
                                JSONObject jsonObject = data1.getJSONObject(i);
                                jsonObject.getString("addresstype");
                                if(jsonObject.getString("addresstype").equalsIgnoreCase(getString(R.string.home))){
                                    home = true;
                                }
                                else if(jsonObject.getString("addresstype").equalsIgnoreCase(getString(R.string.office))){
                                    office = true;
                                }
                            }
                        }



                    } else {
                        sessionManager.snackbarToast(obj.getString("message"),view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void getmylocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        LatLng latLng=new LatLng(lat,lng);
                        MarkerOptions markerOptions=new MarkerOptions().position(latLng).title(""+address);
                        googleMap.addMarker(markerOptions);
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
                        gmap = googleMap;
                       /* gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                // creating marker
                                MarkerOptions markerOptions = new MarkerOptions();

                                // set marker position
                                markerOptions.position(latLng);

                                // set Lattitude & longitude on marker

                                markerOptions.title(latLng.latitude + ":" + latLng.longitude);
                                Geocoder geocoder;
                                List<Address> addresses;
                                geocoder = new Geocoder(getContext(), Locale.getDefault());

                                try {
                                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                    String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String country = addresses.get(0).getCountryName();
                                    String postalCode = addresses.get(0).getPostalCode();
                                    String knownName = addresses.get(0).getFeatureName();
                                    Log.d("kkkkkkk",""+address);
                                    addr.setText(""+address);
                                    faddr = address;
                                    flat = latLng.latitude;
                                    flng = latLng.longitude;
                                    // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                gmap.clear();
                                gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                                gmap.addMarker(markerOptions);
                            }
                        });*/
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
         if (view == spinner_edit_txt){
             if(update == true){
                 if(home==true || office== true){
                     if(home == true && office == true){
                         txt_home.setVisibility(View.GONE);
                         txt_office.setVisibility(View.GONE);
                         dropDown.setVisibility(view.VISIBLE);
                         spinner_edit_txt.setVisibility(view.GONE);
                     }
                     else if(home == true){
                         txt_home.setVisibility(View.GONE);
                         dropDown.setVisibility(view.VISIBLE);
                         spinner_edit_txt.setVisibility(view.GONE);
                     }
                     else  if(office == true){
                         txt_office.setVisibility(View.GONE);
                         dropDown.setVisibility(view.VISIBLE);
                         spinner_edit_txt.setVisibility(view.GONE);
                     }
                 }
                 else {
                     dropDown.setVisibility(view.VISIBLE);
                     spinner_edit_txt.setVisibility(view.GONE);
                 }
             }
             else {
                 if(home==true || office== true){
                     if(home == true && office == true){
                         txt_home.setVisibility(View.GONE);
                         txt_office.setVisibility(View.GONE);
                         dropDown.setVisibility(view.VISIBLE);
                         spinner_edit_txt.setVisibility(view.GONE);
                     }
                     else if(home == true){
                         txt_home.setVisibility(View.GONE);
                         dropDown.setVisibility(view.VISIBLE);
                         spinner_edit_txt.setVisibility(view.GONE);
                     }
                     else  if(office == true){
                         txt_office.setVisibility(View.GONE);
                         dropDown.setVisibility(view.VISIBLE);
                         spinner_edit_txt.setVisibility(view.GONE);
                     }
                 }
                 else {
                     dropDown.setVisibility(view.VISIBLE);
                     spinner_edit_txt.setVisibility(view.GONE);
                 }
             }

         }
         else if(view==txt_add_address){
             dropDown.setVisibility(view.GONE);
             spinner_edit_txt.setVisibility(view.VISIBLE);
         }
         else if(view==txt_home){
                   txt_add_address.setText(R.string.home);
                   spinner_edit_txt.setText(R.string.home);
                   dropDown.setVisibility(View.GONE);
                   spinner_edit_txt.setVisibility(view.VISIBLE);
             lay_others.setVisibility(View.GONE);
             edt_others.setText(R.string.home);
               }
         else if(view==txt_office){
             txt_add_address.setText(R.string.office);
             spinner_edit_txt.setText(R.string.office);
             dropDown.setVisibility(View.GONE);
             spinner_edit_txt.setVisibility(view.VISIBLE);
             edt_others.setText(R.string.office);
             lay_others.setVisibility(View.GONE);
         }
         else if(view==txt_others){
             txt_add_address.setText(R.string.others);
             spinner_edit_txt.setText(R.string.others);
             edt_others.setText("");
             dropDown.setVisibility(View.GONE);
             spinner_edit_txt.setVisibility(view.VISIBLE);
             lay_others.setVisibility(View.VISIBLE);
         }
         else if(view == backmap){
             if(update == true){
                 Bundle args = new Bundle();
                 args.putInt("faid",faid);
                 args.putString("faddr",address);
                 args.putDouble("flat",lat);
                 args.putDouble("flng", lng);
                 args.putInt("fbranchid",fbranchid);
                 args.putString("fcity", fcity);
                 args.putString("fatype",add_type);
                 args.putBoolean("update",true);
                 ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddaddressMapFragment(),args);
             }
             else {
                 Bundle args = new Bundle();
                 args.putBoolean("update",false);
                 ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddaddressMapFragment(),args);
             }
         }
         else if(view==confirm_btn){
             add_type = edt_others.getText().toString();
             if(add_type.equals("")){
                 sessionManager.snackbarToast(getString(R.string.enterallfields),view);
             }
             else {
                 if(update == true){
                     add_type = edt_others.getText().toString();
                     JSONObject jsonObject = new JSONObject();
                     try {
                         jsonObject.put("userid", sessionManager.getUserId());
                         jsonObject.put("address_id",faid);
                         jsonObject.put("lat",lat);
                         jsonObject.put("lng",lng);
                         jsonObject.put("addresstype",add_type);
                         jsonObject.put("address",address);
                         jsonObject.put("city",fcity);
                         jsonObject.put("branchid",fbranchid);
                         updateaddressrequestjson(jsonObject);
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 }
                 else {
                     add_type = edt_others.getText().toString();
                     JSONObject requestbody = new JSONObject();
                     try {
                         requestbody.put("userid", sessionManager.getUserId());
                         requestbody.put("address", address);
                         requestbody.put("city", fcity);
                         requestbody.put("branchid", fbranchid);
                         requestbody.put("lat", lat);
                         requestbody.put("lng", lng);
                         requestbody.put("addresstype", add_type);
                         addaddresstrequestJSON(requestbody);
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
             }
         }
    }

    private void updateaddressrequestjson(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updateaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("adressresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        sessionManager.snackbarToast(obj.getString("message"),view);

                    } else {
                        sessionManager.snackbarToast(obj.getString("message"),view);
                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View promptView = layoutInflater.inflate(R.layout.popup_noted, null);
                        final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();
                        alertD.setView(promptView);
                        alertD.setCancelable(true);
                        TextView txt_noted = (TextView) promptView.findViewById(R.id.txt_noted);
                        txt_noted.setText(""+getString(R.string.addressupdated));
                        CardView cardView = (CardView) promptView.findViewById(R.id.main);
                        cardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertD.dismiss();
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(),RohahomeDashboradActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertD.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                alertD.dismiss();
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(),RohahomeDashboradActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertD.show();
                        alertD.getWindow().setGravity(Gravity.CENTER);
                        alertD.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                        layoutParams.copyFrom(alertD.getWindow().getAttributes());
                        layoutParams.width=500;
                        layoutParams.height=500;
                       // layoutParams.horizontalMargin=20;
                        alertD.getWindow().setAttributes(layoutParams);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void addaddresstrequestJSON(JSONObject requestbody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = requestbody.toString();
        Log.d("reques34", String.valueOf(requestbody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.addaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("adressresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        sessionManager.snackbarToast(obj.getString("message"),view);
                    } else {
                        /*String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        tok.getString("userid");
                        tok.getString("address");
                        tok.getString("lat");
                        tok.getString("lng");*/
                       // snackbarToast(obj.getString("message"));

                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View promptView = layoutInflater.inflate(R.layout.popup_noted, null);
                        final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();
                        alertD.setView(promptView);
                        alertD.setCancelable(true);
                        CardView cardView = (CardView) promptView.findViewById(R.id.main);
                        cardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertD.dismiss();
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(),RohahomeDashboradActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertD.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                alertD.dismiss();
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(),RohahomeDashboradActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertD.show();
                        alertD.getWindow().setGravity(Gravity.CENTER);
                        alertD.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                        layoutParams.copyFrom(alertD.getWindow().getAttributes());
                        layoutParams.width=500;
                        layoutParams.height=500;
                        // layoutParams.horizontalMargin=20;
                        alertD.getWindow().setAttributes(layoutParams);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

}

