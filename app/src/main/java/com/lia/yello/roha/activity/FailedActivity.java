package com.lia.yello.roha.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.lia.yello.roha.R;

public class FailedActivity extends AppCompatActivity implements View.OnClickListener {
    MaterialButton tryagain;
    MaterialTextView content;
    String from="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failed);
        tryagain = (MaterialButton)findViewById(R.id.tryagain);
        content = (MaterialTextView) findViewById(R.id.materialTextView);
        tryagain.setOnClickListener(this);
//        Intent bundle = new Intent();
//        from = bundle.getBundleExtra("from").toString();
//        if (from.equals("order")||from == "order"){
//            cont
//        }

    }
    @Override
    public void onBackPressed() {

        Intent mainIntent = new Intent(this, RohaWaterDashboardActivity.class);

        startActivity(mainIntent);
    }

    @Override
    public void onClick(View view) {
        if(view == tryagain){
            Intent intent = new Intent(this,RohaWaterDashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }




}