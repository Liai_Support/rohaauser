package com.lia.yello.roha.model;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.lia.yello.roha.R;

public class ListorderDatamodel2 {
    LatLng latlng;
    int cartoncount;
    Context context;
    String mosquename,ismecca,units,status,delivered_date,place,delivered_time,discountvalue,discounttype,unitprice;
    String imgpath1,imgpath2,imgpath3,imgpath4;


    public ListorderDatamodel2(Context context,
                               String mosquename,
                               String delivered_time,
                               int cartoncount,
                               String status,
                               String delivered_date,
                               String units,
                               String ismecca,
                               String place,
                               String imgpath1,
                               String imgpath2,
                               String imgpath3,
                               String imgpath4,
                               String discountvalue,
                               String discounttype,String unitprice) {
        this.context = context;
        this.mosquename = mosquename;
        this.delivered_time = delivered_time;
        this.cartoncount = cartoncount;
        this.units = units;
        this.status = status;
        this.delivered_date = delivered_date;
        this.ismecca = ismecca;
        this.place = place;
        this.imgpath1 = imgpath1;
        this.imgpath2 = imgpath2;
        this.imgpath3 = imgpath3;
        this.imgpath4 = imgpath4;
        this.discounttype = discounttype;
        this.discountvalue = discountvalue;
        this.unitprice = unitprice  ;
    }

    public String getUnitprice() {
        return unitprice;
    }

    public String getDiscounttype(){
        return discounttype;
    }

    public String getDiscountvalue(){
        return discountvalue;
    }
    public String getDelivered_time() {
        return delivered_time;
    }

    public String getIsmecca(){
        return ismecca;
    }
    public String getImgpath1() {
        return imgpath1;
    }

    public String getImgpath2() {
        return imgpath2;
    }
    public String getImgpath3() {
        return imgpath3;
    }
    public String getImgpath4() {
        return imgpath4;
    }
    public int getCartoncount() {
        return cartoncount;
    }

    public String getUnits() {
        return units;
    }

    public String getMosquename() {
        return mosquename;
    }

    public String getStatus() {
        return status;
    }

    public String getDelivered_date() {
        return delivered_date;
    }

    public String getPlace(){
        return place;
    }

}
