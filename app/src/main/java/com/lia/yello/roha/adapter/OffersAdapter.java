package com.lia.yello.roha.adapter;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.OffersFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.OfferDataModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyViewHolder> {
    private Context mcontext;
    private List<OfferDataModel> dataSet;
    private OffersFragment offersFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.img_offer);
        }
    }

    public OffersAdapter(Context context,List<OfferDataModel>  data, OffersFragment offersFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.offersFragment = offersFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_offers, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        Picasso.get().load(dataSet.get(listPosition).getOimage()).placeholder(R.drawable.defaultimg).error(R.drawable.defaultimg).into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dataSet.get(listPosition).getType().equalsIgnoreCase("SAR")){
                    final Dialog dialog = new Dialog(offersFragment.getActivity());
                    dialog.setContentView(R.layout.popup_offers_promocode);
                    dialog.setTitle(mcontext.getString(R.string.offerdetails));
                    dialog.setCancelable(true);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.gravity = Gravity.CENTER;

                    dialog.getWindow().setAttributes(lp);

                    // set the custom dialog components - text, image and button
                    TextView promocode = (TextView) dialog.findViewById(R.id.promocode);
                    ImageView cpy = (ImageView) dialog.findViewById(R.id.promo_copy);
                    TextView from = (TextView) dialog.findViewById(R.id.validfrom);
                    TextView to = (TextView) dialog.findViewById(R.id.validto);
                    TextView value = (TextView) dialog.findViewById(R.id.value);
                    TextView moa = (TextView) dialog.findViewById(R.id.moa);

                    promocode.setText(""+dataSet.get(listPosition).getPromocode());
                    from.setText(""+dataSet.get(listPosition).getFrom());
                    to.setText(""+dataSet.get(listPosition).getTo());
                    value.setText(""+dataSet.get(listPosition).getValue()+" SAR");
                    moa.setText(""+dataSet.get(listPosition).getMinimumorderamount()+" SAR");
                    cpy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ClipboardManager clipboard = (ClipboardManager) mcontext.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("label", dataSet.get(listPosition).getPromocode());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(mcontext,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
                        }
                    });
               /* Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/
                    dialog.show();
                }
                else if(dataSet.get(listPosition).getType().equalsIgnoreCase("Carton")){
                    final Dialog dialog = new Dialog(offersFragment.getActivity());
                    dialog.setContentView(R.layout.popup_offers_promocode);
                    dialog.setTitle(mcontext.getString(R.string.offerdetails));
                    dialog.setCancelable(true);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.gravity = Gravity.CENTER;

                    dialog.getWindow().setAttributes(lp);

                    // set the custom dialog components - text, image and button
                    TextView promocode = (TextView) dialog.findViewById(R.id.promocode);
                    ImageView cpy = (ImageView) dialog.findViewById(R.id.promo_copy);
                    TextView from = (TextView) dialog.findViewById(R.id.validfrom);
                    TextView to = (TextView) dialog.findViewById(R.id.validto);
                    TextView value = (TextView) dialog.findViewById(R.id.value);
                    TextView moa = (TextView) dialog.findViewById(R.id.moa);
                    TextView disc = (TextView) dialog.findViewById(R.id.txt_disc);

                    disc.setText(""+mcontext.getString(R.string.offer));
                    promocode.setText(""+dataSet.get(listPosition).getPromocode());
                    from.setText(""+dataSet.get(listPosition).getFrom());
                    to.setText(""+dataSet.get(listPosition).getTo());

                    value.setText(""+(int) dataSet.get(listPosition).getValue()+" - "+dataSet.get(listPosition).getCartonname());
                    moa.setText(""+dataSet.get(listPosition).getMinimumorderamount()+" SAR");
                    cpy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ClipboardManager clipboard = (ClipboardManager) mcontext.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("label", dataSet.get(listPosition).getPromocode());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(mcontext,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
                        }
                    });
               /* Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/
                    dialog.show();
                }
                else {
                    final Dialog dialog = new Dialog(offersFragment.getActivity());
                    dialog.setContentView(R.layout.popup_offers_promocode);
                    dialog.setTitle(mcontext.getString(R.string.offerdetails));
                    dialog.setCancelable(true);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.gravity = Gravity.CENTER;

                    dialog.getWindow().setAttributes(lp);

                    // set the custom dialog components - text, image and button
                    TextView promocode = (TextView) dialog.findViewById(R.id.promocode);
                    ImageView cpy = (ImageView) dialog.findViewById(R.id.promo_copy);
                    TextView from = (TextView) dialog.findViewById(R.id.validfrom);
                    TextView to = (TextView) dialog.findViewById(R.id.validto);
                    TextView value = (TextView) dialog.findViewById(R.id.value);
                    TextView moa = (TextView) dialog.findViewById(R.id.moa);

                    promocode.setText(""+dataSet.get(listPosition).getPromocode());
                    from.setText(""+dataSet.get(listPosition).getFrom());
                    to.setText(""+dataSet.get(listPosition).getTo());
                    value.setText(""+dataSet.get(listPosition).getValue()+" %");
                    moa.setText(""+dataSet.get(listPosition).getMinimumorderamount()+" SAR");

                    cpy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ClipboardManager clipboard = (ClipboardManager) mcontext.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("label", dataSet.get(listPosition).getPromocode());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(mcontext,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();
                        }
                    });
                    dialog.show();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}