package com.lia.yello.roha.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.ReceiverDetailsFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.ReceiverDataModel;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ReceiverAdapter extends RecyclerView.Adapter<ReceiverAdapter.MyViewHolder> {
    private Context mcontext;
    private ReceiverDetailsFragment receiverDetailsActivity;
    private List<ReceiverDataModel> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        ImageView receivericon;
        EditText edit_recievername,edit_receivermno,edit_receiverdesc;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.edit_recievername = (EditText) itemView.findViewById(R.id.edit_receivername);
            this.edit_receivermno = (EditText) itemView.findViewById(R.id.edit_receivermno);
            this.edit_receiverdesc = (EditText) itemView.findViewById(R.id.edit_receiverdesc);
            this.receivericon = (ImageView) itemView.findViewById(R.id.receiveicon);
        }
    }

    public ReceiverAdapter(Context context,List<ReceiverDataModel> data,ReceiverDetailsFragment receiverDetailsActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.receiverDetailsActivity = receiverDetailsActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_receiver_details, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        TextView textViewName = holder.textViewName;
        ImageView receivericon = holder.receivericon;
        textViewName.setText(dataSet.get(listPosition).getName());
        holder.edit_receivermno.setText(dataSet.get(listPosition).getRmno());
        holder.edit_receiverdesc.setText(dataSet.get(listPosition).getRdesc());
        holder.edit_recievername.setText(dataSet.get(listPosition).getRname());

        if (dataSet.get(listPosition).getPlace().equalsIgnoreCase(StaticInfo.MECCA)){
            receivericon.setImageResource(R.drawable.mecca_new_icon);
        }
        else if(dataSet.get(listPosition).getPlace().equalsIgnoreCase(StaticInfo.MEDINA)){
            receivericon.setImageResource(R.drawable.medina_new_icon);
        }
        else {
            receivericon.setImageResource(R.drawable.mosque_icon1);
        }


        holder.edit_recievername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                dataSet.get(listPosition).setRname(holder.edit_recievername.getText().toString());
                receiverDetailsActivity.load(listPosition);
            }
        });
        holder.edit_receivermno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                dataSet.get(listPosition).setRmno(holder.edit_receivermno.getText().toString());
                receiverDetailsActivity.load(listPosition);
            }
        });
        holder.edit_receiverdesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                dataSet.get(listPosition).setRdesc(holder.edit_receiverdesc.getText().toString());
                receiverDetailsActivity.load(listPosition);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}