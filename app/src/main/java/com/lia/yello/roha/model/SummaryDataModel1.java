package com.lia.yello.roha.model;

import com.google.android.gms.maps.model.LatLng;

public class SummaryDataModel1 {

    String title,bname;
    LatLng latlng;
    int bid,bstock,qty;

    public SummaryDataModel1(String title, LatLng latlng, int bid,String bname,int bstock,int qty) {
        this.title = title;
        this.latlng = latlng;
        this.bid = bid;
        this.bname = bname;
        this.bstock = bstock;
        this.qty = qty;
    }

    public  int getBid(){
        return bid;
    }
    public String getTitle() {
        return title;
    }

    public  int getQty(){
        return qty;
    }

    public void setQty(int qty){
        this.qty = qty;
    }

    public LatLng getLatlng() {
        return latlng;
    }

    public int getBstock(){
        return bstock;
    }

    public String getBname(){
        return bname;
    }

}
