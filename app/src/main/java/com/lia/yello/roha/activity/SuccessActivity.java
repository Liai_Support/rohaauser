package com.lia.yello.roha.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textview.MaterialTextView;
import com.lia.yello.roha.Fragment.NewInvoice;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.lia.yello.roha.utility.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    View view;
    private SessionManager sessionManager;
    public JSONArray newarray = new JSONArray() ;
    ImageView back;
    Button viewinvoice;
    boolean iban;
    TextView rateus,referafriend,success_order_id;
    public String paymenttype;
    RelativeLayout fullsucesslin;
    String orderid;
    MaterialTextView deliverdaysText;
    String totalamt,deliveryfees, subtotal,vat,vatcollected,date,qrcode,orderidd,itemname, quantity,cost;
    Boolean iswishlist =true;
    String from = "";


    public ArrayList<String> overallmarkersbranchname2= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_success_new);
        view = (RelativeLayout) findViewById(R.id.fullsucess);

        sessionManager = new SessionManager(this,this,view);
        deliverdaysText = (MaterialTextView)findViewById(R.id.delday);
        success_order_id = (TextView)findViewById(R.id.success_order_id);
        fullsucesslin = (RelativeLayout)findViewById(R.id.fullsucess);
        viewinvoice = (Button)findViewById(R.id.viewinvoice);
        viewinvoice.setOnClickListener(this);
        fullsucesslin.setVisibility(View.GONE);
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(this);
        }
        Bundle args = getIntent().getExtras();
        if(args != null){
            iban = args.getBoolean("ibandata",false);
            if(iban == true){
                orderid = args.getString("orderid");
                fullsucesslin.setVisibility(View.VISIBLE);
                success_order_id.setText(""+orderid);
                LocalData.removeAllValues();
/*
                if(sessionManager.getchoose().equalsIgnoreCase("rohahome")){
                    ((RohahomeDashboradActivity)this).listcart();
                    ((RohahomeDashboradActivity) getActivity()).navigationselectclear();
                }
*/
            }
            else if(iban == false){
                placeorder();
            }
        }



    }

    private void placeorder(){
        sessionManager.progressdialogshow();
        if(sessionManager.getchoose().equals("rohahome")){
            placeorderhomeJSON();
            deliverdaysText.setVisibility(View.GONE);
        }
        else {
            if(sessionManager.getIsWish() == 1){
                deliverdaysText.setVisibility(View.VISIBLE);
                try {
                    JSONArray conv=LocalData.getInstance().getBodyarray();
                    Log.d("hgyfjnbj",""+conv);
                    for(int i=0;i<conv.length();i++){
                        JSONObject js =  conv.getJSONObject(i);
                        String mosquename = js.getString("mosquename");
                        byte[] data1 = mosquename.getBytes("UTF-8");
                        String bmosquename = Base64.encodeToString(data1, Base64.NO_WRAP);
                        String placename = js.getString("place");
                        byte[] data11 = placename.getBytes("UTF-8");
                        String bplace = Base64.encodeToString(data11, Base64.NO_WRAP);
                        double lat = js.getDouble("lat");
                        double lng = js.getDouble("lng");
                        int ismeccca = js.getInt("ismecca");
                        int cartcount = js.getInt("cartcount");
                        double productprice = js.getDouble("productprice");
                        String rname = js.getString("receivername");
                        byte[] data2 = rname.getBytes("UTF-8");
                        String brname = Base64.encodeToString(data2, Base64.DEFAULT);
                        String rmno = js.getString("receivermno");
                        byte[] data4 = rmno.getBytes("UTF-8");
                        String brmno = Base64.encodeToString(data4, Base64.DEFAULT);
                        String rdesc = js.getString("receiverdesc");
                        byte[] data3 = rdesc.getBytes("UTF-8");
                        String brdesc = Base64.encodeToString(data3, Base64.DEFAULT);
                        String unit = js.getString("units_qty");
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("lat",lat);
                        jsonObject.put("lng",lng);
                        jsonObject.put("ismecca",ismeccca);
                        jsonObject.put("place",bplace);
                        jsonObject.put("mosquename",bmosquename);
                        jsonObject.put("cartcount",cartcount);
                        jsonObject.put("productprice",productprice);
                        jsonObject.put("receivername",brname);
                        jsonObject.put("receivermno",brmno);
                        jsonObject.put("receiverdesc",brdesc);
                        jsonObject.put("units_qty",unit);
                        newarray.put(jsonObject);
                    }
                    Log.d("bchzxbc",""+newarray);
                } catch (JSONException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.d("bchzxbc",""+newarray);
                placeordermeccamedinarequestJSON(newarray);
            }
            else if(sessionManager.getIsWish() == 0){
                deliverdaysText.setVisibility(View.GONE);

                placeorderiswishrequestJSON(LocalData.getInstance().getWishlistArray());
            }
        }
    }

    private void placeorderhomeJSON() {
      /*  final String requestBody = response.toString();
        Log.d("reques34",String.valueOf(response));*/
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringplaceorderhome,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("dsvddscv",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                fullsucesslin.setVisibility(View.VISIBLE);
                                success_order_id.setText(""+StaticInfo.order_prefix+"H"+jsonObject.getString("orderId"));
                                sessionManager.snackbarToast(obj.getString("message"),view);
//                                RohahomeDashboradActivity.listcart();
//                                ((RohahomeDashboradActivity) getActivity()).navigationselectclear();

                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getDeliverycharge());
                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getSubtotal());
                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getOverallMarkersbranchname());
                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getVat());
                                LocalData.removeAllValues();
                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue",view);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        sessionManager.snackbarToast(getString(R.string.orderfailed),view);

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ContentType","multipart/form-data; boundary=tYhL_Uv6shSE" );
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("qty",""+LocalData.getInstance().getTotalqty());
                params.put("price",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("branch",""+LocalData.getInstance().getBranchid());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("orderaddress",""+sessionManager.base64convert(LocalData.getInstance().getOrderaddress()));
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("orderlat",""+LocalData.getInstance().getOrderlat());
                params.put("orderlng",""+LocalData.getInstance().getOrderlng());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                //params.put("discountamount",""+LocalData.getInstance().getDiscountvalue());
                params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());

                Log.d("param",""+params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
               /* params.put("data", ""+response);
                Log.d("param order",""+params);*/
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    private void placeordermeccamedinarequestJSON(JSONArray response) {
        final String requestBody = response.toString();
        Log.d("reques34",requestBody);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringplaceorderwater,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("Response1",""+response.toString());
                        Log.d("Response7",""+new String(String.valueOf(response)));
                        Log.d("Response2",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                fullsucesslin.setVisibility(View.VISIBLE);
                                success_order_id.setText(""+StaticInfo.order_prefix+"M"+jsonObject.getString("orderId"));
                                sessionManager.snackbarToast(obj.getString("message"),view);
                                subtotal=LocalData.getInstance().getSubtotal();
                                vat=LocalData.getInstance().getVat();
                                deliveryfees=LocalData.getInstance().getDeliverycharge();
                                itemname=LocalData.getInstance().getitemname();
                                cost=LocalData.getInstance().getCost();
                                quantity=LocalData.getInstance().getQuantity();
                                orderidd=StaticInfo.order_prefix+"M"+jsonObject.getString("orderId");
                                iswishlist=false;

                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getDeliverycharge());
                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getSubtotal());
                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getOverallMarkersTitle());
                                Log.d("e34r5t6y7u8i9o0",""+LocalData.getInstance().getVat());
                                //  LocalData.removeAllValues();
                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue",view);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        sessionManager.snackbarToast(getString(R.string.orderfailed),view);

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ContentType","multipart/form-data; boundary=tYhL_Uv6shSE" );
//                params.put("Accept", "charset=utf-8");
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("totalcarton",""+LocalData.getInstance().getTotalqty());
                params.put("totalprice",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("iswish",""+ sessionManager.getIsWish());
                params.put("unitsqty",""+LocalData.getInstance().getUnitqty());
                params.put("ismecca",""+ LocalData.getInstance().getIsmeccaIntHeaderArray());
                params.put("place",""+ LocalData.getInstance().getIsmeccaStrHeaderArray());
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());
                params.put("appversion","2.7");
                Log.d("param",""+params);
                return params;
            }



            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("data", ""+requestBody);
                Log.d("param order",""+params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    private void placeorderiswishrequestJSON(ArrayList<JSONObject> response) {
        final String requestBody = response.toString();
        Log.d("reques34",requestBody);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.URLstringplaceorderwater,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("dsvddscv",""+new String(response.data));
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            if(obj.getBoolean("error") == false){
                                JSONObject jsonObject = new JSONObject(obj.getString("data"));
                                fullsucesslin.setVisibility(View.VISIBLE);
                                success_order_id.setText(""+StaticInfo.order_prefix+""+jsonObject.getString("orderId"));
                                sessionManager.snackbarToast(obj.getString("message"),view);
                                subtotal=LocalData.getInstance().getSubtotal();
                                vat=LocalData.getInstance().getVat();
                                deliveryfees=LocalData.getInstance().getDeliverycharge();
                                itemname=LocalData.getInstance().getitemname();
                                cost=LocalData.getInstance().getCost();
                                quantity=LocalData.getInstance().getQuantity();
                                orderidd=StaticInfo.order_prefix+"H"+jsonObject.getString("orderId");

                                Log.d("rtrt",""+subtotal);
                                Log.d("qeq",""+vat);
                                Log.d("wew",""+qrcode);
                                Log.d("sdf",""+deliveryfees);
                                LocalData.removeAllValues();

                                Log.d("wer","woiejrhd");
                            }
                            else {
                                sessionManager.snackbarToast(obj.getString("message"),view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            sessionManager.snackbarToast("Backend Side Issue",view);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        sessionManager.snackbarToast(getString(R.string.orderfailed),view);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ContentType","multipart/form-data; boundary=tYhL_Uv6shSE" );
                params.put("authorization", "Bearer "+ sessionManager.gettoken());
                params.put("userid",""+ sessionManager.getUserId());
                params.put("totalcarton",""+LocalData.getInstance().getTotalqty());
                params.put("totalprice",""+LocalData.getInstance().getTotalprice());
                params.put("vat",""+LocalData.getInstance().getVat());
                params.put("deliverycharge",""+LocalData.getInstance().getDeliverycharge());
                params.put("subtotal",""+LocalData.getInstance().getSubtotal());
                params.put("iswish",""+ sessionManager.getIsWish());
                params.put("unitsqty",""+LocalData.getInstance().getUnitqty());
                params.put("place",""+LocalData.getInstance().getWishlistId());
                params.put("reftoken",""+LocalData.getInstance().getTransactionid());
                params.put("paymenttype",""+LocalData.getInstance().getPaymenttype());
                params.put("productid",""+LocalData.getInstance().getProductid());
                params.put("ibanno",""+LocalData.getInstance().getIbanno());
                params.put("dayid",""+LocalData.getInstance().getDayid());
                params.put("timeid",""+LocalData.getInstance().getTimeid());
                params.put("discountid",""+LocalData.getInstance().getDiscountid());
                params.put("discountvalue",""+LocalData.getInstance().getDiscountvalue());
                params.put("discounttype",""+LocalData.getInstance().getDiscounttype());
                params.put("notes",""+LocalData.getInstance().getNotes());
                params.put("appversion","2.7");

                Log.d("param",""+params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("data", ""+requestBody);
                Log.d("param order",""+params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    @Override
    public void onClick(View view) {
        if(view == back){
            finish();
            Intent intent = new Intent(this,RohahomeDashboradActivity.class);
            startActivity(intent);
        }
        else if(view == referafriend){
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String body = getString(R.string.checkoutthisapp) + " : " + "https://play.google.com/store/apps/details?id=com.lia.yello.roha";
                    /*String sub = "Your Subject";
                    myIntent.putExtra(Intent.EXTRA_SUBJECT,sub);*/
            myIntent.putExtra(Intent.EXTRA_TEXT, body);
            startActivity(Intent.createChooser(myIntent, "Share Using"));
        }
        else if(view == rateus){
            Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
            myWebLink.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.lia.yello.roha"));
            startActivity(myWebLink);
        }
        else if(view == viewinvoice){
            if (iswishlist==true){
                Log.d("cgcgcg",""+LocalData.getInstance().getSubtotal()+"knknkn");
                Log.d("cgcgcg",""+LocalData.getInstance().getDeliverycharge()+"nmnbvc");
                Log.d("cgcgcg",""+LocalData.getInstance().getDiscountid()+"bhdff");
                Log.d("cgcgcg",""+LocalData.getInstance().getPaymentamount()+"asertytrdes");
                Bundle args = new Bundle();
                args.putString("orderid",orderidd);
                args.putString("subtotal",subtotal);
                args.putString("vat",vat);
                args.putString("deliveryfees",deliveryfees);
                args.putString("qrcode",qrcode);
                args.putString("cost",cost);
                args.putString("quantity",quantity);
                args.putString("itemname",itemname);

                args.putString("iswish","true");
                subchangefragment(new NewInvoice(),args);
            }
            else{
                Log.d("cgcgcg",""+LocalData.getInstance().getSubtotal()+"knknkn");
                Log.d("cgcgcg",""+LocalData.getInstance().getDeliverycharge()+"nmnbvc");
                Log.d("cgcgcg",""+LocalData.getInstance().getDiscountid()+"bhdff");
                Log.d("cgcgcg",""+LocalData.getInstance().getPaymentamount()+"asertytrdes");
                Bundle args = new Bundle();
                args.putString("orderid",orderidd);
                args.putString("subtotal",subtotal);
                args.putString("vat",vat);
                args.putString("deliveryfees",deliveryfees);
                args.putString("qrcode",qrcode);
                args.putString("cost",cost);
                args.putString("quantity",quantity);
                args.putString("iswish","false");
              subchangefragment(new NewInvoice(),args);
            }



        }

    }


    public void subchangefragment(Fragment fragment, Bundle bundle){
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment,fragment.getClass().getSimpleName()).addToBackStack(fragment.getClass().getSimpleName()).commit();
      /*  txt_delv_add.setVisibility(View.GONE);
        txt_loc.setVisibility(View.GONE);
        profile_txt.setVisibility(View.VISIBLE);
        side_nav_icon.setVisibility(View.GONE);
        notification.setVisibility(View.GONE);*/
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(this::onClick);
    }
    public void removeallfragments(){
        for(int i=0;i<getSupportFragmentManager().getBackStackEntryCount();i++){
            getSupportFragmentManager().popBackStack();
        }
//        homefragmentload();
    }
/*
    public void navigationselectclear(){
        for (int i = 0; i < botm_nav.getMenu().size(); i++) {
            botm_nav.getMenu().getItem(i).setCheckable(false);
        }
        for(int j=0;j < navigationView.getMenu().size();j++){
            navigationView.getMenu().getItem(j).setCheckable(false);
        }
    }
*/
}