package com.lia.yello.roha.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.RohawaterHomeFragment;
import com.lia.yello.roha.R;

import java.util.ArrayList;

public class MosqueListAdapter  extends RecyclerView.Adapter<MosqueListAdapter.MyViewHolder> {
    private Context mcontext;
    private RohawaterHomeFragment rohawaterHomeFragment;
    private ArrayList<String> title = new ArrayList<>();
    private ArrayList<String> branchname = new ArrayList<>();
    AlertDialog alertDialog;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView delete;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.delete = (ImageView) itemView.findViewById(R.id.close);
        }
    }

    public MosqueListAdapter(AlertDialog alertDialog, Context context, ArrayList<String> branchname, ArrayList<String> title, RohawaterHomeFragment rohawaterHomeFragment) {
        this.title = title;
        this.branchname = branchname;
        this.alertDialog = alertDialog;
        this.mcontext = context;
        this.rohawaterHomeFragment = rohawaterHomeFragment;
    }

    @Override
    public MosqueListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_mosquelist, parent, false);
        MosqueListAdapter.MyViewHolder myViewHolder = new MosqueListAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.name.setText(""+(listPosition+1)+"."+title.get(listPosition).toString()+" - "+branchname.get(listPosition));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(title.size()>1){
                    for(int i=0;i<rohawaterHomeFragment.overallmarkerstitle.size();i++){
                        if(rohawaterHomeFragment.overallmarkerstitle.get(i).equalsIgnoreCase(title.get(listPosition))){
                            rohawaterHomeFragment.deletedbranch.add(branchname.get(listPosition));
                            rohawaterHomeFragment.overallmarkerstitle.remove(i);
                            rohawaterHomeFragment.overallmarkersLatLng.remove(i);
                            rohawaterHomeFragment.overallmarkersbranchid.remove(i);
                            rohawaterHomeFragment.overallmarkersbranchname.remove(i);
                            rohawaterHomeFragment.overallmarkersbranchstock.remove(i);
                            notifyDataSetChanged();
                        }
                    }
                }
                else {
                    for(int i=0;i<rohawaterHomeFragment.overallmarkerstitle.size();i++){
                        if(rohawaterHomeFragment.overallmarkerstitle.get(i).equalsIgnoreCase(title.get(listPosition))){
                            rohawaterHomeFragment.deletedbranch.add(branchname.get(listPosition));
                            rohawaterHomeFragment.overallmarkerstitle.remove(i);
                            rohawaterHomeFragment.overallmarkersLatLng.remove(i);
                            rohawaterHomeFragment.overallmarkersbranchid.remove(i);
                            rohawaterHomeFragment.overallmarkersbranchname.remove(i);
                            rohawaterHomeFragment.overallmarkersbranchstock.remove(i);
                            notifyDataSetChanged();
                            alertDialog.dismiss();
                            rohawaterHomeFragment.loadproductdata(rohawaterHomeFragment.currentbranchname);
                            rohawaterHomeFragment.setmarkercount();
                        }
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return title.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

