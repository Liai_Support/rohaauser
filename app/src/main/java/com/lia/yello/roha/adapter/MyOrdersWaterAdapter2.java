package com.lia.yello.roha.adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lia.yello.roha.Fragment.OrderTrackingFragment;
import com.lia.yello.roha.Fragment.WateOrderPhotorFragment;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.model.ListorderDatamodel2;
import com.lia.yello.roha.R;

import java.util.List;

public class MyOrdersWaterAdapter2 extends RecyclerView.Adapter<MyOrdersWaterAdapter2.MyViewHolder> {
    private Context mcontext;
    private List<ListorderDatamodel2> dataSet;
    private OrderTrackingFragment donationActivity2;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mosque_name,sub_tot_carton_count;
        CardView main;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.main = (CardView) itemView.findViewById(R.id.card1);
            this.mosque_name = (TextView) itemView.findViewById(R.id.mosque_name);
            this.sub_tot_carton_count = (TextView) itemView.findViewById(R.id.sub_tot_carton_count);
        }
    }

    public MyOrdersWaterAdapter2(Context context, List<ListorderDatamodel2> data, OrderTrackingFragment donationActivity2) {
        this.dataSet = data;
        mcontext = context;
        this.donationActivity2 = donationActivity2;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_water_myorder2, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        if(dataSet.get(listPosition).getDiscounttype().equalsIgnoreCase("Carton") && !dataSet.get(listPosition).getDiscounttype().isEmpty() && !dataSet.get(listPosition).getDiscountvalue().isEmpty()){
            if (donationActivity2.iswish == 0) {
                holder.mosque_name.setText("" + dataSet.get(listPosition).getPlace());
                holder.sub_tot_carton_count.setText("" + dataSet.get(listPosition).getCartoncount() + " " + mcontext.getString(R.string.quantity));
            }
            else {
                // holder.mosque_name.setText(""+dataSet.get(listPosition).getMosquename()+" - "+dataSet.get(listPosition).getPlace());
                holder.mosque_name.setText("" + dataSet.get(listPosition).getMosquename());
                int cartoncount = dataSet.get(listPosition).getCartoncount();
                int disc = Integer.parseInt(dataSet.get(listPosition).getDiscountvalue());
                int fine = cartoncount-disc;
                holder.sub_tot_carton_count.setText(""+fine+"+"+disc+" "+ mcontext.getString(R.string.quantity));
            }
        }
        else {
            if (donationActivity2.iswish == 0) {
                holder.mosque_name.setText("" + dataSet.get(listPosition).getPlace());
                holder.sub_tot_carton_count.setText("" + dataSet.get(listPosition).getCartoncount() + " " + mcontext.getString(R.string.quantity));
            }
            else {
                // holder.mosque_name.setText(""+dataSet.get(listPosition).getMosquename()+" - "+dataSet.get(listPosition).getPlace());
                holder.mosque_name.setText("" + dataSet.get(listPosition).getMosquename());
                holder.sub_tot_carton_count.setText("" + dataSet.get(listPosition).getCartoncount() + " " + mcontext.getString(R.string.quantity));
            }
        }

        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (donationActivity2.sessionManager.getchoose().equals("rohahome")) {
                    Bundle args = new Bundle();
                    args.putInt("orderid", donationActivity2.donationid);
                    args.putInt("iswish", donationActivity2.iswish);
                    args.putString("orderstatus", donationActivity2.status);
                    args.putString("delivered_date", dataSet.get(listPosition).getDelivered_date());
                    args.putString("delivered_time", dataSet.get(listPosition).getDelivered_time());
                    args.putString("status", dataSet.get(listPosition).getStatus());
                    args.putString("image_path1", dataSet.get(listPosition).getImgpath1());
                    args.putString("image_path2", dataSet.get(listPosition).getImgpath2());
                    args.putString("image_path3", dataSet.get(listPosition).getImgpath3());
                    args.putString("image_path4", dataSet.get(listPosition).getImgpath4());
                    args.putDouble("rating", donationActivity2.rating);
                    args.putString("comment", donationActivity2.comment);
                    args.putString("suggestion", donationActivity2.suggestions);
                    ((RohahomeDashboradActivity) donationActivity2.getActivity()).subchangefragment(new WateOrderPhotorFragment(), args);
                } else if (donationActivity2.sessionManager.getchoose().equals("rohawater")) {
                    Bundle args = new Bundle();
                    args.putInt("orderid", donationActivity2.donationid);
                    args.putInt("iswish", donationActivity2.iswish);
                    args.putString("orderstatus", donationActivity2.status);
                    args.putString("delivered_date", dataSet.get(listPosition).getDelivered_date());
                    args.putString("delivered_time", dataSet.get(listPosition).getDelivered_time());
                    args.putString("status", dataSet.get(listPosition).getStatus());
                    args.putString("image_path1", dataSet.get(listPosition).getImgpath1());
                    args.putString("image_path2", dataSet.get(listPosition).getImgpath2());
                    args.putString("image_path3", dataSet.get(listPosition).getImgpath3());
                    args.putString("image_path4", dataSet.get(listPosition).getImgpath4());
                    args.putDouble("rating", donationActivity2.rating);
                    args.putString("comment", donationActivity2.comment);
                    args.putString("suggestion", donationActivity2.suggestions);
                    ((RohaWaterDashboardActivity) donationActivity2.getActivity()).subchangefragment(new WateOrderPhotorFragment(), args);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}