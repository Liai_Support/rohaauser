package com.lia.yello.roha.model;

import android.util.Log;

import org.json.JSONArray;

public class HomeorderlistDataModel1 {
    String place,status, payment_type, reftoken, order_date, order_time, order_address, time_form, time_to, type, session, day;
    int id,no_of_carton, qty, discountid, cartoncount, branchid;
    double amount, vat, delivery_charge, subtotal, order_lat, order_lng, discount, rating;
    String deliverydate,delivered_time, comment, suggestion, discountname, branchname, cartonname, discounttype,discountvalue;
    JSONArray details;

    public HomeorderlistDataModel1(String place,
                                   String status,
                                   String payment_type,
                                   String reftoken,
                                   String order_date,
                                   String order_time,
                                   String order_address,
                                   String time_form,
                                   String time_to,
                                   String session,
                                   String day,
                                   int id,
                                   int no_of_carton,
                                   int discountid,
                                   int cartoncount,
                                   int branchid,
                                   double amount,
                                   double vat,
                                   double delivery_charge,
                                   double subtotal,
                                   double order_lat,
                                   double order_lng,
                                   double rating,
                                   String deliverydate,
                                   String delivered_time,
                                   String comment,
                                   String suggestion,
                                   String discountname,
                                   String branchname,
                                   String cartonname,
                                   String discounttype,
                                   String discountvalue,
                                   JSONArray details)
    {
        this.place = place;
        this.status = status;
        this.payment_type = payment_type;
        this.reftoken = reftoken;
        this.order_date = order_date;
        this.order_time = order_time;
        this.order_address = order_address;
        this.time_form = time_form;
        this.time_to = time_to;
        this.type = type;
        this.session = session;
        this.day = day;
        this.id = id;
        this.no_of_carton = no_of_carton;
        this.qty = qty;
        this.discountid = discountid;
        this.cartoncount = cartoncount;
        this.branchid = branchid;
        this.amount = amount;
        this.vat = vat;
        this.delivery_charge = delivery_charge;
        this.subtotal = subtotal;
        this.order_lat = order_lat;
        this.order_lng = order_lng;
        this.rating = rating;
        this.deliverydate = deliverydate;
        this.delivered_time = delivered_time;
        this.comment = comment;
        this.suggestion = suggestion;
        this.discountname = discountname;
        this.branchname = branchname;
        this.cartonname = cartonname;
        this.discounttype = discounttype;
        this.discountvalue = discountvalue;
        this.details = details;
    }

    public String getDiscountvalue(){
        return discountvalue;
    }

    public String getPlace() {
        return place;
    }

    public String getStatus() {
        return status;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public String getReftoken() {
        return reftoken;
    }

    public String getOrder_date() {
        return order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public String getOrder_address() {
        return order_address;
    }

    public String getTime_form() {
        return time_form;
    }

    public String getTime_to() {
        return time_to;
    }

    public String getType() {
        return type;
    }

    public String getSession() {
        return session;
    }

    public String getDay() {
        return day;
    }

    public int getId() {
        return id;
    }

    public int getNo_of_carton() {
        return no_of_carton;
    }

    public int getQty() {
        return qty;
    }

    public int getDiscountid() {
        return discountid;
    }

    public int getCartoncount() {
        return cartoncount;
    }

    public int getBranchid() {
        return branchid;
    }

    public double getAmount() {
        return amount;
    }

    public double getVat() {
        return vat;
    }

    public double getDelivery_charge() {
        return delivery_charge;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public double getOrder_lat() {
        return order_lat;
    }

    public double getOrder_lng() {
        return order_lng;
    }

    public double getRating() {
        return rating;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public String getDelivered_time() {
        return delivered_time;
    }

    public String getComment() {
        return comment;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public String getDiscountname() {
        return discountname;
    }

    public String getBranchname() {
        return branchname;
    }

    public String getCartonname() {
        return cartonname;
    }

    public String getDiscounttype() {
        return discounttype;
    }

    public JSONArray getDetails() {
        return details;
    }
}


