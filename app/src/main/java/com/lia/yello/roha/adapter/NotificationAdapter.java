package com.lia.yello.roha.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.NotificationFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.NotificationDataModel;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Context mcontext;
    private List<NotificationDataModel> dataSet;
    private NotificationFragment notificationFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView message,tittle,date;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.message = (TextView) itemView.findViewById(R.id.message);
            this.tittle = (TextView) itemView.findViewById(R.id.notification_title);
            this.date = (TextView) itemView.findViewById(R.id.ordertime);
        }
    }

    public NotificationAdapter(Context context,List<NotificationDataModel>  data, NotificationFragment notificationFragment) {
        this.dataSet = data;
        this.mcontext = context;
        this.notificationFragment = notificationFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_notification, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        holder.tittle.setText(dataSet.get(listPosition).getTitle());
        holder.message.setText(dataSet.get(listPosition).getMessage());
        holder.date.setText(dataSet.get(listPosition).getDate());

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}