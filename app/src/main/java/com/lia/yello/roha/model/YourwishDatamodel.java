package com.lia.yello.roha.model;

public class YourwishDatamodel {
    private int id;
    private String title;
    private String image;
    private String icon;

    public YourwishDatamodel(int id, String title, String image, String icon) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.icon = icon;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}