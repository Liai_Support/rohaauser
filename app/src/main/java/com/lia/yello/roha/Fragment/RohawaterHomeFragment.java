package com.lia.yello.roha.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.navigation.NavigationView;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.adapter.MosqueListAdapter;
import com.lia.yello.roha.adapter.NotificationAdapter;
import com.lia.yello.roha.adapter.WaterproductdataAdapter;
import com.lia.yello.roha.model.NotificationDataModel;
import com.lia.yello.roha.model.PlacesItem;
import com.lia.yello.roha.model.WaterproductDataModel;
import com.lia.yello.roha.utility.LocalData;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static android.provider.ContactsContract.ProviderStatus.STATUS;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.FacebookSdk.isDebugEnabled;


public class RohawaterHomeFragment extends Fragment implements OnMapReadyCallback,
        LocationListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMarkerClickListener,
        View.OnClickListener,
        GoogleMap.OnPolygonClickListener,
        GoogleMap.OnPolylineClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener
{
    View view;
    public SessionManager sessionManager;
    private static RecyclerView.Adapter adapter1;
    private RecyclerView.LayoutManager layoutManager;
    LinearLayoutManager HorizontalLayout;
    private static RecyclerView recyclerView1;
    public String countrycode, phonenumber, username, email,place;
    public String currentbranchname = "";
    public int currentbranchid = 0;
    public int currentbranchstock = 0 ;
    public int possiblemarkerselectcount = 0;
    public Boolean issearch = false;
    boolean contains1;
    public ArrayList<String> deletedbranch = new ArrayList<>();
    ImageView mylocation;
    public CardView popup_home;
    public TextView selectedmosquecount,btnMecca, btnMedina, titleText,mecca_count, medina_count, referafriend,rateus, version;
    public LinearLayout your_selection, rohaa_select, popupwishlist,navheader,linear_mecca, linear_medina;
    RelativeLayout main_relative;
    Button yes, no;
    public NavigationView navigationView;
    //public GifImageView wishlist;
    public ImageView wishlist;
    public View headerview;
    public FrameLayout map;
    private GoogleMap mMap;
    private View mapView;

    private Circle mCircle;
    private Circle iCircle;
    private Circle jCircle;
    private Polygon polygon;
    private Polygon jpolygon;


    public Double meccakabbalat = 21.422487;
    public Double meccakabbalong = 39.826206;
//    public Double meccakabbalat = 24.6128055;
//    public Double meccakabbalong = 46.6403433;
    public Double medinakabbalat = 24.467197;
    public Double medinakabbalong = 39.6089658;
    public Double jeddahkabbalat = 21.4739744;
    public Double jeddahkabbalong = 39.19250479999999;
    public Double riyadhkabbalat = 24.733280606;
    public Double riyadhkabbalong = 46.518958769738;

    public Double currentlocationlatitue,currentlocationlongitute;
    public Double clat,clong;

    public List<LatLng> points = new ArrayList<>();
    public ArrayList<WaterproductDataModel> data;
    public ArrayList<LatLng> overallmarkersLatLng = new ArrayList<>();
    public ArrayList<String> overallmarkerstitle = new ArrayList<>();
    public ArrayList<String> overallmarkersbranchname = new ArrayList<>();
    public ArrayList<Integer> overallmarkersbranchid = new ArrayList<>();
    public ArrayList<Integer> overallmarkersbranchstock = new ArrayList<>();
    public ArrayList<LatLng> meccamarkersLatLng = new ArrayList<>();
    public ArrayList<LatLng> medinamarkersLatLng = new ArrayList<>();

    private ArrayList<LatLng> hashSetlatlang = new ArrayList<>();
    private ArrayList<MarkerOptions> markers = new ArrayList<MarkerOptions>();
    ArrayList<PlacesItem> placesarrayList = new ArrayList<PlacesItem>();

    MarkerOptions markerOptions = new MarkerOptions();
    Marker marker;
    boolean ismeccaclick = false,ismedinaclick = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private final float DEFAULT_ZOOM = 17;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private final static int GEOFENCE_RADIUS_IN_METERS = 1000;
    private final static long GEOFENCE_EXPIRATION_IN_MILLISECONDS = Geofence.NEVER_EXPIRE;
    private static int EARTH_RADIUS = 6371000;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_roha_water_home, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        LocalData.removeAllValues();
        main_relative = (RelativeLayout) view.findViewById(R.id.main);
        mylocation = (ImageView) view.findViewById(R.id.my_location);
        linear_mecca = (LinearLayout) view.findViewById(R.id.linear_mecca);
        linear_medina = (LinearLayout) view.findViewById(R.id.linear_medina);
        mecca_count = (TextView) view.findViewById(R.id.mecca_count_txt);
        medina_count = (TextView) view.findViewById(R.id.medina_count_txt);
        btnMecca = (TextView) view.findViewById(R.id.btnmecca);
        btnMedina = (TextView) view.findViewById(R.id.txtmedina);
        wishlist = (ImageView) view.findViewById(R.id.wishlist);
        your_selection = (LinearLayout) view.findViewById(R.id.your_selection);
        rohaa_select = (LinearLayout) view.findViewById(R.id.rohaa_select);
        popup_home = (CardView) view.findViewById(R.id.popup_home);
        popupwishlist = (LinearLayout) view.findViewById(R.id.poupup_wishlist);
        yes = (Button) popupwishlist.findViewById(R.id.yes);
        no = (Button) popupwishlist.findViewById(R.id.no);
        version = (TextView) view.findViewById(R.id.version);
        selectedmosquecount = (TextView) view.findViewById(R.id.selectedmosque);

        selectedmosquecount.setOnClickListener(this);
        mylocation.setOnClickListener(this);
        linear_mecca.setOnClickListener(this);
        linear_medina.setOnClickListener(this);
        wishlist.setOnClickListener(this);
        your_selection.setOnClickListener(this);
        rohaa_select.setOnClickListener(this);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
        mecca_count.setOnClickListener(this);
        medina_count.setOnClickListener(this);

        mecca_count.setText("");
        medina_count.setText("");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        countrycode = getActivity().getIntent().getStringExtra("ucountrycode");
        username = getActivity().getIntent().getStringExtra("uname");
        email = getActivity().getIntent().getStringExtra("uemail");
        phonenumber = "+" + countrycode + "" + getActivity().getIntent().getStringExtra("uphone");

        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView1.setLayoutManager(layoutManager);
        HorizontalLayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);
        recyclerView1.setLayoutManager(HorizontalLayout);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());


        if (StaticInfo.rohawaterfirst == true) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.popup_water_dashboard, null);
            //  View promptView = layoutInflater.inflate(R.layout.chossingapp, null);

            final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();

            LinearLayout btnAdd1 = (LinearLayout) promptView.findViewById(R.id.your_selection);

            LinearLayout btnAdd2 = (LinearLayout) promptView.findViewById(R.id.rohaa_select);

            btnAdd1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    StaticInfo.rohawaterfirst = false;
                    sessionManager.setIsWish(1);
                    popup_home.setVisibility(View.GONE);
                    map = (FrameLayout) view.findViewById(R.id.map_main);
                    map.setVisibility(View.VISIBLE);
                    recyclerView1.setVisibility(View.VISIBLE);
                    wishlist.setVisibility(View.VISIBLE);
                    getDeviceLocation1();
                    alertD.dismiss();
                    // btnAdd1 has been clicked
                }
            });

            btnAdd2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    StaticInfo.rohawaterfirst = false;
                    alertD.dismiss();
                    iswishpopup();
                }
            });

            alertD.setView(promptView);
            alertD.setCancelable(false);
            alertD.show();
            alertD.getWindow().setGravity(Gravity.CENTER);
            alertD.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_border_10dp_white));

            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(alertD.getWindow().getAttributes());
            //layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            // layoutParams.width = 500;
            // layoutParams.height = 500;
            alertD.getWindow().setAttributes(layoutParams);
            waterproductdatarequest(0);
        }
        else {
            popup_home.setVisibility(View.GONE);
            map = (FrameLayout) view.findViewById(R.id.map_main);
            map.setVisibility(View.VISIBLE);
            recyclerView1.setVisibility(View.VISIBLE);
            wishlist.setVisibility(View.VISIBLE);
            if(overallmarkerstitle.size()==0){
                getDeviceLocation1();
            }
            else {
               /* Fragment fragment = new RohawaterHomeFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();*/
                Intent intent = new Intent(getActivity(), RohaWaterDashboardActivity.class);
                startActivity(intent);
            }
        }

        if (sessionManager.getguest() == 0) {
           /* navigationView.inflateHeaderView(R.layout.donation_card1);
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.profile_menu);*/
            headerview = navigationView.getHeaderView(0);
            navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);
            Menu menu = navigationView.getMenu();
            MenuItem menuItem = menu.findItem(R.id.signoff);
            menuItem.setTitle(R.string.login);
            menuItem.setIcon(R.drawable.login_icon);
            MenuItem menuItem1 = menu.findItem(R.id.reset);
            menuItem1.setVisible(false);
            LinearLayout profile = (LinearLayout) navheader.findViewById(R.id.profile);
            profile.setVisibility(View.GONE);
        }
        ((RohaWaterDashboardActivity)getActivity()).progressBar.setEnabled(false);
        ((RohaWaterDashboardActivity)getActivity()).progressBar.setVisibility(View.GONE);
        return view;
    }

    private void waterproductdatarequest(int branchid){
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("branchid", branchid);
                productdatarequestJSON(requestbody);
                sessionManager.progressdialogshow();
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
    private void productdatarequestJSON(JSONObject response)    {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringwaterproductdata, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("productdataresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        recyclerView1.setVisibility(View.VISIBLE);
                        JSONObject obj1 = new JSONObject(obj.getString("data"));
                        JSONArray data1 = new JSONArray(obj1.getString("product_details"));
                        data = new ArrayList<WaterproductDataModel>();
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject = data1.getJSONObject(i);

                            WaterproductDataModel datamodel = new WaterproductDataModel(jsonObject.getString("pimage"),jsonObject.getString("product_name"),Integer.parseInt(jsonObject.getString("id")),Integer.parseInt(jsonObject.getString("product_master_id")), Integer.parseInt(jsonObject.getString("totQty")), Double.parseDouble(jsonObject.getString("totCost")), Double.parseDouble(jsonObject.getString("UnitCost")),jsonObject.getInt("stockquantity"));
                            data.add(datamodel);
                            currentbranchstock = jsonObject.getInt("stockquantity");
                            possiblemarkerselectcount = jsonObject.getInt("stockquantity")/10;
                            //possiblemarkerselectcount = 3;

                        }
                        adapter1 = new WaterproductdataAdapter(getApplicationContext(), data, RohawaterHomeFragment.this);
                        recyclerView1.setAdapter(adapter1);


                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        })  {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    public void conResume(ArrayList<String> branchname){
        for (int i=0;i<branchname.size();i++){
            if(branchname.get(i).equalsIgnoreCase(currentbranchname)){
                loadproductdata(currentbranchname);
                break;
            }
        }
        setmarkercount();
    }

    public void loadproductdata(String branchname){
        Log.d(TAG, "loadproductdata: "+branchname);
        int branchid = sessionManager.isbranchAvailable(branchname);

            if (branchid ==1 || branchid == 2 || branchid == 3 || branchid == 9 ) {

                currentbranchname = branchname;
                currentbranchid = branchid;
                waterproductdatarequest(branchid);
                loadmarkers(currentlocationlatitue, currentlocationlongitute);

            }
        else {
            noservice(branchname);
            currentbranchname = "";
            currentbranchid = 0;
            issearch = false;
            waterproductdatarequest(0);
        }
    }

    @Override
    public void onClick(View view) {
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(getActivity());
        }
        else {
            if(sessionManager.isGpsEnabled() == false || sessionManager.isPermissionsEnabled() == false){
                sessionManager.gpsEnableRequest();
                sessionManager.permissionsEnableRequest();
            }
            else {
                 if (view == rateus) {
                    Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                    myWebLink.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.lia.yello.roha"));
                    startActivity(myWebLink);
                }
                else if (view == referafriend) {
                    Intent myIntent = new Intent(Intent.ACTION_SEND);
                    myIntent.setType("text/plain");
                    String body = getString(R.string.checkoutthisapp) + " : " + "https://play.google.com/store/apps/details?id=com.lia.yello.roha";
                    String sub = "Your Subject";
                    myIntent.putExtra(Intent.EXTRA_SUBJECT,sub);
                    myIntent.putExtra(Intent.EXTRA_TEXT, body);
                    startActivity(Intent.createChooser(myIntent, "Share Using"));
                }
                if (view == linear_mecca) {
                    //sessionManager.snackbarToast(getString(R.string.clientmsg),linear_mecca);
                    if(ismeccaclick != true){
                        loadproductdata(StaticInfo.MECCA);
                    }
                }
                else if (view == linear_medina) {
                    if(ismedinaclick != true){
                        loadproductdata(StaticInfo.MEDINA);
                    }
                }
                else if (view == wishlist) {
                    StaticInfo.rohawaterfirst = false;
                    iswishpopup();
                }
                else if(view == mylocation){
                    if(sessionManager.isGpsEnabled() == false || sessionManager.isPermissionsEnabled() == false){
                        sessionManager.gpsEnableRequest();
                        sessionManager.permissionsEnableRequest();
                    }
                    else {
                            linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
                            linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
                            linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                            linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                            ismeccaclick = false;
                            ismedinaclick = false;
                            getDeviceLocation1();
                    }
                }
                else if(view == selectedmosquecount){
                    showmosquelistpopup();
                }
            }
        }
    }

    public void iswishpopup(){
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View promptView = layoutInflater.inflate(R.layout.popup_wishlist, null);

        final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();

        Button btnAdd1 = (Button) promptView.findViewById(R.id.yes);

        Button btnAdd2 = (Button) promptView.findViewById(R.id.no);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sessionManager.setIsWish(0);
                alertD.dismiss();
                ((RohaWaterDashboardActivity)getActivity()).subchangefragment(new YourwishFragment(), null);
                // btnAdd1 has been clicked
            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sessionManager.setIsWish(1);
                alertD.dismiss();
                // btnAdd2 has been clicked

            }
        });

        alertD.setView(promptView);
        alertD.setCancelable(false);
        alertD.show();
        alertD.getWindow().setGravity(Gravity.BOTTOM);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertD.getWindow().getAttributes());
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        // layoutParams.width = 500;
        // layoutParams.height = 500;
        alertD.getWindow().setAttributes(layoutParams);
    }

    private void showmosquelistpopup() {
        if(overallmarkersLatLng.size()>0){
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            View mView = getLayoutInflater().inflate(R.layout.popup_mosquelist,null);
            ImageView close = (ImageView) mView.findViewById(R.id.close);
            TextView title = (TextView) mView.findViewById(R.id.title);
            RecyclerView recyclerView = (RecyclerView) mView.findViewById(R.id.mosquelist);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
            alert.setView(mView);
            final AlertDialog alertDialog = alert.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(deletedbranch.size()!=0){
                        for (int i=0;i<deletedbranch.size();i++){
                            if(deletedbranch.get(i).equalsIgnoreCase(currentbranchname)){
                                loadproductdata(currentbranchname);
                                break;
                            }
                        }
                        setmarkercount();
                        alertDialog.dismiss();
                    }
                    else {
                        alertDialog.dismiss();
                    }
                }
            });
            RecyclerView.Adapter adapter = new MosqueListAdapter(alertDialog,getContext(),overallmarkersbranchname,overallmarkerstitle,RohawaterHomeFragment.this);
            recyclerView.setAdapter(adapter);
            alertDialog.show();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapsearch();
        // No explanation needed; request the permission
        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("savdfsd", "first453");
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        else {
            Log.d("savdfsd", "first144");
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
            Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
            task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    // Add polylines to the map.
                    // Polylines are useful to show a route or some other connection between points.
                    // Position the map's camera near Alice Springs in the center of Australia,
                    // and set the zoom factor so most of Australia shows on the screen.
                    //getDeviceLocation();
                    if(StaticInfo.rohawaterfirst == false){
                        //getDeviceLocation1();
                    }
                    //mecca();
                }
            });
            task.addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        try {
                            resolvable.startResolutionForResult(getActivity(), 51);
                        } catch (IntentSender.SendIntentException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 0);
        }
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnPolylineClickListener(this);
        mMap.setOnPolygonClickListener(this);
       // mMap.setOnPolylineClickListener(this);
       // mMap.setOnPolygonClickListener(this);
       /* if (StaticInfo.homeFirst == false) {
            commondatarequestJSON();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51) {
            if (resultCode == RESULT_OK) {
                Log.d("rdfgvbh", "" + requestCode + "" + resultCode);
                Log.d("savdfsd", "first" + "8");
                /*if(StaticInfo.homeFirst == false) {
                    getDeviceLocation1();
                }*/
                /*Intent intent = new Intent(HomeActivity.this,HomeActivity.class);
                startActivity(intent);*/
            } else {
                sessionManager.gpsEnableRequest();
                Log.d("savdfsd", "first9");
            }
        } else {
            Log.d("savdfsd", "first867");
        }
    }

    public void mapsearch(){
        if (!sessionManager.isNetworkEnabled() == true){
            sessionManager.noNetworkDialog(getActivity());
        }
        else {
            /**
             * Initialize Places. For simplicity, the API key is hard-coded. In a production
             * environment we recommend using a secure mechanism to manage API keys.
             */
            // Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
            if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), StaticInfo.googlemapapikey);
            }


            // Initialize the AutocompleteSupportFragment.
            AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                    getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

            //autocompleteFragment.setText("Choose your city or mosque");
            //autocompleteFragment.setHint(getString(R.string.chooseyourcity));
            //autocompleteFragment.setTypeFilter(TypeFilter.ESTABLISHMENT);
            // autocompleteFragment.setLocationBias(RectangularBounds.newInstance(new LatLng(565,767),new LatLng(678,8798)));
           // autocompleteFragment.setCountries("SA");
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG,Place.Field.ADDRESS,Place.Field.ADDRESS_COMPONENTS,Place.Field.BUSINESS_STATUS,Place.Field.TYPES,Place.Field.VIEWPORT, Place.Field.UTC_OFFSET, Place.Field.PHOTO_METADATAS));
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    Log.d("TAG", "Place: " + place.getName() + ", " + place.getId() + "," + place.getLatLng()+","+place.getAddress()+"-end-"+place.getAddressComponents());
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getActivity(), Locale.ENGLISH);
                    try {
                        addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();
                        Log.d("dsvdfvdfv",""+city);
                        currentlocationlatitue = place.getLatLng().latitude;
                        currentlocationlongitute = place.getLatLng().longitude;

                        if(city!=null && !city.equalsIgnoreCase("")) {
                            issearch = true;
                            loadproductdata(sessionManager.getcityname(city));
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"We could not find that location..",Toast.LENGTH_SHORT).show();
                        }
                       // mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                       // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 17F));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Status status) {
                    // TODO: Handle the error.
                    Log.d("TAG", "An error occurred: " + status);
                }
            });
        }
    }

    private void getDeviceLocation1() {
        if(sessionManager.isGpsEnabled() == false || sessionManager.isPermissionsEnabled() == false){
            sessionManager.gpsEnableRequest();
            sessionManager.permissionsEnableRequest();
        }
        else {
            Log.d("TAG", "getDeviceLocation: getting the devices current location");
           // mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
            try {
                final Task task = mFusedLocationProviderClient.getLastLocation();
                if(task != null){
                    task.addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task!=null && task.isSuccessful()) {
                            Log.d("TAG", "onComplete: found task!");
                            Location currentLocation = (Location) task.getResult();
                            if(currentLocation != null){

                            currentlocationlatitue = currentLocation.getLatitude();
                                currentlocationlongitute = currentLocation.getLongitude();
                              mMap.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.cperson)));

                                Log.d("vbnm", "" + currentLocation);
                                moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                        DEFAULT_ZOOM);

                                Location location = (Location) task.getResult();
                                //you can hard-code the lat & long if you have issues with getting it
                                //remove the below if-condition and use the following couple of lines
                                //double latitude = 37.422005;
                                //double longitude = -122.084095

                                if (location != null) {
                                    double latitude = currentlocationlatitue;
                                    double longitude = currentlocationlongitute;
                                    sessionManager.setclat(String.valueOf(latitude));
                                    sessionManager.setclng(String.valueOf(longitude));
                                    LocalData.LocationAddress locationAddress = new LocalData.LocationAddress();
                                    sessionManager.progressdialogshow();
                                    locationAddress.getAddressFromLocation(latitude, longitude, getApplicationContext(), new GeocoderHandler());
                                }
                                else {
                                    Log.d("TAG", "onComplete: current task is null");
                                }
                            }
                            else {
                                Log.d("TAG", "onComplete: current task is null");
                            }
                        } else {
                            Log.d("TAG", "onComplete: current task is null");
                            //Toast.makeText(getActivity(), "unable to get current task", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                }
                else {
                    Log.d("TAG", "onComplete: current task is null");
                    //Toast.makeText(getActivity(), "unable to get current task", Toast.LENGTH_SHORT).show();
                }
            } catch (SecurityException e) {
                Log.e("TAG", "getDeviceLocation: SecurityException: " + e.getMessage());
            }
        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationCity = "";
            String locationAddress = "";
            int pincode;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    Log.d("gvbhnjmds",""+bundle);
                    Log.d("gvbhnjbvcmds",""+message.getData().toString());
                    locationAddress = bundle.getString("address");
                    locationCity = bundle.getString("city");
                   // pincode = Integer.parseInt(bundle.getString("pincode"));
                    break;
                default:
                    locationCity = "";
                    locationAddress = "";
                    pincode = 0;
            }
            if(locationCity != null && !locationCity.equals("") ) {
                sessionManager.setcaddr(locationAddress);
                sessionManager.setccity(sessionManager.getcityname(locationCity));
                sessionManager.progressdialogdismiss();
                loadproductdata(sessionManager.getccity());
            }
            else {
                sessionManager.progressdialogdismiss();
                Toast.makeText(getApplicationContext(),"We could not find your city..",Toast.LENGTH_SHORT).show();
            }

            /*if(pincode!=0) {
                if (pincode>=24211 && pincode<=24441) {
                    mecca(currentlocationlatitue, currentlocationlongitute);
                    linear_jeddah.setVisibility(View.GONE);
                } else if (pincode>=42221 && pincode<=42724) {
                    medina(currentlocationlatitue, currentlocationlongitute);
                    linear_jeddah.setVisibility(View.GONE);
                } else if (pincode>=21589 && pincode<=24441) {
                    linear_jeddah.setVisibility(View.VISIBLE);
                    jeddah(currentlocationlatitue, currentlocationlongitute);
                } else if (currentcity.equalsIgnoreCase("Chennai")) {
                    linear_jeddah.setVisibility(View.GONE);
                    //chennai(currentlocationlatitue,currentlocationlongitute);
                    noservice();
                } else {
                    linear_jeddah.setVisibility(View.GONE);
                    noservice();
                }
            }
            else {
                linear_jeddah.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"Currentlocation detail fetch failed",Toast.LENGTH_SHORT).show();
            }*/
        }

    }

    private void noservice(String currentcity){
            ((RohaWaterDashboardActivity)getActivity()).txt_delv_add.setVisibility(View.GONE);
            ((RohaWaterDashboardActivity)getActivity()).txt_loc.setVisibility(View.GONE);
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setVisibility(View.VISIBLE);
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(""+getString(R.string.ordertype));
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(""+getString(R.string.noservice)+" "+currentcity)
                    .setCancelable(true)
                    .setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            sessionManager.setIsWish(1);
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            dialog.dismiss();
                            iswishpopup();
                        }
                    });
            final AlertDialog dialog = builder.create();
            dialog.show(); //show() should be called before dialog.getButton().
            final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positiveButton.setBackgroundColor(Color.TRANSPARENT);
            //positiveButton.setTextColor(Color.BLACK);
            LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
            positiveButtonLL.gravity = Gravity.CENTER;
            positiveButton.setLayoutParams(positiveButtonLL);
    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d("TAG", "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void loadmarkers(Double currentlatitue,Double currentlongitute){
        ((RohaWaterDashboardActivity)getActivity()).txt_delv_add.setVisibility(View.GONE);
        ((RohaWaterDashboardActivity)getActivity()).txt_loc.setVisibility(View.GONE);
        ((RohaWaterDashboardActivity)getActivity()).profile_txt.setVisibility(View.VISIBLE);
        ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(""+sessionManager.getbranchname(currentbranchid)+" "+getString(R.string.mosques));
        sessionManager.setIsWish(1);
        hashSetlatlang = new ArrayList<>();
        mMap.clear();
//        mMap.addPolygon(new PolygonOptions().addAll(lat).strokeWidth(2).strokeColor(0x700a420b));
        Double[] meccalat = {21.344886, 21.351343, 21.346888, 21.336781, 21.362595, 21.367332, 21.357075, 21.365828, 21.368079, 21.345759, 21.344759, 21.337717, 21.338878, 21.325532, 21.333511, 21.333918, 21.335440, 21.326930, 21.326544, 21.316423, 21.324543, 21.316161, 21.316161, 21.315677, 21.311705, 21.314328, 21.316486, 21.310148, 21.327618, 21.310834, 21.324434, 21.327610, 21.341206, 21.378948, 21.402451, 21.405460, 21.426701, 21.428494, 21.485591, 21.496025, 21.519537, 21.527580, 21.515047, 21.529462, 21.509023, 21.518114, 21.503815, 21.506010, 21.498435, 21.496697, 21.467104, 21.489271, 21.459138, 21.460154, 21.438827, 21.430680, 21.404469, 21.421265, 21.394080, 21.398541, 21.376575, 21.359246};
        Double[] meccalng = {39.639473, 39.635942, 39.650686, 39.663071, 39.681508, 39.689621, 39.715946, 39.714872, 39.707887, 39.714371, 39.709947, 39.723147, 39.708071, 39.731026, 39.729912, 39.751481, 39.744339, 39.759828, 39.767215, 39.779262, 39.767747, 39.783354, 39.783354, 39.786592, 39.794778, 39.789981, 39.812870, 39.799774, 39.881660, 39.832154, 39.904515, 39.921925, 39.939201, 39.975077, 39.970574, 39.973645, 39.978136, 39.974158, 39.941409, 39.936092, 39.924862, 39.929269, 39.885085, 39.917685, 39.866028, 39.873088, 39.849129, 39.858128, 39.843263, 39.833089, 39.801255, 39.830817, 39.741689, 39.706391, 39.680793, 39.647376, 39.643663, 39.635210, 39.630283, 39.636298, 39.628460, 39.632750};
        Double[] medinalat = {24.519193008026196,24.518819636432095,24.519773501037456,24.5247934487535,24.521810543568993,24.527704298145153,24.53254702358313,24.53987831708815,24.537440496783322,24.54231029531173,24.54477089448188,24.54629669791303,24.54681637567668,24.542571667643085,24.54329905452073,24.537936116531665,24.53438865599243,24.523962244438557,24.527088764205512,24.53086427621187,24.51009603017816,24.516709944057858,24.50707189647936,24.49943848002909,24.504869263641133,24.44643232912617,24.4958093862325,24.438488471322348,24.441618641812454,24.433237058058666,24.430469935604567,24.42360696574122,24.427463425640646,24.41859397168759,24.42057617504238,24.415108478703406,24.416616927133266,24.413859514944168,24.412911869566262,24.412710066587277,24.411315754890524,24.408325580619724,24.415734635057014,24.41063309524505,24.409172203413664,24.41252871840054,24.411332851864675,24.412380037117064,24.411955058026972,24.411959942852224,24.411437876083653,24.41187812200426,24.41016445153576,24.40893711579283,24.40693671614657,24.407152267448804,24.40592154394715,24.406118167410447,24.405106957766165,24.401122187801697,24.40015673767835,24.403591044082177,24.397722622574477,24.40050908813579,24.395309223812184,24.3980813918598,24.398743967350654,24.39636631280098,24.396339748281363,24.39354708382916,24.392134544931963,24.38802294700577,24.385428281238116,24.383521258020018,24.378561641479326,24.37816006312593,24.37808127421948,24.37681881271996,24.379881496861607,24.381848733751692,24.383761278858053,24.384556153773836,24.382728819574204,24.384516761319862,24.381346697318055,24.38271049723924,24.388868789814715,24.38708487933488,24.38618161618898,24.39447441649407,24.392582797204735,24.390651148347406,24.39600082116144,24.399648972681984,24.397984905922378,24.412134880532086,24.411775540567728,24.45445107038094,24.453377376690227,24.475360330535267,24.5188193313895,24.49402363227367,24.506041361986977};
        Double[] medinalng = {39.666082710027695,39.66885678470135,39.66205034404993,39.654409401118755,39.65826407074928,39.650575183331966,39.64426629245281,39.632165506482124,39.636251516640186,39.62808586657047,39.62392643094063,39.62035808712244,39.617417715489864,39.58041831851006,39.58083339035511,39.57726739346981,39.57588169723749,39.57174941897392,39.57301005721092,39.57447320222854,39.564614742994316,39.568932093679905,39.56137463450432,39.556610360741615,39.559524580836296,39.5423212274909,39.55551568418741,39.540207982063286,39.54102203249931,39.53842934221029,39.5367606729269,39.53237257897854,39.53484691679478,39.53083634376526,39.53056912869215,39.53348200768232,39.53176874667406,39.536080062389374,39.53808702528477,39.53874181956053,39.54185988754034,39.550732634961605,39.55128014087677,39.55348089337349,39.552739597857,39.55360762774944,39.5553969964385,39.55440826714039,39.5615154504776,39.558464102447026,39.55652218312025,39.563109688460834,39.56479478627443,39.56451114267111,39.563155956566334,39.56512603908777,39.564942978322506,39.56351604312658,39.566951617598534,39.571432918310165,39.569733403623104,39.56929586827755,39.57257319241762,39.57293529063463,39.576345048844814,39.582066871225834,39.57936991006136,39.58434842526913,39.58207156509161,39.58580084145069,39.587030969560146,39.5864499360323,39.58428539335728,39.582215063273914,39.58631616085768,39.589329957962036,39.6006116643548,39.59725722670556,39.60523780435324,39.61032025516033,39.61538661271334,39.62412994354963,39.62039932608604,39.62412994354963,39.61723431944847,39.620351046323776,39.64558828622103,39.636087231338024,39.62930828332901,39.657696448266506,39.65362150222063,39.64948218315839,39.66115951538086,39.66900900006294,39.66543696820736,39.683615639805794,39.68335345387459,39.69601113349199,39.69592161476612,39.69766203314066,39.66882761567831,39.689817912876606,39.68404445797205};
        Double[] jeddahlat = {21.001833, 21.027912, 21.120318, 21.210915, 21.247968, 21.321929, 21.341200, 21.346091, 21.355451, 21.385660, 21.392180, 21.407727, 21.412873, 21.453985, 21.463871, 21.468109, 21.467702, 21.475393, 21.481722, 21.496773, 21.523168, 21.524615, 21.530114, 21.529129, 21.535101, 21.567451, 21.592643, 21.647549, 21.766802, 21.802323, 21.820412, 21.850109, 21.904813, 21.889850, 21.879312, 21.844924, 21.836643, 21.825396, 21.810750, 21.807772, 21.802558, 21.771289, 21.754660, 21.708841, 21.677711, 21.597190, 21.587368, 21.557460, 21.550127, 21.538120, 21.508067, 21.500881, 21.511159, 21.517941, 21.513919, 21.522292, 21.529740, 21.533302, 21.529884, 21.522250, 21.507594, 21.508408, 21.502965, 21.498713, 21.495563, 21.496176, 21.492802, 21.491860, 21.484257, 21.483761, 21.475165, 21.473607, 21.466657, 21.444669, 21.427662, 21.433300, 21.412924, 21.409969, 21.396604, 21.359497, 21.346884, 21.357126, 21.362607, 21.366949, 21.348308, 21.341488, 21.337154, 21.336334, 21.335153, 21.328769, 21.323560, 21.322722, 21.301344, 21.268248, 21.239512, 21.231780, 21.215520, 21.188490, 21.184361, 21.190463, 21.152637, 21.124377, 21.090196, 21.072803, 21.031252, 21.008916};
        Double[] jeddahlng = {39.275922, 39.299475, 39.336875, 39.348947, 39.392656, 39.362698, 39.367125, 39.377584, 39.380495, 39.356085, 39.362239, 39.358887, 39.367053, 39.372934, 39.381150, 39.380213, 39.375333, 39.370621, 39.371047, 39.361881, 39.359290, 39.355023, 39.353327, 39.344663, 39.326422, 39.322080, 39.330939, 39.314949, 39.276049, 39.258605, 39.254794, 39.251394, 39.209951, 39.178510, 39.167488, 39.168866, 39.166416, 39.155452, 39.115398, 39.035645, 39.030099, 39.049192, 39.050958, 39.082625, 39.091841, 39.104450, 39.104695, 39.109920, 39.112029, 39.108083, 39.130274, 39.135926, 39.138432, 39.139445, 39.145327, 39.147068, 39.146421, 39.154711, 39.156331, 39.149294, 39.156258, 39.159396, 39.159366, 39.154454, 39.158633, 39.164016, 39.163960, 39.152578, 39.154004, 39.149841, 39.150542, 39.146929, 39.148049, 39.152930, 39.153442, 39.168531, 39.172517, 39.186648, 39.180454, 39.162000, 39.133632, 39.130933, 39.136707, 39.132604, 39.122340, 39.124202, 39.125942, 39.121612, 39.104655, 39.108180, 39.098652, 39.104804, 39.101815, 39.122229, 39.139277, 39.143641, 39.161613, 39.149699, 39.154745, 39.166905, 39.165843, 39.177298, 39.196782, 39.220221, 39.258106, 39.274946};
            Double[] riyadhlat = {24.733280606,24.688743945792,24.668771807659, 24.597682523738,24.53135382168,24.486581955043,24.46891208184,24.41819007828,24.38070082634,24.407393464645,24.4480078553535,24.6184255966,24.8462276394473,25.06295352274,25.251675478420,25.176740472162983,25.106097289000,24.9700783387910,24.8279364426};
            Double[] riyadhlng = {46.518958769738,46.51775714,46.474208794534,46.458525247871,46.48663647428,46.58058788627,46.660335659980,46.70960448682,46.7511797323,46.9029525294899,47.06905771046,47.0388469845056,47.0289761200,46.915115006268,46.76584135740,46.52906063944,46.412407681345,46.341817639768,46.37382209300};

        points = new ArrayList<LatLng>();
        if(currentbranchname.equalsIgnoreCase(StaticInfo.MECCA)) {
            ismeccaclick = true;
            ismedinaclick = false;
            linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            for (int i = 0; i < meccalat.length; i++) {
                points.add(new LatLng(meccalat[i], meccalng[i]));
            }
            // Store a data object with the polygon,
            // used here to indicate an arbitrary type.
            polygon = mMap.addPolygon(new PolygonOptions().addAll(points).strokeWidth(1).strokeColor(Color.MAGENTA).fillColor(Color.TRANSPARENT));
            if(sessionManager.getccity().equalsIgnoreCase(StaticInfo.MECCA) || issearch == true){
                clat = currentlatitue;
                clong = currentlongitute;
            }
            else {
                clat = meccakabbalat;
                clong = meccakabbalong;
            }
            marker = mMap.addMarker(new MarkerOptions().position(new LatLng(meccakabbalat, meccakabbalong)).icon(BitmapDescriptorFactory.fromResource(R.drawable.mecca_kabba)));
            //      Main  Circle
            mCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(meccakabbalat, meccakabbalong))
                    .strokeColor(Color.GREEN)
                    .strokeWidth(1)
                    .radius(25000)
                    .fillColor(Color.TRANSPARENT));
//      inner Circle
            iCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(meccakabbalat, meccakabbalong))
                    .strokeColor(Color.RED)
                    .strokeWidth(1)
                    .radius(1000)
                    .fillColor(Color.TRANSPARENT));
//            getaddressrequest(meccakabbalat, meccakabbalong);
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("latitude", meccakabbalat);
                requestbody.put("longitude", meccakabbalong);
                getaddressrequest2(requestbody);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        else if(currentbranchname.equalsIgnoreCase(StaticInfo.MEDINA)){
            ismedinaclick = true;
            ismeccaclick = false;
            linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            for (int i = 0; i < medinalat.length; i++) {
                points.add(new LatLng(medinalat[i], medinalng[i]));
            }
            // Store a data object with the polygon,
            // used here to indicate an arbitrary type.
            polygon = mMap.addPolygon(new PolygonOptions().addAll(points).strokeColor(Color.TRANSPARENT).fillColor(Color.TRANSPARENT));
            if(sessionManager.getccity().equalsIgnoreCase(StaticInfo.MEDINA) || issearch == true){
                clat = currentlatitue;
                clong = currentlongitute;
            }
            else {
                clong = medinakabbalong;
                clat = medinakabbalat;
            }
            marker= mMap.addMarker(new MarkerOptions().position(new LatLng(medinakabbalat, medinakabbalong)).icon(BitmapDescriptorFactory.fromResource(R.drawable.medina_kabba)));
   //             Main  Circle
            mCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(medinakabbalat, medinakabbalong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(25000)
                    .fillColor(Color.TRANSPARENT));
             // inner Circle
            iCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(medinakabbalat, medinakabbalong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(1000)
                    .fillColor(Color.TRANSPARENT));
//            getaddressrequest(medinakabbalat, medinakabbalong);
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("latitude", medinakabbalat);
                requestbody.put("longitude", medinakabbalong);
                getaddressrequest2(requestbody);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(currentbranchname.equalsIgnoreCase(StaticInfo.RIYADH)){
            ismeccaclick = false;
            ismedinaclick = false;
            linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            for (int i = 0; i < riyadhlat.length; i++) {
                points.add(new LatLng(riyadhlat[i], riyadhlng[i]));
            }
            // Store a data object with the polygon,
            // used here to indicate an arbitrary type.
            polygon = mMap.addPolygon(new PolygonOptions().addAll(points).strokeColor(Color.TRANSPARENT).fillColor(Color.TRANSPARENT));
            if(sessionManager.getccity().equalsIgnoreCase(StaticInfo.RIYADH) || issearch == true){
                clat = currentlatitue;
                clong = currentlongitute;
            }
            else {
                clat = riyadhkabbalat;
                clong = riyadhkabbalong;
            }
             //   Main  Circle
            jCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(riyadhkabbalat, riyadhkabbalong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(45000)
                    .fillColor(Color.TRANSPARENT));
             //   inner Circle
            iCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(riyadhkabbalat, riyadhkabbalong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(1000)
                    .fillColor(Color.TRANSPARENT));
//            getaddressrequest(riyadhkabbalat, riyadhkabbalong);
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("latitude", riyadhkabbalat);
                requestbody.put("longitude", riyadhkabbalong);
                getaddressrequest2(requestbody);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(currentbranchname.equalsIgnoreCase(StaticInfo.JEDDAH)){
            ismeccaclick = false;
            ismedinaclick = false;
            linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            for (int i = 0; i < jeddahlat.length; i++) {
                points.add(new LatLng(jeddahlat[i], jeddahlng[i]));
            }
            // Store a data object with the polygon,
            // used here to indicate an arbitrary type.
            polygon = mMap.addPolygon(new PolygonOptions().addAll(points).strokeColor(Color.TRANSPARENT).fillColor(Color.TRANSPARENT));
            if(sessionManager.getccity().equalsIgnoreCase(StaticInfo.JEDDAH) || issearch == true){
                clat = currentlatitue;
                clong = currentlongitute;
            }
            else {
                clat = jeddahkabbalat;
                clong = jeddahkabbalong;
            }
             //   Main  Circle
            jCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(jeddahkabbalat, jeddahkabbalong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(45000)
                    .fillColor(Color.TRANSPARENT));
             //   inner Circle
            iCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(jeddahkabbalat, jeddahkabbalong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(1000)
                    .fillColor(Color.TRANSPARENT));
//            getaddressrequest(jeddahkabbalat, jeddahkabbalong);
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("latitude", jeddahkabbalat);
                requestbody.put("longitude", jeddahkabbalong);
                getaddressrequest2(requestbody);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            ismeccaclick = false;
            ismedinaclick = false;
            linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
            linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            clat = currentlatitue;
            clong = currentlongitute;
            //   Main  Circle
            jCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(clat, clong))
                    .strokeColor(Color.GREEN)
                    .strokeWidth(1)
                    .radius(5000)
                    .fillColor(Color.TRANSPARENT));
            //   inner Circle
            iCircle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(clat, clong))
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(1)
                    .radius(1000)
                    .fillColor(Color.TRANSPARENT));
//            getaddressrequest(clat, clong);
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("latitude", clat);
                requestbody.put("longitude", clong);
                getaddressrequest2(requestbody);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        LatLng kabbaLatLng = new LatLng(clat, clong);
        if(issearch == true){
            issearch = false;
            mMap.moveCamera(CameraUpdateFactory.newLatLng(kabbaLatLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(kabbaLatLng, 18F));
        }
        else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(kabbaLatLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(kabbaLatLng, 15F));
        }

    }

    private void getaddressrequest(double latitude, double longitude) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&type=mosque" );
        googlePlacesUrl.append("&rankby=distance" );
        googlePlacesUrl.append("&sensor=true");
       // googlePlacesUrl.append("&language=ar");
        googlePlacesUrl.append("&key=AIzaSyAv1vK2PSA9AYlDp3-gZuU1wfoMptiapjA");
        Log.i( " req", googlePlacesUrl.toString());
        RequestQueue queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                googlePlacesUrl.toString(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i( " responswwww", response.toString());
                        parseLocationResult(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
            }
        });
        queue.add(jsonObjReq);
    }


    private void getaddressrequest2(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request123",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.mapdata, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("notification", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        Log.d("dvdxv",""+obj.getJSONArray("data"));
//                        JSONObject jsonObject=obj.getJSONObject("data");
                        JSONArray data1 = obj.getJSONArray("data");
                        Log.d("cfghjkkjhgf",""+data1);
                        parseLocationResult(obj);
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    private void parseLocationResult(JSONObject result) {
        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        String id, place_id, placeName = null, reference, icon, vicinity = null;
        double latitude, longitude;
        try {
            JSONArray jsonArray = result.getJSONArray("data");
//            if (result.getString(STATUS).equalsIgnoreCase("OK")) {
                float[] distance = new float[2];
                float[] mdistance = new float[2];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject place = jsonArray.getJSONObject(i);
                    latitude = Double.parseDouble(place.getString("latitude"));
                    longitude = Double.parseDouble(place.getString("longitude"));
                    placeName=place.getString("placeEng");
                    final LatLng latLng = new LatLng(latitude, longitude);
                    Log.d("incoming lat", "" + latLng);
                    if (!hashSetlatlang.contains(latLng)) {
                        Log.d("loop lat", "" + latLng);
                        if((currentbranchname.equalsIgnoreCase(StaticInfo.MECCA)) || (currentbranchname.equalsIgnoreCase(StaticInfo.MEDINA))){
                            if (mdistance[0] < mCircle.getRadius()) {
                                hashSetlatlang.add(latLng);
                                Log.d("mecca_circle", "Inside mecca circle");
                                PlacesItem placesItem = new PlacesItem(placeName, latitude, longitude);
                                placesarrayList.add(placesItem);
                                markerOptions.position(latLng);
                                contains1 = PolyUtil.containsLocation(latLng.latitude, latLng.longitude, points, true);
                                System.out.println("contains1: " + contains1);
                                if (contains1 == true) {
                                    Log.d("circle", " Inside circle");
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_green_new));
                                    }
                                } else if (contains1 == false) {
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_brown_new));
                                    }
                                    Log.d("circle", " Outside circle");
                                }
                                // markerOptions.title(placeName + " : " + vicinity);
                                markerOptions.title(placeName);
                                MarkerOptions b = markerOptions.position(latLng);
                                Marker n = mMap.addMarker(b);
                                dropPinEffect(n);
                                //animateMarker(n,n.getPosition());
                                //bounceMarker(n);
                                // mMap.addMarker(markerOptions);
                                markers.add(markerOptions);
                            } else {
                                Log.d("mecca_circle", "outside mecca circle");
                            }
                        }
                        else if(currentbranchname.equalsIgnoreCase(StaticInfo.JEDDAH)){
                            if (mdistance[0] < jCircle.getRadius()) {
                                hashSetlatlang.add(latLng);
                                Log.d("jeddah_circle", "Inside jeddah circle");
                                PlacesItem placesItem = new PlacesItem(placeName, latitude, longitude);
                                placesarrayList.add(placesItem);
                                markerOptions.position(latLng);
                                contains1 = PolyUtil.containsLocation(latLng.latitude, latLng.longitude, points, true);
                                System.out.println("contains1: " + contains1);
                                if (contains1 == true) {
                                    Log.d("circle", " Inside circle");
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.visible(true);
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_brown_new));
                                    }
                                }
                                else if (contains1 == false) {
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.visible(false);
                                        // markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.black_marker));
                                        Log.d("circle", " Outside circle");
                                    }
                                }

                                // markerOptions.title(placeName + " : " + vicinity);
                                markerOptions.title(placeName);
                                MarkerOptions b = markerOptions.position(latLng);
                                Marker n = mMap.addMarker(b);
                                dropPinEffect(n);
                                //animateMarker(n,n.getPosition());
                                //bounceMarker(n);
                                // mMap.addMarker(markerOptions);
                                markers.add(markerOptions);
                            } else {
                                Log.d("jeddah_circle", "outside jeddah circle");
                            }
                        }
                        else if(currentbranchname.equalsIgnoreCase(StaticInfo.RIYADH)){
                            if (mdistance[0] < jCircle.getRadius()) {
                                hashSetlatlang.add(latLng);
                                Log.d("riyadh", "Inside riyadh circle");
                                PlacesItem placesItem = new PlacesItem(placeName, latitude, longitude);
                                placesarrayList.add(placesItem);
                                markerOptions.position(latLng);
                                contains1 = PolyUtil.containsLocation(latLng.latitude, latLng.longitude, points, true);
                                System.out.println("contains1: " + contains1);
                                if (contains1 == true) {
                                    Log.d("circle", " Inside circle");
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.visible(true);
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_brown_new));
                                    }
                                }
                                else if (contains1 == false) {
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.visible(false);
                                        // markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.black_marker));
                                        Log.d("circle", " Outside circle");
                                    }
                                }

                                // markerOptions.title(placeName + " : " + vicinity);
                                markerOptions.title(placeName);
                                MarkerOptions b = markerOptions.position(latLng);
                                Marker n = mMap.addMarker(b);
                                dropPinEffect(n);
                                //animateMarker(n,n.getPosition());
                                //bounceMarker(n);
                                // mMap.addMarker(markerOptions);
                                markers.add(markerOptions);
                            } else {
                                Log.d("riyadh circle", "outside riyadh circle");
                            }
                        }
                        else {
                            if (mdistance[0] < jCircle.getRadius()) {
                                hashSetlatlang.add(latLng);
                                Log.d("circle", "Inside circle");
                                PlacesItem placesItem = new PlacesItem(placeName, latitude, longitude);
                                placesarrayList.add(placesItem);
                                markerOptions.position(latLng);
                               // contains1 = PolyUtil.containsLocation(latLng.latitude, latLng.longitude, points, true);
                               // System.out.println("contains1: " + contains1);
                                    MarkerOptions a = markerOptions.position(latLng);
                                    Marker m = mMap.addMarker(a);
                                    m.setVisible(false);
                                    String res = checkmarker(m);
                                    if(!res.equals("")){
                                        IconGenerator iconGenerator = new IconGenerator(getActivity());
                                        iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
                                        View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
                                        titleText=inflatedView.findViewById(R.id.titletext1);
                                        Log.d("erfgv3",""+m.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                                        titleText.setText(""+res);
                                        iconGenerator.setContentView(inflatedView);
                                        //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                                    }
                                    else {
                                        markerOptions.visible(true);
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_brown_new));
                                    }
                                // markerOptions.title(placeName + " : " + vicinity);
                                markerOptions.title(placeName);
                                MarkerOptions b = markerOptions.position(latLng);
                                Marker n = mMap.addMarker(b);
                                dropPinEffect(n);
                                //animateMarker(n,n.getPosition());
                                //bounceMarker(n);
                                // mMap.addMarker(markerOptions);
                                markers.add(markerOptions);
                            }
                            else {
                                Log.d("circle", "outside main circle");
                            }
                        }
                    }
                }
//            }
//            else if (result.getString(STATUS).equalsIgnoreCase("ZERO_RESULTS")) {
//               /* Toast.makeText(getBaseContext(), "No mosque found !!!",
//                       Toast.LENGTH_LONG).show();*/
//            }

        } catch (JSONException e) {
            e.printStackTrace();
            // Log.e(TAG, "parseLocationResult: Error=" + e.getMessage());
        }
    }

    @Override
    public void onCameraIdle() {
        if(!currentbranchname.equals("")){
            LatLng midLatLng = mMap.getCameraPosition().target;
        if (!hashSetlatlang.isEmpty()) {
            Log.d("midlatIdlestateassdd", "" +hashSetlatlang);
            if(Arrays.asList(hashSetlatlang).contains(midLatLng)){
                Log.d("midlatIdlestate", "" +midLatLng);
            }
            else {
                float[] mdistance = new float[2];
                if ((currentbranchname.equalsIgnoreCase(StaticInfo.MECCA))) {
                    Location.distanceBetween(midLatLng.latitude,
                            midLatLng.longitude, mCircle.getCenter().latitude,
                            mCircle.getCenter().longitude, mdistance);
                    if(mdistance[0] < mCircle.getRadius()) {
                        Log.d("mdistance&radios1", "" + mdistance + " & " + mCircle.getRadius());
                        Log.d("circle", " Inside main circle");
//                        getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("latitude", midLatLng.latitude);
                            requestbody.put("longitude", midLatLng.longitude);
                            getaddressrequest2(requestbody);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(currentbranchname.equalsIgnoreCase(StaticInfo.MEDINA)){
                    Location.distanceBetween(midLatLng.latitude,
                            midLatLng.longitude, mCircle.getCenter().latitude,
                            mCircle.getCenter().longitude, mdistance);
                    if(mdistance[0] < mCircle.getRadius()) {
                        Log.d("mdistance&radios1", "" + mdistance + " & " + mCircle.getRadius());
                        Log.d("circle", " Inside main circle");
//                        getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("latitude", midLatLng.latitude);
                            requestbody.put("longitude", midLatLng.longitude);
                            getaddressrequest2(requestbody);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (currentbranchname.equalsIgnoreCase(StaticInfo.JEDDAH)) {
                    Log.d("mdistance&radios2",""+mdistance+" & "+jCircle.getRadius());
                    Log.d("circle", " Outside main circle");
                    Location.distanceBetween(midLatLng.latitude,
                            midLatLng.longitude, jCircle.getCenter().latitude,
                            jCircle.getCenter().longitude, mdistance);
                    if (mdistance[0] < jCircle.getRadius()) {
                       /* if(contains2 == true){
                            getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        }*/
//                        getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("latitude", midLatLng.latitude);
                            requestbody.put("longitude", midLatLng.longitude);
                            getaddressrequest2(requestbody);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (currentbranchname.equalsIgnoreCase(StaticInfo.RIYADH)) {
                    Log.d("mdistance&radios2",""+mdistance+" & "+jCircle.getRadius());
                    Log.d("circle", " Outside main circle");
                    Location.distanceBetween(midLatLng.latitude,
                            midLatLng.longitude, jCircle.getCenter().latitude,
                            jCircle.getCenter().longitude, mdistance);
                    if (mdistance[0] < jCircle.getRadius()) {
                       /* if(contains2 == true){
                            getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        }*/
//                        getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("latitude", midLatLng.latitude);
                            requestbody.put("longitude", midLatLng.longitude);
                            getaddressrequest2(requestbody);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    Log.d("mdistance&radios2",""+mdistance+" & "+jCircle.getRadius());
                    Log.d("circle", " Outside main circle");
                    Location.distanceBetween(midLatLng.latitude,
                            midLatLng.longitude, jCircle.getCenter().latitude,
                            jCircle.getCenter().longitude, mdistance);
                    if (mdistance[0] < jCircle.getRadius()) {
                       /* if(contains2 == true){
                            getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        }*/
//                        getaddressrequest(midLatLng.latitude, midLatLng.longitude);
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("latitude", midLatLng.latitude);
                            requestbody.put("longitude", midLatLng.longitude);
                            getaddressrequest2(requestbody);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (checkandremovemarker(marker) != true) {
            if(possiblemarkerselectcount > 0){
                int selectedmarkercount = 0;
                for (int i=0;i<overallmarkersLatLng.size();i++){
                    if(overallmarkersbranchname.get(i).equalsIgnoreCase(currentbranchname)){
                        selectedmarkercount++;
                    }
                }
                if(selectedmarkercount < possiblemarkerselectcount){
                    if (overallmarkersLatLng.size() == 0) {
                        addmarker(marker);
                    }
                    else {
                        addmarker(marker);
                    }
                }
                else {
                    sessionManager.snackbarToast(getString(R.string.stocknotavailable)+" "+getString(R.string.youcannotselectmoremosquesin)+" "+currentbranchname,view);
                }
            }
            else {
                sessionManager.snackbarToast(getString(R.string.stocknotavailable),view);
            }
        }
        setmarkercount();
        return true;
    }

    public void setmarkercount(){
        int meccacount = 0;
        int medinacount = 0;
        deletedbranch = new ArrayList<>();
        for(int i=0;i<overallmarkersLatLng.size();i++){
            if(overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.MECCA)){
                meccacount++;
            }
            if(overallmarkersbranchname.get(i).equalsIgnoreCase(StaticInfo.MEDINA)){
                medinacount++;
            }
        }

        if(meccacount == 0){
            mecca_count.setText("");
        }
        else {
            mecca_count.setText(""+meccacount);
        }
        if(medinacount == 0){
            medina_count.setText("");
        }
        else {
            medina_count.setText(""+medinacount);
        }

        if(overallmarkersLatLng.size()>0){
            selectedmosquecount.setVisibility(View.VISIBLE);
            selectedmosquecount.setText(getString(R.string.selectedmosques)+" - "+overallmarkersLatLng.size());
        }
        else {
            selectedmosquecount.setVisibility(View.GONE);
        }
    }

    private void addmarker(Marker marker) {
        LatLng latLngmedina = new LatLng(medinakabbalat,medinakabbalong);
        LatLng latLngmecca = new LatLng(meccakabbalat,meccakabbalong);
        if(marker.getPosition().equals(latLngmedina) || marker.getPosition().equals(latLngmecca)){
            // marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.medina_kabba));
        }
        else {
            overallmarkersLatLng.add(marker.getPosition());
            overallmarkerstitle.add(marker.getTitle());
            overallmarkersbranchid.add(currentbranchid);
            overallmarkersbranchname.add(currentbranchname);
            overallmarkersbranchstock.add(currentbranchstock);
            IconGenerator iconGenerator = new IconGenerator(getContext());
            iconGenerator.setBackground(getActivity().getDrawable(R.drawable.custom_map_marker_bg));
            View inflatedView = View.inflate(getContext(), R.layout.custom_marker, null);
            titleText = inflatedView.findViewById(R.id.titletext1);
            titleText.setText(marker.getTitle());
            iconGenerator.setContentView(inflatedView);
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
            //startDropMarkerAnimation(marker);
        }
        /*markclick = true;*/
    }

    private boolean checkandremovemarker(Marker marker) {
        boolean result = false;
        for (int i = 0; i< overallmarkersLatLng.size(); i++){
            Log.d("bnmfnf",""+ overallmarkersLatLng.get(i));
            Log.d("fvgtcb","loop"+ overallmarkersLatLng.get(i)+"="+marker.getPosition());
            if(overallmarkersLatLng.get(i).equals(marker.getPosition())){
                overallmarkersLatLng.remove(marker.getPosition());
                overallmarkerstitle.remove(marker.getTitle());
                overallmarkersbranchid.remove(i);
                overallmarkersbranchname.remove(i);
                overallmarkersbranchstock.remove(i);
                //marker.remove();
                boolean contains2 = PolyUtil.containsLocation(marker.getPosition().latitude, marker.getPosition().longitude, points, true);
                if (contains2 == true) {
                    if(currentbranchname.equalsIgnoreCase(StaticInfo.MECCA) || currentbranchname.equalsIgnoreCase(StaticInfo.MEDINA)){
                        //startDropMarkerAnimation(marker);
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_green_new));
                    }
                    else {
                        //startDropMarkerAnimation(marker);
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_brown_new));
                    }
                }
                else if (contains2 == false ){
                    //startDropMarkerAnimation(marker);
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_brown_new));
                }
                Log.d("fvgb","marker remove");
                result = true;
                break;
            }
            else {
                result = false;
            }
        }
        return result;
    }

    private String checkmarker(Marker marker){
        String result = "";
        for (int i=0;i<overallmarkersLatLng.size();i++){
            if(overallmarkersLatLng.get(i).equals(marker.getPosition())){
                Log.e("ewrdth",""+overallmarkerstitle.get(i)+"==="+marker.getPosition()+""+marker.getTitle());
                Log.d("drt",""+i+"-"+result);
                Log.d("fgbhnj",""+marker.getTitle());
               // marker_title = overallmarkerstitle.get(i);
               /* IconGenerator iconGenerator = new IconGenerator(this);
                iconGenerator.setBackground(this.getDrawable(R.drawable.bg_custom_marker));
                View inflatedView = View.inflate(this, R.layout.custom_marker, null);
                titleText=inflatedView.findViewById(R.id.titletext1);
                Log.d("erfgv",""+marker.getTitle()+" and "+markerOptions.getTitle()+""+markerOptions.getPosition());
                titleText.setText(markerstitle.get(i));
                iconGenerator.setContentView(inflatedView);
                //m.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()));*/
                return overallmarkerstitle.get(i);
            }
            else {
                result = "";
            }
        }
        return result;
    }

    private void dropPinEffect(final Marker marker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500;
        final Interpolator interpolator = new BounceInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0) {
                    // Post again 15ms later.
                    handler.postDelayed(this, 15);
                } else {
                    // marker.showInfoWindow();

                }
            }
        });
    }

    private void startDropMarkerAnimation(final Marker marker) {
        final LatLng target = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point targetPoint = proj.toScreenLocation(target);
        final long duration = (long) (200 + (targetPoint.y * 0.6));
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        startPoint.y = 0;
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new LinearOutSlowInInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * target.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * target.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later == 60 frames per second
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public void animateMarker(final Marker marker, final LatLng toPosition) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        startPoint.offset(0, -100);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                marker.setPosition(toPosition);
                if (t < 1.0) {
                    // Post again 16ms later.
                    Log.d("dcvbn","check");
                    handler.postDelayed(this, 16);
                } /*else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }*/
            }
        });
    }

    private void bounceMarker(final Marker marker){
        final Handler handler = new Handler();
        final long startTime = SystemClock.uptimeMillis();
        final long duration = 1500;
        Projection proj = mMap.getProjection();
        final LatLng markerLatLng = marker.getPosition();
        Point startPoint = proj.toScreenLocation(markerLatLng);
        startPoint.offset(0, -100);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new BounceInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - startTime;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * markerLatLng.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * markerLatLng.latitude + (1 - t) * startLatLng.latitude;
               /* MarkerOptions a = new MarkerOptions().position(new LatLng(lat,lng));
                Marker m = mMap.addMarker(a);
                m.setPosition(new LatLng(lat,lng));*/
                //double lng = markerLatLng.longitude;
                //double lat = markerLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                marker.setPosition(markerLatLng);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public void onPolygonClick(Polygon polygon) {
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onCameraMoveStarted(int i) {
    }

    @Override
    public void onCameraMoveCanceled() {
    }

    @Override
    public void onCameraMove() {
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if(sessionManager.isGpsEnabled() == false || sessionManager.isPermissionsEnabled() == false){
            sessionManager.gpsEnableRequest();
            sessionManager.permissionsEnableRequest();
        }
        else {
            if(overallmarkersLatLng.size()>0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.selectedmasjidwillnolongetbeavailableifchangedlocation));
                builder.setMessage(getString(R.string.areyousuretocontinue));
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       /* overallmarkersLatLng.clear();
                        overallmarkerstitle.clear();
                        markersplace.clear();
                        jeddahmarkersLatLng.clear();
                        meccamarkersLatLng.clear();
                        medinamarkersLatLng.clear();*/
                        mecca_count.setText("" + meccamarkersLatLng.size());
                        medina_count.setText("" + medinamarkersLatLng.size());
                        linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
                        linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                        linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
                        linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                        getDeviceLocation1();
                    }
                });
                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                /*((Button)alert.findViewById(android.R.id.button1)).setBackgroundColor(getResources().getColor(R.color.yellow_update));
                ((Button)alert.findViewById(android.R.id.button1)).setTextColor(Color.BLACK);
                ((Button)alert.findViewById(android.R.id.button2)).setBackgroundColor(getResources().getColor(R.color.yellow_update));
                ((Button)alert.findViewById(android.R.id.button2)).setTextColor(Color.BLACK);*/
            }
            else {
                /*overallmarkersLatLng.clear();
                overallmarkerstitle.clear();
                markersplace.clear();
                jeddahmarkersLatLng.clear();
                meccamarkersLatLng.clear();
                medinamarkersLatLng.clear();*/
                mecca_count.setText("" + meccamarkersLatLng.size());
                medina_count.setText("" + medinamarkersLatLng.size());
                linear_mecca.setBackgroundResource(R.drawable.bg_border_5dp_white);
                linear_mecca.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                linear_medina.setBackgroundResource(R.drawable.bg_border_5dp_white);
                linear_medina.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                btnMecca.setBackgroundColor(getResources().getColor(R.color.white));
                btnMedina.setBackgroundColor(getResources().getColor(R.color.white));
                getDeviceLocation1();
            }
        }
        return true;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d("ONATTACH13","worked");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("ONSTART13","worked");
    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
        Log.d("ONRESUME13","worked");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("ONPAUSE13","worked");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("ONSTOP13","worked");
    }
}