package com.lia.yello.roha.model;
public class WalletDataModel {
    String transaction_id,orderfor,iswish,operation,order_id,date;
    int id;
    double amount;
    public WalletDataModel(int id,String order_id,String orderfor,String iswish,String operation,String
                           amount,String transaction_id,String date) {

        this.id = id;
        this.order_id = order_id;
        this.orderfor = orderfor;
        this.iswish = iswish;
        this.operation = operation;
        if(amount.equalsIgnoreCase("")){
            this.amount = 0.0;
        }
        else {
            this.amount = Double.parseDouble(amount);
        }
        this.transaction_id = transaction_id;
        this.date = date;
    }

    public String getOrderfor(){
        return orderfor;
    }

    public String getIswish(){
        return iswish;
    }

    public int getId() {
        return id;
    }

    public String getOrder_id() {
        return order_id;
    }


    public String getOperation() {
        return operation;
    }

    public double getAmount(){
        return amount;
    }
    public String getTransaction_id(){
        return transaction_id;
    }
    public String getDate(){
        return date;
    }

}