package com.lia.yello.roha.Fragment;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.FaqAdapter;
import com.lia.yello.roha.model.FaqDataModel;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class HelpFragment extends Fragment {


  View view;
 public SessionManager sessionManager;
    public RecyclerView recyclerView;
    public ArrayList<FaqDataModel> data;
    private static RecyclerView.Adapter adapter;
    Button faq;
    boolean iswater = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_help, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
        faq=(Button) view.findViewById(R.id.btn_faq);
        recyclerView=(RecyclerView)view.findViewById(R.id.faq_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.GONE);
        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerView.getVisibility()==View.VISIBLE){
                    recyclerView.setVisibility(View.GONE);

                }else {
                    recyclerView.setVisibility(View.VISIBLE);

                }
            }
        });
        faqrequestJSON();


        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.helpandsupport);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.helpandsupport);
        }
        return  view;
    }

    private void faqrequestJSON( ) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, StaticInfo.list_faq, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("faqresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        Log.d("dvdxv",""+obj.getJSONObject("data"));
                        JSONObject jsonObject=obj.getJSONObject("data");
                        JSONArray data1 = jsonObject.getJSONArray("FAQ_data");
                        Log.d("cfghjkkjhgf",""+data1);
                        data = new ArrayList<FaqDataModel>();
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject1 = data1.getJSONObject(i);
                            FaqDataModel datamodel = new FaqDataModel(jsonObject1.getInt("id"),jsonObject1.getString("question"),jsonObject1.getString("answer"));
                            data.add(datamodel);

                        }
                        adapter = new FaqAdapter(getContext(), data, HelpFragment.this);
                        recyclerView.setAdapter(adapter);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater") && iswater==true){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }
        }
    }

}