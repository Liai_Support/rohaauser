package com.lia.yello.roha.Fragment;

import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.lia.yello.roha.R;
import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class WateOrderPhotorFragment extends Fragment implements View.OnClickListener{

    View view;
    ImageView img_orderplaced,img_outfordelivery,img_pendingforattaesting,img_delivered,img1,img2,img3,img4,singleimg;
    Bundle args = new Bundle();

    String status,imgS1,imgS2,imgS3,imgS4;
    public SessionManager sessionManager;
    TextView orderid;
    Button rohahome,rohawater;
    int donationid;
    public int iswish;
    String orderstatus;
    Double rating;
    String comment,suggestions;
    TextView delivereddateBlack,deliveredtimeBlack;
    EditText ratetxt;
    String delivered_date,delivered_time;
    RelativeLayout four_images,single_images;
    LinearLayout reviewlayout;
    CardView blackcard2;
    RatingBar ratingBar;
    Button submit;
    LinearLayout linearLayout;
    boolean iswater = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wate_order_photor, container, false);
        sessionManager = new SessionManager(getActivity(),getContext(),view);

        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
          /*  OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }));
            }*/
           /* ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });*/
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
        }
        rohahome = (Button) view.findViewById(R.id.txt_roha_home);
        rohawater = (Button) view.findViewById(R.id.txt_roha_water);
        orderid = (TextView) view.findViewById(R.id.orderid);

        ratetxt = (EditText) view.findViewById(R.id.ratetxt);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        blackcard2 = (CardView) view.findViewById(R.id.blackcard);
        delivereddateBlack = (TextView) view.findViewById(R.id.deliverydateB);
        deliveredtimeBlack = (TextView) view.findViewById(R.id.timestampdelivered);
        four_images=(RelativeLayout)view.findViewById(R.id.four_images);
        single_images=(RelativeLayout)view.findViewById(R.id.single_img);
        reviewlayout = (LinearLayout) view.findViewById(R.id.reviewlayout);
        linearLayout = (LinearLayout) view.findViewById(R.id.lin_top);
        linearLayout.setVisibility(View.GONE);

        img1 = (ImageView) view.findViewById(R.id.orderimg1);
        img2 = (ImageView) view.findViewById(R.id.orderimg2);
        img3 = (ImageView) view.findViewById(R.id.orderimg3);
        img4 = (ImageView) view.findViewById(R.id.orderimg4);
        singleimg = (ImageView) view.findViewById(R.id.singleorderimg);
        submit = (Button) view.findViewById(R.id.submit);

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        singleimg.setOnClickListener(this);
        submit.setOnClickListener(this);

        img_orderplaced = (ImageView) view.findViewById(R.id.img_orderplaced);
        img_outfordelivery = (ImageView) view.findViewById(R.id.img_outfordelivery);
        img_pendingforattaesting = (ImageView) view.findViewById(R.id.img_pendingforattaesting);
        img_delivered = (ImageView) view.findViewById(R.id.img_delivered);

        args = getArguments();

        donationid = args.getInt("orderid",0);
        iswish = args.getInt("iswish",0);
        status = args.getString("status");
        orderstatus = args.getString("orderstatus");
        delivered_time = args.getString("delivered_time");
        delivered_date = args.getString("delivered_date");
        imgS1 = args.getString("image_path1");
        imgS2 = args.getString("image_path2");
        imgS3 = args.getString("image_path3");
        imgS4 = args.getString("image_path4");
        rating = args.getDouble("rating");
        suggestions = args.getString("suggestion");
        comment = args.getString("comment");

        delivereddateBlack.setText(""+delivered_date);
        deliveredtimeBlack.setText(""+delivered_time);
        rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
        rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
        rohawater.setTextColor(Color.BLACK);

        if(iswish == 0){
            reviewlayout.setVisibility(View.GONE);
            blackcard2.setVisibility(View.GONE);
            orderid.setText(""+getString(R.string.orderid)+" : "+ StaticInfo.order_prefix+donationid);
            four_images.setVisibility(View.VISIBLE);
            single_images.setVisibility(View.GONE);
            if(status.equalsIgnoreCase("pending") || status.equalsIgnoreCase("hold")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
            }
            else if(status.equalsIgnoreCase("assigned") || status.equalsIgnoreCase("out for delivery")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
            }
            else if(status.equalsIgnoreCase("completed")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                img_pendingforattaesting.setImageDrawable(getResources().getDrawable(R.drawable.pendingforattesting_yellow));
                img_delivered.setImageDrawable(getResources().getDrawable(R.drawable.delivered_yellow));
                blackcard2.setVisibility(View.VISIBLE);
                showimage();
            }
        }
        else {
            reviewlayout.setVisibility(View.GONE);
            blackcard2.setVisibility(View.GONE);
            orderid.setText(""+getString(R.string.orderid)+" : "+StaticInfo.order_prefix+"M"+donationid);
            four_images.setVisibility(View.VISIBLE);
            single_images.setVisibility(View.GONE);
            if(status.equalsIgnoreCase("pending") || status.equalsIgnoreCase("hold")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
            }
            else if(status.equalsIgnoreCase("assigned") || status.equalsIgnoreCase("out for delivery")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
            }
            else if(status.equalsIgnoreCase("completed")){
                img_orderplaced.setImageDrawable(getResources().getDrawable(R.drawable.orderplace_yellow));
                img_outfordelivery.setImageDrawable(getResources().getDrawable(R.drawable.outfordelivery_yellow));
                img_pendingforattaesting.setImageDrawable(getResources().getDrawable(R.drawable.pendingforattesting_yellow));
                img_delivered.setImageDrawable(getResources().getDrawable(R.drawable.delivered_yellow));
                blackcard2.setVisibility(View.VISIBLE);
                showimage();
            }
        }

        if(orderstatus.equalsIgnoreCase("completed")){
            if(rating == null || rating == 0){
                reviewlayout.setVisibility(View.VISIBLE);
            }
            else {
                reviewlayout.setVisibility(View.GONE);
            }
        }
        else {
            reviewlayout.setVisibility(View.GONE);
        }
        return view;
    }

    private void showimage() {
        if(!imgS1.isEmpty() && !imgS2.isEmpty() && !imgS3.isEmpty() && !imgS4.isEmpty()){
            Picasso.get().load(imgS1).into(img1);
            Picasso.get().load(imgS2).into(img2);
            Picasso.get().load(imgS3).into(img3);
            Picasso.get().load(imgS4).into(img4);
        }

      /*  if(!imgS1.isEmpty()){
            Picasso.get().load(imgS1).into(singleimg);
        }*/
    }

    private void showimageonly(String img){
        final Dialog nagDialog = new Dialog(getContext());
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(true);
        nagDialog.setContentView(R.layout.popup_preview_image);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(nagDialog.getWindow().getAttributes());
        layoutParams.width =  WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height =  WindowManager.LayoutParams.FIRST_SUB_WINDOW;
        //layoutParams.height = 700;
        //nagDialog.getWindow().setBackgroundDrawable(null);
        nagDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)nagDialog.findViewById(R.id.btnIvClose);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview.setBackgroundDrawable(dd);
        if(!img.isEmpty()) {
            Glide.with(this).load(img).placeholder(R.drawable.defaultimg).into(ivPreview);
            //Picasso.get().load(dataSet.get(listPosition).getImgpath1()).placeholder(R.drawable.defaultimg).error(R.mipmap.ic_launcher_round).into(ivPreview);
        }
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    @Override
    public void onClick(View view) {
        if(view == submit){
            if (ratetxt.getText().toString().equals("") || ratingBar.getRating()==0){
                sessionManager.snackbarToast(getString(R.string.enterallfields),view);
            }
            else{
                float rating = ratingBar.getRating();
                String review = ratetxt.getText().toString();
                String suggestion = ratetxt.getText().toString();
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("order_id",donationid);
                    requestbody.put("cust_id", sessionManager.getUserId());
                    requestbody.put("rate", rating);
                    requestbody.put("comment", review);
                    requestbody.put("suggestion", suggestion);
                    requestbody.put("type", "water");
                    reviewJSON(requestbody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        else if(view == img1){
            showimageonly(imgS1);
        }
        else if(view == img2){
            showimageonly(imgS2);
        }
        else if(view == img3){
            showimageonly(imgS3);
        }
        else if(view == img4){
            showimageonly(imgS4);
        }
        else if(view == singleimg){
            showimageonly(imgS1);
        }
    }

    private void reviewJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request123",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.review, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("reviewresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false) {
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg, view);
                    }
                    else {
                        String msg = obj.getString("message");
                        sessionManager.snackbarToast(msg,view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

}