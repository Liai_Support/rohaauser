package com.lia.yello.roha.model;

public class BannerDataModel {
    int id;
    String oimage,description;

    public BannerDataModel(int id, String description,String image) {

        this.id = id;
        this.description = description;
        this.oimage = image;
    }

    public int getId() {
        return id;
    }

    public String getOimage(){
        return oimage;
    }


    public String getDescription(){
        return description;
    }
}
