package com.lia.yello.roha.model;

public class HomeorderlistDataModel2 {
    String pname,pimage;
    Double ptprice,psprice;
    int pid,pqty;
    public HomeorderlistDataModel2(int pid,String pname,String pimage,Double psprice,int pqty,Double ptprice){
        this.pid=pid;
        this.pimage=pimage;
        this.pname=pname;
        this.psprice=psprice;
        this.pqty=pqty;
        this.ptprice = ptprice;
    }

    public int getPid() {
        return pid;
    }

    public  int getPqty(){
        return pqty;
    }

    public Double getPtprice(){
        return ptprice;
    }

    public Double getPsprice(){
        return psprice;
    }

    public String getPname() {
        return pname;
    }

    public String getPimage() {
        return pimage;
    }

}
