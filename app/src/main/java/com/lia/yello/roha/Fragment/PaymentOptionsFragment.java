package com.lia.yello.roha.Fragment;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.utility.SessionManager;

public class PaymentOptionsFragment extends Fragment {
    View  view;
    Button btn_add_debit,btn_wallet;
    public TextView profile_txt;
    SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=  inflater.inflate(R.layout.fragment_payment_options, container, false);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(getActivity());
        }

        if(sessionManager.getchoose().equals("rohahome")){
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.paymentoptions);
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.paymentoptions);
        }
        btn_add_debit=(Button)view.findViewById(R.id.btn_add_debit_card);
        btn_wallet=(Button)view.findViewById(R.id.btn_wallet);
        btn_add_debit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionManager.getchoose().equals("rohawater")){
                    ((RohaWaterDashboardActivity)getActivity()).subchangefragment(new AddPaymentCardFragment(),null);
                }
                else if(sessionManager.getchoose().equals("rohahome")){
                    ((RohahomeDashboradActivity)getActivity()).subchangefragment(new AddPaymentCardFragment(),null);
                }
            }
        });
        btn_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionManager.getchoose().equals("rohawater")){
                    ((RohaWaterDashboardActivity)getActivity()).navigationchangefragment(new WalletFragment(),1,4);
                }
                else if(sessionManager.getchoose().equals("rohahome")){
                    ((RohahomeDashboradActivity)getActivity()).navigationchangefragment(new WalletFragment(),1,4);
                }
            }
        });
        return view;
    }
}