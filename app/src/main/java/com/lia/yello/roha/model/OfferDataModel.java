package com.lia.yello.roha.model;

public class OfferDataModel {
    int id;
    String oimage,oname,promocode,from,to,type,cartonname;
    double value,minimumorderamount;

    public OfferDataModel(String cartonname,int id, String name,String image,String pcode,String from,String to,double value,String type,Double minimumorderamount) {
        this.id = id;
        this.cartonname = cartonname;
        this.oname = name;
        this.oimage = image;
        this.promocode = pcode;
        this.from = from;
        this.to = to;
        this.value = value;
        this.type = type;
        this.minimumorderamount = minimumorderamount;
    }

    public String getCartonname(){
        return cartonname;
    }

    public Double getMinimumorderamount(){
        return minimumorderamount;
    }
    public int getId() {
        return id;
    }

    public String getOimage(){
        return oimage;
    }

    public String getOname(){
        return oname;
    }
    public String getPromocode(){
        return promocode;
    }

    public String getFrom(){
        return from;
    }

    public String getTo(){
        return to;
    }

    public double getValue(){
        return value;
    }

    public String getType(){
        return type;
    }

}
