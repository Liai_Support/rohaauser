package com.lia.yello.roha.model;

import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;

public class ReceiverDataModel {

    String name;
    String place,rname,rmno,rdesc;

    public ReceiverDataModel(String name,String place,String rname,String rmno,String rdesc) {
        this.name = name;
        this.place = place;
        this.rdesc = rdesc;
        this.rmno = rmno;
        this.rname = rname;
    }
    public String getPlace(){
        return place;
    }
    public String getName() {
        return name;
    }

    public String getRname(){
        return rname;
    }

    public String getRmno(){
        return rmno;
    }

    public String getRdesc(){
        return rdesc;
    }

    public void setRname(String rname){
        this.rname = rname;
    }

    public void setRmno(String rmno){
        this.rmno = rmno;
    }

    public void setRdesc(String rdesc){
        this.rdesc = rdesc;
    }

}
