package com.lia.yello.roha.adapter;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.yello.roha.Fragment.WalletFragment;
import com.lia.yello.roha.R;
import com.lia.yello.roha.model.WalletDataModel;
import com.lia.yello.roha.utility.StaticInfo;

import java.util.List;
public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.MyViewHolder> {

    private Context mcontext;
    private List<WalletDataModel> dataSet;

    public WalletAdapter(Context context, List<WalletDataModel> data) {
        this.dataSet = data;
        this.mcontext = context;

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id,date,amt;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.id = (TextView) itemView.findViewById(R.id.id_wallet);
            this.date = (TextView) itemView.findViewById(R.id.wallet_date);
            this.amt = (TextView) itemView.findViewById(R.id.amt_wallet);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_wallethistory, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,  final int listPosition) {
        holder.date.setText(""+dataSet.get(listPosition).getDate());
        if(dataSet.get(listPosition).getOperation().equals("credit")){
            holder.id.setText(""+dataSet.get(listPosition).getTransaction_id());
            holder.amt.setTextColor(Color.GREEN);
            holder.amt.setText("+ "+dataSet.get(listPosition).getAmount());
        }
        else if(dataSet.get(listPosition).getOperation().equals("debit")) {
            if(dataSet.get(listPosition).getOrderfor().equalsIgnoreCase("home")){
                holder.id.setText(""+ StaticInfo.order_prefix+"H"+dataSet.get(listPosition).getOrder_id());
                holder.amt.setText("- "+dataSet.get(listPosition).getAmount());
            }
            else if(dataSet.get(listPosition).getOrderfor().equalsIgnoreCase("water")){
                if(dataSet.get(listPosition).getIswish().equalsIgnoreCase("1")){
                    holder.id.setText(""+ StaticInfo.order_prefix+"M"+dataSet.get(listPosition).getOrder_id());
                    holder.amt.setText("- "+dataSet.get(listPosition).getAmount());
                }
                else {
                    holder.id.setText(""+ StaticInfo.order_prefix+""+dataSet.get(listPosition).getOrder_id());
                    holder.amt.setText("- "+dataSet.get(listPosition).getAmount());
                }

            }
        }

    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
















