package com.lia.yello.roha.Fragment;

import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.yello.roha.R;
import com.lia.yello.roha.activity.RohaWaterDashboardActivity;
import com.lia.yello.roha.activity.RohahomeDashboradActivity;
import com.lia.yello.roha.adapter.MyOrdersWaterAdapter1;
import com.lia.yello.roha.adapter.MyOrdersHomeAdapter1;
import com.lia.yello.roha.model.HomeorderlistDataModel1;
import com.lia.yello.roha.model.ListorderDatamodel1;

import com.lia.yello.roha.utility.SessionManager;
import com.lia.yello.roha.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MyOrderFragment extends Fragment {
    View view;
    private static ArrayList<HomeorderlistDataModel1> data;
    private static RecyclerView.Adapter adapter1;
    private static RecyclerView.Adapter adapter2;
    private static RecyclerView recycler_myorder_water,recycler_myorder_home;
    public SessionManager sessionManager;
    private static ArrayList<ListorderDatamodel1> data1;
    Button rohahome,rohawater;
    boolean iswater = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_order, container, false);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (!sessionManager.isNetworkEnabled() == true) {
            sessionManager.noNetworkDialog(getActivity());
        }
        rohahome = (Button) view.findViewById(R.id.txt_roha_home);
        rohawater = (Button) view.findViewById(R.id.txt_roha_water);
        recycler_myorder_water = (RecyclerView) view.findViewById(R.id.myorder_water_recycler);
        recycler_myorder_home = (RecyclerView) view.findViewById(R.id.myorder_home_recycler);
        recycler_myorder_home.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_myorder_water.setLayoutManager(new LinearLayoutManager(getContext()));

        if(sessionManager.getchoose().equals("rohahome")){
            iswater = false;
            ((RohahomeDashboradActivity)getActivity()).profile_txt.setText(R.string.myorders);
            rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
            rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            recycler_myorder_home.setVisibility(View.VISIBLE);
            recycler_myorder_water.setVisibility(View.GONE);
            rohahome.setTextColor(getContext().getResources().getColor(R.color.black));
            rohawater.setTextColor(getContext().getResources().getColor(R.color.white));
            OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                    public void handleOnBackPressed() {
                        this.setEnabled(false);
                        ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                    }
                }));
            }
            ((RohahomeDashboradActivity)getActivity()).back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RohahomeDashboradActivity) getActivity()).removeallfragments();
                }
            });
        }
        else if(sessionManager.getchoose().equals("rohawater")){
            iswater = true;
            ((RohaWaterDashboardActivity)getActivity()).profile_txt.setText(R.string.myorders);
            rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
            rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
            recycler_myorder_home.setVisibility(View.GONE);
            recycler_myorder_water.setVisibility(View.VISIBLE);
            rohawater.setTextColor(getContext().getResources().getColor(R.color.black));
            rohahome.setTextColor(getContext().getResources().getColor(R.color.white));
        }

        getorderlist();
        getinvoicelist();

        rohawater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
                recycler_myorder_home.setVisibility(View.GONE);
                recycler_myorder_water.setVisibility(View.VISIBLE);
                rohawater.setTextColor(getContext().getResources().getColor(R.color.black));
                rohahome.setTextColor(getContext().getResources().getColor(R.color.white));
            }
        });
        rohahome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rohawater.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey1)));
                rohahome.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_update)));
                recycler_myorder_home.setVisibility(View.VISIBLE);
                recycler_myorder_water.setVisibility(View.GONE);
                rohahome.setTextColor(getContext().getResources().getColor(R.color.black));
                rohawater.setTextColor(getContext().getResources().getColor(R.color.white));
            }
        });
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //back press identifier
        if(sessionManager.getchoose().equalsIgnoreCase("rohawater") && iswater == true){
            int size = ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().size();
            for (int i = 0; i < size; i++) {
                ((RohaWaterDashboardActivity)getActivity()).botm_nav.getMenu().getItem(i).setCheckable(false);
            }
            int size1 = ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().size();
            for (int i = 0; i < size1; i++) {
                ((RohaWaterDashboardActivity)getActivity()).navigationView.getMenu().getItem(i).setCheckable(false);
            }
        }
    }

    public void getinvoicelist() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("userid", sessionManager.getUserId());
            getinvoicelistJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void getorderlist() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getUserId());
            orderlistrequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }






    private void orderlistrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.URLstringorderlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("waterorderlist", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        data1 = new ArrayList<ListorderDatamodel1>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        for (int i=0; i<obj1.length(); i++){
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            ListorderDatamodel1 datamodel = new ListorderDatamodel1(
                                    getContext(),
                                    jsonObject.getString("id"),
                                    jsonObject.getString("no_of_carton"),
                                    jsonObject.getString("iswish"),
                                    jsonObject.getString("rating"),
                                    jsonObject.getString("vat"),
                                    jsonObject.getString("subtotal"),
                                    jsonObject.getString("total_price"),
                                    jsonObject.getString("delivery_charge"),
                                    jsonObject.getString("payment_type"),
                                    jsonObject.getString("order_date"),
                                    jsonObject.getString("units"),
                                    jsonObject.getString("status"),
                                    jsonObject.getString("delivered_date"),
                                    jsonObject.getString("delivered_time"),
                                    jsonObject.getString("order_time"),
                                    jsonObject.getString("place"),
                                    jsonObject.getString("ismecca"),
                                    jsonObject.getString("comment"),
                                    jsonObject.getString("suggestion"),
                                    jsonObject.getString("day"),
                                    jsonObject.getString("time_from"),
                                    jsonObject.getString("time_to"),
                                    jsonObject.getString("discount_id"),
                                    jsonObject.getString("session"),
                                    jsonObject.getString("discounttype"),
                            jsonObject.getString("discountvalue"),
                            jsonObject.getJSONArray("details"),
                            jsonObject);
                            data1.add(datamodel);
                        }
                        adapter2 = new MyOrdersWaterAdapter1(getContext(), data1, MyOrderFragment.this,obj1);
                        recycler_myorder_water.setAdapter(adapter2);
                    }
                    else {
                        //sessionManager.snackbarToast(getString(R.string.nodataavailable),view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer "+ sessionManager.gettoken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    private void getinvoicelistJSON(JSONObject responsebody) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = responsebody.toString();
        Log.d("reques34", String.valueOf(responsebody));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.home_orderlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("homeorderlist", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        Log.d("dvdxv", "" + obj.getJSONArray("data"));
                        JSONArray data1 = obj.getJSONArray("data");
                        data = new ArrayList<HomeorderlistDataModel1>();
                        for (int i = 0; i < data1.length(); i++) {
                            JSONObject jsonObject = data1.getJSONObject(i);
                            HomeorderlistDataModel1 datamodel = new HomeorderlistDataModel1(
                                    jsonObject.getString("place"),
                                    jsonObject.getString("status"),
                                    jsonObject.getString("payment_type"),
                                    jsonObject.getString("refToken"),
                                    jsonObject.getString("order_date"),
                                    jsonObject.getString("order_time"),
                                    jsonObject.getString("address"),
                                    jsonObject.getString("time_from"),
                                    jsonObject.getString("time_to"),
                                    jsonObject.getString("session"),
                                    jsonObject.getString("day"),
                                    jsonObject.getInt("id"),
                                    jsonObject.getInt("no_of_carton"),
                                    jsonObject.getInt("discount_id"),
                                    Integer.parseInt(jsonObject.getString("Carton_count")),
                                    jsonObject.getInt("branch_id"),
                                    jsonObject.getDouble("total_price"),
                                    jsonObject.getDouble("vat"),
                                    jsonObject.getDouble("delivery_charge"),
                                    jsonObject.getDouble("subtotal"),
                                    jsonObject.getDouble("lat"),
                                    jsonObject.getDouble("lng"),
                                    jsonObject.getDouble("rating"),
                                    jsonObject.getString("delivered_date"),
                                    jsonObject.getString("delivered_time"),
                                    jsonObject.getString("comment"),
                                    jsonObject.getString("suggestion"),
                                    jsonObject.getString("discount_name"),
                                    jsonObject.getString("branch_name"),
                                    jsonObject.getString("Carton_name"),
                                    jsonObject.getString("discounttype"),
                                    jsonObject.getString("discountvalue"),
                                    jsonObject.getJSONArray("details"));
                            data.add(datamodel);
                        }
                        adapter1 = new MyOrdersHomeAdapter1(getContext(), data, MyOrderFragment.this);
                        recycler_myorder_home.setAdapter(adapter1);
                        Log.d("dvdcvxv", "" + adapter1);
                    }
                    else {
                        //sessionManager.snackbarToast(getString(R.string.nodataavailable),view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
                sessionManager.progressdialogdismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
}
