package com.lia.yello.roha.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class GetAddressModel : Serializable {

    @SerializedName("results")
    @Expose
    var predictions: List<Prediction>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null

    @SerializedName("errorMessage")
    @Expose
    var errorMessage: String? = null


}

class Prediction : Serializable {

    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("matched_substrings")
    @Expose
    var matchedSubstrings: List<MatchedSubstring>? = null
    @SerializedName("place_id")
    @Expose
    var placeId: String? = null
    @SerializedName("reference")
    @Expose
    var reference: String? = null
    @SerializedName("structured_formatting")
    @Expose
    var structuredFormatting: StructuredFormatting? = null
    @SerializedName("terms")
    @Expose
    var terms: List<Term>? = null
    @SerializedName("types")
    @Expose
    var types: List<String>? = null

    @SerializedName("address_components")
    @Expose
    var addressComponents: List<AddressComponent>? = null

    @SerializedName("formatted_address")
    @Expose
    var formatted_address: String? = null


}


class MainTextMatchedSubstring : Serializable {

    @SerializedName("length")
    @Expose
    var length: Int? = null
    @SerializedName("offset")
    @Expose
    var offset: Int? = null


}

class MatchedSubstring : Serializable {

    @SerializedName("length")
    @Expose
    var length: Int? = null
    @SerializedName("offset")
    @Expose
    var offset: Int? = null


}

class StructuredFormatting : Serializable {

    @SerializedName("main_text")
    @Expose
    var mainText: String? = null
    @SerializedName("main_text_matched_substrings")
    @Expose
    var mainTextMatchedSubstrings: List<MainTextMatchedSubstring>? = null
    @SerializedName("secondary_text")
    @Expose
    var secondaryText: String? = null


}

class Term : Serializable {

    @SerializedName("offset")
    @Expose
    var offset: Int? = null
    @SerializedName("value")
    @Expose
    var value: String? = null


}

class AddressComponent : Serializable {

    @SerializedName("long_name")
    @Expose
    var long_name: String? = null

    @SerializedName("short_name")
    @Expose
    var short_name: String? = null

    @SerializedName("types")
    @Expose
    var types: ArrayList<String>? = null

}
